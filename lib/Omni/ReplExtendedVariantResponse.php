<?php

class Omni_ReplExtendedVariantResponse {

    /**
     * @var Omni_ExtendedVariantValue[] $ExtendedVariantValues
     * @access public
     */
    public $ExtendedVariantValues = null;

    /**
     * @var string $LastKey
     * @access public
     */
    public $LastKey = null;

    /**
     * @var string $MaxKey
     * @access public
     */
    public $MaxKey = null;

    /**
     * @var int $RecordsRemaining
     * @access public
     */
    public $RecordsRemaining = null;

    /**
     * @param int $RecordsRemaining
     * @access public
     */
    public function __construct($RecordsRemaining = null){
      $this->RecordsRemaining = $RecordsRemaining;
    }

    /**
     * @return Omni_ExtendedVariantValue[]
     */
    public function getExtendedVariantValues(){
      return $this->ExtendedVariantValues;
    }

    /**
     * @param Omni_ExtendedVariantValue[] $ExtendedVariantValues
     * @return Omni_ReplExtendedVariantResponse
     */
    public function setExtendedVariantValues($ExtendedVariantValues){
      $this->ExtendedVariantValues = $ExtendedVariantValues;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastKey(){
      return $this->LastKey;
    }

    /**
     * @param string $LastKey
     * @return Omni_ReplExtendedVariantResponse
     */
    public function setLastKey($LastKey){
      $this->LastKey = $LastKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getMaxKey(){
      return $this->MaxKey;
    }

    /**
     * @param string $MaxKey
     * @return Omni_ReplExtendedVariantResponse
     */
    public function setMaxKey($MaxKey){
      $this->MaxKey = $MaxKey;
      return $this;
    }

    /**
     * @return int
     */
    public function getRecordsRemaining(){
      return $this->RecordsRemaining;
    }

    /**
     * @param int $RecordsRemaining
     * @return Omni_ReplExtendedVariantResponse
     */
    public function setRecordsRemaining($RecordsRemaining){
      $this->RecordsRemaining = $RecordsRemaining;
      return $this;
    }

}
