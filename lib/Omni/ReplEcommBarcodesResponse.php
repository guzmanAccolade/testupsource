<?php

class Omni_ReplEcommBarcodesResponse {

    /**
     * @var Omni_ReplBarcodeResponse $ReplEcommBarcodesResult
     * @access public
     */
    public $ReplEcommBarcodesResult = null;

    /**
     * @param Omni_ReplBarcodeResponse $ReplEcommBarcodesResult
     * @access public
     */
    public function __construct($ReplEcommBarcodesResult = null){
      $this->ReplEcommBarcodesResult = $ReplEcommBarcodesResult;
    }

    /**
     * @return Omni_ReplBarcodeResponse
     */
    public function getReplEcommBarcodesResult(){
      return $this->ReplEcommBarcodesResult;
    }

    /**
     * @param Omni_ReplBarcodeResponse $ReplEcommBarcodesResult
     * @return Omni_ReplEcommBarcodesResponse
     */
    public function setReplEcommBarcodesResult($ReplEcommBarcodesResult){
      $this->ReplEcommBarcodesResult = $ReplEcommBarcodesResult;
      return $this;
    }

}
