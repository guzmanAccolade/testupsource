<?php

class Omni_ReplEcommUnitOfMeasuresResponse {

    /**
     * @var Omni_ReplUnitOfMeasureResponse $ReplEcommUnitOfMeasuresResult
     * @access public
     */
    public $ReplEcommUnitOfMeasuresResult = null;

    /**
     * @param Omni_ReplUnitOfMeasureResponse $ReplEcommUnitOfMeasuresResult
     * @access public
     */
    public function __construct($ReplEcommUnitOfMeasuresResult = null){
      $this->ReplEcommUnitOfMeasuresResult = $ReplEcommUnitOfMeasuresResult;
    }

    /**
     * @return Omni_ReplUnitOfMeasureResponse
     */
    public function getReplEcommUnitOfMeasuresResult(){
      return $this->ReplEcommUnitOfMeasuresResult;
    }

    /**
     * @param Omni_ReplUnitOfMeasureResponse $ReplEcommUnitOfMeasuresResult
     * @return Omni_ReplEcommUnitOfMeasuresResponse
     */
    public function setReplEcommUnitOfMeasuresResult($ReplEcommUnitOfMeasuresResult){
      $this->ReplEcommUnitOfMeasuresResult = $ReplEcommUnitOfMeasuresResult;
      return $this;
    }

}
