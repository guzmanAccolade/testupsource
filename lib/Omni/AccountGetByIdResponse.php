<?php

class Omni_AccountGetByIdResponse {

    /**
     * @var Omni_Account $AccountGetByIdResult
     * @access public
     */
    public $AccountGetByIdResult = null;

    /**
     * @param Omni_Account $AccountGetByIdResult
     * @access public
     */
    public function __construct($AccountGetByIdResult = null){
      $this->AccountGetByIdResult = $AccountGetByIdResult;
    }

    /**
     * @return Omni_Account
     */
    public function getAccountGetByIdResult(){
      return $this->AccountGetByIdResult;
    }

    /**
     * @param Omni_Account $AccountGetByIdResult
     * @return Omni_AccountGetByIdResponse
     */
    public function setAccountGetByIdResult($AccountGetByIdResult){
      $this->AccountGetByIdResult = $AccountGetByIdResult;
      return $this;
    }

}
