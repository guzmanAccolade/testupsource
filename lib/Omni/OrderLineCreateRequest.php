<?php

class Omni_OrderLineCreateRequest {

    /**
     * @var float $Amount
     * @access public
     */
    public $Amount = null;

    /**
     * @var float $DiscountAmount
     * @access public
     */
    public $DiscountAmount = null;

    /**
     * @var float $DiscountPercent
     * @access public
     */
    public $DiscountPercent = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var int $LineNumber
     * @access public
     */
    public $LineNumber = null;

    /**
     * @var Omni_LineType $LineType
     * @access public
     */
    public $LineType = null;

    /**
     * @var float $NetAmount
     * @access public
     */
    public $NetAmount = null;

    /**
     * @var float $NetPrice
     * @access public
     */
    public $NetPrice = null;

    /**
     * @var string $OrderId
     * @access public
     */
    public $OrderId = null;

    /**
     * @var float $Price
     * @access public
     */
    public $Price = null;

    /**
     * @var float $Quantity
     * @access public
     */
    public $Quantity = null;

    /**
     * @var float $TaxAmount
     * @access public
     */
    public $TaxAmount = null;

    /**
     * @var string $UomId
     * @access public
     */
    public $UomId = null;

    /**
     * @var string $VariantId
     * @access public
     */
    public $VariantId = null;

    /**
     * @param float $Amount
     * @param float $DiscountAmount
     * @param float $DiscountPercent
     * @param int $LineNumber
     * @param Omni_LineType $LineType
     * @param float $NetAmount
     * @param float $NetPrice
     * @param float $Price
     * @param float $Quantity
     * @param float $TaxAmount
     * @access public
     */
    public function __construct($Amount = null, $DiscountAmount = null, $DiscountPercent = null, $LineNumber = null, $LineType = null, $NetAmount = null, $NetPrice = null, $Price = null, $Quantity = null, $TaxAmount = null){
      $this->Amount = $Amount;
      $this->DiscountAmount = $DiscountAmount;
      $this->DiscountPercent = $DiscountPercent;
      $this->LineNumber = $LineNumber;
      $this->LineType = $LineType;
      $this->NetAmount = $NetAmount;
      $this->NetPrice = $NetPrice;
      $this->Price = $Price;
      $this->Quantity = $Quantity;
      $this->TaxAmount = $TaxAmount;
    }

    /**
     * @return float
     */
    public function getAmount(){
      return $this->Amount;
    }

    /**
     * @param float $Amount
     * @return Omni_OrderLineCreateRequest
     */
    public function setAmount($Amount){
      $this->Amount = $Amount;
      return $this;
    }

    /**
     * @return float
     */
    public function getDiscountAmount(){
      return $this->DiscountAmount;
    }

    /**
     * @param float $DiscountAmount
     * @return Omni_OrderLineCreateRequest
     */
    public function setDiscountAmount($DiscountAmount){
      $this->DiscountAmount = $DiscountAmount;
      return $this;
    }

    /**
     * @return float
     */
    public function getDiscountPercent(){
      return $this->DiscountPercent;
    }

    /**
     * @param float $DiscountPercent
     * @return Omni_OrderLineCreateRequest
     */
    public function setDiscountPercent($DiscountPercent){
      $this->DiscountPercent = $DiscountPercent;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_OrderLineCreateRequest
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return int
     */
    public function getLineNumber(){
      return $this->LineNumber;
    }

    /**
     * @param int $LineNumber
     * @return Omni_OrderLineCreateRequest
     */
    public function setLineNumber($LineNumber){
      $this->LineNumber = $LineNumber;
      return $this;
    }

    /**
     * @return Omni_LineType
     */
    public function getLineType(){
      return $this->LineType;
    }

    /**
     * @param Omni_LineType $LineType
     * @return Omni_OrderLineCreateRequest
     */
    public function setLineType($LineType){
      $this->LineType = $LineType;
      return $this;
    }

    /**
     * @return float
     */
    public function getNetAmount(){
      return $this->NetAmount;
    }

    /**
     * @param float $NetAmount
     * @return Omni_OrderLineCreateRequest
     */
    public function setNetAmount($NetAmount){
      $this->NetAmount = $NetAmount;
      return $this;
    }

    /**
     * @return float
     */
    public function getNetPrice(){
      return $this->NetPrice;
    }

    /**
     * @param float $NetPrice
     * @return Omni_OrderLineCreateRequest
     */
    public function setNetPrice($NetPrice){
      $this->NetPrice = $NetPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderId(){
      return $this->OrderId;
    }

    /**
     * @param string $OrderId
     * @return Omni_OrderLineCreateRequest
     */
    public function setOrderId($OrderId){
      $this->OrderId = $OrderId;
      return $this;
    }

    /**
     * @return float
     */
    public function getPrice(){
      return $this->Price;
    }

    /**
     * @param float $Price
     * @return Omni_OrderLineCreateRequest
     */
    public function setPrice($Price){
      $this->Price = $Price;
      return $this;
    }

    /**
     * @return float
     */
    public function getQuantity(){
      return $this->Quantity;
    }

    /**
     * @param float $Quantity
     * @return Omni_OrderLineCreateRequest
     */
    public function setQuantity($Quantity){
      $this->Quantity = $Quantity;
      return $this;
    }

    /**
     * @return float
     */
    public function getTaxAmount(){
      return $this->TaxAmount;
    }

    /**
     * @param float $TaxAmount
     * @return Omni_OrderLineCreateRequest
     */
    public function setTaxAmount($TaxAmount){
      $this->TaxAmount = $TaxAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getUomId(){
      return $this->UomId;
    }

    /**
     * @param string $UomId
     * @return Omni_OrderLineCreateRequest
     */
    public function setUomId($UomId){
      $this->UomId = $UomId;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantId(){
      return $this->VariantId;
    }

    /**
     * @param string $VariantId
     * @return Omni_OrderLineCreateRequest
     */
    public function setVariantId($VariantId){
      $this->VariantId = $VariantId;
      return $this;
    }

}
