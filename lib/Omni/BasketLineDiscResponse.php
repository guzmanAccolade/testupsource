<?php

class Omni_BasketLineDiscResponse {

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $DiscountAmount
     * @access public
     */
    public $DiscountAmount = null;

    /**
     * @var string $DiscountPercent
     * @access public
     */
    public $DiscountPercent = null;

    /**
     * @var Omni_DiscountType $DiscountType
     * @access public
     */
    public $DiscountType = null;

    /**
     * @var int $LineNumber
     * @access public
     */
    public $LineNumber = null;

    /**
     * @var string $No
     * @access public
     */
    public $No = null;

    /**
     * @var string $OfferNumber
     * @access public
     */
    public $OfferNumber = null;

    /**
     * @var string $PeriodicDiscGroup
     * @access public
     */
    public $PeriodicDiscGroup = null;

    /**
     * @var Omni_PeriodicDiscType $PeriodicDiscType
     * @access public
     */
    public $PeriodicDiscType = null;

    /**
     * @var string $Quantity
     * @access public
     */
    public $Quantity = null;

    /**
     * @param Omni_DiscountType $DiscountType
     * @param int $LineNumber
     * @param Omni_PeriodicDiscType $PeriodicDiscType
     * @access public
     */
    public function __construct($DiscountType = null, $LineNumber = null, $PeriodicDiscType = null){
      $this->DiscountType = $DiscountType;
      $this->LineNumber = $LineNumber;
      $this->PeriodicDiscType = $PeriodicDiscType;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_BasketLineDiscResponse
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getDiscountAmount(){
      return $this->DiscountAmount;
    }

    /**
     * @param string $DiscountAmount
     * @return Omni_BasketLineDiscResponse
     */
    public function setDiscountAmount($DiscountAmount){
      $this->DiscountAmount = $DiscountAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getDiscountPercent(){
      return $this->DiscountPercent;
    }

    /**
     * @param string $DiscountPercent
     * @return Omni_BasketLineDiscResponse
     */
    public function setDiscountPercent($DiscountPercent){
      $this->DiscountPercent = $DiscountPercent;
      return $this;
    }

    /**
     * @return Omni_DiscountType
     */
    public function getDiscountType(){
      return $this->DiscountType;
    }

    /**
     * @param Omni_DiscountType $DiscountType
     * @return Omni_BasketLineDiscResponse
     */
    public function setDiscountType($DiscountType){
      $this->DiscountType = $DiscountType;
      return $this;
    }

    /**
     * @return int
     */
    public function getLineNumber(){
      return $this->LineNumber;
    }

    /**
     * @param int $LineNumber
     * @return Omni_BasketLineDiscResponse
     */
    public function setLineNumber($LineNumber){
      $this->LineNumber = $LineNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getNo(){
      return $this->No;
    }

    /**
     * @param string $No
     * @return Omni_BasketLineDiscResponse
     */
    public function setNo($No){
      $this->No = $No;
      return $this;
    }

    /**
     * @return string
     */
    public function getOfferNumber(){
      return $this->OfferNumber;
    }

    /**
     * @param string $OfferNumber
     * @return Omni_BasketLineDiscResponse
     */
    public function setOfferNumber($OfferNumber){
      $this->OfferNumber = $OfferNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getPeriodicDiscGroup(){
      return $this->PeriodicDiscGroup;
    }

    /**
     * @param string $PeriodicDiscGroup
     * @return Omni_BasketLineDiscResponse
     */
    public function setPeriodicDiscGroup($PeriodicDiscGroup){
      $this->PeriodicDiscGroup = $PeriodicDiscGroup;
      return $this;
    }

    /**
     * @return Omni_PeriodicDiscType
     */
    public function getPeriodicDiscType(){
      return $this->PeriodicDiscType;
    }

    /**
     * @param Omni_PeriodicDiscType $PeriodicDiscType
     * @return Omni_BasketLineDiscResponse
     */
    public function setPeriodicDiscType($PeriodicDiscType){
      $this->PeriodicDiscType = $PeriodicDiscType;
      return $this;
    }

    /**
     * @return string
     */
    public function getQuantity(){
      return $this->Quantity;
    }

    /**
     * @param string $Quantity
     * @return Omni_BasketLineDiscResponse
     */
    public function setQuantity($Quantity){
      $this->Quantity = $Quantity;
      return $this;
    }

}
