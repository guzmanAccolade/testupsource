<?php

class Omni_ChangePasswordResponse {

    /**
     * @var boolean $ChangePasswordResult
     * @access public
     */
    public $ChangePasswordResult = null;

    /**
     * @param boolean $ChangePasswordResult
     * @access public
     */
    public function __construct($ChangePasswordResult = null){
      $this->ChangePasswordResult = $ChangePasswordResult;
    }

    /**
     * @return boolean
     */
    public function getChangePasswordResult(){
      return $this->ChangePasswordResult;
    }

    /**
     * @param boolean $ChangePasswordResult
     * @return Omni_ChangePasswordResponse
     */
    public function setChangePasswordResult($ChangePasswordResult){
      $this->ChangePasswordResult = $ChangePasswordResult;
      return $this;
    }

}
