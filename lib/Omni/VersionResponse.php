<?php

class Omni_VersionResponse {

    /**
     * @var string $VersionResult
     * @access public
     */
    public $VersionResult = null;

    /**
     * @param string $VersionResult
     * @access public
     */
    public function __construct($VersionResult = null){
      $this->VersionResult = $VersionResult;
    }

    /**
     * @return string
     */
    public function getVersionResult(){
      return $this->VersionResult;
    }

    /**
     * @param string $VersionResult
     * @return Omni_VersionResponse
     */
    public function setVersionResult($VersionResult){
      $this->VersionResult = $VersionResult;
      return $this;
    }

}
