<?php

class Omni_ContactGetByIdResponse {

    /**
     * @var Omni_Contact $ContactGetByIdResult
     * @access public
     */
    public $ContactGetByIdResult = null;

    /**
     * @param Omni_Contact $ContactGetByIdResult
     * @access public
     */
    public function __construct($ContactGetByIdResult = null){
      $this->ContactGetByIdResult = $ContactGetByIdResult;
    }

    /**
     * @return Omni_Contact
     */
    public function getContactGetByIdResult(){
      return $this->ContactGetByIdResult;
    }

    /**
     * @param Omni_Contact $ContactGetByIdResult
     * @return Omni_ContactGetByIdResponse
     */
    public function setContactGetByIdResult($ContactGetByIdResult){
      $this->ContactGetByIdResult = $ContactGetByIdResult;
      return $this;
    }

}
