<?php

class Omni_PublishedOffersGetByCardIdResponse {

    /**
     * @var Omni_PublishedOffer[] $PublishedOffersGetByCardIdResult
     * @access public
     */
    public $PublishedOffersGetByCardIdResult = null;

    /**
     * @param Omni_PublishedOffer[] $PublishedOffersGetByCardIdResult
     * @access public
     */
    public function __construct($PublishedOffersGetByCardIdResult = null){
      $this->PublishedOffersGetByCardIdResult = $PublishedOffersGetByCardIdResult;
    }

    /**
     * @return Omni_PublishedOffer[]
     */
    public function getPublishedOffersGetByCardIdResult(){
      return $this->PublishedOffersGetByCardIdResult;
    }

    /**
     * @param Omni_PublishedOffer[] $PublishedOffersGetByCardIdResult
     * @return Omni_PublishedOffersGetByCardIdResponse
     */
    public function setPublishedOffersGetByCardIdResult($PublishedOffersGetByCardIdResult){
      $this->PublishedOffersGetByCardIdResult = $PublishedOffersGetByCardIdResult;
      return $this;
    }

}
