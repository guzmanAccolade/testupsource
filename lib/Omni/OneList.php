<?php

class Omni_OneList {

    /**
     * @var string $CardId
     * @access public
     */
    public $CardId = null;

    /**
     * @var string $ContactId
     * @access public
     */
    public $ContactId = null;

    /**
     * @var Omni_OneListCoupon[] $Coupons
     * @access public
     */
    public $Coupons = null;

    /**
     * @var dateTime $CreateDate
     * @access public
     */
    public $CreateDate = null;

    /**
     * @var string $CustomerId
     * @access public
     */
    public $CustomerId = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var boolean $IsDefaultList
     * @access public
     */
    public $IsDefaultList = null;

    /**
     * @var Omni_OneListItem[] $Items
     * @access public
     */
    public $Items = null;

    /**
     * @var Omni_ListType $ListType
     * @access public
     */
    public $ListType = null;

    /**
     * @var Omni_OneListOffer[] $Offers
     * @access public
     */
    public $Offers = null;

    /**
     * @var Omni_OneListPublishedOffer[] $PublishedOffers
     * @access public
     */
    public $PublishedOffers = null;

    /**
     * @param dateTime $CreateDate
     * @param boolean $IsDefaultList
     * @param Omni_ListType $ListType
     * @access public
     */
    public function __construct($CreateDate = null, $IsDefaultList = null, $ListType = null){
      $this->CreateDate = $CreateDate;
      $this->IsDefaultList = $IsDefaultList;
      $this->ListType = $ListType;
    }

    /**
     * @return string
     */
    public function getCardId(){
      return $this->CardId;
    }

    /**
     * @param string $CardId
     * @return Omni_OneList
     */
    public function setCardId($CardId){
      $this->CardId = $CardId;
      return $this;
    }

    /**
     * @return string
     */
    public function getContactId(){
      return $this->ContactId;
    }

    /**
     * @param string $ContactId
     * @return Omni_OneList
     */
    public function setContactId($ContactId){
      $this->ContactId = $ContactId;
      return $this;
    }

    /**
     * @return Omni_OneListCoupon[]
     */
    public function getCoupons(){
      return $this->Coupons;
    }

    /**
     * @param Omni_OneListCoupon[] $Coupons
     * @return Omni_OneList
     */
    public function setCoupons($Coupons){
      $this->Coupons = $Coupons;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getCreateDate(){
      return $this->CreateDate;
    }

    /**
     * @param dateTime $CreateDate
     * @return Omni_OneList
     */
    public function setCreateDate($CreateDate){
      $this->CreateDate = $CreateDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId(){
      return $this->CustomerId;
    }

    /**
     * @param string $CustomerId
     * @return Omni_OneList
     */
    public function setCustomerId($CustomerId){
      $this->CustomerId = $CustomerId;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_OneList
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_OneList
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDefaultList(){
      return $this->IsDefaultList;
    }

    /**
     * @param boolean $IsDefaultList
     * @return Omni_OneList
     */
    public function setIsDefaultList($IsDefaultList){
      $this->IsDefaultList = $IsDefaultList;
      return $this;
    }

    /**
     * @return Omni_OneListItem[]
     */
    public function getItems(){
      return $this->Items;
    }

    /**
     * @param Omni_OneListItem[] $Items
     * @return Omni_OneList
     */
    public function setItems($Items){
      $this->Items = $Items;
      return $this;
    }

    /**
     * @return Omni_ListType
     */
    public function getListType(){
      return $this->ListType;
    }

    /**
     * @param Omni_ListType $ListType
     * @return Omni_OneList
     */
    public function setListType($ListType){
      $this->ListType = $ListType;
      return $this;
    }

    /**
     * @return Omni_OneListOffer[]
     */
    public function getOffers(){
      return $this->Offers;
    }

    /**
     * @param Omni_OneListOffer[] $Offers
     * @return Omni_OneList
     */
    public function setOffers($Offers){
      $this->Offers = $Offers;
      return $this;
    }

    /**
     * @return Omni_OneListPublishedOffer[]
     */
    public function getPublishedOffers(){
      return $this->PublishedOffers;
    }

    /**
     * @param Omni_OneListPublishedOffer[] $PublishedOffers
     * @return Omni_OneList
     */
    public function setPublishedOffers($PublishedOffers){
      $this->PublishedOffers = $PublishedOffers;
      return $this;
    }

}
