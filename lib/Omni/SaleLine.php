<?php

class Omni_SaleLine {

    /**
     * @var float $Amount
     * @access public
     */
    public $Amount = null;

    /**
     * @var string $CurrencyCode
     * @access public
     */
    public $CurrencyCode = null;

    /**
     * @var float $DiscountAmount
     * @access public
     */
    public $DiscountAmount = null;

    /**
     * @var string $ExternalId
     * @access public
     */
    public $ExternalId = null;

    /**
     * @var Omni_ExtraInfoLine[] $ExtraInfoLines
     * @access public
     */
    public $ExtraInfoLines = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $ItemDescription
     * @access public
     */
    public $ItemDescription = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var float $NetAmount
     * @access public
     */
    public $NetAmount = null;

    /**
     * @var float $Quantity
     * @access public
     */
    public $Quantity = null;

    /**
     * @var string $StoreId
     * @access public
     */
    public $StoreId = null;

    /**
     * @var string $TenderDescription
     * @access public
     */
    public $TenderDescription = null;

    /**
     * @var string $TerminalId
     * @access public
     */
    public $TerminalId = null;

    /**
     * @var string $TransactionId
     * @access public
     */
    public $TransactionId = null;

    /**
     * @var string $Uom
     * @access public
     */
    public $Uom = null;

    /**
     * @var string $UomDescription
     * @access public
     */
    public $UomDescription = null;

    /**
     * @var string $Variant
     * @access public
     */
    public $Variant = null;

    /**
     * @var string $VariantDescription
     * @access public
     */
    public $VariantDescription = null;

    /**
     * @var float $VatAmount
     * @access public
     */
    public $VatAmount = null;

    /**
     * @param string $CurrencyCode
     * @param string $ExternalId
     * @param string $ItemDescription
     * @param string $ItemId
     * @param float $Quantity
     * @param string $TenderDescription
     * @param string $UomDescription
     * @param string $VariantDescription
     * @access public
     */
    public function __construct($CurrencyCode = null, $ExternalId = null, $ItemDescription = null, $ItemId = null, $Quantity = null, $TenderDescription = null, $UomDescription = null, $VariantDescription = null){
      $this->CurrencyCode = $CurrencyCode;
      $this->ExternalId = $ExternalId;
      $this->ItemDescription = $ItemDescription;
      $this->ItemId = $ItemId;
      $this->Quantity = $Quantity;
      $this->TenderDescription = $TenderDescription;
      $this->UomDescription = $UomDescription;
      $this->VariantDescription = $VariantDescription;
    }

    /**
     * @return float
     */
    public function getAmount(){
      return $this->Amount;
    }

    /**
     * @param float $Amount
     * @return Omni_SaleLine
     */
    public function setAmount($Amount){
      $this->Amount = $Amount;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(){
      return $this->CurrencyCode;
    }

    /**
     * @param string $CurrencyCode
     * @return Omni_SaleLine
     */
    public function setCurrencyCode($CurrencyCode){
      $this->CurrencyCode = $CurrencyCode;
      return $this;
    }

    /**
     * @return float
     */
    public function getDiscountAmount(){
      return $this->DiscountAmount;
    }

    /**
     * @param float $DiscountAmount
     * @return Omni_SaleLine
     */
    public function setDiscountAmount($DiscountAmount){
      $this->DiscountAmount = $DiscountAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getExternalId(){
      return $this->ExternalId;
    }

    /**
     * @param string $ExternalId
     * @return Omni_SaleLine
     */
    public function setExternalId($ExternalId){
      $this->ExternalId = $ExternalId;
      return $this;
    }

    /**
     * @return Omni_ExtraInfoLine[]
     */
    public function getExtraInfoLines(){
      return $this->ExtraInfoLines;
    }

    /**
     * @param Omni_ExtraInfoLine[] $ExtraInfoLines
     * @return Omni_SaleLine
     */
    public function setExtraInfoLines($ExtraInfoLines){
      $this->ExtraInfoLines = $ExtraInfoLines;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_SaleLine
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemDescription(){
      return $this->ItemDescription;
    }

    /**
     * @param string $ItemDescription
     * @return Omni_SaleLine
     */
    public function setItemDescription($ItemDescription){
      $this->ItemDescription = $ItemDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_SaleLine
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return float
     */
    public function getNetAmount(){
      return $this->NetAmount;
    }

    /**
     * @param float $NetAmount
     * @return Omni_SaleLine
     */
    public function setNetAmount($NetAmount){
      $this->NetAmount = $NetAmount;
      return $this;
    }

    /**
     * @return float
     */
    public function getQuantity(){
      return $this->Quantity;
    }

    /**
     * @param float $Quantity
     * @return Omni_SaleLine
     */
    public function setQuantity($Quantity){
      $this->Quantity = $Quantity;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->StoreId;
    }

    /**
     * @param string $StoreId
     * @return Omni_SaleLine
     */
    public function setStoreId($StoreId){
      $this->StoreId = $StoreId;
      return $this;
    }

    /**
     * @return string
     */
    public function getTenderDescription(){
      return $this->TenderDescription;
    }

    /**
     * @param string $TenderDescription
     * @return Omni_SaleLine
     */
    public function setTenderDescription($TenderDescription){
      $this->TenderDescription = $TenderDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getTerminalId(){
      return $this->TerminalId;
    }

    /**
     * @param string $TerminalId
     * @return Omni_SaleLine
     */
    public function setTerminalId($TerminalId){
      $this->TerminalId = $TerminalId;
      return $this;
    }

    /**
     * @return string
     */
    public function getTransactionId(){
      return $this->TransactionId;
    }

    /**
     * @param string $TransactionId
     * @return Omni_SaleLine
     */
    public function setTransactionId($TransactionId){
      $this->TransactionId = $TransactionId;
      return $this;
    }

    /**
     * @return string
     */
    public function getUom(){
      return $this->Uom;
    }

    /**
     * @param string $Uom
     * @return Omni_SaleLine
     */
    public function setUom($Uom){
      $this->Uom = $Uom;
      return $this;
    }

    /**
     * @return string
     */
    public function getUomDescription(){
      return $this->UomDescription;
    }

    /**
     * @param string $UomDescription
     * @return Omni_SaleLine
     */
    public function setUomDescription($UomDescription){
      $this->UomDescription = $UomDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariant(){
      return $this->Variant;
    }

    /**
     * @param string $Variant
     * @return Omni_SaleLine
     */
    public function setVariant($Variant){
      $this->Variant = $Variant;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantDescription(){
      return $this->VariantDescription;
    }

    /**
     * @param string $VariantDescription
     * @return Omni_SaleLine
     */
    public function setVariantDescription($VariantDescription){
      $this->VariantDescription = $VariantDescription;
      return $this;
    }

    /**
     * @return float
     */
    public function getVatAmount(){
      return $this->VatAmount;
    }

    /**
     * @param float $VatAmount
     * @return Omni_SaleLine
     */
    public function setVatAmount($VatAmount){
      $this->VatAmount = $VatAmount;
      return $this;
    }

}
