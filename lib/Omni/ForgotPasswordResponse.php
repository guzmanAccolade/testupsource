<?php

class Omni_ForgotPasswordResponse {

    /**
     * @var boolean $ForgotPasswordResult
     * @access public
     */
    public $ForgotPasswordResult = null;

    /**
     * @param boolean $ForgotPasswordResult
     * @access public
     */
    public function __construct($ForgotPasswordResult = null){
      $this->ForgotPasswordResult = $ForgotPasswordResult;
    }

    /**
     * @return boolean
     */
    public function getForgotPasswordResult(){
      return $this->ForgotPasswordResult;
    }

    /**
     * @param boolean $ForgotPasswordResult
     * @return Omni_ForgotPasswordResponse
     */
    public function setForgotPasswordResult($ForgotPasswordResult){
      $this->ForgotPasswordResult = $ForgotPasswordResult;
      return $this;
    }

}
