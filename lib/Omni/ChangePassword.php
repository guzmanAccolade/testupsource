<?php

class Omni_ChangePassword {

    /**
     * @var string $userName
     * @access public
     */
    public $userName = null;

    /**
     * @var string $newPassword
     * @access public
     */
    public $newPassword = null;

    /**
     * @var string $oldPassword
     * @access public
     */
    public $oldPassword = null;

    /**
     * @param string $userName
     * @param string $newPassword
     * @param string $oldPassword
     * @access public
     */
    public function __construct($userName = null, $newPassword = null, $oldPassword = null){
      $this->userName = $userName;
      $this->newPassword = $newPassword;
      $this->oldPassword = $oldPassword;
    }

    /**
     * @return string
     */
    public function getUserName(){
      return $this->userName;
    }

    /**
     * @param string $userName
     * @return Omni_ChangePassword
     */
    public function setUserName($userName){
      $this->userName = $userName;
      return $this;
    }

    /**
     * @return string
     */
    public function getNewPassword(){
      return $this->newPassword;
    }

    /**
     * @param string $newPassword
     * @return Omni_ChangePassword
     */
    public function setNewPassword($newPassword){
      $this->newPassword = $newPassword;
      return $this;
    }

    /**
     * @return string
     */
    public function getOldPassword(){
      return $this->oldPassword;
    }

    /**
     * @param string $oldPassword
     * @return Omni_ChangePassword
     */
    public function setOldPassword($oldPassword){
      $this->oldPassword = $oldPassword;
      return $this;
    }

}
