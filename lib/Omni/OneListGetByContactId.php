<?php

class Omni_OneListGetByContactId {

    /**
     * @var string $contactId
     * @access public
     */
    public $contactId = null;

    /**
     * @var Omni_ListType $listType
     * @access public
     */
    public $listType = null;

    /**
     * @var boolean $includeLines
     * @access public
     */
    public $includeLines = null;

    /**
     * @param string $contactId
     * @param Omni_ListType $listType
     * @param boolean $includeLines
     * @access public
     */
    public function __construct($contactId = null, $listType = null, $includeLines = null){
      $this->contactId = $contactId;
      $this->listType = $listType;
      $this->includeLines = $includeLines;
    }

    /**
     * @return string
     */
    public function getContactId(){
      return $this->contactId;
    }

    /**
     * @param string $contactId
     * @return Omni_OneListGetByContactId
     */
    public function setContactId($contactId){
      $this->contactId = $contactId;
      return $this;
    }

    /**
     * @return Omni_ListType
     */
    public function getListType(){
      return $this->listType;
    }

    /**
     * @param Omni_ListType $listType
     * @return Omni_OneListGetByContactId
     */
    public function setListType($listType){
      $this->listType = $listType;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeLines(){
      return $this->includeLines;
    }

    /**
     * @param boolean $includeLines
     * @return Omni_OneListGetByContactId
     */
    public function setIncludeLines($includeLines){
      $this->includeLines = $includeLines;
      return $this;
    }

}
