<?php

class Omni_PeriodicDiscType {
    const __default = 'Unknown';
    const Unknown = 'Unknown';
    const Multibuy = 'Multibuy';
    const MixMatch = 'MixMatch';
    const DiscOffer = 'DiscOffer';
    const ItemPoint = 'ItemPoint';


}
