<?php

class Omni_AuthorizationStatus {
    const __default = 'Approved';
    const Approved = 'Approved';
    const Declined = 'Declined';
    const Cancelled = 'Cancelled';
    const Failure = 'Failure';
    const UserRejected = 'UserRejected';
    const Unknown = 'Unknown';


}
