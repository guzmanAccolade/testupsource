<?php

class Omni_Notification {

    /**
     * @var string $ContactId
     * @access public
     */
    public $ContactId = null;

    /**
     * @var dateTime $Created
     * @access public
     */
    public $Created = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $Details
     * @access public
     */
    public $Details = null;

    /**
     * @var dateTime $ExpiryDate
     * @access public
     */
    public $ExpiryDate = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var Omni_ImageView[] $Images
     * @access public
     */
    public $Images = null;

    /**
     * @var Omni_NotificationTextType $NotificationTextType
     * @access public
     */
    public $NotificationTextType = null;

    /**
     * @var string $QRText
     * @access public
     */
    public $QRText = null;

    /**
     * @var Omni_NotificationStatus $Status
     * @access public
     */
    public $Status = null;

    /**
     * @param dateTime $Created
     * @param Omni_NotificationTextType $NotificationTextType
     * @param Omni_NotificationStatus $Status
     * @access public
     */
    public function __construct($Created = null, $NotificationTextType = null, $Status = null){
      $this->Created = $Created;
      $this->NotificationTextType = $NotificationTextType;
      $this->Status = $Status;
    }

    /**
     * @return string
     */
    public function getContactId(){
      return $this->ContactId;
    }

    /**
     * @param string $ContactId
     * @return Omni_Notification
     */
    public function setContactId($ContactId){
      $this->ContactId = $ContactId;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getCreated(){
      return $this->Created;
    }

    /**
     * @param dateTime $Created
     * @return Omni_Notification
     */
    public function setCreated($Created){
      $this->Created = $Created;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_Notification
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getDetails(){
      return $this->Details;
    }

    /**
     * @param string $Details
     * @return Omni_Notification
     */
    public function setDetails($Details){
      $this->Details = $Details;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getExpiryDate(){
      return $this->ExpiryDate;
    }

    /**
     * @param dateTime $ExpiryDate
     * @return Omni_Notification
     */
    public function setExpiryDate($ExpiryDate){
      $this->ExpiryDate = $ExpiryDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Notification
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return Omni_ImageView[]
     */
    public function getImages(){
      return $this->Images;
    }

    /**
     * @param Omni_ImageView[] $Images
     * @return Omni_Notification
     */
    public function setImages($Images){
      $this->Images = $Images;
      return $this;
    }

    /**
     * @return Omni_NotificationTextType
     */
    public function getNotificationTextType(){
      return $this->NotificationTextType;
    }

    /**
     * @param Omni_NotificationTextType $NotificationTextType
     * @return Omni_Notification
     */
    public function setNotificationTextType($NotificationTextType){
      $this->NotificationTextType = $NotificationTextType;
      return $this;
    }

    /**
     * @return string
     */
    public function getQRText(){
      return $this->QRText;
    }

    /**
     * @param string $QRText
     * @return Omni_Notification
     */
    public function setQRText($QRText){
      $this->QRText = $QRText;
      return $this;
    }

    /**
     * @return Omni_NotificationStatus
     */
    public function getStatus(){
      return $this->Status;
    }

    /**
     * @param Omni_NotificationStatus $Status
     * @return Omni_Notification
     */
    public function setStatus($Status){
      $this->Status = $Status;
      return $this;
    }

}
