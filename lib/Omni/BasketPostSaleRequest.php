<?php

class Omni_BasketPostSaleRequest {

    /**
     * @var Omni_BasketPostSaleDiscLineRequest[] $BasketPostSaleDiscLineRequests
     * @access public
     */
    public $BasketPostSaleDiscLineRequests = null;

    /**
     * @var Omni_BasketPostSaleLineRequest[] $BasketPostSaleLineRequests
     * @access public
     */
    public $BasketPostSaleLineRequests = null;

    /**
     * @var float $BasketPrice
     * @access public
     */
    public $BasketPrice = null;

    /**
     * @var Omni_Address $BillingAddress
     * @access public
     */
    public $BillingAddress = null;

    /**
     * @var string $ContactId
     * @access public
     */
    public $ContactId = null;

    /**
     * @var string $CurrencyCode
     * @access public
     */
    public $CurrencyCode = null;

    /**
     * @var string $Email
     * @access public
     */
    public $Email = null;

    /**
     * @var string $ExternalOrderId
     * @access public
     */
    public $ExternalOrderId = null;

    /**
     * @var string $FullName
     * @access public
     */
    public $FullName = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var Omni_ItemNumberType $ItemNumberType
     * @access public
     */
    public $ItemNumberType = null;

    /**
     * @var string $MemberCardNo
     * @access public
     */
    public $MemberCardNo = null;

    /**
     * @var string $MobilePhoneNumber
     * @access public
     */
    public $MobilePhoneNumber = null;

    /**
     * @var string $PhoneNumber
     * @access public
     */
    public $PhoneNumber = null;

    /**
     * @var boolean $PriceIncludesVAT
     * @access public
     */
    public $PriceIncludesVAT = null;

    /**
     * @var string $ShipmentMethodCode
     * @access public
     */
    public $ShipmentMethodCode = null;

    /**
     * @var Omni_Address $ShippingAddress
     * @access public
     */
    public $ShippingAddress = null;

    /**
     * @var Omni_SourceType $SourceType
     * @access public
     */
    public $SourceType = null;

    /**
     * @var Omni_TenderPaymentLine[] $TenderLines
     * @access public
     */
    public $TenderLines = null;

    /**
     * @param float $BasketPrice
     * @param Omni_ItemNumberType $ItemNumberType
     * @param boolean $PriceIncludesVAT
     * @param Omni_SourceType $SourceType
     * @access public
     */
    public function __construct($BasketPrice = null, $ItemNumberType = null, $PriceIncludesVAT = null, $SourceType = null){
      $this->BasketPrice = $BasketPrice;
      $this->ItemNumberType = $ItemNumberType;
      $this->PriceIncludesVAT = $PriceIncludesVAT;
      $this->SourceType = $SourceType;
    }

    /**
     * @return Omni_BasketPostSaleDiscLineRequest[]
     */
    public function getBasketPostSaleDiscLineRequests(){
      return $this->BasketPostSaleDiscLineRequests;
    }

    /**
     * @param Omni_BasketPostSaleDiscLineRequest[] $BasketPostSaleDiscLineRequests
     * @return Omni_BasketPostSaleRequest
     */
    public function setBasketPostSaleDiscLineRequests($BasketPostSaleDiscLineRequests){
      $this->BasketPostSaleDiscLineRequests = $BasketPostSaleDiscLineRequests;
      return $this;
    }

    /**
     * @return Omni_BasketPostSaleLineRequest[]
     */
    public function getBasketPostSaleLineRequests(){
      return $this->BasketPostSaleLineRequests;
    }

    /**
     * @param Omni_BasketPostSaleLineRequest[] $BasketPostSaleLineRequests
     * @return Omni_BasketPostSaleRequest
     */
    public function setBasketPostSaleLineRequests($BasketPostSaleLineRequests){
      $this->BasketPostSaleLineRequests = $BasketPostSaleLineRequests;
      return $this;
    }

    /**
     * @return float
     */
    public function getBasketPrice(){
      return $this->BasketPrice;
    }

    /**
     * @param float $BasketPrice
     * @return Omni_BasketPostSaleRequest
     */
    public function setBasketPrice($BasketPrice){
      $this->BasketPrice = $BasketPrice;
      return $this;
    }

    /**
     * @return Omni_Address
     */
    public function getBillingAddress(){
      return $this->BillingAddress;
    }

    /**
     * @param Omni_Address $BillingAddress
     * @return Omni_BasketPostSaleRequest
     */
    public function setBillingAddress($BillingAddress){
      $this->BillingAddress = $BillingAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getContactId(){
      return $this->ContactId;
    }

    /**
     * @param string $ContactId
     * @return Omni_BasketPostSaleRequest
     */
    public function setContactId($ContactId){
      $this->ContactId = $ContactId;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(){
      return $this->CurrencyCode;
    }

    /**
     * @param string $CurrencyCode
     * @return Omni_BasketPostSaleRequest
     */
    public function setCurrencyCode($CurrencyCode){
      $this->CurrencyCode = $CurrencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmail(){
      return $this->Email;
    }

    /**
     * @param string $Email
     * @return Omni_BasketPostSaleRequest
     */
    public function setEmail($Email){
      $this->Email = $Email;
      return $this;
    }

    /**
     * @return string
     */
    public function getExternalOrderId(){
      return $this->ExternalOrderId;
    }

    /**
     * @param string $ExternalOrderId
     * @return Omni_BasketPostSaleRequest
     */
    public function setExternalOrderId($ExternalOrderId){
      $this->ExternalOrderId = $ExternalOrderId;
      return $this;
    }

    /**
     * @return string
     */
    public function getFullName(){
      return $this->FullName;
    }

    /**
     * @param string $FullName
     * @return Omni_BasketPostSaleRequest
     */
    public function setFullName($FullName){
      $this->FullName = $FullName;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_BasketPostSaleRequest
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return Omni_ItemNumberType
     */
    public function getItemNumberType(){
      return $this->ItemNumberType;
    }

    /**
     * @param Omni_ItemNumberType $ItemNumberType
     * @return Omni_BasketPostSaleRequest
     */
    public function setItemNumberType($ItemNumberType){
      $this->ItemNumberType = $ItemNumberType;
      return $this;
    }

    /**
     * @return string
     */
    public function getMemberCardNo(){
      return $this->MemberCardNo;
    }

    /**
     * @param string $MemberCardNo
     * @return Omni_BasketPostSaleRequest
     */
    public function setMemberCardNo($MemberCardNo){
      $this->MemberCardNo = $MemberCardNo;
      return $this;
    }

    /**
     * @return string
     */
    public function getMobilePhoneNumber(){
      return $this->MobilePhoneNumber;
    }

    /**
     * @param string $MobilePhoneNumber
     * @return Omni_BasketPostSaleRequest
     */
    public function setMobilePhoneNumber($MobilePhoneNumber){
      $this->MobilePhoneNumber = $MobilePhoneNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(){
      return $this->PhoneNumber;
    }

    /**
     * @param string $PhoneNumber
     * @return Omni_BasketPostSaleRequest
     */
    public function setPhoneNumber($PhoneNumber){
      $this->PhoneNumber = $PhoneNumber;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPriceIncludesVAT(){
      return $this->PriceIncludesVAT;
    }

    /**
     * @param boolean $PriceIncludesVAT
     * @return Omni_BasketPostSaleRequest
     */
    public function setPriceIncludesVAT($PriceIncludesVAT){
      $this->PriceIncludesVAT = $PriceIncludesVAT;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipmentMethodCode(){
      return $this->ShipmentMethodCode;
    }

    /**
     * @param string $ShipmentMethodCode
     * @return Omni_BasketPostSaleRequest
     */
    public function setShipmentMethodCode($ShipmentMethodCode){
      $this->ShipmentMethodCode = $ShipmentMethodCode;
      return $this;
    }

    /**
     * @return Omni_Address
     */
    public function getShippingAddress(){
      return $this->ShippingAddress;
    }

    /**
     * @param Omni_Address $ShippingAddress
     * @return Omni_BasketPostSaleRequest
     */
    public function setShippingAddress($ShippingAddress){
      $this->ShippingAddress = $ShippingAddress;
      return $this;
    }

    /**
     * @return Omni_SourceType
     */
    public function getSourceType(){
      return $this->SourceType;
    }

    /**
     * @param Omni_SourceType $SourceType
     * @return Omni_BasketPostSaleRequest
     */
    public function setSourceType($SourceType){
      $this->SourceType = $SourceType;
      return $this;
    }

    /**
     * @return Omni_TenderPaymentLine[]
     */
    public function getTenderLines(){
      return $this->TenderLines;
    }

    /**
     * @param Omni_TenderPaymentLine[] $TenderLines
     * @return Omni_BasketPostSaleRequest
     */
    public function setTenderLines($TenderLines){
      $this->TenderLines = $TenderLines;
      return $this;
    }

}
