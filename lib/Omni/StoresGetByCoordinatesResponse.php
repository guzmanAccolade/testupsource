<?php

class Omni_StoresGetByCoordinatesResponse {

    /**
     * @var Omni_Store[] $StoresGetByCoordinatesResult
     * @access public
     */
    public $StoresGetByCoordinatesResult = null;

    /**
     * @param Omni_Store[] $StoresGetByCoordinatesResult
     * @access public
     */
    public function __construct($StoresGetByCoordinatesResult = null){
      $this->StoresGetByCoordinatesResult = $StoresGetByCoordinatesResult;
    }

    /**
     * @return Omni_Store[]
     */
    public function getStoresGetByCoordinatesResult(){
      return $this->StoresGetByCoordinatesResult;
    }

    /**
     * @param Omni_Store[] $StoresGetByCoordinatesResult
     * @return Omni_StoresGetByCoordinatesResponse
     */
    public function setStoresGetByCoordinatesResult($StoresGetByCoordinatesResult){
      $this->StoresGetByCoordinatesResult = $StoresGetByCoordinatesResult;
      return $this;
    }

}
