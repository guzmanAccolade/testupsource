<?php

class Omni_CurrencyRoundingMethod {
    const __default = 'RoundNearest';
    const RoundNearest = 'RoundNearest';
    const RoundDown = 'RoundDown';
    const RoundUp = 'RoundUp';


}
