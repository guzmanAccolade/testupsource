<?php

class Omni_ReplRequest {

    /**
     * @var int $BatchSize
     * @access public
     */
    public $BatchSize = null;

    /**
     * @var boolean $FullReplication
     * @access public
     */
    public $FullReplication = null;

    /**
     * @var string $LastKey
     * @access public
     */
    public $LastKey = null;

    /**
     * @var string $MaxKey
     * @access public
     */
    public $MaxKey = null;

    /**
     * @var string $StoreId
     * @access public
     */
    public $StoreId = null;

    /**
     * @var string $TerminalId
     * @access public
     */
    public $TerminalId = null;

    /**
     * @param int $BatchSize
     * @param boolean $FullReplication
     * @access public
     */
    public function __construct($BatchSize = null, $FullReplication = null){
      $this->BatchSize = $BatchSize;
      $this->FullReplication = $FullReplication;
    }

    /**
     * @return int
     */
    public function getBatchSize(){
      return $this->BatchSize;
    }

    /**
     * @param int $BatchSize
     * @return Omni_ReplRequest
     */
    public function setBatchSize($BatchSize){
      $this->BatchSize = $BatchSize;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFullReplication(){
      return $this->FullReplication;
    }

    /**
     * @param boolean $FullReplication
     * @return Omni_ReplRequest
     */
    public function setFullReplication($FullReplication){
      $this->FullReplication = $FullReplication;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastKey(){
      return $this->LastKey;
    }

    /**
     * @param string $LastKey
     * @return Omni_ReplRequest
     */
    public function setLastKey($LastKey){
      $this->LastKey = $LastKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getMaxKey(){
      return $this->MaxKey;
    }

    /**
     * @param string $MaxKey
     * @return Omni_ReplRequest
     */
    public function setMaxKey($MaxKey){
      $this->MaxKey = $MaxKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->StoreId;
    }

    /**
     * @param string $StoreId
     * @return Omni_ReplRequest
     */
    public function setStoreId($StoreId){
      $this->StoreId = $StoreId;
      return $this;
    }

    /**
     * @return string
     */
    public function getTerminalId(){
      return $this->TerminalId;
    }

    /**
     * @param string $TerminalId
     * @return Omni_ReplRequest
     */
    public function setTerminalId($TerminalId){
      $this->TerminalId = $TerminalId;
      return $this;
    }

}
