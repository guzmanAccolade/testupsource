<?php

class Omni_StoresGetAllResponse {

    /**
     * @var Omni_Store[] $StoresGetAllResult
     * @access public
     */
    public $StoresGetAllResult = null;

    /**
     * @param Omni_Store[] $StoresGetAllResult
     * @access public
     */
    public function __construct($StoresGetAllResult = null){
      $this->StoresGetAllResult = $StoresGetAllResult;
    }

    /**
     * @return Omni_Store[]
     */
    public function getStoresGetAllResult(){
      return $this->StoresGetAllResult;
    }

    /**
     * @param Omni_Store[] $StoresGetAllResult
     * @return Omni_StoresGetAllResponse
     */
    public function setStoresGetAllResult($StoresGetAllResult){
      $this->StoresGetAllResult = $StoresGetAllResult;
      return $this;
    }

}
