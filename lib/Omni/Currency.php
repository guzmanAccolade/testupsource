<?php

class Omni_Currency {

    /**
     * @var string $CurrencyCode
     * @access public
     */
    public $CurrencyCode = null;

    /**
     * @var string $CurrencyPrefix
     * @access public
     */
    public $CurrencyPrefix = null;

    /**
     * @var string $CurrencySuffix
     * @access public
     */
    public $CurrencySuffix = null;

    /**
     * @var boolean $Del
     * @access public
     */
    public $Del = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var float $RoundOfAmount
     * @access public
     */
    public $RoundOfAmount = null;

    /**
     * @var float $RoundOfSales
     * @access public
     */
    public $RoundOfSales = null;

    /**
     * @var int $RoundOfTypeAmount
     * @access public
     */
    public $RoundOfTypeAmount = null;

    /**
     * @var int $RoundOfTypeSales
     * @access public
     */
    public $RoundOfTypeSales = null;

    /**
     * @var string $Symbol
     * @access public
     */
    public $Symbol = null;

    /**
     * @param string $CurrencyCode
     * @param string $CurrencyPrefix
     * @param string $CurrencySuffix
     * @param boolean $Del
     * @param float $RoundOfAmount
     * @param float $RoundOfSales
     * @param int $RoundOfTypeAmount
     * @param int $RoundOfTypeSales
     * @access public
     */
    public function __construct($CurrencyCode = null, $CurrencyPrefix = null, $CurrencySuffix = null, $Del = null, $RoundOfAmount = null, $RoundOfSales = null, $RoundOfTypeAmount = null, $RoundOfTypeSales = null){
      $this->CurrencyCode = $CurrencyCode;
      $this->CurrencyPrefix = $CurrencyPrefix;
      $this->CurrencySuffix = $CurrencySuffix;
      $this->Del = $Del;
      $this->RoundOfAmount = $RoundOfAmount;
      $this->RoundOfSales = $RoundOfSales;
      $this->RoundOfTypeAmount = $RoundOfTypeAmount;
      $this->RoundOfTypeSales = $RoundOfTypeSales;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(){
      return $this->CurrencyCode;
    }

    /**
     * @param string $CurrencyCode
     * @return Omni_Currency
     */
    public function setCurrencyCode($CurrencyCode){
      $this->CurrencyCode = $CurrencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyPrefix(){
      return $this->CurrencyPrefix;
    }

    /**
     * @param string $CurrencyPrefix
     * @return Omni_Currency
     */
    public function setCurrencyPrefix($CurrencyPrefix){
      $this->CurrencyPrefix = $CurrencyPrefix;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencySuffix(){
      return $this->CurrencySuffix;
    }

    /**
     * @param string $CurrencySuffix
     * @return Omni_Currency
     */
    public function setCurrencySuffix($CurrencySuffix){
      $this->CurrencySuffix = $CurrencySuffix;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDel(){
      return $this->Del;
    }

    /**
     * @param boolean $Del
     * @return Omni_Currency
     */
    public function setDel($Del){
      $this->Del = $Del;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_Currency
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return float
     */
    public function getRoundOfAmount(){
      return $this->RoundOfAmount;
    }

    /**
     * @param float $RoundOfAmount
     * @return Omni_Currency
     */
    public function setRoundOfAmount($RoundOfAmount){
      $this->RoundOfAmount = $RoundOfAmount;
      return $this;
    }

    /**
     * @return float
     */
    public function getRoundOfSales(){
      return $this->RoundOfSales;
    }

    /**
     * @param float $RoundOfSales
     * @return Omni_Currency
     */
    public function setRoundOfSales($RoundOfSales){
      $this->RoundOfSales = $RoundOfSales;
      return $this;
    }

    /**
     * @return int
     */
    public function getRoundOfTypeAmount(){
      return $this->RoundOfTypeAmount;
    }

    /**
     * @param int $RoundOfTypeAmount
     * @return Omni_Currency
     */
    public function setRoundOfTypeAmount($RoundOfTypeAmount){
      $this->RoundOfTypeAmount = $RoundOfTypeAmount;
      return $this;
    }

    /**
     * @return int
     */
    public function getRoundOfTypeSales(){
      return $this->RoundOfTypeSales;
    }

    /**
     * @param int $RoundOfTypeSales
     * @return Omni_Currency
     */
    public function setRoundOfTypeSales($RoundOfTypeSales){
      $this->RoundOfTypeSales = $RoundOfTypeSales;
      return $this;
    }

    /**
     * @return string
     */
    public function getSymbol(){
      return $this->Symbol;
    }

    /**
     * @param string $Symbol
     * @return Omni_Currency
     */
    public function setSymbol($Symbol){
      $this->Symbol = $Symbol;
      return $this;
    }

}
