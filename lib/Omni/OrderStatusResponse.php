<?php

class Omni_OrderStatusResponse {

    /**
     * @var string $DocumentNo
     * @access public
     */
    public $DocumentNo = null;

    /**
     * @var string $DocumentType
     * @access public
     */
    public $DocumentType = null;

    /**
     * @var string $WebOrderShippingStatus
     * @access public
     */
    public $WebOrderShippingStatus = null;

    /**
     * @var string $WebOrderStatus_x0020_
     * @access public
     */
    public $WebOrderStatus_x0020_ = null;

    /**
     * @access public
     */
    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getDocumentNo(){
      return $this->DocumentNo;
    }

    /**
     * @param string $DocumentNo
     * @return Omni_OrderStatusResponse
     */
    public function setDocumentNo($DocumentNo){
      $this->DocumentNo = $DocumentNo;
      return $this;
    }

    /**
     * @return string
     */
    public function getDocumentType(){
      return $this->DocumentType;
    }

    /**
     * @param string $DocumentType
     * @return Omni_OrderStatusResponse
     */
    public function setDocumentType($DocumentType){
      $this->DocumentType = $DocumentType;
      return $this;
    }

    /**
     * @return string
     */
    public function getWebOrderShippingStatus(){
      return $this->WebOrderShippingStatus;
    }

    /**
     * @param string $WebOrderShippingStatus
     * @return Omni_OrderStatusResponse
     */
    public function setWebOrderShippingStatus($WebOrderShippingStatus){
      $this->WebOrderShippingStatus = $WebOrderShippingStatus;
      return $this;
    }

    /**
     * @return string
     */
    public function getWebOrderStatus_x0020_(){
      return $this->WebOrderStatus_x0020_;
    }

    /**
     * @param string $WebOrderStatus_x0020_
     * @return Omni_OrderStatusResponse
     */
    public function setWebOrderStatus_x0020_($WebOrderStatus_x0020_){
      $this->WebOrderStatus_x0020_ = $WebOrderStatus_x0020_;
      return $this;
    }

}
