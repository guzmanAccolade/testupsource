<?php

class Omni_ReplEcommExtendedVariantsResponse {

    /**
     * @var Omni_ReplExtendedVariantResponse $ReplEcommExtendedVariantsResult
     * @access public
     */
    public $ReplEcommExtendedVariantsResult = null;

    /**
     * @param Omni_ReplExtendedVariantResponse $ReplEcommExtendedVariantsResult
     * @access public
     */
    public function __construct($ReplEcommExtendedVariantsResult = null){
      $this->ReplEcommExtendedVariantsResult = $ReplEcommExtendedVariantsResult;
    }

    /**
     * @return Omni_ReplExtendedVariantResponse
     */
    public function getReplEcommExtendedVariantsResult(){
      return $this->ReplEcommExtendedVariantsResult;
    }

    /**
     * @param Omni_ReplExtendedVariantResponse $ReplEcommExtendedVariantsResult
     * @return Omni_ReplEcommExtendedVariantsResponse
     */
    public function setReplEcommExtendedVariantsResult($ReplEcommExtendedVariantsResult){
      $this->ReplEcommExtendedVariantsResult = $ReplEcommExtendedVariantsResult;
      return $this;
    }

}
