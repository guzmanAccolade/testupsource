<?php

class Omni_ContactUpdate {

    /**
     * @var Omni_Contact $contact
     * @access public
     */
    public $contact = null;

    /**
     * @param Omni_Contact $contact
     * @access public
     */
    public function __construct($contact = null){
      $this->contact = $contact;
    }

    /**
     * @return Omni_Contact
     */
    public function getContact(){
      return $this->contact;
    }

    /**
     * @param Omni_Contact $contact
     * @return Omni_ContactUpdate
     */
    public function setContact($contact){
      $this->contact = $contact;
      return $this;
    }

}
