<?php

class Omni_LogoutResponse {

    /**
     * @var boolean $LogoutResult
     * @access public
     */
    public $LogoutResult = null;

    /**
     * @param boolean $LogoutResult
     * @access public
     */
    public function __construct($LogoutResult = null){
      $this->LogoutResult = $LogoutResult;
    }

    /**
     * @return boolean
     */
    public function getLogoutResult(){
      return $this->LogoutResult;
    }

    /**
     * @param boolean $LogoutResult
     * @return Omni_LogoutResponse
     */
    public function setLogoutResult($LogoutResult){
      $this->LogoutResult = $LogoutResult;
      return $this;
    }

}
