<?php

class Omni_ReplPriceResponse {

    /**
     * @var string $LastKey
     * @access public
     */
    public $LastKey = null;

    /**
     * @var string $MaxKey
     * @access public
     */
    public $MaxKey = null;

    /**
     * @var Omni_Price[] $Prices
     * @access public
     */
    public $Prices = null;

    /**
     * @var int $RecordsRemaining
     * @access public
     */
    public $RecordsRemaining = null;

    /**
     * @param int $RecordsRemaining
     * @access public
     */
    public function __construct($RecordsRemaining = null){
      $this->RecordsRemaining = $RecordsRemaining;
    }

    /**
     * @return string
     */
    public function getLastKey(){
      return $this->LastKey;
    }

    /**
     * @param string $LastKey
     * @return Omni_ReplPriceResponse
     */
    public function setLastKey($LastKey){
      $this->LastKey = $LastKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getMaxKey(){
      return $this->MaxKey;
    }

    /**
     * @param string $MaxKey
     * @return Omni_ReplPriceResponse
     */
    public function setMaxKey($MaxKey){
      $this->MaxKey = $MaxKey;
      return $this;
    }

    /**
     * @return Omni_Price[]
     */
    public function getPrices(){
      return $this->Prices;
    }

    /**
     * @param Omni_Price[] $Prices
     * @return Omni_ReplPriceResponse
     */
    public function setPrices($Prices){
      $this->Prices = $Prices;
      return $this;
    }

    /**
     * @return int
     */
    public function getRecordsRemaining(){
      return $this->RecordsRemaining;
    }

    /**
     * @param int $RecordsRemaining
     * @return Omni_ReplPriceResponse
     */
    public function setRecordsRemaining($RecordsRemaining){
      $this->RecordsRemaining = $RecordsRemaining;
      return $this;
    }

}
