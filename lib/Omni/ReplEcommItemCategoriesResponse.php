<?php

class Omni_ReplEcommItemCategoriesResponse {

    /**
     * @var Omni_ReplItemCategoryResponse $ReplEcommItemCategoriesResult
     * @access public
     */
    public $ReplEcommItemCategoriesResult = null;

    /**
     * @param Omni_ReplItemCategoryResponse $ReplEcommItemCategoriesResult
     * @access public
     */
    public function __construct($ReplEcommItemCategoriesResult = null){
      $this->ReplEcommItemCategoriesResult = $ReplEcommItemCategoriesResult;
    }

    /**
     * @return Omni_ReplItemCategoryResponse
     */
    public function getReplEcommItemCategoriesResult(){
      return $this->ReplEcommItemCategoriesResult;
    }

    /**
     * @param Omni_ReplItemCategoryResponse $ReplEcommItemCategoriesResult
     * @return Omni_ReplEcommItemCategoriesResponse
     */
    public function setReplEcommItemCategoriesResult($ReplEcommItemCategoriesResult){
      $this->ReplEcommItemCategoriesResult = $ReplEcommItemCategoriesResult;
      return $this;
    }

}
