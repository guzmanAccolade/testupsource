<?php

class Omni_StoresGetbyItemInStockResponse {

    /**
     * @var Omni_Store[] $StoresGetbyItemInStockResult
     * @access public
     */
    public $StoresGetbyItemInStockResult = null;

    /**
     * @param Omni_Store[] $StoresGetbyItemInStockResult
     * @access public
     */
    public function __construct($StoresGetbyItemInStockResult = null){
      $this->StoresGetbyItemInStockResult = $StoresGetbyItemInStockResult;
    }

    /**
     * @return Omni_Store[]
     */
    public function getStoresGetbyItemInStockResult(){
      return $this->StoresGetbyItemInStockResult;
    }

    /**
     * @param Omni_Store[] $StoresGetbyItemInStockResult
     * @return Omni_StoresGetbyItemInStockResponse
     */
    public function setStoresGetbyItemInStockResult($StoresGetbyItemInStockResult){
      $this->StoresGetbyItemInStockResult = $StoresGetbyItemInStockResult;
      return $this;
    }

}
