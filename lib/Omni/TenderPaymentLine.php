<?php

class Omni_TenderPaymentLine {

    /**
     * @var float $Amount
     * @access public
     */
    public $Amount = null;

    /**
     * @var string $CardOrCustNumber
     * @access public
     */
    public $CardOrCustNumber = null;

    /**
     * @var string $CardType
     * @access public
     */
    public $CardType = null;

    /**
     * @var string $CurrencyCode
     * @access public
     */
    public $CurrencyCode = null;

    /**
     * @var string $EFTAuthCode
     * @access public
     */
    public $EFTAuthCode = null;

    /**
     * @var string $EFTCardName
     * @access public
     */
    public $EFTCardName = null;

    /**
     * @var string $EFTCardNumber
     * @access public
     */
    public $EFTCardNumber = null;

    /**
     * @var string $EFTMessage
     * @access public
     */
    public $EFTMessage = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var int $LineNumber
     * @access public
     */
    public $LineNumber = null;

    /**
     * @var float $Points
     * @access public
     */
    public $Points = null;

    /**
     * @var Omni_TenderType $Type
     * @access public
     */
    public $Type = null;

    /**
     * @param float $Amount
     * @param int $LineNumber
     * @param float $Points
     * @param Omni_TenderType $Type
     * @access public
     */
    public function __construct($Amount = null, $LineNumber = null, $Points = null, $Type = null){
      $this->Amount = $Amount;
      $this->LineNumber = $LineNumber;
      $this->Points = $Points;
      $this->Type = $Type;
    }

    /**
     * @return float
     */
    public function getAmount(){
      return $this->Amount;
    }

    /**
     * @param float $Amount
     * @return Omni_TenderPaymentLine
     */
    public function setAmount($Amount){
      $this->Amount = $Amount;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardOrCustNumber(){
      return $this->CardOrCustNumber;
    }

    /**
     * @param string $CardOrCustNumber
     * @return Omni_TenderPaymentLine
     */
    public function setCardOrCustNumber($CardOrCustNumber){
      $this->CardOrCustNumber = $CardOrCustNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardType(){
      return $this->CardType;
    }

    /**
     * @param string $CardType
     * @return Omni_TenderPaymentLine
     */
    public function setCardType($CardType){
      $this->CardType = $CardType;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(){
      return $this->CurrencyCode;
    }

    /**
     * @param string $CurrencyCode
     * @return Omni_TenderPaymentLine
     */
    public function setCurrencyCode($CurrencyCode){
      $this->CurrencyCode = $CurrencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getEFTAuthCode(){
      return $this->EFTAuthCode;
    }

    /**
     * @param string $EFTAuthCode
     * @return Omni_TenderPaymentLine
     */
    public function setEFTAuthCode($EFTAuthCode){
      $this->EFTAuthCode = $EFTAuthCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getEFTCardName(){
      return $this->EFTCardName;
    }

    /**
     * @param string $EFTCardName
     * @return Omni_TenderPaymentLine
     */
    public function setEFTCardName($EFTCardName){
      $this->EFTCardName = $EFTCardName;
      return $this;
    }

    /**
     * @return string
     */
    public function getEFTCardNumber(){
      return $this->EFTCardNumber;
    }

    /**
     * @param string $EFTCardNumber
     * @return Omni_TenderPaymentLine
     */
    public function setEFTCardNumber($EFTCardNumber){
      $this->EFTCardNumber = $EFTCardNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getEFTMessage(){
      return $this->EFTMessage;
    }

    /**
     * @param string $EFTMessage
     * @return Omni_TenderPaymentLine
     */
    public function setEFTMessage($EFTMessage){
      $this->EFTMessage = $EFTMessage;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_TenderPaymentLine
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return int
     */
    public function getLineNumber(){
      return $this->LineNumber;
    }

    /**
     * @param int $LineNumber
     * @return Omni_TenderPaymentLine
     */
    public function setLineNumber($LineNumber){
      $this->LineNumber = $LineNumber;
      return $this;
    }

    /**
     * @return float
     */
    public function getPoints(){
      return $this->Points;
    }

    /**
     * @param float $Points
     * @return Omni_TenderPaymentLine
     */
    public function setPoints($Points){
      $this->Points = $Points;
      return $this;
    }

    /**
     * @return Omni_TenderType
     */
    public function getType(){
      return $this->Type;
    }

    /**
     * @param Omni_TenderType $Type
     * @return Omni_TenderPaymentLine
     */
    public function setType($Type){
      $this->Type = $Type;
      return $this;
    }

}
