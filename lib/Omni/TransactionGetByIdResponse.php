<?php

class Omni_TransactionGetByIdResponse {

    /**
     * @var Omni_Transaction $TransactionGetByIdResult
     * @access public
     */
    public $TransactionGetByIdResult = null;

    /**
     * @param Omni_Transaction $TransactionGetByIdResult
     * @access public
     */
    public function __construct($TransactionGetByIdResult = null){
      $this->TransactionGetByIdResult = $TransactionGetByIdResult;
    }

    /**
     * @return Omni_Transaction
     */
    public function getTransactionGetByIdResult(){
      return $this->TransactionGetByIdResult;
    }

    /**
     * @param Omni_Transaction $TransactionGetByIdResult
     * @return Omni_TransactionGetByIdResponse
     */
    public function setTransactionGetByIdResult($TransactionGetByIdResult){
      $this->TransactionGetByIdResult = $TransactionGetByIdResult;
      return $this;
    }

}
