<?php

class Omni_ProductGroup {

    /**
     * @var boolean $Del
     * @access public
     */
    public $Del = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $ItemCategoryID
     * @access public
     */
    public $ItemCategoryID = null;

    /**
     * @param boolean $Del
     * @access public
     */
    public function __construct($Del = null){
      $this->Del = $Del;
    }

    /**
     * @return boolean
     */
    public function getDel(){
      return $this->Del;
    }

    /**
     * @param boolean $Del
     * @return Omni_ProductGroup
     */
    public function setDel($Del){
      $this->Del = $Del;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_ProductGroup
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_ProductGroup
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemCategoryID(){
      return $this->ItemCategoryID;
    }

    /**
     * @param string $ItemCategoryID
     * @return Omni_ProductGroup
     */
    public function setItemCategoryID($ItemCategoryID){
      $this->ItemCategoryID = $ItemCategoryID;
      return $this;
    }

}
