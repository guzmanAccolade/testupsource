<?php

class Omni_OneListDeleteByIdResponse {

    /**
     * @var boolean $OneListDeleteByIdResult
     * @access public
     */
    public $OneListDeleteByIdResult = null;

    /**
     * @param boolean $OneListDeleteByIdResult
     * @access public
     */
    public function __construct($OneListDeleteByIdResult = null){
      $this->OneListDeleteByIdResult = $OneListDeleteByIdResult;
    }

    /**
     * @return boolean
     */
    public function getOneListDeleteByIdResult(){
      return $this->OneListDeleteByIdResult;
    }

    /**
     * @param boolean $OneListDeleteByIdResult
     * @return Omni_OneListDeleteByIdResponse
     */
    public function setOneListDeleteByIdResult($OneListDeleteByIdResult){
      $this->OneListDeleteByIdResult = $OneListDeleteByIdResult;
      return $this;
    }

}
