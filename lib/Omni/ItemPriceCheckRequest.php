<?php

class Omni_ItemPriceCheckRequest {

    /**
     * @var string $CurrencyCode
     * @access public
     */
    public $CurrencyCode = null;

    /**
     * @var int $CurrencyFactor
     * @access public
     */
    public $CurrencyFactor = null;

    /**
     * @var string $CustDiscGroup
     * @access public
     */
    public $CustDiscGroup = null;

    /**
     * @var string $CustomerId
     * @access public
     */
    public $CustomerId = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var Omni_ItemPriceCheckLineRequest[] $ItemPriceCheckLineRequests
     * @access public
     */
    public $ItemPriceCheckLineRequests = null;

    /**
     * @var string $MemberCardNo
     * @access public
     */
    public $MemberCardNo = null;

    /**
     * @var string $MemberPriceGroupCode
     * @access public
     */
    public $MemberPriceGroupCode = null;

    /**
     * @var string $StaffId
     * @access public
     */
    public $StaffId = null;

    /**
     * @var string $StoreId
     * @access public
     */
    public $StoreId = null;

    /**
     * @var string $TerminalId
     * @access public
     */
    public $TerminalId = null;

    /**
     * @param int $CurrencyFactor
     * @access public
     */
    public function __construct($CurrencyFactor = null){
      $this->CurrencyFactor = $CurrencyFactor;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(){
      return $this->CurrencyCode;
    }

    /**
     * @param string $CurrencyCode
     * @return Omni_ItemPriceCheckRequest
     */
    public function setCurrencyCode($CurrencyCode){
      $this->CurrencyCode = $CurrencyCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getCurrencyFactor(){
      return $this->CurrencyFactor;
    }

    /**
     * @param int $CurrencyFactor
     * @return Omni_ItemPriceCheckRequest
     */
    public function setCurrencyFactor($CurrencyFactor){
      $this->CurrencyFactor = $CurrencyFactor;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustDiscGroup(){
      return $this->CustDiscGroup;
    }

    /**
     * @param string $CustDiscGroup
     * @return Omni_ItemPriceCheckRequest
     */
    public function setCustDiscGroup($CustDiscGroup){
      $this->CustDiscGroup = $CustDiscGroup;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId(){
      return $this->CustomerId;
    }

    /**
     * @param string $CustomerId
     * @return Omni_ItemPriceCheckRequest
     */
    public function setCustomerId($CustomerId){
      $this->CustomerId = $CustomerId;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_ItemPriceCheckRequest
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return Omni_ItemPriceCheckLineRequest[]
     */
    public function getItemPriceCheckLineRequests(){
      return $this->ItemPriceCheckLineRequests;
    }

    /**
     * @param Omni_ItemPriceCheckLineRequest[] $ItemPriceCheckLineRequests
     * @return Omni_ItemPriceCheckRequest
     */
    public function setItemPriceCheckLineRequests($ItemPriceCheckLineRequests){
      $this->ItemPriceCheckLineRequests = $ItemPriceCheckLineRequests;
      return $this;
    }

    /**
     * @return string
     */
    public function getMemberCardNo(){
      return $this->MemberCardNo;
    }

    /**
     * @param string $MemberCardNo
     * @return Omni_ItemPriceCheckRequest
     */
    public function setMemberCardNo($MemberCardNo){
      $this->MemberCardNo = $MemberCardNo;
      return $this;
    }

    /**
     * @return string
     */
    public function getMemberPriceGroupCode(){
      return $this->MemberPriceGroupCode;
    }

    /**
     * @param string $MemberPriceGroupCode
     * @return Omni_ItemPriceCheckRequest
     */
    public function setMemberPriceGroupCode($MemberPriceGroupCode){
      $this->MemberPriceGroupCode = $MemberPriceGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStaffId(){
      return $this->StaffId;
    }

    /**
     * @param string $StaffId
     * @return Omni_ItemPriceCheckRequest
     */
    public function setStaffId($StaffId){
      $this->StaffId = $StaffId;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->StoreId;
    }

    /**
     * @param string $StoreId
     * @return Omni_ItemPriceCheckRequest
     */
    public function setStoreId($StoreId){
      $this->StoreId = $StoreId;
      return $this;
    }

    /**
     * @return string
     */
    public function getTerminalId(){
      return $this->TerminalId;
    }

    /**
     * @param string $TerminalId
     * @return Omni_ItemPriceCheckRequest
     */
    public function setTerminalId($TerminalId){
      $this->TerminalId = $TerminalId;
      return $this;
    }

}
