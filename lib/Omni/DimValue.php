<?php

class Omni_DimValue {

    /**
     * @var int $DisplayOrder
     * @access public
     */
    public $DisplayOrder = null;

    /**
     * @var string $Value
     * @access public
     */
    public $Value = null;

    /**
     * @param int $DisplayOrder
     * @access public
     */
    public function __construct($DisplayOrder = null){
      $this->DisplayOrder = $DisplayOrder;
    }

    /**
     * @return int
     */
    public function getDisplayOrder(){
      return $this->DisplayOrder;
    }

    /**
     * @param int $DisplayOrder
     * @return Omni_DimValue
     */
    public function setDisplayOrder($DisplayOrder){
      $this->DisplayOrder = $DisplayOrder;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue(){
      return $this->Value;
    }

    /**
     * @param string $Value
     * @return Omni_DimValue
     */
    public function setValue($Value){
      $this->Value = $Value;
      return $this;
    }

}
