<?php

class Omni_OneListPublishedOffer {

    /**
     * @var dateTime $CreateDate
     * @access public
     */
    public $CreateDate = null;

    /**
     * @var int $DisplayOrderId
     * @access public
     */
    public $DisplayOrderId = null;

    /**
     * @var Omni_PublishedOffer $PublishedOffer
     * @access public
     */
    public $PublishedOffer = null;

    /**
     * @param dateTime $CreateDate
     * @param int $DisplayOrderId
     * @access public
     */
    public function __construct($CreateDate = null, $DisplayOrderId = null){
      $this->CreateDate = $CreateDate;
      $this->DisplayOrderId = $DisplayOrderId;
    }

    /**
     * @return dateTime
     */
    public function getCreateDate(){
      return $this->CreateDate;
    }

    /**
     * @param dateTime $CreateDate
     * @return Omni_OneListPublishedOffer
     */
    public function setCreateDate($CreateDate){
      $this->CreateDate = $CreateDate;
      return $this;
    }

    /**
     * @return int
     */
    public function getDisplayOrderId(){
      return $this->DisplayOrderId;
    }

    /**
     * @param int $DisplayOrderId
     * @return Omni_OneListPublishedOffer
     */
    public function setDisplayOrderId($DisplayOrderId){
      $this->DisplayOrderId = $DisplayOrderId;
      return $this;
    }

    /**
     * @return Omni_PublishedOffer
     */
    public function getPublishedOffer(){
      return $this->PublishedOffer;
    }

    /**
     * @param Omni_PublishedOffer $PublishedOffer
     * @return Omni_OneListPublishedOffer
     */
    public function setPublishedOffer($PublishedOffer){
      $this->PublishedOffer = $PublishedOffer;
      return $this;
    }

}
