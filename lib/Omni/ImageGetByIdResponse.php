<?php

class Omni_ImageGetByIdResponse {

    /**
     * @var Omni_ImageView $ImageGetByIdResult
     * @access public
     */
    public $ImageGetByIdResult = null;

    /**
     * @param Omni_ImageView $ImageGetByIdResult
     * @access public
     */
    public function __construct($ImageGetByIdResult = null){
      $this->ImageGetByIdResult = $ImageGetByIdResult;
    }

    /**
     * @return Omni_ImageView
     */
    public function getImageGetByIdResult(){
      return $this->ImageGetByIdResult;
    }

    /**
     * @param Omni_ImageView $ImageGetByIdResult
     * @return Omni_ImageGetByIdResponse
     */
    public function setImageGetByIdResult($ImageGetByIdResult){
      $this->ImageGetByIdResult = $ImageGetByIdResult;
      return $this;
    }

}
