<?php

class Omni_ReplEcommProductGroupsResponse {

    /**
     * @var Omni_ReplProductGroupResponse $ReplEcommProductGroupsResult
     * @access public
     */
    public $ReplEcommProductGroupsResult = null;

    /**
     * @param Omni_ReplProductGroupResponse $ReplEcommProductGroupsResult
     * @access public
     */
    public function __construct($ReplEcommProductGroupsResult = null){
      $this->ReplEcommProductGroupsResult = $ReplEcommProductGroupsResult;
    }

    /**
     * @return Omni_ReplProductGroupResponse
     */
    public function getReplEcommProductGroupsResult(){
      return $this->ReplEcommProductGroupsResult;
    }

    /**
     * @param Omni_ReplProductGroupResponse $ReplEcommProductGroupsResult
     * @return Omni_ReplEcommProductGroupsResponse
     */
    public function setReplEcommProductGroupsResult($ReplEcommProductGroupsResult){
      $this->ReplEcommProductGroupsResult = $ReplEcommProductGroupsResult;
      return $this;
    }

}
