<?php

class Omni_TransactionGetById {

    /**
     * @var string $storeId
     * @access public
     */
    public $storeId = null;

    /**
     * @var string $terminalId
     * @access public
     */
    public $terminalId = null;

    /**
     * @var string $transactionId
     * @access public
     */
    public $transactionId = null;

    /**
     * @var boolean $includeLines
     * @access public
     */
    public $includeLines = null;

    /**
     * @param string $storeId
     * @param string $terminalId
     * @param string $transactionId
     * @param boolean $includeLines
     * @access public
     */
    public function __construct($storeId = null, $terminalId = null, $transactionId = null, $includeLines = null){
      $this->storeId = $storeId;
      $this->terminalId = $terminalId;
      $this->transactionId = $transactionId;
      $this->includeLines = $includeLines;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->storeId;
    }

    /**
     * @param string $storeId
     * @return Omni_TransactionGetById
     */
    public function setStoreId($storeId){
      $this->storeId = $storeId;
      return $this;
    }

    /**
     * @return string
     */
    public function getTerminalId(){
      return $this->terminalId;
    }

    /**
     * @param string $terminalId
     * @return Omni_TransactionGetById
     */
    public function setTerminalId($terminalId){
      $this->terminalId = $terminalId;
      return $this;
    }

    /**
     * @return string
     */
    public function getTransactionId(){
      return $this->transactionId;
    }

    /**
     * @param string $transactionId
     * @return Omni_TransactionGetById
     */
    public function setTransactionId($transactionId){
      $this->transactionId = $transactionId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeLines(){
      return $this->includeLines;
    }

    /**
     * @param boolean $includeLines
     * @return Omni_TransactionGetById
     */
    public function setIncludeLines($includeLines){
      $this->includeLines = $includeLines;
      return $this;
    }

}
