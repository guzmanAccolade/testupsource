<?php

class Omni_OrderSearchRequest {

    /**
     * @var string $ContactId
     * @access public
     */
    public $ContactId = null;

    /**
     * @var dateTime $DateFrom
     * @access public
     */
    public $DateFrom = null;

    /**
     * @var dateTime $DateTo
     * @access public
     */
    public $DateTo = null;

    /**
     * @var Omni_OrderQueueStatusFilterType $OrderStatusFilter
     * @access public
     */
    public $OrderStatusFilter = null;

    /**
     * @var Omni_OrderQueueType $OrderType
     * @access public
     */
    public $OrderType = null;

    /**
     * @var int $PageNumber
     * @access public
     */
    public $PageNumber = null;

    /**
     * @var int $PageSize
     * @access public
     */
    public $PageSize = null;

    /**
     * @var string $SearchKey
     * @access public
     */
    public $SearchKey = null;

    /**
     * @var string $StoreId
     * @access public
     */
    public $StoreId = null;

    /**
     * @param Omni_OrderQueueStatusFilterType $OrderStatusFilter
     * @param Omni_OrderQueueType $OrderType
     * @param int $PageNumber
     * @param int $PageSize
     * @access public
     */
    public function __construct($OrderStatusFilter = null, $OrderType = null, $PageNumber = null, $PageSize = null){
      $this->OrderStatusFilter = $OrderStatusFilter;
      $this->OrderType = $OrderType;
      $this->PageNumber = $PageNumber;
      $this->PageSize = $PageSize;
    }

    /**
     * @return string
     */
    public function getContactId(){
      return $this->ContactId;
    }

    /**
     * @param string $ContactId
     * @return Omni_OrderSearchRequest
     */
    public function setContactId($ContactId){
      $this->ContactId = $ContactId;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getDateFrom(){
      return $this->DateFrom;
    }

    /**
     * @param dateTime $DateFrom
     * @return Omni_OrderSearchRequest
     */
    public function setDateFrom($DateFrom){
      $this->DateFrom = $DateFrom;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getDateTo(){
      return $this->DateTo;
    }

    /**
     * @param dateTime $DateTo
     * @return Omni_OrderSearchRequest
     */
    public function setDateTo($DateTo){
      $this->DateTo = $DateTo;
      return $this;
    }

    /**
     * @return Omni_OrderQueueStatusFilterType
     */
    public function getOrderStatusFilter(){
      return $this->OrderStatusFilter;
    }

    /**
     * @param Omni_OrderQueueStatusFilterType $OrderStatusFilter
     * @return Omni_OrderSearchRequest
     */
    public function setOrderStatusFilter($OrderStatusFilter){
      $this->OrderStatusFilter = $OrderStatusFilter;
      return $this;
    }

    /**
     * @return Omni_OrderQueueType
     */
    public function getOrderType(){
      return $this->OrderType;
    }

    /**
     * @param Omni_OrderQueueType $OrderType
     * @return Omni_OrderSearchRequest
     */
    public function setOrderType($OrderType){
      $this->OrderType = $OrderType;
      return $this;
    }

    /**
     * @return int
     */
    public function getPageNumber(){
      return $this->PageNumber;
    }

    /**
     * @param int $PageNumber
     * @return Omni_OrderSearchRequest
     */
    public function setPageNumber($PageNumber){
      $this->PageNumber = $PageNumber;
      return $this;
    }

    /**
     * @return int
     */
    public function getPageSize(){
      return $this->PageSize;
    }

    /**
     * @param int $PageSize
     * @return Omni_OrderSearchRequest
     */
    public function setPageSize($PageSize){
      $this->PageSize = $PageSize;
      return $this;
    }

    /**
     * @return string
     */
    public function getSearchKey(){
      return $this->SearchKey;
    }

    /**
     * @param string $SearchKey
     * @return Omni_OrderSearchRequest
     */
    public function setSearchKey($SearchKey){
      $this->SearchKey = $SearchKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->StoreId;
    }

    /**
     * @param string $StoreId
     * @return Omni_OrderSearchRequest
     */
    public function setStoreId($StoreId){
      $this->StoreId = $StoreId;
      return $this;
    }

}
