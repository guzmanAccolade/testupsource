<?php

class Omni_ResetPassword {

    /**
     * @var string $userName
     * @access public
     */
    public $userName = null;

    /**
     * @var string $resetCode
     * @access public
     */
    public $resetCode = null;

    /**
     * @var string $newPassword
     * @access public
     */
    public $newPassword = null;

    /**
     * @param string $userName
     * @param string $resetCode
     * @param string $newPassword
     * @access public
     */
    public function __construct($userName = null, $resetCode = null, $newPassword = null){
      $this->userName = $userName;
      $this->resetCode = $resetCode;
      $this->newPassword = $newPassword;
    }

    /**
     * @return string
     */
    public function getUserName(){
      return $this->userName;
    }

    /**
     * @param string $userName
     * @return Omni_ResetPassword
     */
    public function setUserName($userName){
      $this->userName = $userName;
      return $this;
    }

    /**
     * @return string
     */
    public function getResetCode(){
      return $this->resetCode;
    }

    /**
     * @param string $resetCode
     * @return Omni_ResetPassword
     */
    public function setResetCode($resetCode){
      $this->resetCode = $resetCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getNewPassword(){
      return $this->newPassword;
    }

    /**
     * @param string $newPassword
     * @return Omni_ResetPassword
     */
    public function setNewPassword($newPassword){
      $this->newPassword = $newPassword;
      return $this;
    }

}
