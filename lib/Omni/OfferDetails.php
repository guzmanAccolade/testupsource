<?php

class Omni_OfferDetails {

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var Omni_ImageView $Image
     * @access public
     */
    public $Image = null;

    /**
     * @var string $LineNumber
     * @access public
     */
    public $LineNumber = null;

    /**
     * @var string $OfferId
     * @access public
     */
    public $OfferId = null;

    /**
     * @access public
     */
    public function __construct(){

    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_OfferDetails
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return Omni_ImageView
     */
    public function getImage(){
      return $this->Image;
    }

    /**
     * @param Omni_ImageView $Image
     * @return Omni_OfferDetails
     */
    public function setImage($Image){
      $this->Image = $Image;
      return $this;
    }

    /**
     * @return string
     */
    public function getLineNumber(){
      return $this->LineNumber;
    }

    /**
     * @param string $LineNumber
     * @return Omni_OfferDetails
     */
    public function setLineNumber($LineNumber){
      $this->LineNumber = $LineNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getOfferId(){
      return $this->OfferId;
    }

    /**
     * @param string $OfferId
     * @return Omni_OfferDetails
     */
    public function setOfferId($OfferId){
      $this->OfferId = $OfferId;
      return $this;
    }

}
