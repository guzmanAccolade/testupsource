<?php

class Omni_ReplEcommVendorResponse {

    /**
     * @var Omni_ReplVendorResponse $ReplEcommVendorResult
     * @access public
     */
    public $ReplEcommVendorResult = null;

    /**
     * @param Omni_ReplVendorResponse $ReplEcommVendorResult
     * @access public
     */
    public function __construct($ReplEcommVendorResult = null){
      $this->ReplEcommVendorResult = $ReplEcommVendorResult;
    }

    /**
     * @return Omni_ReplVendorResponse
     */
    public function getReplEcommVendorResult(){
      return $this->ReplEcommVendorResult;
    }

    /**
     * @param Omni_ReplVendorResponse $ReplEcommVendorResult
     * @return Omni_ReplEcommVendorResponse
     */
    public function setReplEcommVendorResult($ReplEcommVendorResult){
      $this->ReplEcommVendorResult = $ReplEcommVendorResult;
      return $this;
    }

}
