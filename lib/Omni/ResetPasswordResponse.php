<?php

class Omni_ResetPasswordResponse {

    /**
     * @var boolean $ResetPasswordResult
     * @access public
     */
    public $ResetPasswordResult = null;

    /**
     * @param boolean $ResetPasswordResult
     * @access public
     */
    public function __construct($ResetPasswordResult = null){
      $this->ResetPasswordResult = $ResetPasswordResult;
    }

    /**
     * @return boolean
     */
    public function getResetPasswordResult(){
      return $this->ResetPasswordResult;
    }

    /**
     * @param boolean $ResetPasswordResult
     * @return Omni_ResetPasswordResponse
     */
    public function setResetPasswordResult($ResetPasswordResult){
      $this->ResetPasswordResult = $ResetPasswordResult;
      return $this;
    }

}
