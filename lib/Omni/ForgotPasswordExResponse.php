<?php

class Omni_ForgotPasswordExResponse {

    /**
     * @var boolean $ForgotPasswordExResult
     * @access public
     */
    public $ForgotPasswordExResult = null;

    /**
     * @param boolean $ForgotPasswordExResult
     * @access public
     */
    public function __construct($ForgotPasswordExResult = null){
      $this->ForgotPasswordExResult = $ForgotPasswordExResult;
    }

    /**
     * @return boolean
     */
    public function getForgotPasswordExResult(){
      return $this->ForgotPasswordExResult;
    }

    /**
     * @param boolean $ForgotPasswordExResult
     * @return Omni_ForgotPasswordExResponse
     */
    public function setForgotPasswordExResult($ForgotPasswordExResult){
      $this->ForgotPasswordExResult = $ForgotPasswordExResult;
      return $this;
    }

}
