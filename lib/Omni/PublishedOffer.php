<?php

class Omni_PublishedOffer {

    /**
     * @var Omni_OfferCode $Code
     * @access public
     */
    public $Code = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $Details
     * @access public
     */
    public $Details = null;

    /**
     * @var dateTime $ExpirationDate
     * @access public
     */
    public $ExpirationDate = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var Omni_ImageView[] $Images
     * @access public
     */
    public $Images = null;

    /**
     * @var Omni_OfferDetails[] $OfferDetails
     * @access public
     */
    public $OfferDetails = null;

    /**
     * @var string $OfferId
     * @access public
     */
    public $OfferId = null;

    /**
     * @var Omni_OfferType $Type
     * @access public
     */
    public $Type = null;

    /**
     * @var string $ValidationText
     * @access public
     */
    public $ValidationText = null;

    /**
     * @param Omni_OfferCode $Code
     * @param Omni_OfferType $Type
     * @access public
     */
    public function __construct($Code = null, $Type = null){
      $this->Code = $Code;
      $this->Type = $Type;
    }

    /**
     * @return Omni_OfferCode
     */
    public function getCode(){
      return $this->Code;
    }

    /**
     * @param Omni_OfferCode $Code
     * @return Omni_PublishedOffer
     */
    public function setCode($Code){
      $this->Code = $Code;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_PublishedOffer
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getDetails(){
      return $this->Details;
    }

    /**
     * @param string $Details
     * @return Omni_PublishedOffer
     */
    public function setDetails($Details){
      $this->Details = $Details;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getExpirationDate(){
      return $this->ExpirationDate;
    }

    /**
     * @param dateTime $ExpirationDate
     * @return Omni_PublishedOffer
     */
    public function setExpirationDate($ExpirationDate){
      $this->ExpirationDate = $ExpirationDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_PublishedOffer
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return Omni_ImageView[]
     */
    public function getImages(){
      return $this->Images;
    }

    /**
     * @param Omni_ImageView[] $Images
     * @return Omni_PublishedOffer
     */
    public function setImages($Images){
      $this->Images = $Images;
      return $this;
    }

    /**
     * @return Omni_OfferDetails[]
     */
    public function getOfferDetails(){
      return $this->OfferDetails;
    }

    /**
     * @param Omni_OfferDetails[] $OfferDetails
     * @return Omni_PublishedOffer
     */
    public function setOfferDetails($OfferDetails){
      $this->OfferDetails = $OfferDetails;
      return $this;
    }

    /**
     * @return string
     */
    public function getOfferId(){
      return $this->OfferId;
    }

    /**
     * @param string $OfferId
     * @return Omni_PublishedOffer
     */
    public function setOfferId($OfferId){
      $this->OfferId = $OfferId;
      return $this;
    }

    /**
     * @return Omni_OfferType
     */
    public function getType(){
      return $this->Type;
    }

    /**
     * @param Omni_OfferType $Type
     * @return Omni_PublishedOffer
     */
    public function setType($Type){
      $this->Type = $Type;
      return $this;
    }

    /**
     * @return string
     */
    public function getValidationText(){
      return $this->ValidationText;
    }

    /**
     * @param string $ValidationText
     * @return Omni_PublishedOffer
     */
    public function setValidationText($ValidationText){
      $this->ValidationText = $ValidationText;
      return $this;
    }

}
