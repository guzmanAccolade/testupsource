<?php

class Omni_SchemesGetAllResponse {

    /**
     * @var Omni_Scheme[] $SchemesGetAllResult
     * @access public
     */
    public $SchemesGetAllResult = null;

    /**
     * @param Omni_Scheme[] $SchemesGetAllResult
     * @access public
     */
    public function __construct($SchemesGetAllResult = null){
      $this->SchemesGetAllResult = $SchemesGetAllResult;
    }

    /**
     * @return Omni_Scheme[]
     */
    public function getSchemesGetAllResult(){
      return $this->SchemesGetAllResult;
    }

    /**
     * @param Omni_Scheme[] $SchemesGetAllResult
     * @return Omni_SchemesGetAllResponse
     */
    public function setSchemesGetAllResult($SchemesGetAllResult){
      $this->SchemesGetAllResult = $SchemesGetAllResult;
      return $this;
    }

}
