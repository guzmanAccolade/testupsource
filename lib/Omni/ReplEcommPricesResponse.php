<?php

class Omni_ReplEcommPricesResponse {

    /**
     * @var Omni_ReplPriceResponse $ReplEcommPricesResult
     * @access public
     */
    public $ReplEcommPricesResult = null;

    /**
     * @param Omni_ReplPriceResponse $ReplEcommPricesResult
     * @access public
     */
    public function __construct($ReplEcommPricesResult = null){
      $this->ReplEcommPricesResult = $ReplEcommPricesResult;
    }

    /**
     * @return Omni_ReplPriceResponse
     */
    public function getReplEcommPricesResult(){
      return $this->ReplEcommPricesResult;
    }

    /**
     * @param Omni_ReplPriceResponse $ReplEcommPricesResult
     * @return Omni_ReplEcommPricesResponse
     */
    public function setReplEcommPricesResult($ReplEcommPricesResult){
      $this->ReplEcommPricesResult = $ReplEcommPricesResult;
      return $this;
    }

}
