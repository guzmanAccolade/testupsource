<?php

class Omni_ReplVendorResponse {

    /**
     * @var string $LastKey
     * @access public
     */
    public $LastKey = null;

    /**
     * @var string $MaxKey
     * @access public
     */
    public $MaxKey = null;

    /**
     * @var int $RecordsRemaining
     * @access public
     */
    public $RecordsRemaining = null;

    /**
     * @var Omni_Vendor[] $Vendors
     * @access public
     */
    public $Vendors = null;

    /**
     * @param int $RecordsRemaining
     * @access public
     */
    public function __construct($RecordsRemaining = null){
      $this->RecordsRemaining = $RecordsRemaining;
    }

    /**
     * @return string
     */
    public function getLastKey(){
      return $this->LastKey;
    }

    /**
     * @param string $LastKey
     * @return Omni_ReplVendorResponse
     */
    public function setLastKey($LastKey){
      $this->LastKey = $LastKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getMaxKey(){
      return $this->MaxKey;
    }

    /**
     * @param string $MaxKey
     * @return Omni_ReplVendorResponse
     */
    public function setMaxKey($MaxKey){
      $this->MaxKey = $MaxKey;
      return $this;
    }

    /**
     * @return int
     */
    public function getRecordsRemaining(){
      return $this->RecordsRemaining;
    }

    /**
     * @param int $RecordsRemaining
     * @return Omni_ReplVendorResponse
     */
    public function setRecordsRemaining($RecordsRemaining){
      $this->RecordsRemaining = $RecordsRemaining;
      return $this;
    }

    /**
     * @return Omni_Vendor[]
     */
    public function getVendors(){
      return $this->Vendors;
    }

    /**
     * @param Omni_Vendor[] $Vendors
     * @return Omni_ReplVendorResponse
     */
    public function setVendors($Vendors){
      $this->Vendors = $Vendors;
      return $this;
    }

}
