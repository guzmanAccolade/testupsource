<?php

class Omni_StoreServiceType {
    const __default = 'None';
    const None = 'None';
    const Garden = 'Garden';
    const FreeWiFi = 'FreeWiFi';
    const DriveThruWindow = 'DriveThruWindow';
    const GiftCard = 'GiftCard';
    const PlayPlace = 'PlayPlace';
    const FreeRefill = 'FreeRefill';


}
