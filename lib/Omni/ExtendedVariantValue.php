<?php

class Omni_ExtendedVariantValue {

    /**
     * @var string $Code
     * @access public
     */
    public $Code = null;

    /**
     * @var boolean $Del
     * @access public
     */
    public $Del = null;

    /**
     * @var string $Dimensions
     * @access public
     */
    public $Dimensions = null;

    /**
     * @var string $FrameworkCode
     * @access public
     */
    public $FrameworkCode = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var int $Order
     * @access public
     */
    public $Order = null;

    /**
     * @var string $Timestamp
     * @access public
     */
    public $Timestamp = null;

    /**
     * @var string $Value
     * @access public
     */
    public $Value = null;

    /**
     * @param boolean $Del
     * @param int $Order
     * @access public
     */
    public function __construct($Del = null, $Order = null){
      $this->Del = $Del;
      $this->Order = $Order;
    }

    /**
     * @return string
     */
    public function getCode(){
      return $this->Code;
    }

    /**
     * @param string $Code
     * @return Omni_ExtendedVariantValue
     */
    public function setCode($Code){
      $this->Code = $Code;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDel(){
      return $this->Del;
    }

    /**
     * @param boolean $Del
     * @return Omni_ExtendedVariantValue
     */
    public function setDel($Del){
      $this->Del = $Del;
      return $this;
    }

    /**
     * @return string
     */
    public function getDimensions(){
      return $this->Dimensions;
    }

    /**
     * @param string $Dimensions
     * @return Omni_ExtendedVariantValue
     */
    public function setDimensions($Dimensions){
      $this->Dimensions = $Dimensions;
      return $this;
    }

    /**
     * @return string
     */
    public function getFrameworkCode(){
      return $this->FrameworkCode;
    }

    /**
     * @param string $FrameworkCode
     * @return Omni_ExtendedVariantValue
     */
    public function setFrameworkCode($FrameworkCode){
      $this->FrameworkCode = $FrameworkCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_ExtendedVariantValue
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return int
     */
    public function getOrder(){
      return $this->Order;
    }

    /**
     * @param int $Order
     * @return Omni_ExtendedVariantValue
     */
    public function setOrder($Order){
      $this->Order = $Order;
      return $this;
    }

    /**
     * @return string
     */
    public function getTimestamp(){
      return $this->Timestamp;
    }

    /**
     * @param string $Timestamp
     * @return Omni_ExtendedVariantValue
     */
    public function setTimestamp($Timestamp){
      $this->Timestamp = $Timestamp;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue(){
      return $this->Value;
    }

    /**
     * @param string $Value
     * @return Omni_ExtendedVariantValue
     */
    public function setValue($Value){
      $this->Value = $Value;
      return $this;
    }

}
