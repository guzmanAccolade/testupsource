<?php

class Omni_ReplEcommCurrencyResponse {

    /**
     * @var Omni_ReplCurrencyResponse $ReplEcommCurrencyResult
     * @access public
     */
    public $ReplEcommCurrencyResult = null;

    /**
     * @param Omni_ReplCurrencyResponse $ReplEcommCurrencyResult
     * @access public
     */
    public function __construct($ReplEcommCurrencyResult = null){
      $this->ReplEcommCurrencyResult = $ReplEcommCurrencyResult;
    }

    /**
     * @return Omni_ReplCurrencyResponse
     */
    public function getReplEcommCurrencyResult(){
      return $this->ReplEcommCurrencyResult;
    }

    /**
     * @param Omni_ReplCurrencyResponse $ReplEcommCurrencyResult
     * @return Omni_ReplEcommCurrencyResponse
     */
    public function setReplEcommCurrencyResult($ReplEcommCurrencyResult){
      $this->ReplEcommCurrencyResult = $ReplEcommCurrencyResult;
      return $this;
    }

}
