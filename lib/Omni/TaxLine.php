<?php

class Omni_TaxLine {

    /**
     * @var string $TaxAmount
     * @access public
     */
    public $TaxAmount = null;

    /**
     * @var string $TaxCode
     * @access public
     */
    public $TaxCode = null;

    /**
     * @var string $TaxDesription
     * @access public
     */
    public $TaxDesription = null;

    /**
     * @access public
     */
    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getTaxAmount(){
      return $this->TaxAmount;
    }

    /**
     * @param string $TaxAmount
     * @return Omni_TaxLine
     */
    public function setTaxAmount($TaxAmount){
      $this->TaxAmount = $TaxAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getTaxCode(){
      return $this->TaxCode;
    }

    /**
     * @param string $TaxCode
     * @return Omni_TaxLine
     */
    public function setTaxCode($TaxCode){
      $this->TaxCode = $TaxCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getTaxDesription(){
      return $this->TaxDesription;
    }

    /**
     * @param string $TaxDesription
     * @return Omni_TaxLine
     */
    public function setTaxDesription($TaxDesription){
      $this->TaxDesription = $TaxDesription;
      return $this;
    }

}
