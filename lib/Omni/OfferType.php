<?php

class Omni_OfferType {
    const __default = 'General';
    const General = 'General';
    const SpecialMember = 'SpecialMember';
    const PointOffer = 'PointOffer';
    const Club = 'Club';
    const Unknown = 'Unknown';


}
