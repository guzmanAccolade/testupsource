<?php

class Omni_ItemPriceCheck {

    /**
     * @var Omni_ItemPriceCheckRequest $itemPriceCheckRequest
     * @access public
     */
    public $itemPriceCheckRequest = null;

    /**
     * @param Omni_ItemPriceCheckRequest $itemPriceCheckRequest
     * @access public
     */
    public function __construct($itemPriceCheckRequest = null){
      $this->itemPriceCheckRequest = $itemPriceCheckRequest;
    }

    /**
     * @return Omni_ItemPriceCheckRequest
     */
    public function getItemPriceCheckRequest(){
      return $this->itemPriceCheckRequest;
    }

    /**
     * @param Omni_ItemPriceCheckRequest $itemPriceCheckRequest
     * @return Omni_ItemPriceCheck
     */
    public function setItemPriceCheckRequest($itemPriceCheckRequest){
      $this->itemPriceCheckRequest = $itemPriceCheckRequest;
      return $this;
    }

}
