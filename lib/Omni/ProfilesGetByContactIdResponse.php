<?php

class Omni_ProfilesGetByContactIdResponse {

    /**
     * @var Omni_Profile[] $ProfilesGetByContactIdResult
     * @access public
     */
    public $ProfilesGetByContactIdResult = null;

    /**
     * @param Omni_Profile[] $ProfilesGetByContactIdResult
     * @access public
     */
    public function __construct($ProfilesGetByContactIdResult = null){
      $this->ProfilesGetByContactIdResult = $ProfilesGetByContactIdResult;
    }

    /**
     * @return Omni_Profile[]
     */
    public function getProfilesGetByContactIdResult(){
      return $this->ProfilesGetByContactIdResult;
    }

    /**
     * @param Omni_Profile[] $ProfilesGetByContactIdResult
     * @return Omni_ProfilesGetByContactIdResponse
     */
    public function setProfilesGetByContactIdResult($ProfilesGetByContactIdResult){
      $this->ProfilesGetByContactIdResult = $ProfilesGetByContactIdResult;
      return $this;
    }

}
