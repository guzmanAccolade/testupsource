<?php

class Omni_NotificationsGetByContactIdResponse {

    /**
     * @var Omni_Notification[] $NotificationsGetByContactIdResult
     * @access public
     */
    public $NotificationsGetByContactIdResult = null;

    /**
     * @param Omni_Notification[] $NotificationsGetByContactIdResult
     * @access public
     */
    public function __construct($NotificationsGetByContactIdResult = null){
      $this->NotificationsGetByContactIdResult = $NotificationsGetByContactIdResult;
    }

    /**
     * @return Omni_Notification[]
     */
    public function getNotificationsGetByContactIdResult(){
      return $this->NotificationsGetByContactIdResult;
    }

    /**
     * @param Omni_Notification[] $NotificationsGetByContactIdResult
     * @return Omni_NotificationsGetByContactIdResponse
     */
    public function setNotificationsGetByContactIdResult($NotificationsGetByContactIdResult){
      $this->NotificationsGetByContactIdResult = $NotificationsGetByContactIdResult;
      return $this;
    }

}
