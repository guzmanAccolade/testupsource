<?php

class Omni_BasketPostSaleLineRequest {

    /**
     * @var string $CouponCode
     * @access public
     */
    public $CouponCode = null;

    /**
     * @var string $DiscountAmount
     * @access public
     */
    public $DiscountAmount = null;

    /**
     * @var string $DiscountPercent
     * @access public
     */
    public $DiscountPercent = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var int $LineNumber
     * @access public
     */
    public $LineNumber = null;

    /**
     * @var string $NetAmount
     * @access public
     */
    public $NetAmount = null;

    /**
     * @var string $Price
     * @access public
     */
    public $Price = null;

    /**
     * @var float $Quantity
     * @access public
     */
    public $Quantity = null;

    /**
     * @var string $TaxAmount
     * @access public
     */
    public $TaxAmount = null;

    /**
     * @var string $TaxProductCode
     * @access public
     */
    public $TaxProductCode = null;

    /**
     * @var string $UomId
     * @access public
     */
    public $UomId = null;

    /**
     * @var string $VariantId
     * @access public
     */
    public $VariantId = null;

    /**
     * @param int $LineNumber
     * @param float $Quantity
     * @access public
     */
    public function __construct($LineNumber = null, $Quantity = null){
      $this->LineNumber = $LineNumber;
      $this->Quantity = $Quantity;
    }

    /**
     * @return string
     */
    public function getCouponCode(){
      return $this->CouponCode;
    }

    /**
     * @param string $CouponCode
     * @return Omni_BasketPostSaleLineRequest
     */
    public function setCouponCode($CouponCode){
      $this->CouponCode = $CouponCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getDiscountAmount(){
      return $this->DiscountAmount;
    }

    /**
     * @param string $DiscountAmount
     * @return Omni_BasketPostSaleLineRequest
     */
    public function setDiscountAmount($DiscountAmount){
      $this->DiscountAmount = $DiscountAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getDiscountPercent(){
      return $this->DiscountPercent;
    }

    /**
     * @param string $DiscountPercent
     * @return Omni_BasketPostSaleLineRequest
     */
    public function setDiscountPercent($DiscountPercent){
      $this->DiscountPercent = $DiscountPercent;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_BasketPostSaleLineRequest
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_BasketPostSaleLineRequest
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return int
     */
    public function getLineNumber(){
      return $this->LineNumber;
    }

    /**
     * @param int $LineNumber
     * @return Omni_BasketPostSaleLineRequest
     */
    public function setLineNumber($LineNumber){
      $this->LineNumber = $LineNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getNetAmount(){
      return $this->NetAmount;
    }

    /**
     * @param string $NetAmount
     * @return Omni_BasketPostSaleLineRequest
     */
    public function setNetAmount($NetAmount){
      $this->NetAmount = $NetAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getPrice(){
      return $this->Price;
    }

    /**
     * @param string $Price
     * @return Omni_BasketPostSaleLineRequest
     */
    public function setPrice($Price){
      $this->Price = $Price;
      return $this;
    }

    /**
     * @return float
     */
    public function getQuantity(){
      return $this->Quantity;
    }

    /**
     * @param float $Quantity
     * @return Omni_BasketPostSaleLineRequest
     */
    public function setQuantity($Quantity){
      $this->Quantity = $Quantity;
      return $this;
    }

    /**
     * @return string
     */
    public function getTaxAmount(){
      return $this->TaxAmount;
    }

    /**
     * @param string $TaxAmount
     * @return Omni_BasketPostSaleLineRequest
     */
    public function setTaxAmount($TaxAmount){
      $this->TaxAmount = $TaxAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getTaxProductCode(){
      return $this->TaxProductCode;
    }

    /**
     * @param string $TaxProductCode
     * @return Omni_BasketPostSaleLineRequest
     */
    public function setTaxProductCode($TaxProductCode){
      $this->TaxProductCode = $TaxProductCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getUomId(){
      return $this->UomId;
    }

    /**
     * @param string $UomId
     * @return Omni_BasketPostSaleLineRequest
     */
    public function setUomId($UomId){
      $this->UomId = $UomId;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantId(){
      return $this->VariantId;
    }

    /**
     * @param string $VariantId
     * @return Omni_BasketPostSaleLineRequest
     */
    public function setVariantId($VariantId){
      $this->VariantId = $VariantId;
      return $this;
    }

}
