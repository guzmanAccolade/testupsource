<?php

class Omni_ContactGetPointBalanceResponse {

    /**
     * @var int $ContactGetPointBalanceResult
     * @access public
     */
    public $ContactGetPointBalanceResult = null;

    /**
     * @param int $ContactGetPointBalanceResult
     * @access public
     */
    public function __construct($ContactGetPointBalanceResult = null){
      $this->ContactGetPointBalanceResult = $ContactGetPointBalanceResult;
    }

    /**
     * @return int
     */
    public function getContactGetPointBalanceResult(){
      return $this->ContactGetPointBalanceResult;
    }

    /**
     * @param int $ContactGetPointBalanceResult
     * @return Omni_ContactGetPointBalanceResponse
     */
    public function setContactGetPointBalanceResult($ContactGetPointBalanceResult){
      $this->ContactGetPointBalanceResult = $ContactGetPointBalanceResult;
      return $this;
    }

}
