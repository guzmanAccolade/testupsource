<?php

class Omni_EFTVerificationMethod {
    const __default = 'None';
    const None = 'None';
    const Signature = 'Signature';
    const OnlinePIN = 'OnlinePIN';
    const OfflinePIN = 'OfflinePIN';
    const OnlinePINAndSignature = 'OnlinePINAndSignature';
    const OfflinePINAndSignature = 'OfflinePINAndSignature';
    const Unknown = 'Unknown';


}
