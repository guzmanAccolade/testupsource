<?php

class Omni_Address {

    /**
     * @var string $Address1
     * @access public
     */
    public $Address1 = null;

    /**
     * @var string $Address2
     * @access public
     */
    public $Address2 = null;

    /**
     * @var string $City
     * @access public
     */
    public $City = null;

    /**
     * @var string $Country
     * @access public
     */
    public $Country = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $PostCode
     * @access public
     */
    public $PostCode = null;

    /**
     * @var string $StateProvinceRegion
     * @access public
     */
    public $StateProvinceRegion = null;

    /**
     * @var Omni_AddressType $Type
     * @access public
     */
    public $Type = null;

    /**
     * @param Omni_AddressType $Type
     * @access public
     */
    public function __construct($Type = null){
      $this->Type = $Type;
    }

    /**
     * @return string
     */
    public function getAddress1(){
      return $this->Address1;
    }

    /**
     * @param string $Address1
     * @return Omni_Address
     */
    public function setAddress1($Address1){
      $this->Address1 = $Address1;
      return $this;
    }

    /**
     * @return string
     */
    public function getAddress2(){
      return $this->Address2;
    }

    /**
     * @param string $Address2
     * @return Omni_Address
     */
    public function setAddress2($Address2){
      $this->Address2 = $Address2;
      return $this;
    }

    /**
     * @return string
     */
    public function getCity(){
      return $this->City;
    }

    /**
     * @param string $City
     * @return Omni_Address
     */
    public function setCity($City){
      $this->City = $City;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountry(){
      return $this->Country;
    }

    /**
     * @param string $Country
     * @return Omni_Address
     */
    public function setCountry($Country){
      $this->Country = $Country;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Address
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getPostCode(){
      return $this->PostCode;
    }

    /**
     * @param string $PostCode
     * @return Omni_Address
     */
    public function setPostCode($PostCode){
      $this->PostCode = $PostCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStateProvinceRegion(){
      return $this->StateProvinceRegion;
    }

    /**
     * @param string $StateProvinceRegion
     * @return Omni_Address
     */
    public function setStateProvinceRegion($StateProvinceRegion){
      $this->StateProvinceRegion = $StateProvinceRegion;
      return $this;
    }

    /**
     * @return Omni_AddressType
     */
    public function getType(){
      return $this->Type;
    }

    /**
     * @param Omni_AddressType $Type
     * @return Omni_Address
     */
    public function setType($Type){
      $this->Type = $Type;
      return $this;
    }

}
