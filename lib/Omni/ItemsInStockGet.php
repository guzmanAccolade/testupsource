<?php

class Omni_ItemsInStockGet {

    /**
     * @var string $storeId
     * @access public
     */
    public $storeId = null;

    /**
     * @var string $terminalId
     * @access public
     */
    public $terminalId = null;

    /**
     * @var string $itemId
     * @access public
     */
    public $itemId = null;

    /**
     * @var string $variantId
     * @access public
     */
    public $variantId = null;

    /**
     * @var int $arrivingInStockInDays
     * @access public
     */
    public $arrivingInStockInDays = null;

    /**
     * @param string $storeId
     * @param string $terminalId
     * @param string $itemId
     * @param string $variantId
     * @param int $arrivingInStockInDays
     * @access public
     */
    public function __construct($storeId = null, $terminalId = null, $itemId = null, $variantId = null, $arrivingInStockInDays = null){
      $this->storeId = $storeId;
      $this->terminalId = $terminalId;
      $this->itemId = $itemId;
      $this->variantId = $variantId;
      $this->arrivingInStockInDays = $arrivingInStockInDays;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->storeId;
    }

    /**
     * @param string $storeId
     * @return Omni_ItemsInStockGet
     */
    public function setStoreId($storeId){
      $this->storeId = $storeId;
      return $this;
    }

    /**
     * @return string
     */
    public function getTerminalId(){
      return $this->terminalId;
    }

    /**
     * @param string $terminalId
     * @return Omni_ItemsInStockGet
     */
    public function setTerminalId($terminalId){
      $this->terminalId = $terminalId;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->itemId;
    }

    /**
     * @param string $itemId
     * @return Omni_ItemsInStockGet
     */
    public function setItemId($itemId){
      $this->itemId = $itemId;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantId(){
      return $this->variantId;
    }

    /**
     * @param string $variantId
     * @return Omni_ItemsInStockGet
     */
    public function setVariantId($variantId){
      $this->variantId = $variantId;
      return $this;
    }

    /**
     * @return int
     */
    public function getArrivingInStockInDays(){
      return $this->arrivingInStockInDays;
    }

    /**
     * @param int $arrivingInStockInDays
     * @return Omni_ItemsInStockGet
     */
    public function setArrivingInStockInDays($arrivingInStockInDays){
      $this->arrivingInStockInDays = $arrivingInStockInDays;
      return $this;
    }

}
