<?php

class Omni_ImageLink {

    /**
     * @var boolean $Del
     * @access public
     */
    public $Del = null;

    /**
     * @var int $DisplayOrder
     * @access public
     */
    public $DisplayOrder = null;

    /**
     * @var string $ImageId
     * @access public
     */
    public $ImageId = null;

    /**
     * @var string $KeyValue
     * @access public
     */
    public $KeyValue = null;

    /**
     * @var string $TableName
     * @access public
     */
    public $TableName = null;

    /**
     * @param boolean $Del
     * @param int $DisplayOrder
     * @access public
     */
    public function __construct($Del = null, $DisplayOrder = null){
      $this->Del = $Del;
      $this->DisplayOrder = $DisplayOrder;
    }

    /**
     * @return boolean
     */
    public function getDel(){
      return $this->Del;
    }

    /**
     * @param boolean $Del
     * @return Omni_ImageLink
     */
    public function setDel($Del){
      $this->Del = $Del;
      return $this;
    }

    /**
     * @return int
     */
    public function getDisplayOrder(){
      return $this->DisplayOrder;
    }

    /**
     * @param int $DisplayOrder
     * @return Omni_ImageLink
     */
    public function setDisplayOrder($DisplayOrder){
      $this->DisplayOrder = $DisplayOrder;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageId(){
      return $this->ImageId;
    }

    /**
     * @param string $ImageId
     * @return Omni_ImageLink
     */
    public function setImageId($ImageId){
      $this->ImageId = $ImageId;
      return $this;
    }

    /**
     * @return string
     */
    public function getKeyValue(){
      return $this->KeyValue;
    }

    /**
     * @param string $KeyValue
     * @return Omni_ImageLink
     */
    public function setKeyValue($KeyValue){
      $this->KeyValue = $KeyValue;
      return $this;
    }

    /**
     * @return string
     */
    public function getTableName(){
      return $this->TableName;
    }

    /**
     * @param string $TableName
     * @return Omni_ImageLink
     */
    public function setTableName($TableName){
      $this->TableName = $TableName;
      return $this;
    }

}
