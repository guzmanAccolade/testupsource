<?php

class Omni_Club {

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @access public
     */
    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Club
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getName(){
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return Omni_Club
     */
    public function setName($Name){
      $this->Name = $Name;
      return $this;
    }

}
