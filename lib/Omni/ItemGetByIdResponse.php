<?php

class Omni_ItemGetByIdResponse {

    /**
     * @var Omni_Item $ItemGetByIdResult
     * @access public
     */
    public $ItemGetByIdResult = null;

    /**
     * @param Omni_Item $ItemGetByIdResult
     * @access public
     */
    public function __construct($ItemGetByIdResult = null){
      $this->ItemGetByIdResult = $ItemGetByIdResult;
    }

    /**
     * @return Omni_Item
     */
    public function getItemGetByIdResult(){
      return $this->ItemGetByIdResult;
    }

    /**
     * @param Omni_Item $ItemGetByIdResult
     * @return Omni_ItemGetByIdResponse
     */
    public function setItemGetByIdResult($ItemGetByIdResult){
      $this->ItemGetByIdResult = $ItemGetByIdResult;
      return $this;
    }

}
