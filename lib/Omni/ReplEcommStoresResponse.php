<?php

class Omni_ReplEcommStoresResponse {

    /**
     * @var Omni_ReplStoreResponse $ReplEcommStoresResult
     * @access public
     */
    public $ReplEcommStoresResult = null;

    /**
     * @param Omni_ReplStoreResponse $ReplEcommStoresResult
     * @access public
     */
    public function __construct($ReplEcommStoresResult = null){
      $this->ReplEcommStoresResult = $ReplEcommStoresResult;
    }

    /**
     * @return Omni_ReplStoreResponse
     */
    public function getReplEcommStoresResult(){
      return $this->ReplEcommStoresResult;
    }

    /**
     * @param Omni_ReplStoreResponse $ReplEcommStoresResult
     * @return Omni_ReplEcommStoresResponse
     */
    public function setReplEcommStoresResult($ReplEcommStoresResult){
      $this->ReplEcommStoresResult = $ReplEcommStoresResult;
      return $this;
    }

}
