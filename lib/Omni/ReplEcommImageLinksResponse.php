<?php

class Omni_ReplEcommImageLinksResponse {

    /**
     * @var Omni_ReplImageLinkResponse $ReplEcommImageLinksResult
     * @access public
     */
    public $ReplEcommImageLinksResult = null;

    /**
     * @param Omni_ReplImageLinkResponse $ReplEcommImageLinksResult
     * @access public
     */
    public function __construct($ReplEcommImageLinksResult = null){
      $this->ReplEcommImageLinksResult = $ReplEcommImageLinksResult;
    }

    /**
     * @return Omni_ReplImageLinkResponse
     */
    public function getReplEcommImageLinksResult(){
      return $this->ReplEcommImageLinksResult;
    }

    /**
     * @param Omni_ReplImageLinkResponse $ReplEcommImageLinksResult
     * @return Omni_ReplEcommImageLinksResponse
     */
    public function setReplEcommImageLinksResult($ReplEcommImageLinksResult){
      $this->ReplEcommImageLinksResult = $ReplEcommImageLinksResult;
      return $this;
    }

}
