<?php

class Omni_Item {

    /**
     * @var string $BaseUOM
     * @access public
     */
    public $BaseUOM = null;

    /**
     * @var boolean $Del
     * @access public
     */
    public $Del = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $FullDescription
     * @access public
     */
    public $FullDescription = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var float $Price
     * @access public
     */
    public $Price = null;

    /**
     * @var string $ProductGroupCode
     * @access public
     */
    public $ProductGroupCode = null;

    /**
     * @var string $PurchUOM
     * @access public
     */
    public $PurchUOM = null;

    /**
     * @var string $SalesUOM
     * @access public
     */
    public $SalesUOM = null;

    /**
     * @var int $ScaleItem
     * @access public
     */
    public $ScaleItem = null;

    /**
     * @var string $VendorId
     * @access public
     */
    public $VendorId = null;

    /**
     * @var string $VendorItemId
     * @access public
     */
    public $VendorItemId = null;

    /**
     * @param string $BaseUOM
     * @param boolean $Del
     * @param string $FullDescription
     * @param string $ProductGroupCode
     * @param string $PurchUOM
     * @param string $SalesUOM
     * @param int $ScaleItem
     * @param string $VendorId
     * @param string $VendorItemId
     * @access public
     */
    public function __construct($BaseUOM = null, $Del = null, $FullDescription = null, $ProductGroupCode = null, $PurchUOM = null, $SalesUOM = null, $ScaleItem = null, $VendorId = null, $VendorItemId = null){
      $this->BaseUOM = $BaseUOM;
      $this->Del = $Del;
      $this->FullDescription = $FullDescription;
      $this->ProductGroupCode = $ProductGroupCode;
      $this->PurchUOM = $PurchUOM;
      $this->SalesUOM = $SalesUOM;
      $this->ScaleItem = $ScaleItem;
      $this->VendorId = $VendorId;
      $this->VendorItemId = $VendorItemId;
    }

    /**
     * @return string
     */
    public function getBaseUOM(){
      return $this->BaseUOM;
    }

    /**
     * @param string $BaseUOM
     * @return Omni_Item
     */
    public function setBaseUOM($BaseUOM){
      $this->BaseUOM = $BaseUOM;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDel(){
      return $this->Del;
    }

    /**
     * @param boolean $Del
     * @return Omni_Item
     */
    public function setDel($Del){
      $this->Del = $Del;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_Item
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getFullDescription(){
      return $this->FullDescription;
    }

    /**
     * @param string $FullDescription
     * @return Omni_Item
     */
    public function setFullDescription($FullDescription){
      $this->FullDescription = $FullDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Item
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return float
     */
    public function getPrice(){
      return $this->Price;
    }

    /**
     * @param float $Price
     * @return Omni_Item
     */
    public function setPrice($Price){
      $this->Price = $Price;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductGroupCode(){
      return $this->ProductGroupCode;
    }

    /**
     * @param string $ProductGroupCode
     * @return Omni_Item
     */
    public function setProductGroupCode($ProductGroupCode){
      $this->ProductGroupCode = $ProductGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPurchUOM(){
      return $this->PurchUOM;
    }

    /**
     * @param string $PurchUOM
     * @return Omni_Item
     */
    public function setPurchUOM($PurchUOM){
      $this->PurchUOM = $PurchUOM;
      return $this;
    }

    /**
     * @return string
     */
    public function getSalesUOM(){
      return $this->SalesUOM;
    }

    /**
     * @param string $SalesUOM
     * @return Omni_Item
     */
    public function setSalesUOM($SalesUOM){
      $this->SalesUOM = $SalesUOM;
      return $this;
    }

    /**
     * @return int
     */
    public function getScaleItem(){
      return $this->ScaleItem;
    }

    /**
     * @param int $ScaleItem
     * @return Omni_Item
     */
    public function setScaleItem($ScaleItem){
      $this->ScaleItem = $ScaleItem;
      return $this;
    }

    /**
     * @return string
     */
    public function getVendorId(){
      return $this->VendorId;
    }

    /**
     * @param string $VendorId
     * @return Omni_Item
     */
    public function setVendorId($VendorId){
      $this->VendorId = $VendorId;
      return $this;
    }

    /**
     * @return string
     */
    public function getVendorItemId(){
      return $this->VendorItemId;
    }

    /**
     * @param string $VendorItemId
     * @return Omni_Item
     */
    public function setVendorItemId($VendorItemId){
      $this->VendorItemId = $VendorItemId;
      return $this;
    }

}
