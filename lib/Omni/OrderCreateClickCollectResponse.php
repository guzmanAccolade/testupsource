<?php

class Omni_OrderCreateClickCollectResponse {

    /**
     * @var boolean $OrderCreateClickCollectResult
     * @access public
     */
    public $OrderCreateClickCollectResult = null;

    /**
     * @param boolean $OrderCreateClickCollectResult
     * @access public
     */
    public function __construct($OrderCreateClickCollectResult = null){
      $this->OrderCreateClickCollectResult = $OrderCreateClickCollectResult;
    }

    /**
     * @return boolean
     */
    public function getOrderCreateClickCollectResult(){
      return $this->OrderCreateClickCollectResult;
    }

    /**
     * @param boolean $OrderCreateClickCollectResult
     * @return Omni_OrderCreateClickCollectResponse
     */
    public function setOrderCreateClickCollectResult($OrderCreateClickCollectResult){
      $this->OrderCreateClickCollectResult = $OrderCreateClickCollectResult;
      return $this;
    }

}
