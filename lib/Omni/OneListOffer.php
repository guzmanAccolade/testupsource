<?php

class Omni_OneListOffer {

    /**
     * @var dateTime $CreateDate
     * @access public
     */
    public $CreateDate = null;

    /**
     * @var int $DisplayOrderId
     * @access public
     */
    public $DisplayOrderId = null;

    /**
     * @var Omni_Offer $Offer
     * @access public
     */
    public $Offer = null;

    /**
     * @param dateTime $CreateDate
     * @param int $DisplayOrderId
     * @access public
     */
    public function __construct($CreateDate = null, $DisplayOrderId = null){
      $this->CreateDate = $CreateDate;
      $this->DisplayOrderId = $DisplayOrderId;
    }

    /**
     * @return dateTime
     */
    public function getCreateDate(){
      return $this->CreateDate;
    }

    /**
     * @param dateTime $CreateDate
     * @return Omni_OneListOffer
     */
    public function setCreateDate($CreateDate){
      $this->CreateDate = $CreateDate;
      return $this;
    }

    /**
     * @return int
     */
    public function getDisplayOrderId(){
      return $this->DisplayOrderId;
    }

    /**
     * @param int $DisplayOrderId
     * @return Omni_OneListOffer
     */
    public function setDisplayOrderId($DisplayOrderId){
      $this->DisplayOrderId = $DisplayOrderId;
      return $this;
    }

    /**
     * @return Omni_Offer
     */
    public function getOffer(){
      return $this->Offer;
    }

    /**
     * @param Omni_Offer $Offer
     * @return Omni_OneListOffer
     */
    public function setOffer($Offer){
      $this->Offer = $Offer;
      return $this;
    }

}
