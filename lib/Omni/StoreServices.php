<?php

class Omni_StoreServices {

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $StoreId
     * @access public
     */
    public $StoreId = null;

    /**
     * @var Omni_StoreServiceType $StoreServiceType
     * @access public
     */
    public $StoreServiceType = null;

    /**
     * @param Omni_StoreServiceType $StoreServiceType
     * @access public
     */
    public function __construct($StoreServiceType = null){
      $this->StoreServiceType = $StoreServiceType;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_StoreServices
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->StoreId;
    }

    /**
     * @param string $StoreId
     * @return Omni_StoreServices
     */
    public function setStoreId($StoreId){
      $this->StoreId = $StoreId;
      return $this;
    }

    /**
     * @return Omni_StoreServiceType
     */
    public function getStoreServiceType(){
      return $this->StoreServiceType;
    }

    /**
     * @param Omni_StoreServiceType $StoreServiceType
     * @return Omni_StoreServices
     */
    public function setStoreServiceType($StoreServiceType){
      $this->StoreServiceType = $StoreServiceType;
      return $this;
    }

}
