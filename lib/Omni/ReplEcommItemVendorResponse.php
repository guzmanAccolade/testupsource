<?php

class Omni_ReplEcommItemVendorResponse {

    /**
     * @var Omni_ReplItemVendorResponse $ReplEcommItemVendorResult
     * @access public
     */
    public $ReplEcommItemVendorResult = null;

    /**
     * @param Omni_ReplItemVendorResponse $ReplEcommItemVendorResult
     * @access public
     */
    public function __construct($ReplEcommItemVendorResult = null){
      $this->ReplEcommItemVendorResult = $ReplEcommItemVendorResult;
    }

    /**
     * @return Omni_ReplItemVendorResponse
     */
    public function getReplEcommItemVendorResult(){
      return $this->ReplEcommItemVendorResult;
    }

    /**
     * @param Omni_ReplItemVendorResponse $ReplEcommItemVendorResult
     * @return Omni_ReplEcommItemVendorResponse
     */
    public function setReplEcommItemVendorResult($ReplEcommItemVendorResult){
      $this->ReplEcommItemVendorResult = $ReplEcommItemVendorResult;
      return $this;
    }

}
