<?php

class Omni_Device {

    /**
     * @var string $DeviceFriendlyName
     * @access public
     */
    public $DeviceFriendlyName = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $Manufacturer
     * @access public
     */
    public $Manufacturer = null;

    /**
     * @var string $Model
     * @access public
     */
    public $Model = null;

    /**
     * @var string $OsVersion
     * @access public
     */
    public $OsVersion = null;

    /**
     * @var string $Platform
     * @access public
     */
    public $Platform = null;

    /**
     * @var string $SecurityToken
     * @access public
     */
    public $SecurityToken = null;

    /**
     * @access public
     */
    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getDeviceFriendlyName(){
      return $this->DeviceFriendlyName;
    }

    /**
     * @param string $DeviceFriendlyName
     * @return Omni_Device
     */
    public function setDeviceFriendlyName($DeviceFriendlyName){
      $this->DeviceFriendlyName = $DeviceFriendlyName;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Device
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getManufacturer(){
      return $this->Manufacturer;
    }

    /**
     * @param string $Manufacturer
     * @return Omni_Device
     */
    public function setManufacturer($Manufacturer){
      $this->Manufacturer = $Manufacturer;
      return $this;
    }

    /**
     * @return string
     */
    public function getModel(){
      return $this->Model;
    }

    /**
     * @param string $Model
     * @return Omni_Device
     */
    public function setModel($Model){
      $this->Model = $Model;
      return $this;
    }

    /**
     * @return string
     */
    public function getOsVersion(){
      return $this->OsVersion;
    }

    /**
     * @param string $OsVersion
     * @return Omni_Device
     */
    public function setOsVersion($OsVersion){
      $this->OsVersion = $OsVersion;
      return $this;
    }

    /**
     * @return string
     */
    public function getPlatform(){
      return $this->Platform;
    }

    /**
     * @param string $Platform
     * @return Omni_Device
     */
    public function setPlatform($Platform){
      $this->Platform = $Platform;
      return $this;
    }

    /**
     * @return string
     */
    public function getSecurityToken(){
      return $this->SecurityToken;
    }

    /**
     * @param string $SecurityToken
     * @return Omni_Device
     */
    public function setSecurityToken($SecurityToken){
      $this->SecurityToken = $SecurityToken;
      return $this;
    }

}
