<?php

class Omni_OrderStatusCheckResponse {

    /**
     * @var Omni_OrderStatusResponse $OrderStatusCheckResult
     * @access public
     */
    public $OrderStatusCheckResult = null;

    /**
     * @param Omni_OrderStatusResponse $OrderStatusCheckResult
     * @access public
     */
    public function __construct($OrderStatusCheckResult = null){
      $this->OrderStatusCheckResult = $OrderStatusCheckResult;
    }

    /**
     * @return Omni_OrderStatusResponse
     */
    public function getOrderStatusCheckResult(){
      return $this->OrderStatusCheckResult;
    }

    /**
     * @param Omni_OrderStatusResponse $OrderStatusCheckResult
     * @return Omni_OrderStatusCheckResponse
     */
    public function setOrderStatusCheckResult($OrderStatusCheckResult){
      $this->OrderStatusCheckResult = $OrderStatusCheckResult;
      return $this;
    }

}
