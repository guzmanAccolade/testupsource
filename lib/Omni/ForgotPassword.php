<?php

class Omni_ForgotPassword {

    /**
     * @var string $userName
     * @access public
     */
    public $userName = null;

    /**
     * @param string $userName
     * @access public
     */
    public function __construct($userName = null){
      $this->userName = $userName;
    }

    /**
     * @return string
     */
    public function getUserName(){
      return $this->userName;
    }

    /**
     * @param string $userName
     * @return Omni_ForgotPassword
     */
    public function setUserName($userName){
      $this->userName = $userName;
      return $this;
    }

}
