<?php

class Omni_OrderStatusCheck {

    /**
     * @var string $transactionId
     * @access public
     */
    public $transactionId = null;

    /**
     * @param string $transactionId
     * @access public
     */
    public function __construct($transactionId = null){
      $this->transactionId = $transactionId;
    }

    /**
     * @return string
     */
    public function getTransactionId(){
      return $this->transactionId;
    }

    /**
     * @param string $transactionId
     * @return Omni_OrderStatusCheck
     */
    public function setTransactionId($transactionId){
      $this->transactionId = $transactionId;
      return $this;
    }

}
