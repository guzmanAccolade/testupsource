<?php

class Omni_LoginResponse {

    /**
     * @var Omni_Contact $LoginResult
     * @access public
     */
    public $LoginResult = null;

    /**
     * @param Omni_Contact $LoginResult
     * @access public
     */
    public function __construct($LoginResult = null){
      $this->LoginResult = $LoginResult;
    }

    /**
     * @return Omni_Contact
     */
    public function getLoginResult(){
      return $this->LoginResult;
    }

    /**
     * @param Omni_Contact $LoginResult
     * @return Omni_LoginResponse
     */
    public function setLoginResult($LoginResult){
      $this->LoginResult = $LoginResult;
      return $this;
    }

}
