<?php

class Omni_NotificationsUpdateStatus {

    /**
     * @var string $contactId
     * @access public
     */
    public $contactId = null;

    /**
     * @var string[] $notificationIds
     * @access public
     */
    public $notificationIds = null;

    /**
     * @var Omni_NotificationStatus $notificationStatus
     * @access public
     */
    public $notificationStatus = null;

    /**
     * @param string $contactId
     * @param string[] $notificationIds
     * @param Omni_NotificationStatus $notificationStatus
     * @access public
     */
    public function __construct($contactId = null, $notificationIds = null, $notificationStatus = null){
      $this->contactId = $contactId;
      $this->notificationIds = $notificationIds;
      $this->notificationStatus = $notificationStatus;
    }

    /**
     * @return string
     */
    public function getContactId(){
      return $this->contactId;
    }

    /**
     * @param string $contactId
     * @return Omni_NotificationsUpdateStatus
     */
    public function setContactId($contactId){
      $this->contactId = $contactId;
      return $this;
    }

    /**
     * @return string[]
     */
    public function getNotificationIds(){
      return $this->notificationIds;
    }

    /**
     * @param string[] $notificationIds
     * @return Omni_NotificationsUpdateStatus
     */
    public function setNotificationIds($notificationIds){
      $this->notificationIds = $notificationIds;
      return $this;
    }

    /**
     * @return Omni_NotificationStatus
     */
    public function getNotificationStatus(){
      return $this->notificationStatus;
    }

    /**
     * @param Omni_NotificationStatus $notificationStatus
     * @return Omni_NotificationsUpdateStatus
     */
    public function setNotificationStatus($notificationStatus){
      $this->notificationStatus = $notificationStatus;
      return $this;
    }

}
