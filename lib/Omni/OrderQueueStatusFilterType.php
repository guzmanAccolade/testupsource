<?php

class Omni_OrderQueueStatusFilterType {
    const __default = 'None';
    const None = 'None';
    const aNew = 'New';
    const InProcess = 'InProcess';
    const Failed = 'Failed';
    const Processed = 'Processed';


}
