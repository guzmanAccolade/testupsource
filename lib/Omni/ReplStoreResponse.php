<?php

class Omni_ReplStoreResponse {

    /**
     * @var string $LastKey
     * @access public
     */
    public $LastKey = null;

    /**
     * @var string $MaxKey
     * @access public
     */
    public $MaxKey = null;

    /**
     * @var int $RecordsRemaining
     * @access public
     */
    public $RecordsRemaining = null;

    /**
     * @var Omni_Store[] $Stores
     * @access public
     */
    public $Stores = null;

    /**
     * @param int $RecordsRemaining
     * @access public
     */
    public function __construct($RecordsRemaining = null){
      $this->RecordsRemaining = $RecordsRemaining;
    }

    /**
     * @return string
     */
    public function getLastKey(){
      return $this->LastKey;
    }

    /**
     * @param string $LastKey
     * @return Omni_ReplStoreResponse
     */
    public function setLastKey($LastKey){
      $this->LastKey = $LastKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getMaxKey(){
      return $this->MaxKey;
    }

    /**
     * @param string $MaxKey
     * @return Omni_ReplStoreResponse
     */
    public function setMaxKey($MaxKey){
      $this->MaxKey = $MaxKey;
      return $this;
    }

    /**
     * @return int
     */
    public function getRecordsRemaining(){
      return $this->RecordsRemaining;
    }

    /**
     * @param int $RecordsRemaining
     * @return Omni_ReplStoreResponse
     */
    public function setRecordsRemaining($RecordsRemaining){
      $this->RecordsRemaining = $RecordsRemaining;
      return $this;
    }

    /**
     * @return Omni_Store[]
     */
    public function getStores(){
      return $this->Stores;
    }

    /**
     * @param Omni_Store[] $Stores
     * @return Omni_ReplStoreResponse
     */
    public function setStores($Stores){
      $this->Stores = $Stores;
      return $this;
    }

}
