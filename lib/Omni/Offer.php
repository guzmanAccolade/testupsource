<?php

class Omni_Offer {

    /**
     * @var Omni_OfferDetails[] $Details
     * @access public
     */
    public $Details = null;

    /**
     * @var Omni_DiscountType $DiscountType
     * @access public
     */
    public $DiscountType = null;

    /**
     * @var dateTime $ExpiryDate
     * @access public
     */
    public $ExpiryDate = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $PrimaryText
     * @access public
     */
    public $PrimaryText = null;

    /**
     * @var string $SecondaryText
     * @access public
     */
    public $SecondaryText = null;

    /**
     * @var Omni_OfferType $Type
     * @access public
     */
    public $Type = null;

    /**
     * @param Omni_DiscountType $DiscountType
     * @param dateTime $ExpiryDate
     * @param string $PrimaryText
     * @param string $SecondaryText
     * @param Omni_OfferType $Type
     * @access public
     */
    public function __construct($DiscountType = null, $ExpiryDate = null, $PrimaryText = null, $SecondaryText = null, $Type = null){
      $this->DiscountType = $DiscountType;
      $this->ExpiryDate = $ExpiryDate;
      $this->PrimaryText = $PrimaryText;
      $this->SecondaryText = $SecondaryText;
      $this->Type = $Type;
    }

    /**
     * @return Omni_OfferDetails[]
     */
    public function getDetails(){
      return $this->Details;
    }

    /**
     * @param Omni_OfferDetails[] $Details
     * @return Omni_Offer
     */
    public function setDetails($Details){
      $this->Details = $Details;
      return $this;
    }

    /**
     * @return Omni_DiscountType
     */
    public function getDiscountType(){
      return $this->DiscountType;
    }

    /**
     * @param Omni_DiscountType $DiscountType
     * @return Omni_Offer
     */
    public function setDiscountType($DiscountType){
      $this->DiscountType = $DiscountType;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getExpiryDate(){
      return $this->ExpiryDate;
    }

    /**
     * @param dateTime $ExpiryDate
     * @return Omni_Offer
     */
    public function setExpiryDate($ExpiryDate){
      $this->ExpiryDate = $ExpiryDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Offer
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getPrimaryText(){
      return $this->PrimaryText;
    }

    /**
     * @param string $PrimaryText
     * @return Omni_Offer
     */
    public function setPrimaryText($PrimaryText){
      $this->PrimaryText = $PrimaryText;
      return $this;
    }

    /**
     * @return string
     */
    public function getSecondaryText(){
      return $this->SecondaryText;
    }

    /**
     * @param string $SecondaryText
     * @return Omni_Offer
     */
    public function setSecondaryText($SecondaryText){
      $this->SecondaryText = $SecondaryText;
      return $this;
    }

    /**
     * @return Omni_OfferType
     */
    public function getType(){
      return $this->Type;
    }

    /**
     * @param Omni_OfferType $Type
     * @return Omni_Offer
     */
    public function setType($Type){
      $this->Type = $Type;
      return $this;
    }

}
