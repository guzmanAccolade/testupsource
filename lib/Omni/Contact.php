<?php

class Omni_Contact {

    /**
     * @var Omni_Account $Account
     * @access public
     */
    public $Account = null;

    /**
     * @var Omni_Address[] $Addresses
     * @access public
     */
    public $Addresses = null;

    /**
     * @var string $AlternateId
     * @access public
     */
    public $AlternateId = null;

    /**
     * @var Omni_Card $Card
     * @access public
     */
    public $Card = null;

    /**
     * @var Omni_Coupon[] $Coupons
     * @access public
     */
    public $Coupons = null;

    /**
     * @var Omni_Device $Device
     * @access public
     */
    public $Device = null;

    /**
     * @var string $Email
     * @access public
     */
    public $Email = null;

    /**
     * @var Omni_Environment $Environment
     * @access public
     */
    public $Environment = null;

    /**
     * @var string $FirstName
     * @access public
     */
    public $FirstName = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $LastName
     * @access public
     */
    public $LastName = null;

    /**
     * @var string $MiddleName
     * @access public
     */
    public $MiddleName = null;

    /**
     * @var Omni_Notification[] $Notifications
     * @access public
     */
    public $Notifications = null;

    /**
     * @var Omni_Offer[] $Offers
     * @access public
     */
    public $Offers = null;

    /**
     * @var Omni_OneList[] $OneList
     * @access public
     */
    public $OneList = null;

    /**
     * @var string $Password
     * @access public
     */
    public $Password = null;

    /**
     * @var string $Phone
     * @access public
     */
    public $Phone = null;

    /**
     * @var Omni_Profile[] $Profiles
     * @access public
     */
    public $Profiles = null;

    /**
     * @var Omni_PublishedOffer[] $PublishedOffers
     * @access public
     */
    public $PublishedOffers = null;

    /**
     * @var int $RV
     * @access public
     */
    public $RV = null;

    /**
     * @var string $UserName
     * @access public
     */
    public $UserName = null;

    /**
     * @param int $RV
     * @access public
     */
    public function __construct($RV = null){
      $this->RV = $RV;
    }

    /**
     * @return Omni_Account
     */
    public function getAccount(){
      return $this->Account;
    }

    /**
     * @param Omni_Account $Account
     * @return Omni_Contact
     */
    public function setAccount($Account){
      $this->Account = $Account;
      return $this;
    }

    /**
     * @return Omni_Address[]
     */
    public function getAddresses(){
      return $this->Addresses;
    }

    /**
     * @param Omni_Address[] $Addresses
     * @return Omni_Contact
     */
    public function setAddresses($Addresses){
      $this->Addresses = $Addresses;
      return $this;
    }

    /**
     * @return string
     */
    public function getAlternateId(){
      return $this->AlternateId;
    }

    /**
     * @param string $AlternateId
     * @return Omni_Contact
     */
    public function setAlternateId($AlternateId){
      $this->AlternateId = $AlternateId;
      return $this;
    }

    /**
     * @return Omni_Card
     */
    public function getCard(){
      return $this->Card;
    }

    /**
     * @param Omni_Card $Card
     * @return Omni_Contact
     */
    public function setCard($Card){
      $this->Card = $Card;
      return $this;
    }

    /**
     * @return Omni_Coupon[]
     */
    public function getCoupons(){
      return $this->Coupons;
    }

    /**
     * @param Omni_Coupon[] $Coupons
     * @return Omni_Contact
     */
    public function setCoupons($Coupons){
      $this->Coupons = $Coupons;
      return $this;
    }

    /**
     * @return Omni_Device
     */
    public function getDevice(){
      return $this->Device;
    }

    /**
     * @param Omni_Device $Device
     * @return Omni_Contact
     */
    public function setDevice($Device){
      $this->Device = $Device;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmail(){
      return $this->Email;
    }

    /**
     * @param string $Email
     * @return Omni_Contact
     */
    public function setEmail($Email){
      $this->Email = $Email;
      return $this;
    }

    /**
     * @return Omni_Environment
     */
    public function getEnvironment(){
      return $this->Environment;
    }

    /**
     * @param Omni_Environment $Environment
     * @return Omni_Contact
     */
    public function setEnvironment($Environment){
      $this->Environment = $Environment;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(){
      return $this->FirstName;
    }

    /**
     * @param string $FirstName
     * @return Omni_Contact
     */
    public function setFirstName($FirstName){
      $this->FirstName = $FirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Contact
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastName(){
      return $this->LastName;
    }

    /**
     * @param string $LastName
     * @return Omni_Contact
     */
    public function setLastName($LastName){
      $this->LastName = $LastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName(){
      return $this->MiddleName;
    }

    /**
     * @param string $MiddleName
     * @return Omni_Contact
     */
    public function setMiddleName($MiddleName){
      $this->MiddleName = $MiddleName;
      return $this;
    }

    /**
     * @return Omni_Notification[]
     */
    public function getNotifications(){
      return $this->Notifications;
    }

    /**
     * @param Omni_Notification[] $Notifications
     * @return Omni_Contact
     */
    public function setNotifications($Notifications){
      $this->Notifications = $Notifications;
      return $this;
    }

    /**
     * @return Omni_Offer[]
     */
    public function getOffers(){
      return $this->Offers;
    }

    /**
     * @param Omni_Offer[] $Offers
     * @return Omni_Contact
     */
    public function setOffers($Offers){
      $this->Offers = $Offers;
      return $this;
    }

    /**
     * @return Omni_OneList[]
     */
    public function getOneList(){
      return $this->OneList;
    }

    /**
     * @param Omni_OneList[] $OneList
     * @return Omni_Contact
     */
    public function setOneList($OneList){
      $this->OneList = $OneList;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassword(){
      return $this->Password;
    }

    /**
     * @param string $Password
     * @return Omni_Contact
     */
    public function setPassword($Password){
      $this->Password = $Password;
      return $this;
    }

    /**
     * @return string
     */
    public function getPhone(){
      return $this->Phone;
    }

    /**
     * @param string $Phone
     * @return Omni_Contact
     */
    public function setPhone($Phone){
      $this->Phone = $Phone;
      return $this;
    }

    /**
     * @return Omni_Profile[]
     */
    public function getProfiles(){
      return $this->Profiles;
    }

    /**
     * @param Omni_Profile[] $Profiles
     * @return Omni_Contact
     */
    public function setProfiles($Profiles){
      $this->Profiles = $Profiles;
      return $this;
    }

    /**
     * @return Omni_PublishedOffer[]
     */
    public function getPublishedOffers(){
      return $this->PublishedOffers;
    }

    /**
     * @param Omni_PublishedOffer[] $PublishedOffers
     * @return Omni_Contact
     */
    public function setPublishedOffers($PublishedOffers){
      $this->PublishedOffers = $PublishedOffers;
      return $this;
    }

    /**
     * @return int
     */
    public function getRV(){
      return $this->RV;
    }

    /**
     * @param int $RV
     * @return Omni_Contact
     */
    public function setRV($RV){
      $this->RV = $RV;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserName(){
      return $this->UserName;
    }

    /**
     * @param string $UserName
     * @return Omni_Contact
     */
    public function setUserName($UserName){
      $this->UserName = $UserName;
      return $this;
    }

}
