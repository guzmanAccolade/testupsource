<?php

class Omni_TenderLine {

    /**
     * @var float $Amount
     * @access public
     */
    public $Amount = null;

    /**
     * @var string $CurrencyCode
     * @access public
     */
    public $CurrencyCode = null;

    /**
     * @var string $CurrencyFactor
     * @access public
     */
    public $CurrencyFactor = null;

    /**
     * @var string $EFTAuthCode
     * @access public
     */
    public $EFTAuthCode = null;

    /**
     * @var Omni_AuthorizationStatus $EFTAuthStatus
     * @access public
     */
    public $EFTAuthStatus = null;

    /**
     * @var string $EFTCardName
     * @access public
     */
    public $EFTCardName = null;

    /**
     * @var string $EFTCardNumber
     * @access public
     */
    public $EFTCardNumber = null;

    /**
     * @var dateTime $EFTDateTime
     * @access public
     */
    public $EFTDateTime = null;

    /**
     * @var string $EFTMessage
     * @access public
     */
    public $EFTMessage = null;

    /**
     * @var Omni_EFTTransactionType $EFTTransType
     * @access public
     */
    public $EFTTransType = null;

    /**
     * @var string $EFTTransactionId
     * @access public
     */
    public $EFTTransactionId = null;

    /**
     * @var Omni_EFTVerificationMethod $EFTVerificationMethod
     * @access public
     */
    public $EFTVerificationMethod = null;

    /**
     * @var Omni_EntryStatus $EntryStatus
     * @access public
     */
    public $EntryStatus = null;

    /**
     * @var string $ExternalId
     * @access public
     */
    public $ExternalId = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var Omni_ReceiptInfo[] $ReceiptInfo
     * @access public
     */
    public $ReceiptInfo = null;

    /**
     * @var string $StaffId
     * @access public
     */
    public $StaffId = null;

    /**
     * @var string $StoreId
     * @access public
     */
    public $StoreId = null;

    /**
     * @var string $TenderDescription
     * @access public
     */
    public $TenderDescription = null;

    /**
     * @var string $TenderTypeId
     * @access public
     */
    public $TenderTypeId = null;

    /**
     * @var string $TerminalId
     * @access public
     */
    public $TerminalId = null;

    /**
     * @var string $TransactionId
     * @access public
     */
    public $TransactionId = null;

    /**
     * @var string $_sAmount
     * @access public
     */
    public $_sAmount = null;

    /**
     * @param string $CurrencyCode
     * @param string $CurrencyFactor
     * @param string $EFTAuthCode
     * @param Omni_AuthorizationStatus $EFTAuthStatus
     * @param string $EFTCardName
     * @param string $EFTCardNumber
     * @param dateTime $EFTDateTime
     * @param string $EFTMessage
     * @param Omni_EFTTransactionType $EFTTransType
     * @param string $EFTTransactionId
     * @param Omni_EFTVerificationMethod $EFTVerificationMethod
     * @param Omni_EntryStatus $EntryStatus
     * @param string $ExternalId
     * @param Omni_ReceiptInfo[] $ReceiptInfo
     * @param string $StaffId
     * @param string $TenderDescription
     * @param string $TenderTypeId
     * @param string $_sAmount
     * @access public
     */
    public function __construct($CurrencyCode = null, $CurrencyFactor = null, $EFTAuthCode = null, $EFTAuthStatus = null, $EFTCardName = null, $EFTCardNumber = null, $EFTDateTime = null, $EFTMessage = null, $EFTTransType = null, $EFTTransactionId = null, $EFTVerificationMethod = null, $EntryStatus = null, $ExternalId = null, $ReceiptInfo = null, $StaffId = null, $TenderDescription = null, $TenderTypeId = null, $_sAmount = null){
      $this->CurrencyCode = $CurrencyCode;
      $this->CurrencyFactor = $CurrencyFactor;
      $this->EFTAuthCode = $EFTAuthCode;
      $this->EFTAuthStatus = $EFTAuthStatus;
      $this->EFTCardName = $EFTCardName;
      $this->EFTCardNumber = $EFTCardNumber;
      $this->EFTDateTime = $EFTDateTime;
      $this->EFTMessage = $EFTMessage;
      $this->EFTTransType = $EFTTransType;
      $this->EFTTransactionId = $EFTTransactionId;
      $this->EFTVerificationMethod = $EFTVerificationMethod;
      $this->EntryStatus = $EntryStatus;
      $this->ExternalId = $ExternalId;
      $this->ReceiptInfo = $ReceiptInfo;
      $this->StaffId = $StaffId;
      $this->TenderDescription = $TenderDescription;
      $this->TenderTypeId = $TenderTypeId;
      $this->_sAmount = $_sAmount;
    }

    /**
     * @return float
     */
    public function getAmount(){
      return $this->Amount;
    }

    /**
     * @param float $Amount
     * @return Omni_TenderLine
     */
    public function setAmount($Amount){
      $this->Amount = $Amount;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(){
      return $this->CurrencyCode;
    }

    /**
     * @param string $CurrencyCode
     * @return Omni_TenderLine
     */
    public function setCurrencyCode($CurrencyCode){
      $this->CurrencyCode = $CurrencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyFactor(){
      return $this->CurrencyFactor;
    }

    /**
     * @param string $CurrencyFactor
     * @return Omni_TenderLine
     */
    public function setCurrencyFactor($CurrencyFactor){
      $this->CurrencyFactor = $CurrencyFactor;
      return $this;
    }

    /**
     * @return string
     */
    public function getEFTAuthCode(){
      return $this->EFTAuthCode;
    }

    /**
     * @param string $EFTAuthCode
     * @return Omni_TenderLine
     */
    public function setEFTAuthCode($EFTAuthCode){
      $this->EFTAuthCode = $EFTAuthCode;
      return $this;
    }

    /**
     * @return Omni_AuthorizationStatus
     */
    public function getEFTAuthStatus(){
      return $this->EFTAuthStatus;
    }

    /**
     * @param Omni_AuthorizationStatus $EFTAuthStatus
     * @return Omni_TenderLine
     */
    public function setEFTAuthStatus($EFTAuthStatus){
      $this->EFTAuthStatus = $EFTAuthStatus;
      return $this;
    }

    /**
     * @return string
     */
    public function getEFTCardName(){
      return $this->EFTCardName;
    }

    /**
     * @param string $EFTCardName
     * @return Omni_TenderLine
     */
    public function setEFTCardName($EFTCardName){
      $this->EFTCardName = $EFTCardName;
      return $this;
    }

    /**
     * @return string
     */
    public function getEFTCardNumber(){
      return $this->EFTCardNumber;
    }

    /**
     * @param string $EFTCardNumber
     * @return Omni_TenderLine
     */
    public function setEFTCardNumber($EFTCardNumber){
      $this->EFTCardNumber = $EFTCardNumber;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getEFTDateTime(){
      return $this->EFTDateTime;
    }

    /**
     * @param dateTime $EFTDateTime
     * @return Omni_TenderLine
     */
    public function setEFTDateTime($EFTDateTime){
      $this->EFTDateTime = $EFTDateTime;
      return $this;
    }

    /**
     * @return string
     */
    public function getEFTMessage(){
      return $this->EFTMessage;
    }

    /**
     * @param string $EFTMessage
     * @return Omni_TenderLine
     */
    public function setEFTMessage($EFTMessage){
      $this->EFTMessage = $EFTMessage;
      return $this;
    }

    /**
     * @return Omni_EFTTransactionType
     */
    public function getEFTTransType(){
      return $this->EFTTransType;
    }

    /**
     * @param Omni_EFTTransactionType $EFTTransType
     * @return Omni_TenderLine
     */
    public function setEFTTransType($EFTTransType){
      $this->EFTTransType = $EFTTransType;
      return $this;
    }

    /**
     * @return string
     */
    public function getEFTTransactionId(){
      return $this->EFTTransactionId;
    }

    /**
     * @param string $EFTTransactionId
     * @return Omni_TenderLine
     */
    public function setEFTTransactionId($EFTTransactionId){
      $this->EFTTransactionId = $EFTTransactionId;
      return $this;
    }

    /**
     * @return Omni_EFTVerificationMethod
     */
    public function getEFTVerificationMethod(){
      return $this->EFTVerificationMethod;
    }

    /**
     * @param Omni_EFTVerificationMethod $EFTVerificationMethod
     * @return Omni_TenderLine
     */
    public function setEFTVerificationMethod($EFTVerificationMethod){
      $this->EFTVerificationMethod = $EFTVerificationMethod;
      return $this;
    }

    /**
     * @return Omni_EntryStatus
     */
    public function getEntryStatus(){
      return $this->EntryStatus;
    }

    /**
     * @param Omni_EntryStatus $EntryStatus
     * @return Omni_TenderLine
     */
    public function setEntryStatus($EntryStatus){
      $this->EntryStatus = $EntryStatus;
      return $this;
    }

    /**
     * @return string
     */
    public function getExternalId(){
      return $this->ExternalId;
    }

    /**
     * @param string $ExternalId
     * @return Omni_TenderLine
     */
    public function setExternalId($ExternalId){
      $this->ExternalId = $ExternalId;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_TenderLine
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return Omni_ReceiptInfo[]
     */
    public function getReceiptInfo(){
      return $this->ReceiptInfo;
    }

    /**
     * @param Omni_ReceiptInfo[] $ReceiptInfo
     * @return Omni_TenderLine
     */
    public function setReceiptInfo($ReceiptInfo){
      $this->ReceiptInfo = $ReceiptInfo;
      return $this;
    }

    /**
     * @return string
     */
    public function getStaffId(){
      return $this->StaffId;
    }

    /**
     * @param string $StaffId
     * @return Omni_TenderLine
     */
    public function setStaffId($StaffId){
      $this->StaffId = $StaffId;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->StoreId;
    }

    /**
     * @param string $StoreId
     * @return Omni_TenderLine
     */
    public function setStoreId($StoreId){
      $this->StoreId = $StoreId;
      return $this;
    }

    /**
     * @return string
     */
    public function getTenderDescription(){
      return $this->TenderDescription;
    }

    /**
     * @param string $TenderDescription
     * @return Omni_TenderLine
     */
    public function setTenderDescription($TenderDescription){
      $this->TenderDescription = $TenderDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getTenderTypeId(){
      return $this->TenderTypeId;
    }

    /**
     * @param string $TenderTypeId
     * @return Omni_TenderLine
     */
    public function setTenderTypeId($TenderTypeId){
      $this->TenderTypeId = $TenderTypeId;
      return $this;
    }

    /**
     * @return string
     */
    public function getTerminalId(){
      return $this->TerminalId;
    }

    /**
     * @param string $TerminalId
     * @return Omni_TenderLine
     */
    public function setTerminalId($TerminalId){
      $this->TerminalId = $TerminalId;
      return $this;
    }

    /**
     * @return string
     */
    public function getTransactionId(){
      return $this->TransactionId;
    }

    /**
     * @param string $TransactionId
     * @return Omni_TenderLine
     */
    public function setTransactionId($TransactionId){
      $this->TransactionId = $TransactionId;
      return $this;
    }

    /**
     * @return string
     */
    public function get_sAmount(){
      return $this->_sAmount;
    }

    /**
     * @param string $_sAmount
     * @return Omni_TenderLine
     */
    public function set_sAmount($_sAmount){
      $this->_sAmount = $_sAmount;
      return $this;
    }

}
