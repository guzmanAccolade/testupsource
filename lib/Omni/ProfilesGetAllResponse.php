<?php

class Omni_ProfilesGetAllResponse {

    /**
     * @var Omni_Profile[] $ProfilesGetAllResult
     * @access public
     */
    public $ProfilesGetAllResult = null;

    /**
     * @param Omni_Profile[] $ProfilesGetAllResult
     * @access public
     */
    public function __construct($ProfilesGetAllResult = null){
      $this->ProfilesGetAllResult = $ProfilesGetAllResult;
    }

    /**
     * @return Omni_Profile[]
     */
    public function getProfilesGetAllResult(){
      return $this->ProfilesGetAllResult;
    }

    /**
     * @param Omni_Profile[] $ProfilesGetAllResult
     * @return Omni_ProfilesGetAllResponse
     */
    public function setProfilesGetAllResult($ProfilesGetAllResult){
      $this->ProfilesGetAllResult = $ProfilesGetAllResult;
      return $this;
    }

}
