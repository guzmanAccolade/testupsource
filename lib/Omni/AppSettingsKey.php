<?php

class Omni_AppSettingsKey {
    const __default = 'ContactUs';
    const ContactUs = 'ContactUs';
    const SchemeLevel = 'SchemeLevel';
    const TermsOfService = 'TermsOfService';
    const forgotpassword_email_subject = 'forgotpassword_email_subject';
    const forgotpassword_email_body = 'forgotpassword_email_body';
    const forgotpassword_email_url = 'forgotpassword_email_url';
    const forgotpassword_device_email_subject = 'forgotpassword_device_email_subject';
    const forgotpassword_device_email_body = 'forgotpassword_device_email_body';
    const resetpin_email_subject = 'resetpin_email_subject';
    const resetpin_email_body = 'resetpin_email_body';
    const Password_Policy = 'Password_Policy';
    const URL_Displayed_On_Client = 'URL_Displayed_On_Client';


}
