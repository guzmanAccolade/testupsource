<?php

class Omni_ItemVendor {

    /**
     * @var boolean $Del
     * @access public
     */
    public $Del = null;

    /**
     * @var int $DisplayOrder
     * @access public
     */
    public $DisplayOrder = null;

    /**
     * @var boolean $IsFeaturedProduct
     * @access public
     */
    public $IsFeaturedProduct = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var string $VendorId
     * @access public
     */
    public $VendorId = null;

    /**
     * @param boolean $Del
     * @param int $DisplayOrder
     * @param boolean $IsFeaturedProduct
     * @access public
     */
    public function __construct($Del = null, $DisplayOrder = null, $IsFeaturedProduct = null){
      $this->Del = $Del;
      $this->DisplayOrder = $DisplayOrder;
      $this->IsFeaturedProduct = $IsFeaturedProduct;
    }

    /**
     * @return boolean
     */
    public function getDel(){
      return $this->Del;
    }

    /**
     * @param boolean $Del
     * @return Omni_ItemVendor
     */
    public function setDel($Del){
      $this->Del = $Del;
      return $this;
    }

    /**
     * @return int
     */
    public function getDisplayOrder(){
      return $this->DisplayOrder;
    }

    /**
     * @param int $DisplayOrder
     * @return Omni_ItemVendor
     */
    public function setDisplayOrder($DisplayOrder){
      $this->DisplayOrder = $DisplayOrder;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsFeaturedProduct(){
      return $this->IsFeaturedProduct;
    }

    /**
     * @param boolean $IsFeaturedProduct
     * @return Omni_ItemVendor
     */
    public function setIsFeaturedProduct($IsFeaturedProduct){
      $this->IsFeaturedProduct = $IsFeaturedProduct;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_ItemVendor
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return string
     */
    public function getVendorId(){
      return $this->VendorId;
    }

    /**
     * @param string $VendorId
     * @return Omni_ItemVendor
     */
    public function setVendorId($VendorId){
      $this->VendorId = $VendorId;
      return $this;
    }

}
