<?php

class Omni_ReplEcommItemVariantRegistrationsResponse {

    /**
     * @var Omni_ReplItemVariantRegistrationResponse $ReplEcommItemVariantRegistrationsResult
     * @access public
     */
    public $ReplEcommItemVariantRegistrationsResult = null;

    /**
     * @param Omni_ReplItemVariantRegistrationResponse $ReplEcommItemVariantRegistrationsResult
     * @access public
     */
    public function __construct($ReplEcommItemVariantRegistrationsResult = null){
      $this->ReplEcommItemVariantRegistrationsResult = $ReplEcommItemVariantRegistrationsResult;
    }

    /**
     * @return Omni_ReplItemVariantRegistrationResponse
     */
    public function getReplEcommItemVariantRegistrationsResult(){
      return $this->ReplEcommItemVariantRegistrationsResult;
    }

    /**
     * @param Omni_ReplItemVariantRegistrationResponse $ReplEcommItemVariantRegistrationsResult
     * @return Omni_ReplEcommItemVariantRegistrationsResponse
     */
    public function setReplEcommItemVariantRegistrationsResult($ReplEcommItemVariantRegistrationsResult){
      $this->ReplEcommItemVariantRegistrationsResult = $ReplEcommItemVariantRegistrationsResult;
      return $this;
    }

}
