<?php

class Omni_UnitOfMeasure {

    /**
     * @var boolean $Del
     * @access public
     */
    public $Del = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $ShortDescription
     * @access public
     */
    public $ShortDescription = null;

    /**
     * @var int $UnitDecimals
     * @access public
     */
    public $UnitDecimals = null;

    /**
     * @param boolean $Del
     * @param int $UnitDecimals
     * @access public
     */
    public function __construct($Del = null, $UnitDecimals = null){
      $this->Del = $Del;
      $this->UnitDecimals = $UnitDecimals;
    }

    /**
     * @return boolean
     */
    public function getDel(){
      return $this->Del;
    }

    /**
     * @param boolean $Del
     * @return Omni_UnitOfMeasure
     */
    public function setDel($Del){
      $this->Del = $Del;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_UnitOfMeasure
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_UnitOfMeasure
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getShortDescription(){
      return $this->ShortDescription;
    }

    /**
     * @param string $ShortDescription
     * @return Omni_UnitOfMeasure
     */
    public function setShortDescription($ShortDescription){
      $this->ShortDescription = $ShortDescription;
      return $this;
    }

    /**
     * @return int
     */
    public function getUnitDecimals(){
      return $this->UnitDecimals;
    }

    /**
     * @param int $UnitDecimals
     * @return Omni_UnitOfMeasure
     */
    public function setUnitDecimals($UnitDecimals){
      $this->UnitDecimals = $UnitDecimals;
      return $this;
    }

}
