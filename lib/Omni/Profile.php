<?php

class Omni_Profile {

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $Value
     * @access public
     */
    public $Value = null;

    /**
     * @param string $Value
     * @access public
     */
    public function __construct($Value = null){
      $this->Value = $Value;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_Profile
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Profile
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue(){
      return $this->Value;
    }

    /**
     * @param string $Value
     * @return Omni_Profile
     */
    public function setValue($Value){
      $this->Value = $Value;
      return $this;
    }

}
