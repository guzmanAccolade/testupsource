<?php

class Omni_OrderLineAvailability {

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var int $LineNumber
     * @access public
     */
    public $LineNumber = null;

    /**
     * @var Omni_LineType $LineType
     * @access public
     */
    public $LineType = null;

    /**
     * @var string $OrderId
     * @access public
     */
    public $OrderId = null;

    /**
     * @var float $Quantity
     * @access public
     */
    public $Quantity = null;

    /**
     * @var string $UomId
     * @access public
     */
    public $UomId = null;

    /**
     * @var string $VariantId
     * @access public
     */
    public $VariantId = null;

    /**
     * @param int $LineNumber
     * @param Omni_LineType $LineType
     * @param float $Quantity
     * @access public
     */
    public function __construct($LineNumber = null, $LineType = null, $Quantity = null){
      $this->LineNumber = $LineNumber;
      $this->LineType = $LineType;
      $this->Quantity = $Quantity;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_OrderLineAvailability
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return int
     */
    public function getLineNumber(){
      return $this->LineNumber;
    }

    /**
     * @param int $LineNumber
     * @return Omni_OrderLineAvailability
     */
    public function setLineNumber($LineNumber){
      $this->LineNumber = $LineNumber;
      return $this;
    }

    /**
     * @return Omni_LineType
     */
    public function getLineType(){
      return $this->LineType;
    }

    /**
     * @param Omni_LineType $LineType
     * @return Omni_OrderLineAvailability
     */
    public function setLineType($LineType){
      $this->LineType = $LineType;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderId(){
      return $this->OrderId;
    }

    /**
     * @param string $OrderId
     * @return Omni_OrderLineAvailability
     */
    public function setOrderId($OrderId){
      $this->OrderId = $OrderId;
      return $this;
    }

    /**
     * @return float
     */
    public function getQuantity(){
      return $this->Quantity;
    }

    /**
     * @param float $Quantity
     * @return Omni_OrderLineAvailability
     */
    public function setQuantity($Quantity){
      $this->Quantity = $Quantity;
      return $this;
    }

    /**
     * @return string
     */
    public function getUomId(){
      return $this->UomId;
    }

    /**
     * @param string $UomId
     * @return Omni_OrderLineAvailability
     */
    public function setUomId($UomId){
      $this->UomId = $UomId;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantId(){
      return $this->VariantId;
    }

    /**
     * @param string $VariantId
     * @return Omni_OrderLineAvailability
     */
    public function setVariantId($VariantId){
      $this->VariantId = $VariantId;
      return $this;
    }

}
