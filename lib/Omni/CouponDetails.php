<?php

class Omni_CouponDetails {

    /**
     * @var string $CouponId
     * @access public
     */
    public $CouponId = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $LineNumber
     * @access public
     */
    public $LineNumber = null;

    /**
     * @access public
     */
    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getCouponId(){
      return $this->CouponId;
    }

    /**
     * @param string $CouponId
     * @return Omni_CouponDetails
     */
    public function setCouponId($CouponId){
      $this->CouponId = $CouponId;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_CouponDetails
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getLineNumber(){
      return $this->LineNumber;
    }

    /**
     * @param string $LineNumber
     * @return Omni_CouponDetails
     */
    public function setLineNumber($LineNumber){
      $this->LineNumber = $LineNumber;
      return $this;
    }

}
