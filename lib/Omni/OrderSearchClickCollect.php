<?php

class Omni_OrderSearchClickCollect {

    /**
     * @var Omni_OrderSearchRequest $searchRequest
     * @access public
     */
    public $searchRequest = null;

    /**
     * @param Omni_OrderSearchRequest $searchRequest
     * @access public
     */
    public function __construct($searchRequest = null){
      $this->searchRequest = $searchRequest;
    }

    /**
     * @return Omni_OrderSearchRequest
     */
    public function getSearchRequest(){
      return $this->searchRequest;
    }

    /**
     * @param Omni_OrderSearchRequest $searchRequest
     * @return Omni_OrderSearchClickCollect
     */
    public function setSearchRequest($searchRequest){
      $this->searchRequest = $searchRequest;
      return $this;
    }

}
