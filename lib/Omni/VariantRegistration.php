<?php

class Omni_VariantRegistration {

    /**
     * @var string $Dim1
     * @access public
     */
    public $Dim1 = null;

    /**
     * @var string $Dim2
     * @access public
     */
    public $Dim2 = null;

    /**
     * @var string $Dim3
     * @access public
     */
    public $Dim3 = null;

    /**
     * @var string $Dim4
     * @access public
     */
    public $Dim4 = null;

    /**
     * @var string $Dim5
     * @access public
     */
    public $Dim5 = null;

    /**
     * @var string $Dim6
     * @access public
     */
    public $Dim6 = null;

    /**
     * @var Omni_ImageView[] $Images
     * @access public
     */
    public $Images = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var string $VarCode
     * @access public
     */
    public $VarCode = null;

    /**
     * @access public
     */
    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getDim1(){
      return $this->Dim1;
    }

    /**
     * @param string $Dim1
     * @return Omni_VariantRegistration
     */
    public function setDim1($Dim1){
      $this->Dim1 = $Dim1;
      return $this;
    }

    /**
     * @return string
     */
    public function getDim2(){
      return $this->Dim2;
    }

    /**
     * @param string $Dim2
     * @return Omni_VariantRegistration
     */
    public function setDim2($Dim2){
      $this->Dim2 = $Dim2;
      return $this;
    }

    /**
     * @return string
     */
    public function getDim3(){
      return $this->Dim3;
    }

    /**
     * @param string $Dim3
     * @return Omni_VariantRegistration
     */
    public function setDim3($Dim3){
      $this->Dim3 = $Dim3;
      return $this;
    }

    /**
     * @return string
     */
    public function getDim4(){
      return $this->Dim4;
    }

    /**
     * @param string $Dim4
     * @return Omni_VariantRegistration
     */
    public function setDim4($Dim4){
      $this->Dim4 = $Dim4;
      return $this;
    }

    /**
     * @return string
     */
    public function getDim5(){
      return $this->Dim5;
    }

    /**
     * @param string $Dim5
     * @return Omni_VariantRegistration
     */
    public function setDim5($Dim5){
      $this->Dim5 = $Dim5;
      return $this;
    }

    /**
     * @return string
     */
    public function getDim6(){
      return $this->Dim6;
    }

    /**
     * @param string $Dim6
     * @return Omni_VariantRegistration
     */
    public function setDim6($Dim6){
      $this->Dim6 = $Dim6;
      return $this;
    }

    /**
     * @return Omni_ImageView[]
     */
    public function getImages(){
      return $this->Images;
    }

    /**
     * @param Omni_ImageView[] $Images
     * @return Omni_VariantRegistration
     */
    public function setImages($Images){
      $this->Images = $Images;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_VariantRegistration
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return string
     */
    public function getVarCode(){
      return $this->VarCode;
    }

    /**
     * @param string $VarCode
     * @return Omni_VariantRegistration
     */
    public function setVarCode($VarCode){
      $this->VarCode = $VarCode;
      return $this;
    }

}
