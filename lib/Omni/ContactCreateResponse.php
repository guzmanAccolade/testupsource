<?php

class Omni_ContactCreateResponse {

    /**
     * @var Omni_Contact $ContactCreateResult
     * @access public
     */
    public $ContactCreateResult = null;

    /**
     * @param Omni_Contact $ContactCreateResult
     * @access public
     */
    public function __construct($ContactCreateResult = null){
      $this->ContactCreateResult = $ContactCreateResult;
    }

    /**
     * @return Omni_Contact
     */
    public function getContactCreateResult(){
      return $this->ContactCreateResult;
    }

    /**
     * @param Omni_Contact $ContactCreateResult
     * @return Omni_ContactCreateResponse
     */
    public function setContactCreateResult($ContactCreateResult){
      $this->ContactCreateResult = $ContactCreateResult;
      return $this;
    }

}
