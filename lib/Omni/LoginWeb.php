<?php

class Omni_LoginWeb {

    /**
     * @var string $userName
     * @access public
     */
    public $userName = null;

    /**
     * @var string $password
     * @access public
     */
    public $password = null;

    /**
     * @param string $userName
     * @param string $password
     * @access public
     */
    public function __construct($userName = null, $password = null){
      $this->userName = $userName;
      $this->password = $password;
    }

    /**
     * @return string
     */
    public function getUserName(){
      return $this->userName;
    }

    /**
     * @param string $userName
     * @return Omni_LoginWeb
     */
    public function setUserName($userName){
      $this->userName = $userName;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassword(){
      return $this->password;
    }

    /**
     * @param string $password
     * @return Omni_LoginWeb
     */
    public function setPassword($password){
      $this->password = $password;
      return $this;
    }

}
