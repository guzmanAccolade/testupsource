<?php

class Omni_ImageGetById {

    /**
     * @var string $id
     * @access public
     */
    public $id = null;

    /**
     * @var Omni_ImageSize $imageSize
     * @access public
     */
    public $imageSize = null;

    /**
     * @param string $id
     * @param Omni_ImageSize $imageSize
     * @access public
     */
    public function __construct($id = null, $imageSize = null){
      $this->id = $id;
      $this->imageSize = $imageSize;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->id;
    }

    /**
     * @param string $id
     * @return Omni_ImageGetById
     */
    public function setId($id){
      $this->id = $id;
      return $this;
    }

    /**
     * @return Omni_ImageSize
     */
    public function getImageSize(){
      return $this->imageSize;
    }

    /**
     * @param Omni_ImageSize $imageSize
     * @return Omni_ImageGetById
     */
    public function setImageSize($imageSize){
      $this->imageSize = $imageSize;
      return $this;
    }

}
