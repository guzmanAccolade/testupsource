<?php

class Omni_TransactionHeadersGetByContactIdResponse {

    /**
     * @var Omni_Transaction[] $TransactionHeadersGetByContactIdResult
     * @access public
     */
    public $TransactionHeadersGetByContactIdResult = null;

    /**
     * @param Omni_Transaction[] $TransactionHeadersGetByContactIdResult
     * @access public
     */
    public function __construct($TransactionHeadersGetByContactIdResult = null){
      $this->TransactionHeadersGetByContactIdResult = $TransactionHeadersGetByContactIdResult;
    }

    /**
     * @return Omni_Transaction[]
     */
    public function getTransactionHeadersGetByContactIdResult(){
      return $this->TransactionHeadersGetByContactIdResult;
    }

    /**
     * @param Omni_Transaction[] $TransactionHeadersGetByContactIdResult
     * @return Omni_TransactionHeadersGetByContactIdResponse
     */
    public function setTransactionHeadersGetByContactIdResult($TransactionHeadersGetByContactIdResult){
      $this->TransactionHeadersGetByContactIdResult = $TransactionHeadersGetByContactIdResult;
      return $this;
    }

}
