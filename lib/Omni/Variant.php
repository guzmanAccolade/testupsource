<?php

class Omni_Variant {

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var Omni_ImageView[] $Images
     * @access public
     */
    public $Images = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @access public
     */
    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_Variant
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Variant
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return Omni_ImageView[]
     */
    public function getImages(){
      return $this->Images;
    }

    /**
     * @param Omni_ImageView[] $Images
     * @return Omni_Variant
     */
    public function setImages($Images){
      $this->Images = $Images;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_Variant
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

}
