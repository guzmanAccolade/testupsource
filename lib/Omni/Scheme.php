<?php

class Omni_Scheme {

    /**
     * @var Omni_Club $Club
     * @access public
     */
    public $Club = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var Omni_Scheme $NextScheme
     * @access public
     */
    public $NextScheme = null;

    /**
     * @var string $Perks
     * @access public
     */
    public $Perks = null;

    /**
     * @var int $PointsNeeded
     * @access public
     */
    public $PointsNeeded = null;

    /**
     * @param int $PointsNeeded
     * @access public
     */
    public function __construct($PointsNeeded = null){
      $this->PointsNeeded = $PointsNeeded;
    }

    /**
     * @return Omni_Club
     */
    public function getClub(){
      return $this->Club;
    }

    /**
     * @param Omni_Club $Club
     * @return Omni_Scheme
     */
    public function setClub($Club){
      $this->Club = $Club;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_Scheme
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Scheme
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return Omni_Scheme
     */
    public function getNextScheme(){
      return $this->NextScheme;
    }

    /**
     * @param Omni_Scheme $NextScheme
     * @return Omni_Scheme
     */
    public function setNextScheme($NextScheme){
      $this->NextScheme = $NextScheme;
      return $this;
    }

    /**
     * @return string
     */
    public function getPerks(){
      return $this->Perks;
    }

    /**
     * @param string $Perks
     * @return Omni_Scheme
     */
    public function setPerks($Perks){
      $this->Perks = $Perks;
      return $this;
    }

    /**
     * @return int
     */
    public function getPointsNeeded(){
      return $this->PointsNeeded;
    }

    /**
     * @param int $PointsNeeded
     * @return Omni_Scheme
     */
    public function setPointsNeeded($PointsNeeded){
      $this->PointsNeeded = $PointsNeeded;
      return $this;
    }

}
