<?php

class Omni_Inventory {

    /**
     * @var string $BaseUnitOfMeasure
     * @access public
     */
    public $BaseUnitOfMeasure = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var float $QtyActualInventory
     * @access public
     */
    public $QtyActualInventory = null;

    /**
     * @var float $QtyExpectedStock
     * @access public
     */
    public $QtyExpectedStock = null;

    /**
     * @var float $QtyInventory
     * @access public
     */
    public $QtyInventory = null;

    /**
     * @var float $QtySoldNotPosted
     * @access public
     */
    public $QtySoldNotPosted = null;

    /**
     * @var string $StoreId
     * @access public
     */
    public $StoreId = null;

    /**
     * @var string $VariantId
     * @access public
     */
    public $VariantId = null;

    /**
     * @param float $QtyActualInventory
     * @param float $QtyExpectedStock
     * @param float $QtyInventory
     * @param float $QtySoldNotPosted
     * @access public
     */
    public function __construct($QtyActualInventory = null, $QtyExpectedStock = null, $QtyInventory = null, $QtySoldNotPosted = null){
      $this->QtyActualInventory = $QtyActualInventory;
      $this->QtyExpectedStock = $QtyExpectedStock;
      $this->QtyInventory = $QtyInventory;
      $this->QtySoldNotPosted = $QtySoldNotPosted;
    }

    /**
     * @return string
     */
    public function getBaseUnitOfMeasure(){
      return $this->BaseUnitOfMeasure;
    }

    /**
     * @param string $BaseUnitOfMeasure
     * @return Omni_Inventory
     */
    public function setBaseUnitOfMeasure($BaseUnitOfMeasure){
      $this->BaseUnitOfMeasure = $BaseUnitOfMeasure;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_Inventory
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return float
     */
    public function getQtyActualInventory(){
      return $this->QtyActualInventory;
    }

    /**
     * @param float $QtyActualInventory
     * @return Omni_Inventory
     */
    public function setQtyActualInventory($QtyActualInventory){
      $this->QtyActualInventory = $QtyActualInventory;
      return $this;
    }

    /**
     * @return float
     */
    public function getQtyExpectedStock(){
      return $this->QtyExpectedStock;
    }

    /**
     * @param float $QtyExpectedStock
     * @return Omni_Inventory
     */
    public function setQtyExpectedStock($QtyExpectedStock){
      $this->QtyExpectedStock = $QtyExpectedStock;
      return $this;
    }

    /**
     * @return float
     */
    public function getQtyInventory(){
      return $this->QtyInventory;
    }

    /**
     * @param float $QtyInventory
     * @return Omni_Inventory
     */
    public function setQtyInventory($QtyInventory){
      $this->QtyInventory = $QtyInventory;
      return $this;
    }

    /**
     * @return float
     */
    public function getQtySoldNotPosted(){
      return $this->QtySoldNotPosted;
    }

    /**
     * @param float $QtySoldNotPosted
     * @return Omni_Inventory
     */
    public function setQtySoldNotPosted($QtySoldNotPosted){
      $this->QtySoldNotPosted = $QtySoldNotPosted;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->StoreId;
    }

    /**
     * @param string $StoreId
     * @return Omni_Inventory
     */
    public function setStoreId($StoreId){
      $this->StoreId = $StoreId;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantId(){
      return $this->VariantId;
    }

    /**
     * @param string $VariantId
     * @return Omni_Inventory
     */
    public function setVariantId($VariantId){
      $this->VariantId = $VariantId;
      return $this;
    }

}
