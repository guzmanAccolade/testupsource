<?php

class Omni_BasketCalcRequest {

    /**
     * @var Omni_BasketCalcLineRequest[] $BasketCalcLineRequests
     * @access public
     */
    public $BasketCalcLineRequests = null;

    /**
     * @var Omni_BasketCalcType $CalcType
     * @access public
     */
    public $CalcType = null;

    /**
     * @var string $CardId
     * @access public
     */
    public $CardId = null;

    /**
     * @var string $ContactId
     * @access public
     */
    public $ContactId = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var Omni_BasketCalcItemType $ItemType
     * @access public
     */
    public $ItemType = null;

    /**
     * @var Omni_Address $ShippingAddress
     * @access public
     */
    public $ShippingAddress = null;

    /**
     * @var string $StoreId
     * @access public
     */
    public $StoreId = null;

    /**
     * @param Omni_BasketCalcType $CalcType
     * @param Omni_BasketCalcItemType $ItemType
     * @access public
     */
    public function __construct($CalcType = null, $ItemType = null){
      $this->CalcType = $CalcType;
      $this->ItemType = $ItemType;
    }

    /**
     * @return Omni_BasketCalcLineRequest[]
     */
    public function getBasketCalcLineRequests(){
      return $this->BasketCalcLineRequests;
    }

    /**
     * @param Omni_BasketCalcLineRequest[] $BasketCalcLineRequests
     * @return Omni_BasketCalcRequest
     */
    public function setBasketCalcLineRequests($BasketCalcLineRequests){
      $this->BasketCalcLineRequests = $BasketCalcLineRequests;
      return $this;
    }

    /**
     * @return Omni_BasketCalcType
     */
    public function getCalcType(){
      return $this->CalcType;
    }

    /**
     * @param Omni_BasketCalcType $CalcType
     * @return Omni_BasketCalcRequest
     */
    public function setCalcType($CalcType){
      $this->CalcType = $CalcType;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardId(){
      return $this->CardId;
    }

    /**
     * @param string $CardId
     * @return Omni_BasketCalcRequest
     */
    public function setCardId($CardId){
      $this->CardId = $CardId;
      return $this;
    }

    /**
     * @return string
     */
    public function getContactId(){
      return $this->ContactId;
    }

    /**
     * @param string $ContactId
     * @return Omni_BasketCalcRequest
     */
    public function setContactId($ContactId){
      $this->ContactId = $ContactId;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_BasketCalcRequest
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return Omni_BasketCalcItemType
     */
    public function getItemType(){
      return $this->ItemType;
    }

    /**
     * @param Omni_BasketCalcItemType $ItemType
     * @return Omni_BasketCalcRequest
     */
    public function setItemType($ItemType){
      $this->ItemType = $ItemType;
      return $this;
    }

    /**
     * @return Omni_Address
     */
    public function getShippingAddress(){
      return $this->ShippingAddress;
    }

    /**
     * @param Omni_Address $ShippingAddress
     * @return Omni_BasketCalcRequest
     */
    public function setShippingAddress($ShippingAddress){
      $this->ShippingAddress = $ShippingAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->StoreId;
    }

    /**
     * @param string $StoreId
     * @return Omni_BasketCalcRequest
     */
    public function setStoreId($StoreId){
      $this->StoreId = $StoreId;
      return $this;
    }

}
