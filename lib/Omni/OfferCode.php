<?php

class Omni_OfferCode {
    const __default = 'Promotion';
    const Promotion = 'Promotion';
    const Deal = 'Deal';
    const Multibuy = 'Multibuy';
    const MixAndMatch = 'MixAndMatch';
    const DiscountOffer = 'DiscountOffer';
    const TotalDiscount = 'TotalDiscount';
    const TenderType = 'TenderType';
    const ItemPoint = 'ItemPoint';
    const LineDiscount = 'LineDiscount';
    const Coupon = 'Coupon';
    const Unknown = 'Unknown';


}
