<?php

class Omni_AccountGetById {

    /**
     * @var string $accountId
     * @access public
     */
    public $accountId = null;

    /**
     * @param string $accountId
     * @access public
     */
    public function __construct($accountId = null){
      $this->accountId = $accountId;
    }

    /**
     * @return string
     */
    public function getAccountId(){
      return $this->accountId;
    }

    /**
     * @param string $accountId
     * @return Omni_AccountGetById
     */
    public function setAccountId($accountId){
      $this->accountId = $accountId;
      return $this;
    }

}
