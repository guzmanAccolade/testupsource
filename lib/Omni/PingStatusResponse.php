<?php

class Omni_PingStatusResponse {

    /**
     * @var Omni_StatusCode $PingStatusResult
     * @access public
     */
    public $PingStatusResult = null;

    /**
     * @param Omni_StatusCode $PingStatusResult
     * @access public
     */
    public function __construct($PingStatusResult = null){
      $this->PingStatusResult = $PingStatusResult;
    }

    /**
     * @return Omni_StatusCode
     */
    public function getPingStatusResult(){
      return $this->PingStatusResult;
    }

    /**
     * @param Omni_StatusCode $PingStatusResult
     * @return Omni_PingStatusResponse
     */
    public function setPingStatusResult($PingStatusResult){
      $this->PingStatusResult = $PingStatusResult;
      return $this;
    }

}
