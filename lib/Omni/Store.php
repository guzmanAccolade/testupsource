<?php

class Omni_Store {

    /**
     * @var string $City
     * @access public
     */
    public $City = null;

    /**
     * @var string $Country
     * @access public
     */
    public $Country = null;

    /**
     * @var string $County
     * @access public
     */
    public $County = null;

    /**
     * @var string $CultureName
     * @access public
     */
    public $CultureName = null;

    /**
     * @var string $Currency
     * @access public
     */
    public $Currency = null;

    /**
     * @var string $DefaultCustAcct
     * @access public
     */
    public $DefaultCustAcct = null;

    /**
     * @var boolean $Del
     * @access public
     */
    public $Del = null;

    /**
     * @var string $FunctProfile
     * @access public
     */
    public $FunctProfile = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var float $Latitude
     * @access public
     */
    public $Latitude = null;

    /**
     * @var float $Longitude
     * @access public
     */
    public $Longitude = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var string $Phone
     * @access public
     */
    public $Phone = null;

    /**
     * @var string $State
     * @access public
     */
    public $State = null;

    /**
     * @var string $Street
     * @access public
     */
    public $Street = null;

    /**
     * @var string $TaxGroup
     * @access public
     */
    public $TaxGroup = null;

    /**
     * @var int $UserDefaultCustAcct
     * @access public
     */
    public $UserDefaultCustAcct = null;

    /**
     * @var string $ZipCode
     * @access public
     */
    public $ZipCode = null;

    /**
     * @param string $City
     * @param string $Country
     * @param string $County
     * @param string $CultureName
     * @param string $Currency
     * @param string $DefaultCustAcct
     * @param boolean $Del
     * @param string $FunctProfile
     * @param float $Latitude
     * @param float $Longitude
     * @param string $Name
     * @param string $State
     * @param string $Street
     * @param string $TaxGroup
     * @param int $UserDefaultCustAcct
     * @param string $ZipCode
     * @access public
     */
    public function __construct($City = null, $Country = null, $County = null, $CultureName = null, $Currency = null, $DefaultCustAcct = null, $Del = null, $FunctProfile = null, $Latitude = null, $Longitude = null, $Name = null, $State = null, $Street = null, $TaxGroup = null, $UserDefaultCustAcct = null, $ZipCode = null){
      $this->City = $City;
      $this->Country = $Country;
      $this->County = $County;
      $this->CultureName = $CultureName;
      $this->Currency = $Currency;
      $this->DefaultCustAcct = $DefaultCustAcct;
      $this->Del = $Del;
      $this->FunctProfile = $FunctProfile;
      $this->Latitude = $Latitude;
      $this->Longitude = $Longitude;
      $this->Name = $Name;
      $this->State = $State;
      $this->Street = $Street;
      $this->TaxGroup = $TaxGroup;
      $this->UserDefaultCustAcct = $UserDefaultCustAcct;
      $this->ZipCode = $ZipCode;
    }

    /**
     * @return string
     */
    public function getCity(){
      return $this->City;
    }

    /**
     * @param string $City
     * @return Omni_Store
     */
    public function setCity($City){
      $this->City = $City;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountry(){
      return $this->Country;
    }

    /**
     * @param string $Country
     * @return Omni_Store
     */
    public function setCountry($Country){
      $this->Country = $Country;
      return $this;
    }

    /**
     * @return string
     */
    public function getCounty(){
      return $this->County;
    }

    /**
     * @param string $County
     * @return Omni_Store
     */
    public function setCounty($County){
      $this->County = $County;
      return $this;
    }

    /**
     * @return string
     */
    public function getCultureName(){
      return $this->CultureName;
    }

    /**
     * @param string $CultureName
     * @return Omni_Store
     */
    public function setCultureName($CultureName){
      $this->CultureName = $CultureName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(){
      return $this->Currency;
    }

    /**
     * @param string $Currency
     * @return Omni_Store
     */
    public function setCurrency($Currency){
      $this->Currency = $Currency;
      return $this;
    }

    /**
     * @return string
     */
    public function getDefaultCustAcct(){
      return $this->DefaultCustAcct;
    }

    /**
     * @param string $DefaultCustAcct
     * @return Omni_Store
     */
    public function setDefaultCustAcct($DefaultCustAcct){
      $this->DefaultCustAcct = $DefaultCustAcct;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDel(){
      return $this->Del;
    }

    /**
     * @param boolean $Del
     * @return Omni_Store
     */
    public function setDel($Del){
      $this->Del = $Del;
      return $this;
    }

    /**
     * @return string
     */
    public function getFunctProfile(){
      return $this->FunctProfile;
    }

    /**
     * @param string $FunctProfile
     * @return Omni_Store
     */
    public function setFunctProfile($FunctProfile){
      $this->FunctProfile = $FunctProfile;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Store
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return float
     */
    public function getLatitude(){
      return $this->Latitude;
    }

    /**
     * @param float $Latitude
     * @return Omni_Store
     */
    public function setLatitude($Latitude){
      $this->Latitude = $Latitude;
      return $this;
    }

    /**
     * @return float
     */
    public function getLongitude(){
      return $this->Longitude;
    }

    /**
     * @param float $Longitude
     * @return Omni_Store
     */
    public function setLongitude($Longitude){
      $this->Longitude = $Longitude;
      return $this;
    }

    /**
     * @return string
     */
    public function getName(){
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return Omni_Store
     */
    public function setName($Name){
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return string
     */
    public function getPhone(){
      return $this->Phone;
    }

    /**
     * @param string $Phone
     * @return Omni_Store
     */
    public function setPhone($Phone){
      $this->Phone = $Phone;
      return $this;
    }

    /**
     * @return string
     */
    public function getState(){
      return $this->State;
    }

    /**
     * @param string $State
     * @return Omni_Store
     */
    public function setState($State){
      $this->State = $State;
      return $this;
    }

    /**
     * @return string
     */
    public function getStreet(){
      return $this->Street;
    }

    /**
     * @param string $Street
     * @return Omni_Store
     */
    public function setStreet($Street){
      $this->Street = $Street;
      return $this;
    }

    /**
     * @return string
     */
    public function getTaxGroup(){
      return $this->TaxGroup;
    }

    /**
     * @param string $TaxGroup
     * @return Omni_Store
     */
    public function setTaxGroup($TaxGroup){
      $this->TaxGroup = $TaxGroup;
      return $this;
    }

    /**
     * @return int
     */
    public function getUserDefaultCustAcct(){
      return $this->UserDefaultCustAcct;
    }

    /**
     * @param int $UserDefaultCustAcct
     * @return Omni_Store
     */
    public function setUserDefaultCustAcct($UserDefaultCustAcct){
      $this->UserDefaultCustAcct = $UserDefaultCustAcct;
      return $this;
    }

    /**
     * @return string
     */
    public function getZipCode(){
      return $this->ZipCode;
    }

    /**
     * @param string $ZipCode
     * @return Omni_Store
     */
    public function setZipCode($ZipCode){
      $this->ZipCode = $ZipCode;
      return $this;
    }

}
