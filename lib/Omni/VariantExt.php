<?php

class Omni_VariantExt {

    /**
     * @var string $Code
     * @access public
     */
    public $Code = null;

    /**
     * @var string $Dimension
     * @access public
     */
    public $Dimension = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var Omni_DimValue[] $Values
     * @access public
     */
    public $Values = null;

    /**
     * @access public
     */
    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getCode(){
      return $this->Code;
    }

    /**
     * @param string $Code
     * @return Omni_VariantExt
     */
    public function setCode($Code){
      $this->Code = $Code;
      return $this;
    }

    /**
     * @return string
     */
    public function getDimension(){
      return $this->Dimension;
    }

    /**
     * @param string $Dimension
     * @return Omni_VariantExt
     */
    public function setDimension($Dimension){
      $this->Dimension = $Dimension;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_VariantExt
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return Omni_DimValue[]
     */
    public function getValues(){
      return $this->Values;
    }

    /**
     * @param Omni_DimValue[] $Values
     * @return Omni_VariantExt
     */
    public function setValues($Values){
      $this->Values = $Values;
      return $this;
    }

}
