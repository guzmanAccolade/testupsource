<?php

class Omni_ReplEcommItemUnitOfMeasuresResponse {

    /**
     * @var Omni_ReplItemUOMResponse $ReplEcommItemUnitOfMeasuresResult
     * @access public
     */
    public $ReplEcommItemUnitOfMeasuresResult = null;

    /**
     * @param Omni_ReplItemUOMResponse $ReplEcommItemUnitOfMeasuresResult
     * @access public
     */
    public function __construct($ReplEcommItemUnitOfMeasuresResult = null){
      $this->ReplEcommItemUnitOfMeasuresResult = $ReplEcommItemUnitOfMeasuresResult;
    }

    /**
     * @return Omni_ReplItemUOMResponse
     */
    public function getReplEcommItemUnitOfMeasuresResult(){
      return $this->ReplEcommItemUnitOfMeasuresResult;
    }

    /**
     * @param Omni_ReplItemUOMResponse $ReplEcommItemUnitOfMeasuresResult
     * @return Omni_ReplEcommItemUnitOfMeasuresResponse
     */
    public function setReplEcommItemUnitOfMeasuresResult($ReplEcommItemUnitOfMeasuresResult){
      $this->ReplEcommItemUnitOfMeasuresResult = $ReplEcommItemUnitOfMeasuresResult;
      return $this;
    }

}
