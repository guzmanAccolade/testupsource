<?php

class Omni_ContactGetByAlternateId {

    /**
     * @var string $alternateId
     * @access public
     */
    public $alternateId = null;

    /**
     * @param string $alternateId
     * @access public
     */
    public function __construct($alternateId = null){
      $this->alternateId = $alternateId;
    }

    /**
     * @return string
     */
    public function getAlternateId(){
      return $this->alternateId;
    }

    /**
     * @param string $alternateId
     * @return Omni_ContactGetByAlternateId
     */
    public function setAlternateId($alternateId){
      $this->alternateId = $alternateId;
      return $this;
    }

}
