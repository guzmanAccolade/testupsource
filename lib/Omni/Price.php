<?php

class Omni_Price {

    /**
     * @var string $CurrencyCode
     * @access public
     */
    public $CurrencyCode = null;

    /**
     * @var string $CustomerDiscountGroup
     * @access public
     */
    public $CustomerDiscountGroup = null;

    /**
     * @var boolean $Del
     * @access public
     */
    public $Del = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var string $LoyaltySchemeCode
     * @access public
     */
    public $LoyaltySchemeCode = null;

    /**
     * @var string $StoreId
     * @access public
     */
    public $StoreId = null;

    /**
     * @var string $UOMId
     * @access public
     */
    public $UOMId = null;

    /**
     * @var float $UnitPrice
     * @access public
     */
    public $UnitPrice = null;

    /**
     * @var float $UnitPriceInclVAT
     * @access public
     */
    public $UnitPriceInclVAT = null;

    /**
     * @var string $VariantId
     * @access public
     */
    public $VariantId = null;

    /**
     * @param string $CurrencyCode
     * @param string $CustomerDiscountGroup
     * @param boolean $Del
     * @param string $LoyaltySchemeCode
     * @param string $StoreId
     * @param string $UOMId
     * @param float $UnitPrice
     * @param float $UnitPriceInclVAT
     * @access public
     */
    public function __construct($CurrencyCode = null, $CustomerDiscountGroup = null, $Del = null, $LoyaltySchemeCode = null, $StoreId = null, $UOMId = null, $UnitPrice = null, $UnitPriceInclVAT = null){
      $this->CurrencyCode = $CurrencyCode;
      $this->CustomerDiscountGroup = $CustomerDiscountGroup;
      $this->Del = $Del;
      $this->LoyaltySchemeCode = $LoyaltySchemeCode;
      $this->StoreId = $StoreId;
      $this->UOMId = $UOMId;
      $this->UnitPrice = $UnitPrice;
      $this->UnitPriceInclVAT = $UnitPriceInclVAT;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(){
      return $this->CurrencyCode;
    }

    /**
     * @param string $CurrencyCode
     * @return Omni_Price
     */
    public function setCurrencyCode($CurrencyCode){
      $this->CurrencyCode = $CurrencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerDiscountGroup(){
      return $this->CustomerDiscountGroup;
    }

    /**
     * @param string $CustomerDiscountGroup
     * @return Omni_Price
     */
    public function setCustomerDiscountGroup($CustomerDiscountGroup){
      $this->CustomerDiscountGroup = $CustomerDiscountGroup;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDel(){
      return $this->Del;
    }

    /**
     * @param boolean $Del
     * @return Omni_Price
     */
    public function setDel($Del){
      $this->Del = $Del;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_Price
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return string
     */
    public function getLoyaltySchemeCode(){
      return $this->LoyaltySchemeCode;
    }

    /**
     * @param string $LoyaltySchemeCode
     * @return Omni_Price
     */
    public function setLoyaltySchemeCode($LoyaltySchemeCode){
      $this->LoyaltySchemeCode = $LoyaltySchemeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->StoreId;
    }

    /**
     * @param string $StoreId
     * @return Omni_Price
     */
    public function setStoreId($StoreId){
      $this->StoreId = $StoreId;
      return $this;
    }

    /**
     * @return string
     */
    public function getUOMId(){
      return $this->UOMId;
    }

    /**
     * @param string $UOMId
     * @return Omni_Price
     */
    public function setUOMId($UOMId){
      $this->UOMId = $UOMId;
      return $this;
    }

    /**
     * @return float
     */
    public function getUnitPrice(){
      return $this->UnitPrice;
    }

    /**
     * @param float $UnitPrice
     * @return Omni_Price
     */
    public function setUnitPrice($UnitPrice){
      $this->UnitPrice = $UnitPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getUnitPriceInclVAT(){
      return $this->UnitPriceInclVAT;
    }

    /**
     * @param float $UnitPriceInclVAT
     * @return Omni_Price
     */
    public function setUnitPriceInclVAT($UnitPriceInclVAT){
      $this->UnitPriceInclVAT = $UnitPriceInclVAT;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantId(){
      return $this->VariantId;
    }

    /**
     * @param string $VariantId
     * @return Omni_Price
     */
    public function setVariantId($VariantId){
      $this->VariantId = $VariantId;
      return $this;
    }

}
