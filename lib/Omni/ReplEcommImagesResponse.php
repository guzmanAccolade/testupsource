<?php

class Omni_ReplEcommImagesResponse {

    /**
     * @var Omni_ReplImageResponse $ReplEcommImagesResult
     * @access public
     */
    public $ReplEcommImagesResult = null;

    /**
     * @param Omni_ReplImageResponse $ReplEcommImagesResult
     * @access public
     */
    public function __construct($ReplEcommImagesResult = null){
      $this->ReplEcommImagesResult = $ReplEcommImagesResult;
    }

    /**
     * @return Omni_ReplImageResponse
     */
    public function getReplEcommImagesResult(){
      return $this->ReplEcommImagesResult;
    }

    /**
     * @param Omni_ReplImageResponse $ReplEcommImagesResult
     * @return Omni_ReplEcommImagesResponse
     */
    public function setReplEcommImagesResult($ReplEcommImagesResult){
      $this->ReplEcommImagesResult = $ReplEcommImagesResult;
      return $this;
    }

}
