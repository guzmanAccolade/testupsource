<?php

class Omni_OrderClickCollect {

    /**
     * @var string $CardId
     * @access public
     */
    public $CardId = null;

    /**
     * @var string $ContactId
     * @access public
     */
    public $ContactId = null;

    /**
     * @var string $Email
     * @access public
     */
    public $Email = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var Omni_ItemNumberType $ItemNumberType
     * @access public
     */
    public $ItemNumberType = null;

    /**
     * @var Omni_OrderDiscountLineCreateRequest[] $OrderDiscountLineCreateRequests
     * @access public
     */
    public $OrderDiscountLineCreateRequests = null;

    /**
     * @var Omni_OrderLineCreateRequest[] $OrderLineCreateRequests
     * @access public
     */
    public $OrderLineCreateRequests = null;

    /**
     * @var string $PhoneNumber
     * @access public
     */
    public $PhoneNumber = null;

    /**
     * @var Omni_SourceType $SourceType
     * @access public
     */
    public $SourceType = null;

    /**
     * @var string $StoreId
     * @access public
     */
    public $StoreId = null;

    /**
     * @param Omni_ItemNumberType $ItemNumberType
     * @param Omni_SourceType $SourceType
     * @access public
     */
    public function __construct($ItemNumberType = null, $SourceType = null){
      $this->ItemNumberType = $ItemNumberType;
      $this->SourceType = $SourceType;
    }

    /**
     * @return string
     */
    public function getCardId(){
      return $this->CardId;
    }

    /**
     * @param string $CardId
     * @return Omni_OrderClickCollect
     */
    public function setCardId($CardId){
      $this->CardId = $CardId;
      return $this;
    }

    /**
     * @return string
     */
    public function getContactId(){
      return $this->ContactId;
    }

    /**
     * @param string $ContactId
     * @return Omni_OrderClickCollect
     */
    public function setContactId($ContactId){
      $this->ContactId = $ContactId;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmail(){
      return $this->Email;
    }

    /**
     * @param string $Email
     * @return Omni_OrderClickCollect
     */
    public function setEmail($Email){
      $this->Email = $Email;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_OrderClickCollect
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return Omni_ItemNumberType
     */
    public function getItemNumberType(){
      return $this->ItemNumberType;
    }

    /**
     * @param Omni_ItemNumberType $ItemNumberType
     * @return Omni_OrderClickCollect
     */
    public function setItemNumberType($ItemNumberType){
      $this->ItemNumberType = $ItemNumberType;
      return $this;
    }

    /**
     * @return Omni_OrderDiscountLineCreateRequest[]
     */
    public function getOrderDiscountLineCreateRequests(){
      return $this->OrderDiscountLineCreateRequests;
    }

    /**
     * @param Omni_OrderDiscountLineCreateRequest[] $OrderDiscountLineCreateRequests
     * @return Omni_OrderClickCollect
     */
    public function setOrderDiscountLineCreateRequests($OrderDiscountLineCreateRequests){
      $this->OrderDiscountLineCreateRequests = $OrderDiscountLineCreateRequests;
      return $this;
    }

    /**
     * @return Omni_OrderLineCreateRequest[]
     */
    public function getOrderLineCreateRequests(){
      return $this->OrderLineCreateRequests;
    }

    /**
     * @param Omni_OrderLineCreateRequest[] $OrderLineCreateRequests
     * @return Omni_OrderClickCollect
     */
    public function setOrderLineCreateRequests($OrderLineCreateRequests){
      $this->OrderLineCreateRequests = $OrderLineCreateRequests;
      return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(){
      return $this->PhoneNumber;
    }

    /**
     * @param string $PhoneNumber
     * @return Omni_OrderClickCollect
     */
    public function setPhoneNumber($PhoneNumber){
      $this->PhoneNumber = $PhoneNumber;
      return $this;
    }

    /**
     * @return Omni_SourceType
     */
    public function getSourceType(){
      return $this->SourceType;
    }

    /**
     * @param Omni_SourceType $SourceType
     * @return Omni_OrderClickCollect
     */
    public function setSourceType($SourceType){
      $this->SourceType = $SourceType;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->StoreId;
    }

    /**
     * @param string $StoreId
     * @return Omni_OrderClickCollect
     */
    public function setStoreId($StoreId){
      $this->StoreId = $StoreId;
      return $this;
    }

}
