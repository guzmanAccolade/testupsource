<?php

class Omni_ReplImageLinkResponse {

    /**
     * @var Omni_ImageLink[] $ImageLinks
     * @access public
     */
    public $ImageLinks = null;

    /**
     * @var string $LastKey
     * @access public
     */
    public $LastKey = null;

    /**
     * @var string $MaxKey
     * @access public
     */
    public $MaxKey = null;

    /**
     * @var int $RecordsRemaining
     * @access public
     */
    public $RecordsRemaining = null;

    /**
     * @param int $RecordsRemaining
     * @access public
     */
    public function __construct($RecordsRemaining = null){
      $this->RecordsRemaining = $RecordsRemaining;
    }

    /**
     * @return Omni_ImageLink[]
     */
    public function getImageLinks(){
      return $this->ImageLinks;
    }

    /**
     * @param Omni_ImageLink[] $ImageLinks
     * @return Omni_ReplImageLinkResponse
     */
    public function setImageLinks($ImageLinks){
      $this->ImageLinks = $ImageLinks;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastKey(){
      return $this->LastKey;
    }

    /**
     * @param string $LastKey
     * @return Omni_ReplImageLinkResponse
     */
    public function setLastKey($LastKey){
      $this->LastKey = $LastKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getMaxKey(){
      return $this->MaxKey;
    }

    /**
     * @param string $MaxKey
     * @return Omni_ReplImageLinkResponse
     */
    public function setMaxKey($MaxKey){
      $this->MaxKey = $MaxKey;
      return $this;
    }

    /**
     * @return int
     */
    public function getRecordsRemaining(){
      return $this->RecordsRemaining;
    }

    /**
     * @param int $RecordsRemaining
     * @return Omni_ReplImageLinkResponse
     */
    public function setRecordsRemaining($RecordsRemaining){
      $this->RecordsRemaining = $RecordsRemaining;
      return $this;
    }

}
