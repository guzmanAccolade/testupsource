<?php

class Omni_OrderAvailabilityCheckResponse {

    /**
     * @var Omni_OrderLineAvailability[] $OrderAvailabilityCheckResult
     * @access public
     */
    public $OrderAvailabilityCheckResult = null;

    /**
     * @param Omni_OrderLineAvailability[] $OrderAvailabilityCheckResult
     * @access public
     */
    public function __construct($OrderAvailabilityCheckResult = null){
      $this->OrderAvailabilityCheckResult = $OrderAvailabilityCheckResult;
    }

    /**
     * @return Omni_OrderLineAvailability[]
     */
    public function getOrderAvailabilityCheckResult(){
      return $this->OrderAvailabilityCheckResult;
    }

    /**
     * @param Omni_OrderLineAvailability[] $OrderAvailabilityCheckResult
     * @return Omni_OrderAvailabilityCheckResponse
     */
    public function setOrderAvailabilityCheckResult($OrderAvailabilityCheckResult){
      $this->OrderAvailabilityCheckResult = $OrderAvailabilityCheckResult;
      return $this;
    }

}
