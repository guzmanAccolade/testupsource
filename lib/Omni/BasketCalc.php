<?php

class Omni_BasketCalc {

    /**
     * @var Omni_BasketCalcRequest $basketRequest
     * @access public
     */
    public $basketRequest = null;

    /**
     * @param Omni_BasketCalcRequest $basketRequest
     * @access public
     */
    public function __construct($basketRequest = null){
      $this->basketRequest = $basketRequest;
    }

    /**
     * @return Omni_BasketCalcRequest
     */
    public function getBasketRequest(){
      return $this->basketRequest;
    }

    /**
     * @param Omni_BasketCalcRequest $basketRequest
     * @return Omni_BasketCalc
     */
    public function setBasketRequest($basketRequest){
      $this->basketRequest = $basketRequest;
      return $this;
    }

}
