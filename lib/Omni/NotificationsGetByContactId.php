<?php

class Omni_NotificationsGetByContactId {

    /**
     * @var string $contactId
     * @access public
     */
    public $contactId = null;

    /**
     * @var int $numberOfNotifications
     * @access public
     */
    public $numberOfNotifications = null;

    /**
     * @param string $contactId
     * @param int $numberOfNotifications
     * @access public
     */
    public function __construct($contactId = null, $numberOfNotifications = null){
      $this->contactId = $contactId;
      $this->numberOfNotifications = $numberOfNotifications;
    }

    /**
     * @return string
     */
    public function getContactId(){
      return $this->contactId;
    }

    /**
     * @param string $contactId
     * @return Omni_NotificationsGetByContactId
     */
    public function setContactId($contactId){
      $this->contactId = $contactId;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfNotifications(){
      return $this->numberOfNotifications;
    }

    /**
     * @param int $numberOfNotifications
     * @return Omni_NotificationsGetByContactId
     */
    public function setNumberOfNotifications($numberOfNotifications){
      $this->numberOfNotifications = $numberOfNotifications;
      return $this;
    }

}
