<?php

class Omni_TransactionHeadersGetByContactId {

    /**
     * @var string $contactId
     * @access public
     */
    public $contactId = null;

    /**
     * @var int $maxNumberOfTransactions
     * @access public
     */
    public $maxNumberOfTransactions = null;

    /**
     * @param string $contactId
     * @param int $maxNumberOfTransactions
     * @access public
     */
    public function __construct($contactId = null, $maxNumberOfTransactions = null){
      $this->contactId = $contactId;
      $this->maxNumberOfTransactions = $maxNumberOfTransactions;
    }

    /**
     * @return string
     */
    public function getContactId(){
      return $this->contactId;
    }

    /**
     * @param string $contactId
     * @return Omni_TransactionHeadersGetByContactId
     */
    public function setContactId($contactId){
      $this->contactId = $contactId;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxNumberOfTransactions(){
      return $this->maxNumberOfTransactions;
    }

    /**
     * @param int $maxNumberOfTransactions
     * @return Omni_TransactionHeadersGetByContactId
     */
    public function setMaxNumberOfTransactions($maxNumberOfTransactions){
      $this->maxNumberOfTransactions = $maxNumberOfTransactions;
      return $this;
    }

}
