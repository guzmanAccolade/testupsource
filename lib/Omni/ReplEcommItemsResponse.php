<?php

class Omni_ReplEcommItemsResponse {

    /**
     * @var Omni_ReplItemResponse $ReplEcommItemsResult
     * @access public
     */
    public $ReplEcommItemsResult = null;

    /**
     * @param Omni_ReplItemResponse $ReplEcommItemsResult
     * @access public
     */
    public function __construct($ReplEcommItemsResult = null){
      $this->ReplEcommItemsResult = $ReplEcommItemsResult;
    }

    /**
     * @return Omni_ReplItemResponse
     */
    public function getReplEcommItemsResult(){
      return $this->ReplEcommItemsResult;
    }

    /**
     * @param Omni_ReplItemResponse $ReplEcommItemsResult
     * @return Omni_ReplEcommItemsResponse
     */
    public function setReplEcommItemsResult($ReplEcommItemsResult){
      $this->ReplEcommItemsResult = $ReplEcommItemsResult;
      return $this;
    }

}
