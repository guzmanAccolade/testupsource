<?php

class Omni_ReplUnitOfMeasureResponse {

    /**
     * @var string $LastKey
     * @access public
     */
    public $LastKey = null;

    /**
     * @var string $MaxKey
     * @access public
     */
    public $MaxKey = null;

    /**
     * @var int $RecordsRemaining
     * @access public
     */
    public $RecordsRemaining = null;

    /**
     * @var Omni_UnitOfMeasure[] $UnitOfMeasures
     * @access public
     */
    public $UnitOfMeasures = null;

    /**
     * @param int $RecordsRemaining
     * @access public
     */
    public function __construct($RecordsRemaining = null){
      $this->RecordsRemaining = $RecordsRemaining;
    }

    /**
     * @return string
     */
    public function getLastKey(){
      return $this->LastKey;
    }

    /**
     * @param string $LastKey
     * @return Omni_ReplUnitOfMeasureResponse
     */
    public function setLastKey($LastKey){
      $this->LastKey = $LastKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getMaxKey(){
      return $this->MaxKey;
    }

    /**
     * @param string $MaxKey
     * @return Omni_ReplUnitOfMeasureResponse
     */
    public function setMaxKey($MaxKey){
      $this->MaxKey = $MaxKey;
      return $this;
    }

    /**
     * @return int
     */
    public function getRecordsRemaining(){
      return $this->RecordsRemaining;
    }

    /**
     * @param int $RecordsRemaining
     * @return Omni_ReplUnitOfMeasureResponse
     */
    public function setRecordsRemaining($RecordsRemaining){
      $this->RecordsRemaining = $RecordsRemaining;
      return $this;
    }

    /**
     * @return Omni_UnitOfMeasure[]
     */
    public function getUnitOfMeasures(){
      return $this->UnitOfMeasures;
    }

    /**
     * @param Omni_UnitOfMeasure[] $UnitOfMeasures
     * @return Omni_ReplUnitOfMeasureResponse
     */
    public function setUnitOfMeasures($UnitOfMeasures){
      $this->UnitOfMeasures = $UnitOfMeasures;
      return $this;
    }

}
