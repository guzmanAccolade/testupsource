<?php

class Omni_OrderSearchClickCollectResponse {

    /**
     * @var Omni_OrderClickCollect[] $OrderSearchClickCollectResult
     * @access public
     */
    public $OrderSearchClickCollectResult = null;

    /**
     * @param Omni_OrderClickCollect[] $OrderSearchClickCollectResult
     * @access public
     */
    public function __construct($OrderSearchClickCollectResult = null){
      $this->OrderSearchClickCollectResult = $OrderSearchClickCollectResult;
    }

    /**
     * @return Omni_OrderClickCollect[]
     */
    public function getOrderSearchClickCollectResult(){
      return $this->OrderSearchClickCollectResult;
    }

    /**
     * @param Omni_OrderClickCollect[] $OrderSearchClickCollectResult
     * @return Omni_OrderSearchClickCollectResponse
     */
    public function setOrderSearchClickCollectResult($OrderSearchClickCollectResult){
      $this->OrderSearchClickCollectResult = $OrderSearchClickCollectResult;
      return $this;
    }

}
