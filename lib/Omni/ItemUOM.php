<?php

class Omni_ItemUOM {

    /**
     * @var boolean $Del
     * @access public
     */
    public $Del = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var float $QtyPrUom
     * @access public
     */
    public $QtyPrUom = null;

    /**
     * @var string $StoreId
     * @access public
     */
    public $StoreId = null;

    /**
     * @var string $UomCode
     * @access public
     */
    public $UomCode = null;

    /**
     * @param boolean $Del
     * @param float $QtyPrUom
     * @access public
     */
    public function __construct($Del = null, $QtyPrUom = null){
      $this->Del = $Del;
      $this->QtyPrUom = $QtyPrUom;
    }

    /**
     * @return boolean
     */
    public function getDel(){
      return $this->Del;
    }

    /**
     * @param boolean $Del
     * @return Omni_ItemUOM
     */
    public function setDel($Del){
      $this->Del = $Del;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_ItemUOM
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return float
     */
    public function getQtyPrUom(){
      return $this->QtyPrUom;
    }

    /**
     * @param float $QtyPrUom
     * @return Omni_ItemUOM
     */
    public function setQtyPrUom($QtyPrUom){
      $this->QtyPrUom = $QtyPrUom;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->StoreId;
    }

    /**
     * @param string $StoreId
     * @return Omni_ItemUOM
     */
    public function setStoreId($StoreId){
      $this->StoreId = $StoreId;
      return $this;
    }

    /**
     * @return string
     */
    public function getUomCode(){
      return $this->UomCode;
    }

    /**
     * @param string $UomCode
     * @return Omni_ItemUOM
     */
    public function setUomCode($UomCode){
      $this->UomCode = $UomCode;
      return $this;
    }

}
