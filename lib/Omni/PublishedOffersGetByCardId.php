<?php

class Omni_PublishedOffersGetByCardId {

    /**
     * @var string $cardId
     * @access public
     */
    public $cardId = null;

    /**
     * @var string $itemId
     * @access public
     */
    public $itemId = null;

    /**
     * @param string $cardId
     * @param string $itemId
     * @access public
     */
    public function __construct($cardId = null, $itemId = null){
      $this->cardId = $cardId;
      $this->itemId = $itemId;
    }

    /**
     * @return string
     */
    public function getCardId(){
      return $this->cardId;
    }

    /**
     * @param string $cardId
     * @return Omni_PublishedOffersGetByCardId
     */
    public function setCardId($cardId){
      $this->cardId = $cardId;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->itemId;
    }

    /**
     * @param string $itemId
     * @return Omni_PublishedOffersGetByCardId
     */
    public function setItemId($itemId){
      $this->itemId = $itemId;
      return $this;
    }

}
