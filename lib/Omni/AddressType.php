<?php

class Omni_AddressType {
    const __default = 'Residential';
    const Residential = 'Residential';
    const Commercial = 'Commercial';
    const Store = 'Store';
    const Shipping = 'Shipping';
    const Billing = 'Billing';


}
