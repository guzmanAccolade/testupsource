<?php

class Omni_Image {

    /**
     * @var boolean $Del
     * @access public
     */
    public $Del = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $Image64
     * @access public
     */
    public $Image64 = null;

    /**
     * @var string $Location
     * @access public
     */
    public $Location = null;

    /**
     * @var Omni_LocationType $LocationType
     * @access public
     */
    public $LocationType = null;

    /**
     * @param boolean $Del
     * @param Omni_LocationType $LocationType
     * @access public
     */
    public function __construct($Del = null, $LocationType = null){
      $this->Del = $Del;
      $this->LocationType = $LocationType;
    }

    /**
     * @return boolean
     */
    public function getDel(){
      return $this->Del;
    }

    /**
     * @param boolean $Del
     * @return Omni_Image
     */
    public function setDel($Del){
      $this->Del = $Del;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Image
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getImage64(){
      return $this->Image64;
    }

    /**
     * @param string $Image64
     * @return Omni_Image
     */
    public function setImage64($Image64){
      $this->Image64 = $Image64;
      return $this;
    }

    /**
     * @return string
     */
    public function getLocation(){
      return $this->Location;
    }

    /**
     * @param string $Location
     * @return Omni_Image
     */
    public function setLocation($Location){
      $this->Location = $Location;
      return $this;
    }

    /**
     * @return Omni_LocationType
     */
    public function getLocationType(){
      return $this->LocationType;
    }

    /**
     * @param Omni_LocationType $LocationType
     * @return Omni_Image
     */
    public function setLocationType($LocationType){
      $this->LocationType = $LocationType;
      return $this;
    }

}
