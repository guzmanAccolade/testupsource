<?php

class Omni_OneListCoupon {

    /**
     * @var Omni_Coupon $Coupon
     * @access public
     */
    public $Coupon = null;

    /**
     * @var dateTime $CreateDate
     * @access public
     */
    public $CreateDate = null;

    /**
     * @var int $DisplayOrderId
     * @access public
     */
    public $DisplayOrderId = null;

    /**
     * @param dateTime $CreateDate
     * @param int $DisplayOrderId
     * @access public
     */
    public function __construct($CreateDate = null, $DisplayOrderId = null){
      $this->CreateDate = $CreateDate;
      $this->DisplayOrderId = $DisplayOrderId;
    }

    /**
     * @return Omni_Coupon
     */
    public function getCoupon(){
      return $this->Coupon;
    }

    /**
     * @param Omni_Coupon $Coupon
     * @return Omni_OneListCoupon
     */
    public function setCoupon($Coupon){
      $this->Coupon = $Coupon;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getCreateDate(){
      return $this->CreateDate;
    }

    /**
     * @param dateTime $CreateDate
     * @return Omni_OneListCoupon
     */
    public function setCreateDate($CreateDate){
      $this->CreateDate = $CreateDate;
      return $this;
    }

    /**
     * @return int
     */
    public function getDisplayOrderId(){
      return $this->DisplayOrderId;
    }

    /**
     * @param int $DisplayOrderId
     * @return Omni_OneListCoupon
     */
    public function setDisplayOrderId($DisplayOrderId){
      $this->DisplayOrderId = $DisplayOrderId;
      return $this;
    }

}
