<?php

class Omni_MaritalStatus {
    const __default = 'Unknown';
    const Unknown = 'Unknown';
    const Single = 'Single';
    const Married = 'Married';
    const Divorced = 'Divorced';
    const Widowed = 'Widowed';


}
