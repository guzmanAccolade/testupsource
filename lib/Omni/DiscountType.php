<?php

class Omni_DiscountType {
    const __default = 'PeriodicDisc';
    const PeriodicDisc = 'PeriodicDisc';
    const Customer = 'Customer';
    const InfoCode = 'InfoCode';
    const Total = 'Total';
    const Line = 'Line';
    const Promotion = 'Promotion';
    const Deal = 'Deal';
    const TotalDiscount = 'TotalDiscount';
    const TenderType = 'TenderType';
    const ItemPoint = 'ItemPoint';
    const LineDiscount = 'LineDiscount';
    const MemberPoint = 'MemberPoint';
    const Coupon = 'Coupon';
    const Unknown = 'Unknown';


}
