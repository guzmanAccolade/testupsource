<?php

class Omni_BasketPostSale {

    /**
     * @var Omni_BasketPostSaleRequest $basketRequest
     * @access public
     */
    public $basketRequest = null;

    /**
     * @param Omni_BasketPostSaleRequest $basketRequest
     * @access public
     */
    public function __construct($basketRequest = null){
      $this->basketRequest = $basketRequest;
    }

    /**
     * @return Omni_BasketPostSaleRequest
     */
    public function getBasketRequest(){
      return $this->basketRequest;
    }

    /**
     * @param Omni_BasketPostSaleRequest $basketRequest
     * @return Omni_BasketPostSale
     */
    public function setBasketRequest($basketRequest){
      $this->basketRequest = $basketRequest;
      return $this;
    }

}
