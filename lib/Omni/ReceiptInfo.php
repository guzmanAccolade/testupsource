<?php

class Omni_ReceiptInfo {

    /**
     * @var boolean $IsLargeValue
     * @access public
     */
    public $IsLargeValue = null;

    /**
     * @var string $Key
     * @access public
     */
    public $Key = null;

    /**
     * @var string $Type
     * @access public
     */
    public $Type = null;

    /**
     * @var string $Value
     * @access public
     */
    public $Value = null;

    /**
     * @param boolean $IsLargeValue
     * @access public
     */
    public function __construct($IsLargeValue = null){
      $this->IsLargeValue = $IsLargeValue;
    }

    /**
     * @return boolean
     */
    public function getIsLargeValue(){
      return $this->IsLargeValue;
    }

    /**
     * @param boolean $IsLargeValue
     * @return Omni_ReceiptInfo
     */
    public function setIsLargeValue($IsLargeValue){
      $this->IsLargeValue = $IsLargeValue;
      return $this;
    }

    /**
     * @return string
     */
    public function getKey(){
      return $this->Key;
    }

    /**
     * @param string $Key
     * @return Omni_ReceiptInfo
     */
    public function setKey($Key){
      $this->Key = $Key;
      return $this;
    }

    /**
     * @return string
     */
    public function getType(){
      return $this->Type;
    }

    /**
     * @param string $Type
     * @return Omni_ReceiptInfo
     */
    public function setType($Type){
      $this->Type = $Type;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue(){
      return $this->Value;
    }

    /**
     * @param string $Value
     * @return Omni_ReceiptInfo
     */
    public function setValue($Value){
      $this->Value = $Value;
      return $this;
    }

}
