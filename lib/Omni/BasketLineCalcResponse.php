<?php

class Omni_BasketLineCalcResponse {

    /**
     * @var string $BarcodeId
     * @access public
     */
    public $BarcodeId = null;

    /**
     * @var Omni_BasketLineDiscResponse[] $BasketLineDiscResponses
     * @access public
     */
    public $BasketLineDiscResponses = null;

    /**
     * @var string $CardOrCustNo
     * @access public
     */
    public $CardOrCustNo = null;

    /**
     * @var string $CouponCode
     * @access public
     */
    public $CouponCode = null;

    /**
     * @var int $CouponFunction
     * @access public
     */
    public $CouponFunction = null;

    /**
     * @var string $CurrencyCode
     * @access public
     */
    public $CurrencyCode = null;

    /**
     * @var int $CurrencyFactor
     * @access public
     */
    public $CurrencyFactor = null;

    /**
     * @var string $DiscountAmount
     * @access public
     */
    public $DiscountAmount = null;

    /**
     * @var string $DiscountPercent
     * @access public
     */
    public $DiscountPercent = null;

    /**
     * @var Omni_EntryStatus $EntryStatus
     * @access public
     */
    public $EntryStatus = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var int $LineNumber
     * @access public
     */
    public $LineNumber = null;

    /**
     * @var Omni_LineType $LineType
     * @access public
     */
    public $LineType = null;

    /**
     * @var string $ManualDiscountAmount
     * @access public
     */
    public $ManualDiscountAmount = null;

    /**
     * @var string $ManualDiscountPercent
     * @access public
     */
    public $ManualDiscountPercent = null;

    /**
     * @var string $ManualPrice
     * @access public
     */
    public $ManualPrice = null;

    /**
     * @var string $NetAmount
     * @access public
     */
    public $NetAmount = null;

    /**
     * @var string $NetPrice
     * @access public
     */
    public $NetPrice = null;

    /**
     * @var string $Price
     * @access public
     */
    public $Price = null;

    /**
     * @var string $Quantity
     * @access public
     */
    public $Quantity = null;

    /**
     * @var string $TAXAmount
     * @access public
     */
    public $TAXAmount = null;

    /**
     * @var string $TAXBusinessCode
     * @access public
     */
    public $TAXBusinessCode = null;

    /**
     * @var string $TAXProductCode
     * @access public
     */
    public $TAXProductCode = null;

    /**
     * @var string $Uom
     * @access public
     */
    public $Uom = null;

    /**
     * @var int $ValidInTransaction
     * @access public
     */
    public $ValidInTransaction = null;

    /**
     * @var string $VariantId
     * @access public
     */
    public $VariantId = null;

    /**
     * @param int $CouponFunction
     * @param int $CurrencyFactor
     * @param Omni_EntryStatus $EntryStatus
     * @param int $LineNumber
     * @param Omni_LineType $LineType
     * @param int $ValidInTransaction
     * @access public
     */
    public function __construct($CouponFunction = null, $CurrencyFactor = null, $EntryStatus = null, $LineNumber = null, $LineType = null, $ValidInTransaction = null){
      $this->CouponFunction = $CouponFunction;
      $this->CurrencyFactor = $CurrencyFactor;
      $this->EntryStatus = $EntryStatus;
      $this->LineNumber = $LineNumber;
      $this->LineType = $LineType;
      $this->ValidInTransaction = $ValidInTransaction;
    }

    /**
     * @return string
     */
    public function getBarcodeId(){
      return $this->BarcodeId;
    }

    /**
     * @param string $BarcodeId
     * @return Omni_BasketLineCalcResponse
     */
    public function setBarcodeId($BarcodeId){
      $this->BarcodeId = $BarcodeId;
      return $this;
    }

    /**
     * @return Omni_BasketLineDiscResponse[]
     */
    public function getBasketLineDiscResponses(){
      return $this->BasketLineDiscResponses;
    }

    /**
     * @param Omni_BasketLineDiscResponse[] $BasketLineDiscResponses
     * @return Omni_BasketLineCalcResponse
     */
    public function setBasketLineDiscResponses($BasketLineDiscResponses){
      $this->BasketLineDiscResponses = $BasketLineDiscResponses;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardOrCustNo(){
      return $this->CardOrCustNo;
    }

    /**
     * @param string $CardOrCustNo
     * @return Omni_BasketLineCalcResponse
     */
    public function setCardOrCustNo($CardOrCustNo){
      $this->CardOrCustNo = $CardOrCustNo;
      return $this;
    }

    /**
     * @return string
     */
    public function getCouponCode(){
      return $this->CouponCode;
    }

    /**
     * @param string $CouponCode
     * @return Omni_BasketLineCalcResponse
     */
    public function setCouponCode($CouponCode){
      $this->CouponCode = $CouponCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getCouponFunction(){
      return $this->CouponFunction;
    }

    /**
     * @param int $CouponFunction
     * @return Omni_BasketLineCalcResponse
     */
    public function setCouponFunction($CouponFunction){
      $this->CouponFunction = $CouponFunction;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(){
      return $this->CurrencyCode;
    }

    /**
     * @param string $CurrencyCode
     * @return Omni_BasketLineCalcResponse
     */
    public function setCurrencyCode($CurrencyCode){
      $this->CurrencyCode = $CurrencyCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getCurrencyFactor(){
      return $this->CurrencyFactor;
    }

    /**
     * @param int $CurrencyFactor
     * @return Omni_BasketLineCalcResponse
     */
    public function setCurrencyFactor($CurrencyFactor){
      $this->CurrencyFactor = $CurrencyFactor;
      return $this;
    }

    /**
     * @return string
     */
    public function getDiscountAmount(){
      return $this->DiscountAmount;
    }

    /**
     * @param string $DiscountAmount
     * @return Omni_BasketLineCalcResponse
     */
    public function setDiscountAmount($DiscountAmount){
      $this->DiscountAmount = $DiscountAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getDiscountPercent(){
      return $this->DiscountPercent;
    }

    /**
     * @param string $DiscountPercent
     * @return Omni_BasketLineCalcResponse
     */
    public function setDiscountPercent($DiscountPercent){
      $this->DiscountPercent = $DiscountPercent;
      return $this;
    }

    /**
     * @return Omni_EntryStatus
     */
    public function getEntryStatus(){
      return $this->EntryStatus;
    }

    /**
     * @param Omni_EntryStatus $EntryStatus
     * @return Omni_BasketLineCalcResponse
     */
    public function setEntryStatus($EntryStatus){
      $this->EntryStatus = $EntryStatus;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_BasketLineCalcResponse
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return int
     */
    public function getLineNumber(){
      return $this->LineNumber;
    }

    /**
     * @param int $LineNumber
     * @return Omni_BasketLineCalcResponse
     */
    public function setLineNumber($LineNumber){
      $this->LineNumber = $LineNumber;
      return $this;
    }

    /**
     * @return Omni_LineType
     */
    public function getLineType(){
      return $this->LineType;
    }

    /**
     * @param Omni_LineType $LineType
     * @return Omni_BasketLineCalcResponse
     */
    public function setLineType($LineType){
      $this->LineType = $LineType;
      return $this;
    }

    /**
     * @return string
     */
    public function getManualDiscountAmount(){
      return $this->ManualDiscountAmount;
    }

    /**
     * @param string $ManualDiscountAmount
     * @return Omni_BasketLineCalcResponse
     */
    public function setManualDiscountAmount($ManualDiscountAmount){
      $this->ManualDiscountAmount = $ManualDiscountAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getManualDiscountPercent(){
      return $this->ManualDiscountPercent;
    }

    /**
     * @param string $ManualDiscountPercent
     * @return Omni_BasketLineCalcResponse
     */
    public function setManualDiscountPercent($ManualDiscountPercent){
      $this->ManualDiscountPercent = $ManualDiscountPercent;
      return $this;
    }

    /**
     * @return string
     */
    public function getManualPrice(){
      return $this->ManualPrice;
    }

    /**
     * @param string $ManualPrice
     * @return Omni_BasketLineCalcResponse
     */
    public function setManualPrice($ManualPrice){
      $this->ManualPrice = $ManualPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getNetAmount(){
      return $this->NetAmount;
    }

    /**
     * @param string $NetAmount
     * @return Omni_BasketLineCalcResponse
     */
    public function setNetAmount($NetAmount){
      $this->NetAmount = $NetAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getNetPrice(){
      return $this->NetPrice;
    }

    /**
     * @param string $NetPrice
     * @return Omni_BasketLineCalcResponse
     */
    public function setNetPrice($NetPrice){
      $this->NetPrice = $NetPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getPrice(){
      return $this->Price;
    }

    /**
     * @param string $Price
     * @return Omni_BasketLineCalcResponse
     */
    public function setPrice($Price){
      $this->Price = $Price;
      return $this;
    }

    /**
     * @return string
     */
    public function getQuantity(){
      return $this->Quantity;
    }

    /**
     * @param string $Quantity
     * @return Omni_BasketLineCalcResponse
     */
    public function setQuantity($Quantity){
      $this->Quantity = $Quantity;
      return $this;
    }

    /**
     * @return string
     */
    public function getTAXAmount(){
      return $this->TAXAmount;
    }

    /**
     * @param string $TAXAmount
     * @return Omni_BasketLineCalcResponse
     */
    public function setTAXAmount($TAXAmount){
      $this->TAXAmount = $TAXAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getTAXBusinessCode(){
      return $this->TAXBusinessCode;
    }

    /**
     * @param string $TAXBusinessCode
     * @return Omni_BasketLineCalcResponse
     */
    public function setTAXBusinessCode($TAXBusinessCode){
      $this->TAXBusinessCode = $TAXBusinessCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getTAXProductCode(){
      return $this->TAXProductCode;
    }

    /**
     * @param string $TAXProductCode
     * @return Omni_BasketLineCalcResponse
     */
    public function setTAXProductCode($TAXProductCode){
      $this->TAXProductCode = $TAXProductCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getUom(){
      return $this->Uom;
    }

    /**
     * @param string $Uom
     * @return Omni_BasketLineCalcResponse
     */
    public function setUom($Uom){
      $this->Uom = $Uom;
      return $this;
    }

    /**
     * @return int
     */
    public function getValidInTransaction(){
      return $this->ValidInTransaction;
    }

    /**
     * @param int $ValidInTransaction
     * @return Omni_BasketLineCalcResponse
     */
    public function setValidInTransaction($ValidInTransaction){
      $this->ValidInTransaction = $ValidInTransaction;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantId(){
      return $this->VariantId;
    }

    /**
     * @param string $VariantId
     * @return Omni_BasketLineCalcResponse
     */
    public function setVariantId($VariantId){
      $this->VariantId = $VariantId;
      return $this;
    }

}
