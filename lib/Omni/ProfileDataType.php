<?php

class Omni_ProfileDataType {
    const __default = 'Text';
    const Text = 'Text';
    const Integer = 'Integer';
    const Number = 'Number';
    const Date = 'Date';
    const Boolean = 'Boolean';


}
