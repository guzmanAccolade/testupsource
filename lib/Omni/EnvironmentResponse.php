<?php

class Omni_EnvironmentResponse {

    /**
     * @var Omni_Environment $EnvironmentResult
     * @access public
     */
    public $EnvironmentResult = null;

    /**
     * @param Omni_Environment $EnvironmentResult
     * @access public
     */
    public function __construct($EnvironmentResult = null){
      $this->EnvironmentResult = $EnvironmentResult;
    }

    /**
     * @return Omni_Environment
     */
    public function getEnvironmentResult(){
      return $this->EnvironmentResult;
    }

    /**
     * @param Omni_Environment $EnvironmentResult
     * @return Omni_EnvironmentResponse
     */
    public function setEnvironmentResult($EnvironmentResult){
      $this->EnvironmentResult = $EnvironmentResult;
      return $this;
    }

}
