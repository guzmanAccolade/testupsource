<?php

class Omni_DocumentType {
    const __default = 'Quote';
    const Quote = 'Quote';
    const Order = 'Order';
    const Invoice = 'Invoice';
    const CreditMemo = 'CreditMemo';
    const BlankOrder = 'BlankOrder';
    const ReturnOrder = 'ReturnOrder';


}
