<?php

class Omni_OrderCreateClickCollect {

    /**
     * @var Omni_OrderClickCollect $request
     * @access public
     */
    public $request = null;

    /**
     * @param Omni_OrderClickCollect $request
     * @access public
     */
    public function __construct($request = null){
      $this->request = $request;
    }

    /**
     * @return Omni_OrderClickCollect
     */
    public function getRequest(){
      return $this->request;
    }

    /**
     * @param Omni_OrderClickCollect $request
     * @return Omni_OrderCreateClickCollect
     */
    public function setRequest($request){
      $this->request = $request;
      return $this;
    }

}
