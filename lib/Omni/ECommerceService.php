<?php

class Omni_eCommerceService extends \SoapClient {

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'Environment' => '\Omni_Environment',
      'Currency' => '\Omni_Currency',
      'ImageView' => '\Omni_ImageView',
      'ImageSize' => '\Omni_ImageSize',
      'OneList' => '\Omni_OneList',
      'OneListCoupon' => '\Omni_OneListCoupon',
      'Coupon' => '\Omni_Coupon',
      'OneListItem' => '\Omni_OneListItem',
      'Item' => '\Omni_Item',
      'Price' => '\Omni_Price',
      'UOM' => '\Omni_UOM',
      'Variant' => '\Omni_Variant',
      'VariantExt' => '\Omni_VariantExt',
      'DimValue' => '\Omni_DimValue',
      'VariantRegistration' => '\Omni_VariantRegistration',
      'OneListOffer' => '\Omni_OneListOffer',
      'Offer' => '\Omni_Offer',
      'OneListPublishedOffer' => '\Omni_OneListPublishedOffer',
      'PublishedOffer' => '\Omni_PublishedOffer',
      'OfferDetails' => '\Omni_OfferDetails',
      'OrderAvailabilityRequest' => '\Omni_OrderAvailabilityRequest',
      'OrderLineAvailability' => '\Omni_OrderLineAvailability',
      'OrderClickCollect' => '\Omni_OrderClickCollect',
      'OrderDiscountLineCreateRequest' => '\Omni_OrderDiscountLineCreateRequest',
      'OrderLineCreateRequest' => '\Omni_OrderLineCreateRequest',
      'OrderSearchRequest' => '\Omni_OrderSearchRequest',
      'Contact' => '\Omni_Contact',
      'Account' => '\Omni_Account',
      'Scheme' => '\Omni_Scheme',
      'Club' => '\Omni_Club',
      'Address' => '\Omni_Address',
      'Card' => '\Omni_Card',
      'Device' => '\Omni_Device',
      'Transaction' => '\Omni_Transaction',
      'SaleLine' => '\Omni_SaleLine',
      'Store' => '\Omni_Store',
      'StoreHours' => '\Omni_StoreHours',
      'StoreServices' => '\Omni_StoreServices',
      'TaxLine' => '\Omni_TaxLine',
      'TenderLine' => '\Omni_TenderLine',
      'TransactionFooter' => '\Omni_TransactionFooter',
      'TransactionHeader' => '\Omni_TransactionHeader',
      'BasketCalcRequest' => '\Omni_BasketCalcRequest',
      'BasketCalcLineRequest' => '\Omni_BasketCalcLineRequest',
      'BasketCalcResponse' => '\Omni_BasketCalcResponse',
      'BasketLineCalcResponse' => '\Omni_BasketLineCalcResponse',
      'BasketLineDiscResponse' => '\Omni_BasketLineDiscResponse',
      'BasketPostSaleRequest' => '\Omni_BasketPostSaleRequest',
      'BasketPostSaleDiscLineRequest' => '\Omni_BasketPostSaleDiscLineRequest',
      'BasketPostSaleLineRequest' => '\Omni_BasketPostSaleLineRequest',
      'TenderPaymentLine' => '\Omni_TenderPaymentLine',
      'BasketPostSaleResponse' => '\Omni_BasketPostSaleResponse',
      'OrderStatusResponse' => '\Omni_OrderStatusResponse',
      'EnvironmentResponse' => '\Omni_EnvironmentResponse',
      'Ping' => '\Omni_Ping',
      'PingResponse' => '\Omni_PingResponse',
      'PingStatus' => '\Omni_PingStatus',
      'PingStatusResponse' => '\Omni_PingStatusResponse',
      'Version' => '\Omni_Version',
      'VersionResponse' => '\Omni_VersionResponse',
      'NotificationsGetByContactId' => '\Omni_NotificationsGetByContactId',
      'NotificationsGetByContactIdResponse' => '\Omni_NotificationsGetByContactIdResponse',
      'NotificationsUpdateStatus' => '\Omni_NotificationsUpdateStatus',
      'NotificationsUpdateStatusResponse' => '\Omni_NotificationsUpdateStatusResponse',
      'OneListDeleteById' => '\Omni_OneListDeleteById',
      'OneListDeleteByIdResponse' => '\Omni_OneListDeleteByIdResponse',
      'OneListGetByContactId' => '\Omni_OneListGetByContactId',
      'OneListGetByContactIdResponse' => '\Omni_OneListGetByContactIdResponse',
      'OneListSave' => '\Omni_OneListSave',
      'OneListSaveResponse' => '\Omni_OneListSaveResponse',
      'OrderAvailabilityCheck' => '\Omni_OrderAvailabilityCheck',
      'OrderAvailabilityCheckResponse' => '\Omni_OrderAvailabilityCheckResponse',
      'OrderCreateClickCollect' => '\Omni_OrderCreateClickCollect',
      'OrderCreateClickCollectResponse' => '\Omni_OrderCreateClickCollectResponse',
      'OrderSearchClickCollect' => '\Omni_OrderSearchClickCollect',
      'OrderSearchClickCollectResponse' => '\Omni_OrderSearchClickCollectResponse',
      'ChangePassword' => '\Omni_ChangePassword',
      'ChangePasswordResponse' => '\Omni_ChangePasswordResponse',
      'ContactCreate' => '\Omni_ContactCreate',
      'ContactCreateResponse' => '\Omni_ContactCreateResponse',
      'ContactGetByAlternateId' => '\Omni_ContactGetByAlternateId',
      'ContactGetByAlternateIdResponse' => '\Omni_ContactGetByAlternateIdResponse',
      'ContactGetById' => '\Omni_ContactGetById',
      'ContactGetByIdResponse' => '\Omni_ContactGetByIdResponse',
      'ContactGetPointBalance' => '\Omni_ContactGetPointBalance',
      'ContactGetPointBalanceResponse' => '\Omni_ContactGetPointBalanceResponse',
      'GetPointRate' => '\Omni_GetPointRate',
      'GetPointRateResponse' => '\Omni_GetPointRateResponse',
      'ContactSearch' => '\Omni_ContactSearch',
      'ContactSearchResponse' => '\Omni_ContactSearchResponse',
      'ContactUpdate' => '\Omni_ContactUpdate',
      'ContactUpdateResponse' => '\Omni_ContactUpdateResponse',
      'ForgotPassword' => '\Omni_ForgotPassword',
      'ForgotPasswordResponse' => '\Omni_ForgotPasswordResponse',
      'ForgotPasswordEx' => '\Omni_ForgotPasswordEx',
      'ForgotPasswordExResponse' => '\Omni_ForgotPasswordExResponse',
      'Login' => '\Omni_Login',
      'LoginResponse' => '\Omni_LoginResponse',
      'LoginWeb' => '\Omni_LoginWeb',
      'LoginWebResponse' => '\Omni_LoginWebResponse',
      'Logout' => '\Omni_Logout',
      'LogoutResponse' => '\Omni_LogoutResponse',
      'ResetPassword' => '\Omni_ResetPassword',
      'ResetPasswordResponse' => '\Omni_ResetPasswordResponse',
      'TransactionGetById' => '\Omni_TransactionGetById',
      'TransactionGetByIdResponse' => '\Omni_TransactionGetByIdResponse',
      'TransactionHeadersGetByContactId' => '\Omni_TransactionHeadersGetByContactId',
      'TransactionHeadersGetByContactIdResponse' => '\Omni_TransactionHeadersGetByContactIdResponse',
      'ItemPriceCheck' => '\Omni_ItemPriceCheck',
      'ItemPriceCheckResponse' => '\Omni_ItemPriceCheckResponse',
      'ItemsInStockGet' => '\Omni_ItemsInStockGet',
      'ItemsInStockGetResponse' => '\Omni_ItemsInStockGetResponse',
      'ItemGetById' => '\Omni_ItemGetById',
      'ItemGetByIdResponse' => '\Omni_ItemGetByIdResponse',
      'ProfilesGetAll' => '\Omni_ProfilesGetAll',
      'ProfilesGetAllResponse' => '\Omni_ProfilesGetAllResponse',
      'ProfilesGetByContactId' => '\Omni_ProfilesGetByContactId',
      'ProfilesGetByContactIdResponse' => '\Omni_ProfilesGetByContactIdResponse',
      'PublishedOffersGetByCardId' => '\Omni_PublishedOffersGetByCardId',
      'PublishedOffersGetByCardIdResponse' => '\Omni_PublishedOffersGetByCardIdResponse',
      'BasketCalc' => '\Omni_BasketCalc',
      'BasketPostSale' => '\Omni_BasketPostSale',
      'OrderStatusCheck' => '\Omni_OrderStatusCheck',
      'OrderStatusCheckResponse' => '\Omni_OrderStatusCheckResponse',
      'ImageGetById' => '\Omni_ImageGetById',
      'ImageGetByIdResponse' => '\Omni_ImageGetByIdResponse',
      'ImageStreamGetById' => '\Omni_ImageStreamGetById',
      'ImageStreamGetByIdResponse' => '\Omni_ImageStreamGetByIdResponse',
      'AppSettingsGetByKey' => '\Omni_AppSettingsGetByKey',
      'AppSettingsGetByKeyResponse' => '\Omni_AppSettingsGetByKeyResponse',
      'AccountGetById' => '\Omni_AccountGetById',
      'AccountGetByIdResponse' => '\Omni_AccountGetByIdResponse',
      'SchemesGetAll' => '\Omni_SchemesGetAll',
      'SchemesGetAllResponse' => '\Omni_SchemesGetAllResponse',
      'StoreGetById' => '\Omni_StoreGetById',
      'StoreGetByIdResponse' => '\Omni_StoreGetByIdResponse',
      'StoresGetAll' => '\Omni_StoresGetAll',
      'StoresGetAllResponse' => '\Omni_StoresGetAllResponse',
      'StoresGetByCoordinates' => '\Omni_StoresGetByCoordinates',
      'StoresGetByCoordinatesResponse' => '\Omni_StoresGetByCoordinatesResponse',
      'StoresGetbyItemInStock' => '\Omni_StoresGetbyItemInStock',
      'StoresGetbyItemInStockResponse' => '\Omni_StoresGetbyItemInStockResponse',
      'ReplEcommBarcodes' => '\Omni_ReplEcommBarcodes',
      'ReplEcommBarcodesResponse' => '\Omni_ReplEcommBarcodesResponse',
      'ReplEcommCurrency' => '\Omni_ReplEcommCurrency',
      'ReplEcommCurrencyResponse' => '\Omni_ReplEcommCurrencyResponse',
      'ReplEcommExtendedVariants' => '\Omni_ReplEcommExtendedVariants',
      'ReplEcommExtendedVariantsResponse' => '\Omni_ReplEcommExtendedVariantsResponse',
      'ReplEcommImageLinks' => '\Omni_ReplEcommImageLinks',
      'ReplEcommImageLinksResponse' => '\Omni_ReplEcommImageLinksResponse',
      'ReplEcommImages' => '\Omni_ReplEcommImages',
      'ReplEcommImagesResponse' => '\Omni_ReplEcommImagesResponse',
      'ReplEcommItemCategories' => '\Omni_ReplEcommItemCategories',
      'ReplEcommItemCategoriesResponse' => '\Omni_ReplEcommItemCategoriesResponse',
      'ReplEcommItems' => '\Omni_ReplEcommItems',
      'ReplEcommItemsResponse' => '\Omni_ReplEcommItemsResponse',
      'ReplEcommItemUnitOfMeasures' => '\Omni_ReplEcommItemUnitOfMeasures',
      'ReplEcommItemUnitOfMeasuresResponse' => '\Omni_ReplEcommItemUnitOfMeasuresResponse',
      'ReplEcommItemVariantRegistrations' => '\Omni_ReplEcommItemVariantRegistrations',
      'ReplEcommItemVariantRegistrationsResponse' => '\Omni_ReplEcommItemVariantRegistrationsResponse',
      'ReplEcommItemVendor' => '\Omni_ReplEcommItemVendor',
      'ReplEcommItemVendorResponse' => '\Omni_ReplEcommItemVendorResponse',
      'ReplEcommPrices' => '\Omni_ReplEcommPrices',
      'ReplEcommPricesResponse' => '\Omni_ReplEcommPricesResponse',
      'ReplEcommProductGroups' => '\Omni_ReplEcommProductGroups',
      'ReplEcommProductGroupsResponse' => '\Omni_ReplEcommProductGroupsResponse',
      'ReplEcommStores' => '\Omni_ReplEcommStores',
      'ReplEcommStoresResponse' => '\Omni_ReplEcommStoresResponse',
      'ReplEcommUnitOfMeasures' => '\Omni_ReplEcommUnitOfMeasures',
      'ReplEcommUnitOfMeasuresResponse' => '\Omni_ReplEcommUnitOfMeasuresResponse',
      'ReplEcommVendor' => '\Omni_ReplEcommVendor',
      'ReplEcommVendorResponse' => '\Omni_ReplEcommVendorResponse',
      'Notification' => '\Omni_Notification',
      'Profile' => '\Omni_Profile',
      'ExtraInfoLine' => '\Omni_ExtraInfoLine',
      'ContactPOS' => '\Omni_ContactPOS',
      'CouponDetails' => '\Omni_CouponDetails',
      'ItemPriceCheckRequest' => '\Omni_ItemPriceCheckRequest',
      'ItemPriceCheckLineRequest' => '\Omni_ItemPriceCheckLineRequest',
      'ItemPriceCheckLineResponse' => '\Omni_ItemPriceCheckLineResponse',
      'Inventory' => '\Omni_Inventory',
      'ReceiptInfo' => '\Omni_ReceiptInfo',
      'ReplRequest' => '\Omni_ReplRequest',
      'ReplBarcodeResponse' => '\Omni_ReplBarcodeResponse',
      'Barcode' => '\Omni_Barcode',
      'ReplCurrencyResponse' => '\Omni_ReplCurrencyResponse',
      'ReplExtendedVariantResponse' => '\Omni_ReplExtendedVariantResponse',
      'ExtendedVariantValue' => '\Omni_ExtendedVariantValue',
      'ReplImageLinkResponse' => '\Omni_ReplImageLinkResponse',
      'ImageLink' => '\Omni_ImageLink',
      'ReplImageResponse' => '\Omni_ReplImageResponse',
      'Image' => '\Omni_Image',
      'ReplItemCategoryResponse' => '\Omni_ReplItemCategoryResponse',
      'ItemCategory' => '\Omni_ItemCategory',
      'ReplItemResponse' => '\Omni_ReplItemResponse',
      'ReplItemUOMResponse' => '\Omni_ReplItemUOMResponse',
      'ItemUOM' => '\Omni_ItemUOM',
      'ReplItemVariantRegistrationResponse' => '\Omni_ReplItemVariantRegistrationResponse',
      'ItemVariantRegistration' => '\Omni_ItemVariantRegistration',
      'ReplItemVendorResponse' => '\Omni_ReplItemVendorResponse',
      'ItemVendor' => '\Omni_ItemVendor',
      'ReplPriceResponse' => '\Omni_ReplPriceResponse',
      'ReplProductGroupResponse' => '\Omni_ReplProductGroupResponse',
      'ProductGroup' => '\Omni_ProductGroup',
      'ReplStoreResponse' => '\Omni_ReplStoreResponse',
      'ReplUnitOfMeasureResponse' => '\Omni_ReplUnitOfMeasureResponse',
      'UnitOfMeasure' => '\Omni_UnitOfMeasure',
      'ReplVendorResponse' => '\Omni_ReplVendorResponse',
      'Vendor' => '\Omni_Vendor');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = '../eCommerceService.svc.singlewsdl'){
      foreach (self::$classmap as $key => $value) {
    if (!isset($options['classmap'][$key])) {
      $options['classmap'][$key] = $value;
    }
  }

  parent::__construct($wsdl, $options);
    }

    /**
     * @param Omni_Environment $parameters
     * @access public
     * @return Omni_EnvironmentResponse
     */
    public function Environment($parameters){
      return $this->__soapCall('Environment', array($parameters));
    }

    /**
     * @param Omni_Ping $parameters
     * @access public
     * @return Omni_PingResponse
     */
    public function Ping($parameters){
      return $this->__soapCall('Ping', array($parameters));
    }

    /**
     * @param Omni_PingStatus $parameters
     * @access public
     * @return Omni_PingStatusResponse
     */
    public function PingStatus($parameters){
      return $this->__soapCall('PingStatus', array($parameters));
    }

    /**
     * @param Omni_Version $parameters
     * @access public
     * @return Omni_VersionResponse
     */
    public function Version($parameters){
      return $this->__soapCall('Version', array($parameters));
    }

    /**
     * @param Omni_NotificationsGetByContactId $parameters
     * @access public
     * @return Omni_NotificationsGetByContactIdResponse
     */
    public function NotificationsGetByContactId($parameters){
      return $this->__soapCall('NotificationsGetByContactId', array($parameters));
    }

    /**
     * @param Omni_NotificationsUpdateStatus $parameters
     * @access public
     * @return Omni_NotificationsUpdateStatusResponse
     */
    public function NotificationsUpdateStatus($parameters){
      return $this->__soapCall('NotificationsUpdateStatus', array($parameters));
    }

    /**
     * @param Omni_OneListDeleteById $parameters
     * @access public
     * @return Omni_OneListDeleteByIdResponse
     */
    public function OneListDeleteById($parameters){
      return $this->__soapCall('OneListDeleteById', array($parameters));
    }

    /**
     * @param Omni_OneListGetByContactId $parameters
     * @access public
     * @return Omni_OneListGetByContactIdResponse
     */
    public function OneListGetByContactId($parameters){
      return $this->__soapCall('OneListGetByContactId', array($parameters));
    }

    /**
     * @param Omni_OneListSave $parameters
     * @access public
     * @return Omni_OneListSaveResponse
     */
    public function OneListSave($parameters){
      return $this->__soapCall('OneListSave', array($parameters));
    }

    /**
     * @param Omni_OrderAvailabilityCheck $parameters
     * @access public
     * @return Omni_OrderAvailabilityCheckResponse
     */
    public function OrderAvailabilityCheck($parameters){
      return $this->__soapCall('OrderAvailabilityCheck', array($parameters));
    }

    /**
     * @param Omni_OrderCreateClickCollect $parameters
     * @access public
     * @return Omni_OrderCreateClickCollectResponse
     */
    public function OrderCreateClickCollect($parameters){
      return $this->__soapCall('OrderCreateClickCollect', array($parameters));
    }

    /**
     * @param Omni_OrderSearchClickCollect $parameters
     * @access public
     * @return Omni_OrderSearchClickCollectResponse
     */
    public function OrderSearchClickCollect($parameters){
      return $this->__soapCall('OrderSearchClickCollect', array($parameters));
    }

    /**
     * @param Omni_ChangePassword $parameters
     * @access public
     * @return Omni_ChangePasswordResponse
     */
    public function ChangePassword($parameters){
      return $this->__soapCall('ChangePassword', array($parameters));
    }

    /**
     * @param Omni_ContactCreate $parameters
     * @access public
     * @return Omni_ContactCreateResponse
     */
    public function ContactCreate($parameters){
      return $this->__soapCall('ContactCreate', array($parameters));
    }

    /**
     * @param Omni_ContactGetByAlternateId $parameters
     * @access public
     * @return Omni_ContactGetByAlternateIdResponse
     */
    public function ContactGetByAlternateId($parameters){
      return $this->__soapCall('ContactGetByAlternateId', array($parameters));
    }

    /**
     * @param Omni_ContactGetById $parameters
     * @access public
     * @return Omni_ContactGetByIdResponse
     */
    public function ContactGetById($parameters){
      return $this->__soapCall('ContactGetById', array($parameters));
    }

    /**
     * @param Omni_ContactGetPointBalance $parameters
     * @access public
     * @return Omni_ContactGetPointBalanceResponse
     */
    public function ContactGetPointBalance($parameters){
      return $this->__soapCall('ContactGetPointBalance', array($parameters));
    }

    /**
     * @param Omni_GetPointRate $parameters
     * @access public
     * @return Omni_GetPointRateResponse
     */
    public function GetPointRate($parameters){
      return $this->__soapCall('GetPointRate', array($parameters));
    }

    /**
     * @param Omni_ContactSearch $parameters
     * @access public
     * @return Omni_ContactSearchResponse
     */
    public function ContactSearch($parameters){
      return $this->__soapCall('ContactSearch', array($parameters));
    }

    /**
     * @param Omni_ContactUpdate $parameters
     * @access public
     * @return Omni_ContactUpdateResponse
     */
    public function ContactUpdate($parameters){
      return $this->__soapCall('ContactUpdate', array($parameters));
    }

    /**
     * @param Omni_ForgotPassword $parameters
     * @access public
     * @return Omni_ForgotPasswordResponse
     */
    public function ForgotPassword($parameters){
      return $this->__soapCall('ForgotPassword', array($parameters));
    }

    /**
     * @param Omni_ForgotPasswordEx $parameters
     * @access public
     * @return Omni_ForgotPasswordExResponse
     */
    public function ForgotPasswordEx($parameters){
      return $this->__soapCall('ForgotPasswordEx', array($parameters));
    }

    /**
     * @param Omni_Login $parameters
     * @access public
     * @return Omni_LoginResponse
     */
    public function Login($parameters){
      return $this->__soapCall('Login', array($parameters));
    }

    /**
     * @param Omni_LoginWeb $parameters
     * @access public
     * @return Omni_LoginWebResponse
     */
    public function LoginWeb($parameters){
      return $this->__soapCall('LoginWeb', array($parameters));
    }

    /**
     * @param Omni_Logout $parameters
     * @access public
     * @return Omni_LogoutResponse
     */
    public function Logout($parameters){
      return $this->__soapCall('Logout', array($parameters));
    }

    /**
     * @param Omni_ResetPassword $parameters
     * @access public
     * @return Omni_ResetPasswordResponse
     */
    public function ResetPassword($parameters){
      return $this->__soapCall('ResetPassword', array($parameters));
    }

    /**
     * @param Omni_TransactionGetById $parameters
     * @access public
     * @return Omni_TransactionGetByIdResponse
     */
    public function TransactionGetById($parameters){
      return $this->__soapCall('TransactionGetById', array($parameters));
    }

    /**
     * @param Omni_TransactionHeadersGetByContactId $parameters
     * @access public
     * @return Omni_TransactionHeadersGetByContactIdResponse
     */
    public function TransactionHeadersGetByContactId($parameters){
      return $this->__soapCall('TransactionHeadersGetByContactId', array($parameters));
    }

    /**
     * @param Omni_ItemPriceCheck $parameters
     * @access public
     * @return Omni_ItemPriceCheckResponse
     */
    public function ItemPriceCheck($parameters){
      return $this->__soapCall('ItemPriceCheck', array($parameters));
    }

    /**
     * @param Omni_ItemsInStockGet $parameters
     * @access public
     * @return Omni_ItemsInStockGetResponse
     */
    public function ItemsInStockGet($parameters){
      return $this->__soapCall('ItemsInStockGet', array($parameters));
    }

    /**
     * @param Omni_ItemGetById $parameters
     * @access public
     * @return Omni_ItemGetByIdResponse
     */
    public function ItemGetById($parameters){
      return $this->__soapCall('ItemGetById', array($parameters));
    }

    /**
     * @param Omni_ProfilesGetAll $parameters
     * @access public
     * @return Omni_ProfilesGetAllResponse
     */
    public function ProfilesGetAll($parameters){
      return $this->__soapCall('ProfilesGetAll', array($parameters));
    }

    /**
     * @param Omni_ProfilesGetByContactId $parameters
     * @access public
     * @return Omni_ProfilesGetByContactIdResponse
     */
    public function ProfilesGetByContactId($parameters){
      return $this->__soapCall('ProfilesGetByContactId', array($parameters));
    }

    /**
     * @param Omni_PublishedOffersGetByCardId $parameters
     * @access public
     * @return Omni_PublishedOffersGetByCardIdResponse
     */
    public function PublishedOffersGetByCardId($parameters){
      return $this->__soapCall('PublishedOffersGetByCardId', array($parameters));
    }

    /**
     * @param Omni_BasketCalc $parameters
     * @access public
     * @return Omni_BasketCalcResponse
     */
    public function BasketCalc($parameters){
      return $this->__soapCall('BasketCalc', array($parameters));
    }

    /**
     * @param Omni_BasketPostSale $parameters
     * @access public
     * @return Omni_BasketPostSaleResponse
     */
    public function BasketPostSale($parameters){
      return $this->__soapCall('BasketPostSale', array($parameters));
    }

    /**
     * @param Omni_OrderStatusCheck $parameters
     * @access public
     * @return Omni_OrderStatusCheckResponse
     */
    public function OrderStatusCheck($parameters){
      return $this->__soapCall('OrderStatusCheck', array($parameters));
    }

    /**
     * @param Omni_ImageGetById $parameters
     * @access public
     * @return Omni_ImageGetByIdResponse
     */
    public function ImageGetById($parameters){
      return $this->__soapCall('ImageGetById', array($parameters));
    }

    /**
     * @param Omni_ImageStreamGetById $parameters
     * @access public
     * @return Omni_ImageStreamGetByIdResponse
     */
    public function ImageStreamGetById($parameters){
      return $this->__soapCall('ImageStreamGetById', array($parameters));
    }

    /**
     * @param Omni_AppSettingsGetByKey $parameters
     * @access public
     * @return Omni_AppSettingsGetByKeyResponse
     */
    public function AppSettingsGetByKey($parameters){
      return $this->__soapCall('AppSettingsGetByKey', array($parameters));
    }

    /**
     * @param Omni_AccountGetById $parameters
     * @access public
     * @return Omni_AccountGetByIdResponse
     */
    public function AccountGetById($parameters){
      return $this->__soapCall('AccountGetById', array($parameters));
    }

    /**
     * @param Omni_SchemesGetAll $parameters
     * @access public
     * @return Omni_SchemesGetAllResponse
     */
    public function SchemesGetAll($parameters){
      return $this->__soapCall('SchemesGetAll', array($parameters));
    }

    /**
     * @param Omni_StoreGetById $parameters
     * @access public
     * @return Omni_StoreGetByIdResponse
     */
    public function StoreGetById($parameters){
      return $this->__soapCall('StoreGetById', array($parameters));
    }

    /**
     * @param Omni_StoresGetAll $parameters
     * @access public
     * @return Omni_StoresGetAllResponse
     */
    public function StoresGetAll($parameters){
      return $this->__soapCall('StoresGetAll', array($parameters));
    }

    /**
     * @param Omni_StoresGetByCoordinates $parameters
     * @access public
     * @return Omni_StoresGetByCoordinatesResponse
     */
    public function StoresGetByCoordinates($parameters){
      return $this->__soapCall('StoresGetByCoordinates', array($parameters));
    }

    /**
     * @param Omni_StoresGetbyItemInStock $parameters
     * @access public
     * @return Omni_StoresGetbyItemInStockResponse
     */
    public function StoresGetbyItemInStock($parameters){
      return $this->__soapCall('StoresGetbyItemInStock', array($parameters));
    }

    /**
     * @param Omni_ReplEcommBarcodes $parameters
     * @access public
     * @return Omni_ReplEcommBarcodesResponse
     */
    public function ReplEcommBarcodes($parameters){
      return $this->__soapCall('ReplEcommBarcodes', array($parameters));
    }

    /**
     * @param Omni_ReplEcommCurrency $parameters
     * @access public
     * @return Omni_ReplEcommCurrencyResponse
     */
    public function ReplEcommCurrency($parameters){
      return $this->__soapCall('ReplEcommCurrency', array($parameters));
    }

    /**
     * @param Omni_ReplEcommExtendedVariants $parameters
     * @access public
     * @return Omni_ReplEcommExtendedVariantsResponse
     */
    public function ReplEcommExtendedVariants($parameters){
      return $this->__soapCall('ReplEcommExtendedVariants', array($parameters));
    }

    /**
     * @param Omni_ReplEcommImageLinks $parameters
     * @access public
     * @return Omni_ReplEcommImageLinksResponse
     */
    public function ReplEcommImageLinks($parameters){
      return $this->__soapCall('ReplEcommImageLinks', array($parameters));
    }

    /**
     * @param Omni_ReplEcommImages $parameters
     * @access public
     * @return Omni_ReplEcommImagesResponse
     */
    public function ReplEcommImages($parameters){
      return $this->__soapCall('ReplEcommImages', array($parameters));
    }

    /**
     * @param Omni_ReplEcommItemCategories $parameters
     * @access public
     * @return Omni_ReplEcommItemCategoriesResponse
     */
    public function ReplEcommItemCategories($parameters){
      return $this->__soapCall('ReplEcommItemCategories', array($parameters));
    }

    /**
     * @param Omni_ReplEcommItems $parameters
     * @access public
     * @return Omni_ReplEcommItemsResponse
     */
    public function ReplEcommItems($parameters){
      return $this->__soapCall('ReplEcommItems', array($parameters));
    }

    /**
     * @param Omni_ReplEcommItemUnitOfMeasures $parameters
     * @access public
     * @return Omni_ReplEcommItemUnitOfMeasuresResponse
     */
    public function ReplEcommItemUnitOfMeasures($parameters){
      return $this->__soapCall('ReplEcommItemUnitOfMeasures', array($parameters));
    }

    /**
     * @param Omni_ReplEcommItemVariantRegistrations $parameters
     * @access public
     * @return Omni_ReplEcommItemVariantRegistrationsResponse
     */
    public function ReplEcommItemVariantRegistrations($parameters){
      return $this->__soapCall('ReplEcommItemVariantRegistrations', array($parameters));
    }

    /**
     * @param Omni_ReplEcommItemVendor $parameters
     * @access public
     * @return Omni_ReplEcommItemVendorResponse
     */
    public function ReplEcommItemVendor($parameters){
      return $this->__soapCall('ReplEcommItemVendor', array($parameters));
    }

    /**
     * @param Omni_ReplEcommPrices $parameters
     * @access public
     * @return Omni_ReplEcommPricesResponse
     */
    public function ReplEcommPrices($parameters){
      return $this->__soapCall('ReplEcommPrices', array($parameters));
    }

    /**
     * @param Omni_ReplEcommProductGroups $parameters
     * @access public
     * @return Omni_ReplEcommProductGroupsResponse
     */
    public function ReplEcommProductGroups($parameters){
      return $this->__soapCall('ReplEcommProductGroups', array($parameters));
    }

    /**
     * @param Omni_ReplEcommStores $parameters
     * @access public
     * @return Omni_ReplEcommStoresResponse
     */
    public function ReplEcommStores($parameters){
      return $this->__soapCall('ReplEcommStores', array($parameters));
    }

    /**
     * @param Omni_ReplEcommUnitOfMeasures $parameters
     * @access public
     * @return Omni_ReplEcommUnitOfMeasuresResponse
     */
    public function ReplEcommUnitOfMeasures($parameters){
      return $this->__soapCall('ReplEcommUnitOfMeasures', array($parameters));
    }

    /**
     * @param Omni_ReplEcommVendor $parameters
     * @access public
     * @return Omni_ReplEcommVendorResponse
     */
    public function ReplEcommVendor($parameters){
      return $this->__soapCall('ReplEcommVendor', array($parameters));
    }

}
