<?php

class Omni_NotificationsUpdateStatusResponse {

    /**
     * @var boolean $NotificationsUpdateStatusResult
     * @access public
     */
    public $NotificationsUpdateStatusResult = null;

    /**
     * @param boolean $NotificationsUpdateStatusResult
     * @access public
     */
    public function __construct($NotificationsUpdateStatusResult = null){
      $this->NotificationsUpdateStatusResult = $NotificationsUpdateStatusResult;
    }

    /**
     * @return boolean
     */
    public function getNotificationsUpdateStatusResult(){
      return $this->NotificationsUpdateStatusResult;
    }

    /**
     * @param boolean $NotificationsUpdateStatusResult
     * @return Omni_NotificationsUpdateStatusResponse
     */
    public function setNotificationsUpdateStatusResult($NotificationsUpdateStatusResult){
      $this->NotificationsUpdateStatusResult = $NotificationsUpdateStatusResult;
      return $this;
    }

}
