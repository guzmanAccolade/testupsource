<?php

class Omni_ExtraInfoLine {

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @access public
     */
    public function __construct(){
    
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_ExtraInfoLine
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

}
