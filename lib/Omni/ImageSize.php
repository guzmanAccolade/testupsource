<?php

class Omni_ImageSize {

    /**
     * @var int $Height
     * @access public
     */
    public $Height = null;

    /**
     * @var int $Width
     * @access public
     */
    public $Width = null;

    /**
     * @param int $Height
     * @param int $Width
     * @access public
     */
    public function __construct($Height = null, $Width = null){
      $this->Height = $Height;
      $this->Width = $Width;
    }

    /**
     * @return int
     */
    public function getHeight(){
      return $this->Height;
    }

    /**
     * @param int $Height
     * @return Omni_ImageSize
     */
    public function setHeight($Height){
      $this->Height = $Height;
      return $this;
    }

    /**
     * @return int
     */
    public function getWidth(){
      return $this->Width;
    }

    /**
     * @param int $Width
     * @return Omni_ImageSize
     */
    public function setWidth($Width){
      $this->Width = $Width;
      return $this;
    }

}
