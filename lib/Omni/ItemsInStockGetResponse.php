<?php

class Omni_ItemsInStockGetResponse {

    /**
     * @var Omni_Inventory[] $ItemsInStockGetResult
     * @access public
     */
    public $ItemsInStockGetResult = null;

    /**
     * @param Omni_Inventory[] $ItemsInStockGetResult
     * @access public
     */
    public function __construct($ItemsInStockGetResult = null){
      $this->ItemsInStockGetResult = $ItemsInStockGetResult;
    }

    /**
     * @return Omni_Inventory[]
     */
    public function getItemsInStockGetResult(){
      return $this->ItemsInStockGetResult;
    }

    /**
     * @param Omni_Inventory[] $ItemsInStockGetResult
     * @return Omni_ItemsInStockGetResponse
     */
    public function setItemsInStockGetResult($ItemsInStockGetResult){
      $this->ItemsInStockGetResult = $ItemsInStockGetResult;
      return $this;
    }

}
