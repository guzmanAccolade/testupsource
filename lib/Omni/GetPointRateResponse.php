<?php

class Omni_GetPointRateResponse {

    /**
     * @var float $GetPointRateResult
     * @access public
     */
    public $GetPointRateResult = null;

    /**
     * @param float $GetPointRateResult
     * @access public
     */
    public function __construct($GetPointRateResult = null){
      $this->GetPointRateResult = $GetPointRateResult;
    }

    /**
     * @return float
     */
    public function getGetPointRateResult(){
      return $this->GetPointRateResult;
    }

    /**
     * @param float $GetPointRateResult
     * @return Omni_GetPointRateResponse
     */
    public function setGetPointRateResult($GetPointRateResult){
      $this->GetPointRateResult = $GetPointRateResult;
      return $this;
    }

}
