<?php

class Omni_OneListItem {

    /**
     * @var string $BarcodeId
     * @access public
     */
    public $BarcodeId = null;

    /**
     * @var dateTime $CreateDate
     * @access public
     */
    public $CreateDate = null;

    /**
     * @var int $DisplayOrderId
     * @access public
     */
    public $DisplayOrderId = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var Omni_Item $Item
     * @access public
     */
    public $Item = null;

    /**
     * @var float $Quantity
     * @access public
     */
    public $Quantity = null;

    /**
     * @var Omni_UOM $Uom
     * @access public
     */
    public $Uom = null;

    /**
     * @var Omni_Variant $Variant
     * @access public
     */
    public $Variant = null;

    /**
     * @param dateTime $CreateDate
     * @param int $DisplayOrderId
     * @param float $Quantity
     * @access public
     */
    public function __construct($CreateDate = null, $DisplayOrderId = null, $Quantity = null){
      $this->CreateDate = $CreateDate;
      $this->DisplayOrderId = $DisplayOrderId;
      $this->Quantity = $Quantity;
    }

    /**
     * @return string
     */
    public function getBarcodeId(){
      return $this->BarcodeId;
    }

    /**
     * @param string $BarcodeId
     * @return Omni_OneListItem
     */
    public function setBarcodeId($BarcodeId){
      $this->BarcodeId = $BarcodeId;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getCreateDate(){
      return $this->CreateDate;
    }

    /**
     * @param dateTime $CreateDate
     * @return Omni_OneListItem
     */
    public function setCreateDate($CreateDate){
      $this->CreateDate = $CreateDate;
      return $this;
    }

    /**
     * @return int
     */
    public function getDisplayOrderId(){
      return $this->DisplayOrderId;
    }

    /**
     * @param int $DisplayOrderId
     * @return Omni_OneListItem
     */
    public function setDisplayOrderId($DisplayOrderId){
      $this->DisplayOrderId = $DisplayOrderId;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_OneListItem
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return Omni_Item
     */
    public function getItem(){
      return $this->Item;
    }

    /**
     * @param Omni_Item $Item
     * @return Omni_OneListItem
     */
    public function setItem($Item){
      $this->Item = $Item;
      return $this;
    }

    /**
     * @return float
     */
    public function getQuantity(){
      return $this->Quantity;
    }

    /**
     * @param float $Quantity
     * @return Omni_OneListItem
     */
    public function setQuantity($Quantity){
      $this->Quantity = $Quantity;
      return $this;
    }

    /**
     * @return Omni_UOM
     */
    public function getUom(){
      return $this->Uom;
    }

    /**
     * @param Omni_UOM $Uom
     * @return Omni_OneListItem
     */
    public function setUom($Uom){
      $this->Uom = $Uom;
      return $this;
    }

    /**
     * @return Omni_Variant
     */
    public function getVariant(){
      return $this->Variant;
    }

    /**
     * @param Omni_Variant $Variant
     * @return Omni_OneListItem
     */
    public function setVariant($Variant){
      $this->Variant = $Variant;
      return $this;
    }

}
