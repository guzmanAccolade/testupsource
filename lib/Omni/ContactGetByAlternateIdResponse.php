<?php

class Omni_ContactGetByAlternateIdResponse {

    /**
     * @var Omni_Contact $ContactGetByAlternateIdResult
     * @access public
     */
    public $ContactGetByAlternateIdResult = null;

    /**
     * @param Omni_Contact $ContactGetByAlternateIdResult
     * @access public
     */
    public function __construct($ContactGetByAlternateIdResult = null){
      $this->ContactGetByAlternateIdResult = $ContactGetByAlternateIdResult;
    }

    /**
     * @return Omni_Contact
     */
    public function getContactGetByAlternateIdResult(){
      return $this->ContactGetByAlternateIdResult;
    }

    /**
     * @param Omni_Contact $ContactGetByAlternateIdResult
     * @return Omni_ContactGetByAlternateIdResponse
     */
    public function setContactGetByAlternateIdResult($ContactGetByAlternateIdResult){
      $this->ContactGetByAlternateIdResult = $ContactGetByAlternateIdResult;
      return $this;
    }

}
