<?php

class Omni_OrderAvailabilityRequest {

    /**
     * @var string $CardId
     * @access public
     */
    public $CardId = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var Omni_ItemNumberType $ItemNumberType
     * @access public
     */
    public $ItemNumberType = null;

    /**
     * @var Omni_OrderLineAvailability[] $OrderLineAvailabilityRequests
     * @access public
     */
    public $OrderLineAvailabilityRequests = null;

    /**
     * @var Omni_SourceType $SourceType
     * @access public
     */
    public $SourceType = null;

    /**
     * @var string $StoreId
     * @access public
     */
    public $StoreId = null;

    /**
     * @param Omni_ItemNumberType $ItemNumberType
     * @param Omni_SourceType $SourceType
     * @access public
     */
    public function __construct($ItemNumberType = null, $SourceType = null){
      $this->ItemNumberType = $ItemNumberType;
      $this->SourceType = $SourceType;
    }

    /**
     * @return string
     */
    public function getCardId(){
      return $this->CardId;
    }

    /**
     * @param string $CardId
     * @return Omni_OrderAvailabilityRequest
     */
    public function setCardId($CardId){
      $this->CardId = $CardId;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_OrderAvailabilityRequest
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return Omni_ItemNumberType
     */
    public function getItemNumberType(){
      return $this->ItemNumberType;
    }

    /**
     * @param Omni_ItemNumberType $ItemNumberType
     * @return Omni_OrderAvailabilityRequest
     */
    public function setItemNumberType($ItemNumberType){
      $this->ItemNumberType = $ItemNumberType;
      return $this;
    }

    /**
     * @return Omni_OrderLineAvailability[]
     */
    public function getOrderLineAvailabilityRequests(){
      return $this->OrderLineAvailabilityRequests;
    }

    /**
     * @param Omni_OrderLineAvailability[] $OrderLineAvailabilityRequests
     * @return Omni_OrderAvailabilityRequest
     */
    public function setOrderLineAvailabilityRequests($OrderLineAvailabilityRequests){
      $this->OrderLineAvailabilityRequests = $OrderLineAvailabilityRequests;
      return $this;
    }

    /**
     * @return Omni_SourceType
     */
    public function getSourceType(){
      return $this->SourceType;
    }

    /**
     * @param Omni_SourceType $SourceType
     * @return Omni_OrderAvailabilityRequest
     */
    public function setSourceType($SourceType){
      $this->SourceType = $SourceType;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->StoreId;
    }

    /**
     * @param string $StoreId
     * @return Omni_OrderAvailabilityRequest
     */
    public function setStoreId($StoreId){
      $this->StoreId = $StoreId;
      return $this;
    }

}
