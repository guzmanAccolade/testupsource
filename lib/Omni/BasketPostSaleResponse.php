<?php

class Omni_BasketPostSaleResponse {

    /**
     * @var Omni_BasketPostSaleResponse $BasketPostSaleResult
     * @access public
     */
    public $BasketPostSaleResult = null;

    /**
     * @param Omni_BasketPostSaleResponse $BasketPostSaleResult
     * @access public
     */
    public function __construct($BasketPostSaleResult = null){
      $this->BasketPostSaleResult = $BasketPostSaleResult;
    }

    /**
     * @return Omni_BasketPostSaleResponse
     */
    public function getBasketPostSaleResult(){
      return $this->BasketPostSaleResult;
    }

    /**
     * @param Omni_BasketPostSaleResponse $BasketPostSaleResult
     * @return Omni_BasketPostSaleResponse
     */
    public function setBasketPostSaleResult($BasketPostSaleResult){
      $this->BasketPostSaleResult = $BasketPostSaleResult;
      return $this;
    }

}
