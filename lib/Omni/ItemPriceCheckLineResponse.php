<?php

class Omni_ItemPriceCheckLineResponse {

    /**
     * @var string $BarcodeId
     * @access public
     */
    public $BarcodeId = null;

    /**
     * @var string $CurrencyCode
     * @access public
     */
    public $CurrencyCode = null;

    /**
     * @var int $CurrencyFactor
     * @access public
     */
    public $CurrencyFactor = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var int $LineNumber
     * @access public
     */
    public $LineNumber = null;

    /**
     * @var Omni_LineType $LineType
     * @access public
     */
    public $LineType = null;

    /**
     * @var float $NetAmount
     * @access public
     */
    public $NetAmount = null;

    /**
     * @var float $NetPrice
     * @access public
     */
    public $NetPrice = null;

    /**
     * @var float $Price
     * @access public
     */
    public $Price = null;

    /**
     * @var float $Quantity
     * @access public
     */
    public $Quantity = null;

    /**
     * @var string $Uom
     * @access public
     */
    public $Uom = null;

    /**
     * @var string $VariantId
     * @access public
     */
    public $VariantId = null;

    /**
     * @param int $CurrencyFactor
     * @param int $LineNumber
     * @param Omni_LineType $LineType
     * @param float $NetAmount
     * @param float $NetPrice
     * @param float $Price
     * @param float $Quantity
     * @access public
     */
    public function __construct($CurrencyFactor = null, $LineNumber = null, $LineType = null, $NetAmount = null, $NetPrice = null, $Price = null, $Quantity = null){
      $this->CurrencyFactor = $CurrencyFactor;
      $this->LineNumber = $LineNumber;
      $this->LineType = $LineType;
      $this->NetAmount = $NetAmount;
      $this->NetPrice = $NetPrice;
      $this->Price = $Price;
      $this->Quantity = $Quantity;
    }

    /**
     * @return string
     */
    public function getBarcodeId(){
      return $this->BarcodeId;
    }

    /**
     * @param string $BarcodeId
     * @return Omni_ItemPriceCheckLineResponse
     */
    public function setBarcodeId($BarcodeId){
      $this->BarcodeId = $BarcodeId;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(){
      return $this->CurrencyCode;
    }

    /**
     * @param string $CurrencyCode
     * @return Omni_ItemPriceCheckLineResponse
     */
    public function setCurrencyCode($CurrencyCode){
      $this->CurrencyCode = $CurrencyCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getCurrencyFactor(){
      return $this->CurrencyFactor;
    }

    /**
     * @param int $CurrencyFactor
     * @return Omni_ItemPriceCheckLineResponse
     */
    public function setCurrencyFactor($CurrencyFactor){
      $this->CurrencyFactor = $CurrencyFactor;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_ItemPriceCheckLineResponse
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return int
     */
    public function getLineNumber(){
      return $this->LineNumber;
    }

    /**
     * @param int $LineNumber
     * @return Omni_ItemPriceCheckLineResponse
     */
    public function setLineNumber($LineNumber){
      $this->LineNumber = $LineNumber;
      return $this;
    }

    /**
     * @return Omni_LineType
     */
    public function getLineType(){
      return $this->LineType;
    }

    /**
     * @param Omni_LineType $LineType
     * @return Omni_ItemPriceCheckLineResponse
     */
    public function setLineType($LineType){
      $this->LineType = $LineType;
      return $this;
    }

    /**
     * @return float
     */
    public function getNetAmount(){
      return $this->NetAmount;
    }

    /**
     * @param float $NetAmount
     * @return Omni_ItemPriceCheckLineResponse
     */
    public function setNetAmount($NetAmount){
      $this->NetAmount = $NetAmount;
      return $this;
    }

    /**
     * @return float
     */
    public function getNetPrice(){
      return $this->NetPrice;
    }

    /**
     * @param float $NetPrice
     * @return Omni_ItemPriceCheckLineResponse
     */
    public function setNetPrice($NetPrice){
      $this->NetPrice = $NetPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getPrice(){
      return $this->Price;
    }

    /**
     * @param float $Price
     * @return Omni_ItemPriceCheckLineResponse
     */
    public function setPrice($Price){
      $this->Price = $Price;
      return $this;
    }

    /**
     * @return float
     */
    public function getQuantity(){
      return $this->Quantity;
    }

    /**
     * @param float $Quantity
     * @return Omni_ItemPriceCheckLineResponse
     */
    public function setQuantity($Quantity){
      $this->Quantity = $Quantity;
      return $this;
    }

    /**
     * @return string
     */
    public function getUom(){
      return $this->Uom;
    }

    /**
     * @param string $Uom
     * @return Omni_ItemPriceCheckLineResponse
     */
    public function setUom($Uom){
      $this->Uom = $Uom;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantId(){
      return $this->VariantId;
    }

    /**
     * @param string $VariantId
     * @return Omni_ItemPriceCheckLineResponse
     */
    public function setVariantId($VariantId){
      $this->VariantId = $VariantId;
      return $this;
    }

}
