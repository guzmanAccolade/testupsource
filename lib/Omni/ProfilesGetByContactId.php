<?php

class Omni_ProfilesGetByContactId {

    /**
     * @var string $contactId
     * @access public
     */
    public $contactId = null;

    /**
     * @param string $contactId
     * @access public
     */
    public function __construct($contactId = null){
      $this->contactId = $contactId;
    }

    /**
     * @return string
     */
    public function getContactId(){
      return $this->contactId;
    }

    /**
     * @param string $contactId
     * @return Omni_ProfilesGetByContactId
     */
    public function setContactId($contactId){
      $this->contactId = $contactId;
      return $this;
    }

}
