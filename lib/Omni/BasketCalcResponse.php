<?php

class Omni_BasketCalcResponse {

    /**
     * @var Omni_BasketCalcResponse $BasketCalcResult
     * @access public
     */
    public $BasketCalcResult = null;

    /**
     * @param Omni_BasketCalcResponse $BasketCalcResult
     * @access public
     */
    public function __construct($BasketCalcResult = null){
      $this->BasketCalcResult = $BasketCalcResult;
    }

    /**
     * @return Omni_BasketCalcResponse
     */
    public function getBasketCalcResult(){
      return $this->BasketCalcResult;
    }

    /**
     * @param Omni_BasketCalcResponse $BasketCalcResult
     * @return Omni_BasketCalcResponse
     */
    public function setBasketCalcResult($BasketCalcResult){
      $this->BasketCalcResult = $BasketCalcResult;
      return $this;
    }

}
