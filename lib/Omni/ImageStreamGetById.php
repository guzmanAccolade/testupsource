<?php

class Omni_ImageStreamGetById {

    /**
     * @var string $id
     * @access public
     */
    public $id = null;

    /**
     * @var int $width
     * @access public
     */
    public $width = null;

    /**
     * @var int $height
     * @access public
     */
    public $height = null;

    /**
     * @param string $id
     * @param int $width
     * @param int $height
     * @access public
     */
    public function __construct($id = null, $width = null, $height = null){
      $this->id = $id;
      $this->width = $width;
      $this->height = $height;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->id;
    }

    /**
     * @param string $id
     * @return Omni_ImageStreamGetById
     */
    public function setId($id){
      $this->id = $id;
      return $this;
    }

    /**
     * @return int
     */
    public function getWidth(){
      return $this->width;
    }

    /**
     * @param int $width
     * @return Omni_ImageStreamGetById
     */
    public function setWidth($width){
      $this->width = $width;
      return $this;
    }

    /**
     * @return int
     */
    public function getHeight(){
      return $this->height;
    }

    /**
     * @param int $height
     * @return Omni_ImageStreamGetById
     */
    public function setHeight($height){
      $this->height = $height;
      return $this;
    }

}
