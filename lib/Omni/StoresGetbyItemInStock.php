<?php

class Omni_StoresGetbyItemInStock {

    /**
     * @var string $itemId
     * @access public
     */
    public $itemId = null;

    /**
     * @var string $variantId
     * @access public
     */
    public $variantId = null;

    /**
     * @var float $latitude
     * @access public
     */
    public $latitude = null;

    /**
     * @var float $longitude
     * @access public
     */
    public $longitude = null;

    /**
     * @var float $maxDistance
     * @access public
     */
    public $maxDistance = null;

    /**
     * @var int $maxNumberOfStores
     * @access public
     */
    public $maxNumberOfStores = null;

    /**
     * @param string $itemId
     * @param string $variantId
     * @param float $latitude
     * @param float $longitude
     * @param float $maxDistance
     * @param int $maxNumberOfStores
     * @access public
     */
    public function __construct($itemId = null, $variantId = null, $latitude = null, $longitude = null, $maxDistance = null, $maxNumberOfStores = null){
      $this->itemId = $itemId;
      $this->variantId = $variantId;
      $this->latitude = $latitude;
      $this->longitude = $longitude;
      $this->maxDistance = $maxDistance;
      $this->maxNumberOfStores = $maxNumberOfStores;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->itemId;
    }

    /**
     * @param string $itemId
     * @return Omni_StoresGetbyItemInStock
     */
    public function setItemId($itemId){
      $this->itemId = $itemId;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantId(){
      return $this->variantId;
    }

    /**
     * @param string $variantId
     * @return Omni_StoresGetbyItemInStock
     */
    public function setVariantId($variantId){
      $this->variantId = $variantId;
      return $this;
    }

    /**
     * @return float
     */
    public function getLatitude(){
      return $this->latitude;
    }

    /**
     * @param float $latitude
     * @return Omni_StoresGetbyItemInStock
     */
    public function setLatitude($latitude){
      $this->latitude = $latitude;
      return $this;
    }

    /**
     * @return float
     */
    public function getLongitude(){
      return $this->longitude;
    }

    /**
     * @param float $longitude
     * @return Omni_StoresGetbyItemInStock
     */
    public function setLongitude($longitude){
      $this->longitude = $longitude;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaxDistance(){
      return $this->maxDistance;
    }

    /**
     * @param float $maxDistance
     * @return Omni_StoresGetbyItemInStock
     */
    public function setMaxDistance($maxDistance){
      $this->maxDistance = $maxDistance;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxNumberOfStores(){
      return $this->maxNumberOfStores;
    }

    /**
     * @param int $maxNumberOfStores
     * @return Omni_StoresGetbyItemInStock
     */
    public function setMaxNumberOfStores($maxNumberOfStores){
      $this->maxNumberOfStores = $maxNumberOfStores;
      return $this;
    }

}
