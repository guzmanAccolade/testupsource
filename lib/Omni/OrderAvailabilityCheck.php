<?php

class Omni_OrderAvailabilityCheck {

    /**
     * @var Omni_OrderAvailabilityRequest $request
     * @access public
     */
    public $request = null;

    /**
     * @param Omni_OrderAvailabilityRequest $request
     * @access public
     */
    public function __construct($request = null){
      $this->request = $request;
    }

    /**
     * @return Omni_OrderAvailabilityRequest
     */
    public function getRequest(){
      return $this->request;
    }

    /**
     * @param Omni_OrderAvailabilityRequest $request
     * @return Omni_OrderAvailabilityCheck
     */
    public function setRequest($request){
      $this->request = $request;
      return $this;
    }

}
