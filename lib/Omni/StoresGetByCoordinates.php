<?php

class Omni_StoresGetByCoordinates {

    /**
     * @var float $latitude
     * @access public
     */
    public $latitude = null;

    /**
     * @var float $longitude
     * @access public
     */
    public $longitude = null;

    /**
     * @var float $maxDistance
     * @access public
     */
    public $maxDistance = null;

    /**
     * @var int $maxNumberOfStores
     * @access public
     */
    public $maxNumberOfStores = null;

    /**
     * @param float $latitude
     * @param float $longitude
     * @param float $maxDistance
     * @param int $maxNumberOfStores
     * @access public
     */
    public function __construct($latitude = null, $longitude = null, $maxDistance = null, $maxNumberOfStores = null){
      $this->latitude = $latitude;
      $this->longitude = $longitude;
      $this->maxDistance = $maxDistance;
      $this->maxNumberOfStores = $maxNumberOfStores;
    }

    /**
     * @return float
     */
    public function getLatitude(){
      return $this->latitude;
    }

    /**
     * @param float $latitude
     * @return Omni_StoresGetByCoordinates
     */
    public function setLatitude($latitude){
      $this->latitude = $latitude;
      return $this;
    }

    /**
     * @return float
     */
    public function getLongitude(){
      return $this->longitude;
    }

    /**
     * @param float $longitude
     * @return Omni_StoresGetByCoordinates
     */
    public function setLongitude($longitude){
      $this->longitude = $longitude;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaxDistance(){
      return $this->maxDistance;
    }

    /**
     * @param float $maxDistance
     * @return Omni_StoresGetByCoordinates
     */
    public function setMaxDistance($maxDistance){
      $this->maxDistance = $maxDistance;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxNumberOfStores(){
      return $this->maxNumberOfStores;
    }

    /**
     * @param int $maxNumberOfStores
     * @return Omni_StoresGetByCoordinates
     */
    public function setMaxNumberOfStores($maxNumberOfStores){
      $this->maxNumberOfStores = $maxNumberOfStores;
      return $this;
    }

}
