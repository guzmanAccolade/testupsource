<?php

class Omni_ItemVariantRegistration {

    /**
     * @var boolean $Del
     * @access public
     */
    public $Del = null;

    /**
     * @var string $FrameworkCode
     * @access public
     */
    public $FrameworkCode = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var string $VarDim1
     * @access public
     */
    public $VarDim1 = null;

    /**
     * @var string $VarDim2
     * @access public
     */
    public $VarDim2 = null;

    /**
     * @var string $VarDim3
     * @access public
     */
    public $VarDim3 = null;

    /**
     * @var string $VarDim4
     * @access public
     */
    public $VarDim4 = null;

    /**
     * @var string $VarDim5
     * @access public
     */
    public $VarDim5 = null;

    /**
     * @var string $VarDim6
     * @access public
     */
    public $VarDim6 = null;

    /**
     * @var string $VariantId
     * @access public
     */
    public $VariantId = null;

    /**
     * @param boolean $Del
     * @access public
     */
    public function __construct($Del = null){
      $this->Del = $Del;
    }

    /**
     * @return boolean
     */
    public function getDel(){
      return $this->Del;
    }

    /**
     * @param boolean $Del
     * @return Omni_ItemVariantRegistration
     */
    public function setDel($Del){
      $this->Del = $Del;
      return $this;
    }

    /**
     * @return string
     */
    public function getFrameworkCode(){
      return $this->FrameworkCode;
    }

    /**
     * @param string $FrameworkCode
     * @return Omni_ItemVariantRegistration
     */
    public function setFrameworkCode($FrameworkCode){
      $this->FrameworkCode = $FrameworkCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_ItemVariantRegistration
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return string
     */
    public function getVarDim1(){
      return $this->VarDim1;
    }

    /**
     * @param string $VarDim1
     * @return Omni_ItemVariantRegistration
     */
    public function setVarDim1($VarDim1){
      $this->VarDim1 = $VarDim1;
      return $this;
    }

    /**
     * @return string
     */
    public function getVarDim2(){
      return $this->VarDim2;
    }

    /**
     * @param string $VarDim2
     * @return Omni_ItemVariantRegistration
     */
    public function setVarDim2($VarDim2){
      $this->VarDim2 = $VarDim2;
      return $this;
    }

    /**
     * @return string
     */
    public function getVarDim3(){
      return $this->VarDim3;
    }

    /**
     * @param string $VarDim3
     * @return Omni_ItemVariantRegistration
     */
    public function setVarDim3($VarDim3){
      $this->VarDim3 = $VarDim3;
      return $this;
    }

    /**
     * @return string
     */
    public function getVarDim4(){
      return $this->VarDim4;
    }

    /**
     * @param string $VarDim4
     * @return Omni_ItemVariantRegistration
     */
    public function setVarDim4($VarDim4){
      $this->VarDim4 = $VarDim4;
      return $this;
    }

    /**
     * @return string
     */
    public function getVarDim5(){
      return $this->VarDim5;
    }

    /**
     * @param string $VarDim5
     * @return Omni_ItemVariantRegistration
     */
    public function setVarDim5($VarDim5){
      $this->VarDim5 = $VarDim5;
      return $this;
    }

    /**
     * @return string
     */
    public function getVarDim6(){
      return $this->VarDim6;
    }

    /**
     * @param string $VarDim6
     * @return Omni_ItemVariantRegistration
     */
    public function setVarDim6($VarDim6){
      $this->VarDim6 = $VarDim6;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantId(){
      return $this->VariantId;
    }

    /**
     * @param string $VariantId
     * @return Omni_ItemVariantRegistration
     */
    public function setVariantId($VariantId){
      $this->VariantId = $VariantId;
      return $this;
    }

}
