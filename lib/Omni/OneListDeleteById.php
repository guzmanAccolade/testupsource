<?php

class Omni_OneListDeleteById {

    /**
     * @var string $oneListId
     * @access public
     */
    public $oneListId = null;

    /**
     * @var Omni_ListType $listType
     * @access public
     */
    public $listType = null;

    /**
     * @param string $oneListId
     * @param Omni_ListType $listType
     * @access public
     */
    public function __construct($oneListId = null, $listType = null){
      $this->oneListId = $oneListId;
      $this->listType = $listType;
    }

    /**
     * @return string
     */
    public function getOneListId(){
      return $this->oneListId;
    }

    /**
     * @param string $oneListId
     * @return Omni_OneListDeleteById
     */
    public function setOneListId($oneListId){
      $this->oneListId = $oneListId;
      return $this;
    }

    /**
     * @return Omni_ListType
     */
    public function getListType(){
      return $this->listType;
    }

    /**
     * @param Omni_ListType $listType
     * @return Omni_OneListDeleteById
     */
    public function setListType($listType){
      $this->listType = $listType;
      return $this;
    }

}
