<?php

class Omni_ContactSearch {

    /**
     * @var Omni_ContactSearchType $searchType
     * @access public
     */
    public $searchType = null;

    /**
     * @var string $search
     * @access public
     */
    public $search = null;

    /**
     * @var int $maxNumberOfRowsReturned
     * @access public
     */
    public $maxNumberOfRowsReturned = null;

    /**
     * @param Omni_ContactSearchType $searchType
     * @param string $search
     * @param int $maxNumberOfRowsReturned
     * @access public
     */
    public function __construct($searchType = null, $search = null, $maxNumberOfRowsReturned = null){
      $this->searchType = $searchType;
      $this->search = $search;
      $this->maxNumberOfRowsReturned = $maxNumberOfRowsReturned;
    }

    /**
     * @return Omni_ContactSearchType
     */
    public function getSearchType(){
      return $this->searchType;
    }

    /**
     * @param Omni_ContactSearchType $searchType
     * @return Omni_ContactSearch
     */
    public function setSearchType($searchType){
      $this->searchType = $searchType;
      return $this;
    }

    /**
     * @return string
     */
    public function getSearch(){
      return $this->search;
    }

    /**
     * @param string $search
     * @return Omni_ContactSearch
     */
    public function setSearch($search){
      $this->search = $search;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxNumberOfRowsReturned(){
      return $this->maxNumberOfRowsReturned;
    }

    /**
     * @param int $maxNumberOfRowsReturned
     * @return Omni_ContactSearch
     */
    public function setMaxNumberOfRowsReturned($maxNumberOfRowsReturned){
      $this->maxNumberOfRowsReturned = $maxNumberOfRowsReturned;
      return $this;
    }

}
