<?php

class Omni_StoreHours {

    /**
     * @var int $DayOfWeek
     * @access public
     */
    public $DayOfWeek = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $NameOfDay
     * @access public
     */
    public $NameOfDay = null;

    /**
     * @var dateTime $OpenFrom
     * @access public
     */
    public $OpenFrom = null;

    /**
     * @var dateTime $OpenTo
     * @access public
     */
    public $OpenTo = null;

    /**
     * @var Omni_StoreHourType $StoreHourtype
     * @access public
     */
    public $StoreHourtype = null;

    /**
     * @var string $StoreId
     * @access public
     */
    public $StoreId = null;

    /**
     * @param int $DayOfWeek
     * @param dateTime $OpenFrom
     * @param dateTime $OpenTo
     * @param Omni_StoreHourType $StoreHourtype
     * @access public
     */
    public function __construct($DayOfWeek = null, $OpenFrom = null, $OpenTo = null, $StoreHourtype = null){
      $this->DayOfWeek = $DayOfWeek;
      $this->OpenFrom = $OpenFrom;
      $this->OpenTo = $OpenTo;
      $this->StoreHourtype = $StoreHourtype;
    }

    /**
     * @return int
     */
    public function getDayOfWeek(){
      return $this->DayOfWeek;
    }

    /**
     * @param int $DayOfWeek
     * @return Omni_StoreHours
     */
    public function setDayOfWeek($DayOfWeek){
      $this->DayOfWeek = $DayOfWeek;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_StoreHours
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getNameOfDay(){
      return $this->NameOfDay;
    }

    /**
     * @param string $NameOfDay
     * @return Omni_StoreHours
     */
    public function setNameOfDay($NameOfDay){
      $this->NameOfDay = $NameOfDay;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getOpenFrom(){
      return $this->OpenFrom;
    }

    /**
     * @param dateTime $OpenFrom
     * @return Omni_StoreHours
     */
    public function setOpenFrom($OpenFrom){
      $this->OpenFrom = $OpenFrom;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getOpenTo(){
      return $this->OpenTo;
    }

    /**
     * @param dateTime $OpenTo
     * @return Omni_StoreHours
     */
    public function setOpenTo($OpenTo){
      $this->OpenTo = $OpenTo;
      return $this;
    }

    /**
     * @return Omni_StoreHourType
     */
    public function getStoreHourtype(){
      return $this->StoreHourtype;
    }

    /**
     * @param Omni_StoreHourType $StoreHourtype
     * @return Omni_StoreHours
     */
    public function setStoreHourtype($StoreHourtype){
      $this->StoreHourtype = $StoreHourtype;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->StoreId;
    }

    /**
     * @param string $StoreId
     * @return Omni_StoreHours
     */
    public function setStoreId($StoreId){
      $this->StoreId = $StoreId;
      return $this;
    }

}
