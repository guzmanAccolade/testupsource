<?php

class Omni_UOM {

    /**
     * @var int $Decimals
     * @access public
     */
    public $Decimals = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var float $QtyPerUom
     * @access public
     */
    public $QtyPerUom = null;

    /**
     * @var string $ShortDescription
     * @access public
     */
    public $ShortDescription = null;

    /**
     * @param int $Decimals
     * @param float $QtyPerUom
     * @access public
     */
    public function __construct($Decimals = null, $QtyPerUom = null){
      $this->Decimals = $Decimals;
      $this->QtyPerUom = $QtyPerUom;
    }

    /**
     * @return int
     */
    public function getDecimals(){
      return $this->Decimals;
    }

    /**
     * @param int $Decimals
     * @return Omni_UOM
     */
    public function setDecimals($Decimals){
      $this->Decimals = $Decimals;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_UOM
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_UOM
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_UOM
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return float
     */
    public function getQtyPerUom(){
      return $this->QtyPerUom;
    }

    /**
     * @param float $QtyPerUom
     * @return Omni_UOM
     */
    public function setQtyPerUom($QtyPerUom){
      $this->QtyPerUom = $QtyPerUom;
      return $this;
    }

    /**
     * @return string
     */
    public function getShortDescription(){
      return $this->ShortDescription;
    }

    /**
     * @param string $ShortDescription
     * @return Omni_UOM
     */
    public function setShortDescription($ShortDescription){
      $this->ShortDescription = $ShortDescription;
      return $this;
    }

}
