<?php

class Omni_LineType {
    const __default = 'Item';
    const Item = 'Item';
    const Payment = 'Payment';
    const PerDiscount = 'PerDiscount';
    const TotalDiscount = 'TotalDiscount';
    const IncomeExpense = 'IncomeExpense';
    const FreeText = 'FreeText';
    const Coupon = 'Coupon';
    const Unknown = 'Unknown';


}
