<?php

class Omni_AppSettingsGetByKeyResponse {

    /**
     * @var string $AppSettingsGetByKeyResult
     * @access public
     */
    public $AppSettingsGetByKeyResult = null;

    /**
     * @param string $AppSettingsGetByKeyResult
     * @access public
     */
    public function __construct($AppSettingsGetByKeyResult = null){
      $this->AppSettingsGetByKeyResult = $AppSettingsGetByKeyResult;
    }

    /**
     * @return string
     */
    public function getAppSettingsGetByKeyResult(){
      return $this->AppSettingsGetByKeyResult;
    }

    /**
     * @param string $AppSettingsGetByKeyResult
     * @return Omni_AppSettingsGetByKeyResponse
     */
    public function setAppSettingsGetByKeyResult($AppSettingsGetByKeyResult){
      $this->AppSettingsGetByKeyResult = $AppSettingsGetByKeyResult;
      return $this;
    }

}
