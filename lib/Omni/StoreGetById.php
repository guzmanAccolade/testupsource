<?php

class Omni_StoreGetById {

    /**
     * @var string $storeId
     * @access public
     */
    public $storeId = null;

    /**
     * @param string $storeId
     * @access public
     */
    public function __construct($storeId = null){
      $this->storeId = $storeId;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->storeId;
    }

    /**
     * @param string $storeId
     * @return Omni_StoreGetById
     */
    public function setStoreId($storeId){
      $this->storeId = $storeId;
      return $this;
    }

}
