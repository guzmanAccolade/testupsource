<?php

class Omni_ReplEcommItemVariantRegistrations {

    /**
     * @var Omni_ReplRequest $replRequest
     * @access public
     */
    public $replRequest = null;

    /**
     * @param Omni_ReplRequest $replRequest
     * @access public
     */
    public function __construct($replRequest = null){
      $this->replRequest = $replRequest;
    }

    /**
     * @return Omni_ReplRequest
     */
    public function getReplRequest(){
      return $this->replRequest;
    }

    /**
     * @param Omni_ReplRequest $replRequest
     * @return Omni_ReplEcommItemVariantRegistrations
     */
    public function setReplRequest($replRequest){
      $this->replRequest = $replRequest;
      return $this;
    }

}
