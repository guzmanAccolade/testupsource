<?php

class Omni_Login {

    /**
     * @var string $userName
     * @access public
     */
    public $userName = null;

    /**
     * @var string $password
     * @access public
     */
    public $password = null;

    /**
     * @var string $deviceId
     * @access public
     */
    public $deviceId = null;

    /**
     * @param string $userName
     * @param string $password
     * @param string $deviceId
     * @access public
     */
    public function __construct($userName = null, $password = null, $deviceId = null){
      $this->userName = $userName;
      $this->password = $password;
      $this->deviceId = $deviceId;
    }

    /**
     * @return string
     */
    public function getUserName(){
      return $this->userName;
    }

    /**
     * @param string $userName
     * @return Omni_Login
     */
    public function setUserName($userName){
      $this->userName = $userName;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassword(){
      return $this->password;
    }

    /**
     * @param string $password
     * @return Omni_Login
     */
    public function setPassword($password){
      $this->password = $password;
      return $this;
    }

    /**
     * @return string
     */
    public function getDeviceId(){
      return $this->deviceId;
    }

    /**
     * @param string $deviceId
     * @return Omni_Login
     */
    public function setDeviceId($deviceId){
      $this->deviceId = $deviceId;
      return $this;
    }

}
