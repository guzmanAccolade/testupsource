<?php

class Omni_ContactPOS {

    /**
     * @var Omni_Account $Account
     * @access public
     */
    public $Account = null;

    /**
     * @var Omni_Address[] $Addresses
     * @access public
     */
    public $Addresses = null;

    /**
     * @var string $AlternateId
     * @access public
     */
    public $AlternateId = null;

    /**
     * @var dateTime $BirthDay
     * @access public
     */
    public $BirthDay = null;

    /**
     * @var string $BlockedReason
     * @access public
     */
    public $BlockedReason = null;

    /**
     * @var Omni_Card $Card
     * @access public
     */
    public $Card = null;

    /**
     * @var Omni_Card[] $Cards
     * @access public
     */
    public $Cards = null;

    /**
     * @var Omni_Coupon[] $Coupons
     * @access public
     */
    public $Coupons = null;

    /**
     * @var string $Email
     * @access public
     */
    public $Email = null;

    /**
     * @var string $FirstName
     * @access public
     */
    public $FirstName = null;

    /**
     * @var Omni_Gender $Gender
     * @access public
     */
    public $Gender = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var boolean $IsBlocked
     * @access public
     */
    public $IsBlocked = null;

    /**
     * @var string $LastName
     * @access public
     */
    public $LastName = null;

    /**
     * @var Omni_MaritalStatus $MaritalStatus
     * @access public
     */
    public $MaritalStatus = null;

    /**
     * @var string $MiddleName
     * @access public
     */
    public $MiddleName = null;

    /**
     * @var string $MobilePhone
     * @access public
     */
    public $MobilePhone = null;

    /**
     * @var Omni_Offer[] $Offers
     * @access public
     */
    public $Offers = null;

    /**
     * @var string $Password
     * @access public
     */
    public $Password = null;

    /**
     * @var string $Phone
     * @access public
     */
    public $Phone = null;

    /**
     * @var Omni_Profile[] $Profiles
     * @access public
     */
    public $Profiles = null;

    /**
     * @var Omni_Transaction[] $Transactions
     * @access public
     */
    public $Transactions = null;

    /**
     * @var string $UserName
     * @access public
     */
    public $UserName = null;

    /**
     * @param Omni_Gender $Gender
     * @param boolean $IsBlocked
     * @param Omni_MaritalStatus $MaritalStatus
     * @access public
     */
    public function __construct($Gender = null, $IsBlocked = null, $MaritalStatus = null){
      $this->Gender = $Gender;
      $this->IsBlocked = $IsBlocked;
      $this->MaritalStatus = $MaritalStatus;
    }

    /**
     * @return Omni_Account
     */
    public function getAccount(){
      return $this->Account;
    }

    /**
     * @param Omni_Account $Account
     * @return Omni_ContactPOS
     */
    public function setAccount($Account){
      $this->Account = $Account;
      return $this;
    }

    /**
     * @return Omni_Address[]
     */
    public function getAddresses(){
      return $this->Addresses;
    }

    /**
     * @param Omni_Address[] $Addresses
     * @return Omni_ContactPOS
     */
    public function setAddresses($Addresses){
      $this->Addresses = $Addresses;
      return $this;
    }

    /**
     * @return string
     */
    public function getAlternateId(){
      return $this->AlternateId;
    }

    /**
     * @param string $AlternateId
     * @return Omni_ContactPOS
     */
    public function setAlternateId($AlternateId){
      $this->AlternateId = $AlternateId;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getBirthDay(){
      return $this->BirthDay;
    }

    /**
     * @param dateTime $BirthDay
     * @return Omni_ContactPOS
     */
    public function setBirthDay($BirthDay){
      $this->BirthDay = $BirthDay;
      return $this;
    }

    /**
     * @return string
     */
    public function getBlockedReason(){
      return $this->BlockedReason;
    }

    /**
     * @param string $BlockedReason
     * @return Omni_ContactPOS
     */
    public function setBlockedReason($BlockedReason){
      $this->BlockedReason = $BlockedReason;
      return $this;
    }

    /**
     * @return Omni_Card
     */
    public function getCard(){
      return $this->Card;
    }

    /**
     * @param Omni_Card $Card
     * @return Omni_ContactPOS
     */
    public function setCard($Card){
      $this->Card = $Card;
      return $this;
    }

    /**
     * @return Omni_Card[]
     */
    public function getCards(){
      return $this->Cards;
    }

    /**
     * @param Omni_Card[] $Cards
     * @return Omni_ContactPOS
     */
    public function setCards($Cards){
      $this->Cards = $Cards;
      return $this;
    }

    /**
     * @return Omni_Coupon[]
     */
    public function getCoupons(){
      return $this->Coupons;
    }

    /**
     * @param Omni_Coupon[] $Coupons
     * @return Omni_ContactPOS
     */
    public function setCoupons($Coupons){
      $this->Coupons = $Coupons;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmail(){
      return $this->Email;
    }

    /**
     * @param string $Email
     * @return Omni_ContactPOS
     */
    public function setEmail($Email){
      $this->Email = $Email;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(){
      return $this->FirstName;
    }

    /**
     * @param string $FirstName
     * @return Omni_ContactPOS
     */
    public function setFirstName($FirstName){
      $this->FirstName = $FirstName;
      return $this;
    }

    /**
     * @return Omni_Gender
     */
    public function getGender(){
      return $this->Gender;
    }

    /**
     * @param Omni_Gender $Gender
     * @return Omni_ContactPOS
     */
    public function setGender($Gender){
      $this->Gender = $Gender;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_ContactPOS
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsBlocked(){
      return $this->IsBlocked;
    }

    /**
     * @param boolean $IsBlocked
     * @return Omni_ContactPOS
     */
    public function setIsBlocked($IsBlocked){
      $this->IsBlocked = $IsBlocked;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastName(){
      return $this->LastName;
    }

    /**
     * @param string $LastName
     * @return Omni_ContactPOS
     */
    public function setLastName($LastName){
      $this->LastName = $LastName;
      return $this;
    }

    /**
     * @return Omni_MaritalStatus
     */
    public function getMaritalStatus(){
      return $this->MaritalStatus;
    }

    /**
     * @param Omni_MaritalStatus $MaritalStatus
     * @return Omni_ContactPOS
     */
    public function setMaritalStatus($MaritalStatus){
      $this->MaritalStatus = $MaritalStatus;
      return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName(){
      return $this->MiddleName;
    }

    /**
     * @param string $MiddleName
     * @return Omni_ContactPOS
     */
    public function setMiddleName($MiddleName){
      $this->MiddleName = $MiddleName;
      return $this;
    }

    /**
     * @return string
     */
    public function getMobilePhone(){
      return $this->MobilePhone;
    }

    /**
     * @param string $MobilePhone
     * @return Omni_ContactPOS
     */
    public function setMobilePhone($MobilePhone){
      $this->MobilePhone = $MobilePhone;
      return $this;
    }

    /**
     * @return Omni_Offer[]
     */
    public function getOffers(){
      return $this->Offers;
    }

    /**
     * @param Omni_Offer[] $Offers
     * @return Omni_ContactPOS
     */
    public function setOffers($Offers){
      $this->Offers = $Offers;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassword(){
      return $this->Password;
    }

    /**
     * @param string $Password
     * @return Omni_ContactPOS
     */
    public function setPassword($Password){
      $this->Password = $Password;
      return $this;
    }

    /**
     * @return string
     */
    public function getPhone(){
      return $this->Phone;
    }

    /**
     * @param string $Phone
     * @return Omni_ContactPOS
     */
    public function setPhone($Phone){
      $this->Phone = $Phone;
      return $this;
    }

    /**
     * @return Omni_Profile[]
     */
    public function getProfiles(){
      return $this->Profiles;
    }

    /**
     * @param Omni_Profile[] $Profiles
     * @return Omni_ContactPOS
     */
    public function setProfiles($Profiles){
      $this->Profiles = $Profiles;
      return $this;
    }

    /**
     * @return Omni_Transaction[]
     */
    public function getTransactions(){
      return $this->Transactions;
    }

    /**
     * @param Omni_Transaction[] $Transactions
     * @return Omni_ContactPOS
     */
    public function setTransactions($Transactions){
      $this->Transactions = $Transactions;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserName(){
      return $this->UserName;
    }

    /**
     * @param string $UserName
     * @return Omni_ContactPOS
     */
    public function setUserName($UserName){
      $this->UserName = $UserName;
      return $this;
    }

}
