<?php

class Omni_Coupon {

    /**
     * @var Omni_CouponDetails[] $Details
     * @access public
     */
    public $Details = null;

    /**
     * @var dateTime $ExpiryDate
     * @access public
     */
    public $ExpiryDate = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $PrimaryText
     * @access public
     */
    public $PrimaryText = null;

    /**
     * @var string $SecondaryText
     * @access public
     */
    public $SecondaryText = null;

    /**
     * @param dateTime $ExpiryDate
     * @param string $PrimaryText
     * @param string $SecondaryText
     * @access public
     */
    public function __construct($ExpiryDate = null, $PrimaryText = null, $SecondaryText = null){
      $this->ExpiryDate = $ExpiryDate;
      $this->PrimaryText = $PrimaryText;
      $this->SecondaryText = $SecondaryText;
    }

    /**
     * @return Omni_CouponDetails[]
     */
    public function getDetails(){
      return $this->Details;
    }

    /**
     * @param Omni_CouponDetails[] $Details
     * @return Omni_Coupon
     */
    public function setDetails($Details){
      $this->Details = $Details;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getExpiryDate(){
      return $this->ExpiryDate;
    }

    /**
     * @param dateTime $ExpiryDate
     * @return Omni_Coupon
     */
    public function setExpiryDate($ExpiryDate){
      $this->ExpiryDate = $ExpiryDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Coupon
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getPrimaryText(){
      return $this->PrimaryText;
    }

    /**
     * @param string $PrimaryText
     * @return Omni_Coupon
     */
    public function setPrimaryText($PrimaryText){
      $this->PrimaryText = $PrimaryText;
      return $this;
    }

    /**
     * @return string
     */
    public function getSecondaryText(){
      return $this->SecondaryText;
    }

    /**
     * @param string $SecondaryText
     * @return Omni_Coupon
     */
    public function setSecondaryText($SecondaryText){
      $this->SecondaryText = $SecondaryText;
      return $this;
    }

}
