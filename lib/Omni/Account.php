<?php

class Omni_Account {

    /**
     * @var string $BlockedReason
     * @access public
     */
    public $BlockedReason = null;

    /**
     * @var string $ClubCode
     * @access public
     */
    public $ClubCode = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var boolean $IsBlocked
     * @access public
     */
    public $IsBlocked = null;

    /**
     * @var int $PointBalance
     * @access public
     */
    public $PointBalance = null;

    /**
     * @var string $SchemeCode
     * @access public
     */
    public $SchemeCode = null;

    /**
     * @var string $SchemeDescription
     * @access public
     */
    public $SchemeDescription = null;

    /**
     * @param string $BlockedReason
     * @param string $ClubCode
     * @param boolean $IsBlocked
     * @param int $PointBalance
     * @param string $SchemeCode
     * @param string $SchemeDescription
     * @access public
     */
    public function __construct($BlockedReason = null, $ClubCode = null, $IsBlocked = null, $PointBalance = null, $SchemeCode = null, $SchemeDescription = null){
      $this->BlockedReason = $BlockedReason;
      $this->ClubCode = $ClubCode;
      $this->IsBlocked = $IsBlocked;
      $this->PointBalance = $PointBalance;
      $this->SchemeCode = $SchemeCode;
      $this->SchemeDescription = $SchemeDescription;
    }

    /**
     * @return string
     */
    public function getBlockedReason(){
      return $this->BlockedReason;
    }

    /**
     * @param string $BlockedReason
     * @return Omni_Account
     */
    public function setBlockedReason($BlockedReason){
      $this->BlockedReason = $BlockedReason;
      return $this;
    }

    /**
     * @return string
     */
    public function getClubCode(){
      return $this->ClubCode;
    }

    /**
     * @param string $ClubCode
     * @return Omni_Account
     */
    public function setClubCode($ClubCode){
      $this->ClubCode = $ClubCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Account
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsBlocked(){
      return $this->IsBlocked;
    }

    /**
     * @param boolean $IsBlocked
     * @return Omni_Account
     */
    public function setIsBlocked($IsBlocked){
      $this->IsBlocked = $IsBlocked;
      return $this;
    }

    /**
     * @return int
     */
    public function getPointBalance(){
      return $this->PointBalance;
    }

    /**
     * @param int $PointBalance
     * @return Omni_Account
     */
    public function setPointBalance($PointBalance){
      $this->PointBalance = $PointBalance;
      return $this;
    }

    /**
     * @return string
     */
    public function getSchemeCode(){
      return $this->SchemeCode;
    }

    /**
     * @param string $SchemeCode
     * @return Omni_Account
     */
    public function setSchemeCode($SchemeCode){
      $this->SchemeCode = $SchemeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSchemeDescription(){
      return $this->SchemeDescription;
    }

    /**
     * @param string $SchemeDescription
     * @return Omni_Account
     */
    public function setSchemeDescription($SchemeDescription){
      $this->SchemeDescription = $SchemeDescription;
      return $this;
    }

}
