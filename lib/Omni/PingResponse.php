<?php

class Omni_PingResponse {

    /**
     * @var string $PingResult
     * @access public
     */
    public $PingResult = null;

    /**
     * @param string $PingResult
     * @access public
     */
    public function __construct($PingResult = null){
      $this->PingResult = $PingResult;
    }

    /**
     * @return string
     */
    public function getPingResult(){
      return $this->PingResult;
    }

    /**
     * @param string $PingResult
     * @return Omni_PingResponse
     */
    public function setPingResult($PingResult){
      $this->PingResult = $PingResult;
      return $this;
    }

}
