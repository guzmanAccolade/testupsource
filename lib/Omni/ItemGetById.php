<?php

class Omni_ItemGetById {

    /**
     * @var string $itemId
     * @access public
     */
    public $itemId = null;

    /**
     * @param string $itemId
     * @access public
     */
    public function __construct($itemId = null){
      $this->itemId = $itemId;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->itemId;
    }

    /**
     * @param string $itemId
     * @return Omni_ItemGetById
     */
    public function setItemId($itemId){
      $this->itemId = $itemId;
      return $this;
    }

}
