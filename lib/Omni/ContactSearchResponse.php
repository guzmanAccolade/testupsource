<?php

class Omni_ContactSearchResponse {

    /**
     * @var Omni_ContactPOS[] $ContactSearchResult
     * @access public
     */
    public $ContactSearchResult = null;

    /**
     * @param Omni_ContactPOS[] $ContactSearchResult
     * @access public
     */
    public function __construct($ContactSearchResult = null){
      $this->ContactSearchResult = $ContactSearchResult;
    }

    /**
     * @return Omni_ContactPOS[]
     */
    public function getContactSearchResult(){
      return $this->ContactSearchResult;
    }

    /**
     * @param Omni_ContactPOS[] $ContactSearchResult
     * @return Omni_ContactSearchResponse
     */
    public function setContactSearchResult($ContactSearchResult){
      $this->ContactSearchResult = $ContactSearchResult;
      return $this;
    }

}
