<?php

class Omni_BasketCalcLineRequest {

    /**
     * @var string $CouponCode
     * @access public
     */
    public $CouponCode = null;

    /**
     * @var string $ExternalId
     * @access public
     */
    public $ExternalId = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var int $LineNumber
     * @access public
     */
    public $LineNumber = null;

    /**
     * @var float $Quantity
     * @access public
     */
    public $Quantity = null;

    /**
     * @var string $UomId
     * @access public
     */
    public $UomId = null;

    /**
     * @var string $VariantId
     * @access public
     */
    public $VariantId = null;

    /**
     * @param int $LineNumber
     * @param float $Quantity
     * @access public
     */
    public function __construct($LineNumber = null, $Quantity = null){
      $this->LineNumber = $LineNumber;
      $this->Quantity = $Quantity;
    }

    /**
     * @return string
     */
    public function getCouponCode(){
      return $this->CouponCode;
    }

    /**
     * @param string $CouponCode
     * @return Omni_BasketCalcLineRequest
     */
    public function setCouponCode($CouponCode){
      $this->CouponCode = $CouponCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getExternalId(){
      return $this->ExternalId;
    }

    /**
     * @param string $ExternalId
     * @return Omni_BasketCalcLineRequest
     */
    public function setExternalId($ExternalId){
      $this->ExternalId = $ExternalId;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_BasketCalcLineRequest
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return int
     */
    public function getLineNumber(){
      return $this->LineNumber;
    }

    /**
     * @param int $LineNumber
     * @return Omni_BasketCalcLineRequest
     */
    public function setLineNumber($LineNumber){
      $this->LineNumber = $LineNumber;
      return $this;
    }

    /**
     * @return float
     */
    public function getQuantity(){
      return $this->Quantity;
    }

    /**
     * @param float $Quantity
     * @return Omni_BasketCalcLineRequest
     */
    public function setQuantity($Quantity){
      $this->Quantity = $Quantity;
      return $this;
    }

    /**
     * @return string
     */
    public function getUomId(){
      return $this->UomId;
    }

    /**
     * @param string $UomId
     * @return Omni_BasketCalcLineRequest
     */
    public function setUomId($UomId){
      $this->UomId = $UomId;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantId(){
      return $this->VariantId;
    }

    /**
     * @param string $VariantId
     * @return Omni_BasketCalcLineRequest
     */
    public function setVariantId($VariantId){
      $this->VariantId = $VariantId;
      return $this;
    }

}
