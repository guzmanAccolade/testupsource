<?php

class Omni_Logout {

    /**
     * @var string $userName
     * @access public
     */
    public $userName = null;

    /**
     * @var string $deviceId
     * @access public
     */
    public $deviceId = null;

    /**
     * @param string $userName
     * @param string $deviceId
     * @access public
     */
    public function __construct($userName = null, $deviceId = null){
      $this->userName = $userName;
      $this->deviceId = $deviceId;
    }

    /**
     * @return string
     */
    public function getUserName(){
      return $this->userName;
    }

    /**
     * @param string $userName
     * @return Omni_Logout
     */
    public function setUserName($userName){
      $this->userName = $userName;
      return $this;
    }

    /**
     * @return string
     */
    public function getDeviceId(){
      return $this->deviceId;
    }

    /**
     * @param string $deviceId
     * @return Omni_Logout
     */
    public function setDeviceId($deviceId){
      $this->deviceId = $deviceId;
      return $this;
    }

}
