<?php

class Omni_ContactUpdateResponse {

    /**
     * @var Omni_Contact $ContactUpdateResult
     * @access public
     */
    public $ContactUpdateResult = null;

    /**
     * @param Omni_Contact $ContactUpdateResult
     * @access public
     */
    public function __construct($ContactUpdateResult = null){
      $this->ContactUpdateResult = $ContactUpdateResult;
    }

    /**
     * @return Omni_Contact
     */
    public function getContactUpdateResult(){
      return $this->ContactUpdateResult;
    }

    /**
     * @param Omni_Contact $ContactUpdateResult
     * @return Omni_ContactUpdateResponse
     */
    public function setContactUpdateResult($ContactUpdateResult){
      $this->ContactUpdateResult = $ContactUpdateResult;
      return $this;
    }

}
