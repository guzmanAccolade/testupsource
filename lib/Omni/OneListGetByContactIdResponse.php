<?php

class Omni_OneListGetByContactIdResponse {

    /**
     * @var Omni_OneList[] $OneListGetByContactIdResult
     * @access public
     */
    public $OneListGetByContactIdResult = null;

    /**
     * @param Omni_OneList[] $OneListGetByContactIdResult
     * @access public
     */
    public function __construct($OneListGetByContactIdResult = null){
      $this->OneListGetByContactIdResult = $OneListGetByContactIdResult;
    }

    /**
     * @return Omni_OneList[]
     */
    public function getOneListGetByContactIdResult(){
      return $this->OneListGetByContactIdResult;
    }

    /**
     * @param Omni_OneList[] $OneListGetByContactIdResult
     * @return Omni_OneListGetByContactIdResponse
     */
    public function setOneListGetByContactIdResult($OneListGetByContactIdResult){
      $this->OneListGetByContactIdResult = $OneListGetByContactIdResult;
      return $this;
    }

}
