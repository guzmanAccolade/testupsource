<?php

class Omni_ImageStreamGetByIdResponse {

    /**
     * @var Omni_StreamBody $ImageStreamGetByIdResult
     * @access public
     */
    public $ImageStreamGetByIdResult = null;

    /**
     * @param Omni_StreamBody $ImageStreamGetByIdResult
     * @access public
     */
    public function __construct($ImageStreamGetByIdResult = null){
      $this->ImageStreamGetByIdResult = $ImageStreamGetByIdResult;
    }

    /**
     * @return Omni_StreamBody
     */
    public function getImageStreamGetByIdResult(){
      return $this->ImageStreamGetByIdResult;
    }

    /**
     * @param Omni_StreamBody $ImageStreamGetByIdResult
     * @return Omni_ImageStreamGetByIdResponse
     */
    public function setImageStreamGetByIdResult($ImageStreamGetByIdResult){
      $this->ImageStreamGetByIdResult = $ImageStreamGetByIdResult;
      return $this;
    }

}
