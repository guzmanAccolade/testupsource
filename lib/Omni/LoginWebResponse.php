<?php

class Omni_LoginWebResponse {

    /**
     * @var Omni_Contact $LoginWebResult
     * @access public
     */
    public $LoginWebResult = null;

    /**
     * @param Omni_Contact $LoginWebResult
     * @access public
     */
    public function __construct($LoginWebResult = null){
      $this->LoginWebResult = $LoginWebResult;
    }

    /**
     * @return Omni_Contact
     */
    public function getLoginWebResult(){
      return $this->LoginWebResult;
    }

    /**
     * @param Omni_Contact $LoginWebResult
     * @return Omni_LoginWebResponse
     */
    public function setLoginWebResult($LoginWebResult){
      $this->LoginWebResult = $LoginWebResult;
      return $this;
    }

}
