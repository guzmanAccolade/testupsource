<?php

class Omni_Barcode {

    /**
     * @var boolean $Blocked
     * @access public
     */
    public $Blocked = null;

    /**
     * @var boolean $Del
     * @access public
     */
    public $Del = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $ItemId
     * @access public
     */
    public $ItemId = null;

    /**
     * @var string $Uom
     * @access public
     */
    public $Uom = null;

    /**
     * @var string $VariantId
     * @access public
     */
    public $VariantId = null;

    /**
     * @param boolean $Blocked
     * @param boolean $Del
     * @access public
     */
    public function __construct($Blocked = null, $Del = null){
      $this->Blocked = $Blocked;
      $this->Del = $Del;
    }

    /**
     * @return boolean
     */
    public function getBlocked(){
      return $this->Blocked;
    }

    /**
     * @param boolean $Blocked
     * @return Omni_Barcode
     */
    public function setBlocked($Blocked){
      $this->Blocked = $Blocked;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDel(){
      return $this->Del;
    }

    /**
     * @param boolean $Del
     * @return Omni_Barcode
     */
    public function setDel($Del){
      $this->Del = $Del;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription(){
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Omni_Barcode
     */
    public function setDescription($Description){
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Barcode
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemId(){
      return $this->ItemId;
    }

    /**
     * @param string $ItemId
     * @return Omni_Barcode
     */
    public function setItemId($ItemId){
      $this->ItemId = $ItemId;
      return $this;
    }

    /**
     * @return string
     */
    public function getUom(){
      return $this->Uom;
    }

    /**
     * @param string $Uom
     * @return Omni_Barcode
     */
    public function setUom($Uom){
      $this->Uom = $Uom;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantId(){
      return $this->VariantId;
    }

    /**
     * @param string $VariantId
     * @return Omni_Barcode
     */
    public function setVariantId($VariantId){
      $this->VariantId = $VariantId;
      return $this;
    }

}
