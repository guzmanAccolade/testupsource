<?php

class Omni_AppSettingsGetByKey {

    /**
     * @var Omni_AppSettingsKey $key
     * @access public
     */
    public $key = null;

    /**
     * @var string $languageCode
     * @access public
     */
    public $languageCode = null;

    /**
     * @param Omni_AppSettingsKey $key
     * @param string $languageCode
     * @access public
     */
    public function __construct($key = null, $languageCode = null){
      $this->key = $key;
      $this->languageCode = $languageCode;
    }

    /**
     * @return Omni_AppSettingsKey
     */
    public function getKey(){
      return $this->key;
    }

    /**
     * @param Omni_AppSettingsKey $key
     * @return Omni_AppSettingsGetByKey
     */
    public function setKey($key){
      $this->key = $key;
      return $this;
    }

    /**
     * @return string
     */
    public function getLanguageCode(){
      return $this->languageCode;
    }

    /**
     * @param string $languageCode
     * @return Omni_AppSettingsGetByKey
     */
    public function setLanguageCode($languageCode){
      $this->languageCode = $languageCode;
      return $this;
    }

}
