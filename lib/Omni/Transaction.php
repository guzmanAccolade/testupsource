<?php

class Omni_Transaction {

    /**
     * @var float $Amount
     * @access public
     */
    public $Amount = null;

    /**
     * @var string $CurrencyCode
     * @access public
     */
    public $CurrencyCode = null;

    /**
     * @var dateTime $Date
     * @access public
     */
    public $Date = null;

    /**
     * @var float $DiscountAmount
     * @access public
     */
    public $DiscountAmount = null;

    /**
     * @var string $ExternalId
     * @access public
     */
    public $ExternalId = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var float $NetAmount
     * @access public
     */
    public $NetAmount = null;

    /**
     * @var string $ReceiptNumber
     * @access public
     */
    public $ReceiptNumber = null;

    /**
     * @var Omni_SaleLine[] $SaleLines
     * @access public
     */
    public $SaleLines = null;

    /**
     * @var string $Staff
     * @access public
     */
    public $Staff = null;

    /**
     * @var string $StoreId
     * @access public
     */
    public $StoreId = null;

    /**
     * @var string $StoreName
     * @access public
     */
    public $StoreName = null;

    /**
     * @var Omni_TaxLine[] $TaxLines
     * @access public
     */
    public $TaxLines = null;

    /**
     * @var Omni_TenderLine[] $TenderLines
     * @access public
     */
    public $TenderLines = null;

    /**
     * @var string $Terminal
     * @access public
     */
    public $Terminal = null;

    /**
     * @var Omni_TransactionFooter[] $TransactionFooters
     * @access public
     */
    public $TransactionFooters = null;

    /**
     * @var Omni_TransactionHeader[] $TransactionHeaders
     * @access public
     */
    public $TransactionHeaders = null;

    /**
     * @var float $VatAmount
     * @access public
     */
    public $VatAmount = null;

    /**
     * @param string $CurrencyCode
     * @param string $ExternalId
     * @param string $ReceiptNumber
     * @param string $StoreId
     * @param string $StoreName
     * @access public
     */
    public function __construct($CurrencyCode = null, $ExternalId = null, $ReceiptNumber = null, $StoreId = null, $StoreName = null){
      $this->CurrencyCode = $CurrencyCode;
      $this->ExternalId = $ExternalId;
      $this->ReceiptNumber = $ReceiptNumber;
      $this->StoreId = $StoreId;
      $this->StoreName = $StoreName;
    }

    /**
     * @return float
     */
    public function getAmount(){
      return $this->Amount;
    }

    /**
     * @param float $Amount
     * @return Omni_Transaction
     */
    public function setAmount($Amount){
      $this->Amount = $Amount;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(){
      return $this->CurrencyCode;
    }

    /**
     * @param string $CurrencyCode
     * @return Omni_Transaction
     */
    public function setCurrencyCode($CurrencyCode){
      $this->CurrencyCode = $CurrencyCode;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getDate(){
      return $this->Date;
    }

    /**
     * @param dateTime $Date
     * @return Omni_Transaction
     */
    public function setDate($Date){
      $this->Date = $Date;
      return $this;
    }

    /**
     * @return float
     */
    public function getDiscountAmount(){
      return $this->DiscountAmount;
    }

    /**
     * @param float $DiscountAmount
     * @return Omni_Transaction
     */
    public function setDiscountAmount($DiscountAmount){
      $this->DiscountAmount = $DiscountAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getExternalId(){
      return $this->ExternalId;
    }

    /**
     * @param string $ExternalId
     * @return Omni_Transaction
     */
    public function setExternalId($ExternalId){
      $this->ExternalId = $ExternalId;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Transaction
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return float
     */
    public function getNetAmount(){
      return $this->NetAmount;
    }

    /**
     * @param float $NetAmount
     * @return Omni_Transaction
     */
    public function setNetAmount($NetAmount){
      $this->NetAmount = $NetAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getReceiptNumber(){
      return $this->ReceiptNumber;
    }

    /**
     * @param string $ReceiptNumber
     * @return Omni_Transaction
     */
    public function setReceiptNumber($ReceiptNumber){
      $this->ReceiptNumber = $ReceiptNumber;
      return $this;
    }

    /**
     * @return Omni_SaleLine[]
     */
    public function getSaleLines(){
      return $this->SaleLines;
    }

    /**
     * @param Omni_SaleLine[] $SaleLines
     * @return Omni_Transaction
     */
    public function setSaleLines($SaleLines){
      $this->SaleLines = $SaleLines;
      return $this;
    }

    /**
     * @return string
     */
    public function getStaff(){
      return $this->Staff;
    }

    /**
     * @param string $Staff
     * @return Omni_Transaction
     */
    public function setStaff($Staff){
      $this->Staff = $Staff;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(){
      return $this->StoreId;
    }

    /**
     * @param string $StoreId
     * @return Omni_Transaction
     */
    public function setStoreId($StoreId){
      $this->StoreId = $StoreId;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreName(){
      return $this->StoreName;
    }

    /**
     * @param string $StoreName
     * @return Omni_Transaction
     */
    public function setStoreName($StoreName){
      $this->StoreName = $StoreName;
      return $this;
    }

    /**
     * @return Omni_TaxLine[]
     */
    public function getTaxLines(){
      return $this->TaxLines;
    }

    /**
     * @param Omni_TaxLine[] $TaxLines
     * @return Omni_Transaction
     */
    public function setTaxLines($TaxLines){
      $this->TaxLines = $TaxLines;
      return $this;
    }

    /**
     * @return Omni_TenderLine[]
     */
    public function getTenderLines(){
      return $this->TenderLines;
    }

    /**
     * @param Omni_TenderLine[] $TenderLines
     * @return Omni_Transaction
     */
    public function setTenderLines($TenderLines){
      $this->TenderLines = $TenderLines;
      return $this;
    }

    /**
     * @return string
     */
    public function getTerminal(){
      return $this->Terminal;
    }

    /**
     * @param string $Terminal
     * @return Omni_Transaction
     */
    public function setTerminal($Terminal){
      $this->Terminal = $Terminal;
      return $this;
    }

    /**
     * @return Omni_TransactionFooter[]
     */
    public function getTransactionFooters(){
      return $this->TransactionFooters;
    }

    /**
     * @param Omni_TransactionFooter[] $TransactionFooters
     * @return Omni_Transaction
     */
    public function setTransactionFooters($TransactionFooters){
      $this->TransactionFooters = $TransactionFooters;
      return $this;
    }

    /**
     * @return Omni_TransactionHeader[]
     */
    public function getTransactionHeaders(){
      return $this->TransactionHeaders;
    }

    /**
     * @param Omni_TransactionHeader[] $TransactionHeaders
     * @return Omni_Transaction
     */
    public function setTransactionHeaders($TransactionHeaders){
      $this->TransactionHeaders = $TransactionHeaders;
      return $this;
    }

    /**
     * @return float
     */
    public function getVatAmount(){
      return $this->VatAmount;
    }

    /**
     * @param float $VatAmount
     * @return Omni_Transaction
     */
    public function setVatAmount($VatAmount){
      $this->VatAmount = $VatAmount;
      return $this;
    }

}
