<?php

class Omni_Card {

    /**
     * @var string $BlockedBy
     * @access public
     */
    public $BlockedBy = null;

    /**
     * @var string $BlockedReason
     * @access public
     */
    public $BlockedReason = null;

    /**
     * @var Omni_CardStatus $CardStatus
     * @access public
     */
    public $CardStatus = null;

    /**
     * @var dateTime $DateBlocked
     * @access public
     */
    public $DateBlocked = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var boolean $LinkedToAccount
     * @access public
     */
    public $LinkedToAccount = null;

    /**
     * @param string $BlockedBy
     * @param string $BlockedReason
     * @param Omni_CardStatus $CardStatus
     * @param dateTime $DateBlocked
     * @param boolean $LinkedToAccount
     * @access public
     */
    public function __construct($BlockedBy = null, $BlockedReason = null, $CardStatus = null, $DateBlocked = null, $LinkedToAccount = null){
      $this->BlockedBy = $BlockedBy;
      $this->BlockedReason = $BlockedReason;
      $this->CardStatus = $CardStatus;
      $this->DateBlocked = $DateBlocked;
      $this->LinkedToAccount = $LinkedToAccount;
    }

    /**
     * @return string
     */
    public function getBlockedBy(){
      return $this->BlockedBy;
    }

    /**
     * @param string $BlockedBy
     * @return Omni_Card
     */
    public function setBlockedBy($BlockedBy){
      $this->BlockedBy = $BlockedBy;
      return $this;
    }

    /**
     * @return string
     */
    public function getBlockedReason(){
      return $this->BlockedReason;
    }

    /**
     * @param string $BlockedReason
     * @return Omni_Card
     */
    public function setBlockedReason($BlockedReason){
      $this->BlockedReason = $BlockedReason;
      return $this;
    }

    /**
     * @return Omni_CardStatus
     */
    public function getCardStatus(){
      return $this->CardStatus;
    }

    /**
     * @param Omni_CardStatus $CardStatus
     * @return Omni_Card
     */
    public function setCardStatus($CardStatus){
      $this->CardStatus = $CardStatus;
      return $this;
    }

    /**
     * @return dateTime
     */
    public function getDateBlocked(){
      return $this->DateBlocked;
    }

    /**
     * @param dateTime $DateBlocked
     * @return Omni_Card
     */
    public function setDateBlocked($DateBlocked){
      $this->DateBlocked = $DateBlocked;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_Card
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getLinkedToAccount(){
      return $this->LinkedToAccount;
    }

    /**
     * @param boolean $LinkedToAccount
     * @return Omni_Card
     */
    public function setLinkedToAccount($LinkedToAccount){
      $this->LinkedToAccount = $LinkedToAccount;
      return $this;
    }

}
