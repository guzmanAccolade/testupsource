<?php

class Omni_ImageView {

    /**
     * @var string $AvgColor
     * @access public
     */
    public $AvgColor = null;

    /**
     * @var int $DisplayOrder
     * @access public
     */
    public $DisplayOrder = null;

    /**
     * @var string $Format
     * @access public
     */
    public $Format = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $Image
     * @access public
     */
    public $Image = null;

    /**
     * @var Omni_ImageSize $ImgSize
     * @access public
     */
    public $ImgSize = null;

    /**
     * @var string $Location
     * @access public
     */
    public $Location = null;

    /**
     * @var Omni_LocationType $LocationType
     * @access public
     */
    public $LocationType = null;

    /**
     * @var string $ObjectId
     * @access public
     */
    public $ObjectId = null;

    /**
     * @param int $DisplayOrder
     * @param Omni_LocationType $LocationType
     * @access public
     */
    public function __construct($DisplayOrder = null, $LocationType = null){
      $this->DisplayOrder = $DisplayOrder;
      $this->LocationType = $LocationType;
    }

    /**
     * @return string
     */
    public function getAvgColor(){
      return $this->AvgColor;
    }

    /**
     * @param string $AvgColor
     * @return Omni_ImageView
     */
    public function setAvgColor($AvgColor){
      $this->AvgColor = $AvgColor;
      return $this;
    }

    /**
     * @return int
     */
    public function getDisplayOrder(){
      return $this->DisplayOrder;
    }

    /**
     * @param int $DisplayOrder
     * @return Omni_ImageView
     */
    public function setDisplayOrder($DisplayOrder){
      $this->DisplayOrder = $DisplayOrder;
      return $this;
    }

    /**
     * @return string
     */
    public function getFormat(){
      return $this->Format;
    }

    /**
     * @param string $Format
     * @return Omni_ImageView
     */
    public function setFormat($Format){
      $this->Format = $Format;
      return $this;
    }

    /**
     * @return string
     */
    public function getId(){
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return Omni_ImageView
     */
    public function setId($Id){
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getImage(){
      return $this->Image;
    }

    /**
     * @param string $Image
     * @return Omni_ImageView
     */
    public function setImage($Image){
      $this->Image = $Image;
      return $this;
    }

    /**
     * @return Omni_ImageSize
     */
    public function getImgSize(){
      return $this->ImgSize;
    }

    /**
     * @param Omni_ImageSize $ImgSize
     * @return Omni_ImageView
     */
    public function setImgSize($ImgSize){
      $this->ImgSize = $ImgSize;
      return $this;
    }

    /**
     * @return string
     */
    public function getLocation(){
      return $this->Location;
    }

    /**
     * @param string $Location
     * @return Omni_ImageView
     */
    public function setLocation($Location){
      $this->Location = $Location;
      return $this;
    }

    /**
     * @return Omni_LocationType
     */
    public function getLocationType(){
      return $this->LocationType;
    }

    /**
     * @param Omni_LocationType $LocationType
     * @return Omni_ImageView
     */
    public function setLocationType($LocationType){
      $this->LocationType = $LocationType;
      return $this;
    }

    /**
     * @return string
     */
    public function getObjectId(){
      return $this->ObjectId;
    }

    /**
     * @param string $ObjectId
     * @return Omni_ImageView
     */
    public function setObjectId($ObjectId){
      $this->ObjectId = $ObjectId;
      return $this;
    }

}
