<?php

class Omni_StoreGetByIdResponse {

    /**
     * @var Omni_Store $StoreGetByIdResult
     * @access public
     */
    public $StoreGetByIdResult = null;

    /**
     * @param Omni_Store $StoreGetByIdResult
     * @access public
     */
    public function __construct($StoreGetByIdResult = null){
      $this->StoreGetByIdResult = $StoreGetByIdResult;
    }

    /**
     * @return Omni_Store
     */
    public function getStoreGetByIdResult(){
      return $this->StoreGetByIdResult;
    }

    /**
     * @param Omni_Store $StoreGetByIdResult
     * @return Omni_StoreGetByIdResponse
     */
    public function setStoreGetByIdResult($StoreGetByIdResult){
      $this->StoreGetByIdResult = $StoreGetByIdResult;
      return $this;
    }

}
