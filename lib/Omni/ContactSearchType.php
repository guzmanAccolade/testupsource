<?php

class Omni_ContactSearchType {

	const __default     = 'CardId';
	const CardId        = 'CardId';
	const ContactNumber = 'ContactNumber';
	const PhoneNumber   = 'PhoneNumber';
	const Email         = 'Email';
	const Name          = 'Name';

}
