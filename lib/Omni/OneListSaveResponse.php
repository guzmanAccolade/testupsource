<?php

class Omni_OneListSaveResponse {

    /**
     * @var Omni_OneList $OneListSaveResult
     * @access public
     */
    public $OneListSaveResult = null;

    /**
     * @param Omni_OneList $OneListSaveResult
     * @access public
     */
    public function __construct($OneListSaveResult = null){
      $this->OneListSaveResult = $OneListSaveResult;
    }

    /**
     * @return Omni_OneList
     */
    public function getOneListSaveResult(){
      return $this->OneListSaveResult;
    }

    /**
     * @param Omni_OneList $OneListSaveResult
     * @return Omni_OneListSaveResponse
     */
    public function setOneListSaveResult($OneListSaveResult){
      $this->OneListSaveResult = $OneListSaveResult;
      return $this;
    }

}
