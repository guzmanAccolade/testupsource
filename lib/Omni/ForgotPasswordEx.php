<?php

class Omni_ForgotPasswordEx {

    /**
     * @var string $userNameOrEmail
     * @access public
     */
    public $userNameOrEmail = null;

    /**
     * @var string $resetCode
     * @access public
     */
    public $resetCode = null;

    /**
     * @var string $emailSubject
     * @access public
     */
    public $emailSubject = null;

    /**
     * @var string $emailBody
     * @access public
     */
    public $emailBody = null;

    /**
     * @param string $userNameOrEmail
     * @param string $resetCode
     * @param string $emailSubject
     * @param string $emailBody
     * @access public
     */
    public function __construct($userNameOrEmail = null, $resetCode = null, $emailSubject = null, $emailBody = null){
      $this->userNameOrEmail = $userNameOrEmail;
      $this->resetCode = $resetCode;
      $this->emailSubject = $emailSubject;
      $this->emailBody = $emailBody;
    }

    /**
     * @return string
     */
    public function getUserNameOrEmail(){
      return $this->userNameOrEmail;
    }

    /**
     * @param string $userNameOrEmail
     * @return Omni_ForgotPasswordEx
     */
    public function setUserNameOrEmail($userNameOrEmail){
      $this->userNameOrEmail = $userNameOrEmail;
      return $this;
    }

    /**
     * @return string
     */
    public function getResetCode(){
      return $this->resetCode;
    }

    /**
     * @param string $resetCode
     * @return Omni_ForgotPasswordEx
     */
    public function setResetCode($resetCode){
      $this->resetCode = $resetCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmailSubject(){
      return $this->emailSubject;
    }

    /**
     * @param string $emailSubject
     * @return Omni_ForgotPasswordEx
     */
    public function setEmailSubject($emailSubject){
      $this->emailSubject = $emailSubject;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmailBody(){
      return $this->emailBody;
    }

    /**
     * @param string $emailBody
     * @return Omni_ForgotPasswordEx
     */
    public function setEmailBody($emailBody){
      $this->emailBody = $emailBody;
      return $this;
    }

}
