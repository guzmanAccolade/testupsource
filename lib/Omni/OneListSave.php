<?php

class Omni_OneListSave {

    /**
     * @var Omni_OneList $oneList
     * @access public
     */
    public $oneList = null;

    /**
     * @param Omni_OneList $oneList
     * @access public
     */
    public function __construct($oneList = null){
      $this->oneList = $oneList;
    }

    /**
     * @return Omni_OneList
     */
    public function getOneList(){
      return $this->oneList;
    }

    /**
     * @param Omni_OneList $oneList
     * @return Omni_OneListSave
     */
    public function setOneList($oneList){
      $this->oneList = $oneList;
      return $this;
    }

}
