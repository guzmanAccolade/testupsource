<?php

class Accolade_Omni_Block_Adminhtml_Renderer_Order_Export_Log extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Options
{

    public function render(Varien_Object $row)
    {
        $html = '';

        /** @var Accolade_Omni_Model_Order $row */

        $records = $row->getExportLog();
        $date = Mage::getModel('core/date');

        if (is_array($records)) {
            $records = array_reverse($records); //new ones on top

            foreach ($records as $record) {
                $class = 'export-order-log-error';

                if ($record['type'] == $row::EXPORT_SUCCESS) {
                    $class = 'export-order-log-success';
                } else if ($record['type'] == $row::EXPORT_SKIPPED) {
                    $class = 'export-order-log-notice';
                }

                $html .= '<div class="' . $class . '">';
                $html .= $date->date(null, $record['timestamp']) . ' - ';
                $html .= $record['message'];
                $html .= '</div>';
            }

        }

        return $html;
    }

}