<?php

class Accolade_Omni_Block_Adminhtml_Renderer_Order_Link extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

	public function render(Varien_Object $row)
    {
		$url = Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/view', array('order_id' => $row->getData('order_id')));

		return '<a href="' . $url . '">' . $row->getData('increment_id') . '</a>';
	}

}