<?php

class Accolade_Omni_Block_Adminhtml_Renderer_Order_Export_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Options
{

    public function render(Varien_Object $row)
    {
        /** @var  Accolade_Omni_Model_Order $row */
        $labels = $row->getExportStatuses();

        $value = $row->getData('export_status');

        if (!isset($labels[$value])) {
            $value = $row::EXPORT_UNKNOWN;
        }

        if ($value == $row::EXPORT_SUCCESS || $value == $row::EXPORT_SKIPPED) {
            $class = 'grid-severity-notice';
        } else if ($value == $row::EXPORT_FAILED) {
            $class = 'grid-severity-critical';
        } else {
            $class = 'grid-severity-minor';
        }

        return '<span class="' . $class . '"><span>' . $this->__($labels[$value]) . '</span></span>';
    }

}