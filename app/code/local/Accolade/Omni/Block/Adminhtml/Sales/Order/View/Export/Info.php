<?php
/**
 * @method Accolade_Omni_Block_Adminhtml_Sales_Order_View_Export_Info setOrder(Mage_Sales_Model_Order $value)
 */
class Accolade_Omni_Block_Adminhtml_Sales_Order_View_Export_Info extends Mage_Adminhtml_Block_Template
{

    public function getOrder()
    {
        if ($this->getData('order') instanceof Mage_Sales_Model_Order
            && $orderId = $this->getData('order')->getId()) {

            $order = Mage::getModel('accolade_omni/order')->load($orderId);
            return $order;
        }
        return false;
    }

}
