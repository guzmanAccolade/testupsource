<?php

class Accolade_Omni_Block_Adminhtml_Order_Index_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('accolade_omni_order_index_grid');
        $this->setDefaultSort('order_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getModel('accolade_omni/order')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }


    protected function _prepareColumns()
    {
        $this->addColumn('order_id', array(
            'header'    => $this->__('ID'),
            'width'     => '25px',
            'index'     => 'order_id'
        ));

        $this->addColumn('increment_id', array(
            'header'    => $this->__('Increment ID'),
            'width'     => '50px',
            'index'     => 'increment_id',
            'renderer'  => 'accolade_omni/adminhtml_renderer_order_link'
        ));

        $this->addColumn('export_status', array(
            'header'    => $this->__('Export Status'),
            'width'     => '50px',
            'index'     => 'export_status',
            'type'      => 'options',
            'options'   => Accolade_Omni_Model_Order::getExportStatuses(),
            'renderer'  => 'accolade_omni/adminhtml_renderer_order_export_status'
        ));

        $this->addColumn('export_date', array(
            'header'    => $this->__('Export Time'),
            'width'     => '75px',
            'type'      => 'datetime',
            'index'     => 'export_date'
        ));

        $this->addColumn('nav_order_id', array(
            'header'    => $this->__('NAV Order ID'),
            'width'     => '50px',
            'index'     => 'nav_order_id'
        ));

        $this->addColumn('nav_transaction_id', array(
            'header'    => $this->__('NAV Transaction ID'),
            'width'     => '200px',
            'index'     => 'nav_transaction_id'
        ));

        $this->addColumn('export_log', array(
            'header'    => $this->__('Export Log'),
            'width'     => '400px',
            'index'     => 'export_log',
            'renderer'  => 'accolade_omni/adminhtml_renderer_order_export_log',
            'sortable'  => false
        ));

        return parent::_prepareColumns();
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('order_id');
        $this->getMassactionBlock()->setFormFieldName('order_ids');

        $this->getMassactionBlock()->addItem('flush_log', array(
            'label'=> $this->__('Flush Log'),
            'url'  => $this->getUrl('*/*/massFlushLog'),
            'confirm' => $this->__('Are you sure you want to flush logs for selected orders?')
        ));

        $this->getMassactionBlock()->addItem('export', array(
            'label'=> $this->__('Export'),
            'url'  => $this->getUrl('*/*/massExport'),
            'confirm' => $this->__('Are you sure you want to export selected orders? Already successful ones will be discarded.')
        ));

        $this->getMassactionBlock()->addItem('skipped', array(
            'label'=> $this->__('Skip'),
            'url'  => $this->getUrl('*/*/massSkip'),
            'confirm' => $this->__('Are you sure you want to skip selected orders? Already successful ones will be discarded.')
        ));

        if (Mage::getStoreConfigFlag('accolade_omni/orders/mass_reset_enabled')) {
            $this->getMassactionBlock()->addItem('reset', array(
                'label'=> $this->__('Reset Info'),
                'url'  => $this->getUrl('*/*/massReset'),
                'confirm' => $this->__('Are you sure you want to reset all the export related data for selected orders? Info cannot be recovered.')
            ));
        }

        return $this;
    }


    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

}
