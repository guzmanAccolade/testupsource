<?php

class Accolade_Omni_Block_Adminhtml_Order_Index extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_blockGroup = 'accolade_omni';
        $this->_controller = 'adminhtml_order_index';
        $this->_headerText = $this->__('Manage Order Exports');

        parent::__construct();

        $this->_removeButton('add');
    }

}
