<?php

class Accolade_Omni_Block_Adminhtml_System_Config_Form_Field_PaymentMapping extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{

    public function __construct()
    {
        $this->addColumn('method', array(
            'label' => $this->__('Payment Method Code') . ' <span class="required">*</span>',
            'style' => 'width: 150px')
        );

        $this->addColumn('product_no', array(
            'label' => $this->__('NAV Product No') . ' <span class="required">*</span>',
            'style' => 'width: 150px',)
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = $this->__('Add Row');

        return parent::__construct();
    }

}