<?php

class Accolade_Omni_Block_Rewrite_Adminhtml_Customer_Edit_Tab_Addresses extends Mage_Adminhtml_Block_Customer_Edit_Tab_Addresses {

	public function __construct(){
		parent::__construct();
		$this->setTemplate('accolade/omni/customer/tab/addresses.phtml');
	}

	public function initForm(){
		parent::initForm();

		/* @var Accolade_Omni_Model_Rewrite_Customer $customer */
		$customer = Mage::registry('current_customer');

		$contactAddress = $customer->getPrimaryContactAddress();
		if(!$contactAddress) $contactAddress = Mage::getModel('customer/address');
		$this->assign('contactAddress', $contactAddress);

		return $this;
	}

}