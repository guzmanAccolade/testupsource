<?php

class Accolade_Omni_Block_Rewrite_Customer_Account_Dashboard_Address extends Mage_Customer_Block_Account_Dashboard_Address {

	public function getPrimaryContactAddressHtml(){
		$address = $this->getCustomer()->getPrimaryContactAddress();

		if($address instanceof Varien_Object){
			return $address->format('html');
		} else {
			return Mage::helper('customer')->__('You have not set a default contact address.');
		}
	}

}