<?php

class Accolade_Omni_Block_Rewrite_Customer_Account_Changeforgotten extends Mage_Customer_Block_Account_Changeforgotten
{
    /**
     * Retrieve form data
     *
     * @return Varien_Object
     */
    public function getFormData()
    {
        $data = $this->getData('form_data');
        if (is_null($data)) {
            $formData = Mage::getSingleton('customer/session')->getCustomerFormData(true);
            $data = new Varien_Object();
            if ($formData) {
                $data->addData($formData);
                $data->setCustomerData(1);
            }
            $this->setData('form_data', $data);
        }
        return $data;
    }
}
