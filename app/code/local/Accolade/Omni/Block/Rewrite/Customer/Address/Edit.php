<?php

class Accolade_Omni_Block_Rewrite_Customer_Address_Edit extends Mage_Customer_Block_Address_Edit {

	/**
	 * @param Mage_Customer_Model_Address $address
	 * @return $this
	 */
	public function setAddress($address){
		// either non-existent or does not belong to this customer
		if(!($address instanceof Mage_Customer_Model_Address) || $address->getCustomerId() != $this->getCustomer()->getId()){
			$address = Mage::getModel('customer/address');
		}
		$this->_address = $address;
		return $this;
	}

}