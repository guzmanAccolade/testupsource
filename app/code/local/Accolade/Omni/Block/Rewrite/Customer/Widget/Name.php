<?php

class Accolade_Omni_Block_Rewrite_Customer_Widget_Name extends Mage_Customer_Block_Widget_Name
{
    public function _construct()
    {
        parent::_construct();

        // default template location
        $this->setTemplate('accolade/omni/customer/widget/name.phtml');
    }
}
