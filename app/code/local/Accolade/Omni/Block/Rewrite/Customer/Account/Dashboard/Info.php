<?php

class Accolade_Omni_Block_Rewrite_Customer_Account_Dashboard_Info extends Mage_Customer_Block_Account_Dashboard_Info {

	public function getChangePasswordUrl(){
		return Mage::getUrl('*/account/changepass');
	}

	public function getContactAddressHtml(){
		$address = $this->getCustomer()->getPrimaryContactAddress();

		if( $address instanceof Varien_Object ) {
			return $address->format('html');
		} else {
			return Mage::helper('customer')->__('You do not have a contact address.');
		}
	}

}