<?php

class Accolade_Omni_Helper_Config extends Mage_Core_Helper_Abstract {

	const GENERAL_CONFIG = 'accolade_omni/general/';


	public function isEnabled(){
		return Mage::getStoreConfigFlag(self::GENERAL_CONFIG . 'enabled');
	}

	public function getMode(){
		$mode = Mage::getStoreConfig(self::GENERAL_CONFIG . 'mode');
		if($mode != 'live') $mode = 'dev';
		return $mode;
	}

	public function getURL(){
		$mode = $this->getMode();
		return Mage::getStoreConfig(self::GENERAL_CONFIG . 'url_' . $mode);
	}
}