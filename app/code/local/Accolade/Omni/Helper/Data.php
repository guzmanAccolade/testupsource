<?php

class Accolade_Omni_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * @return bool
     */
    public function isOrderExportAllowed()
    {
        //omni enabled and has admin access.
        return Mage::helper('accolade_omni/config')->isEnabled() && Mage::getSingleton('admin/session')->isAllowed('accolade_omni/order');
    }

}