<?php

require_once 'Mage/Customer/controllers/AccountController.php';

class Accolade_Omni_Rewrite_Customer_AccountController extends Mage_Customer_AccountController {

	const OMNI_RESET_CODE = "omni_resetcode";

	/**
	 * Gets customer address on register!
	 *
	 * @param Mage_Customer_Model_Customer $customer
	 * @return array $errors
	 */
	protected function _getErrorsOnCustomerAddress($customer){
		$errors = parent::_getErrorsOnCustomerAddress($customer);

		$address = reset($customer->getAddresses());
		if($address) $address->setIsDefaultContact(true);
		return $errors;
	}


	/**
    * Login post action
    */
    public function loginPostAction(){
	    if(!Mage::helper('accolade_omni/config')->isEnabled()){
		    return parent::loginPostAction();
	    }

        if(!$this->_validateFormKey() || $this->_getSession()->isLoggedIn()){
	        return $this->_redirect('*/*/');
        }
        $session = $this->_getSession();

        if($this->getRequest()->isPost()){
            $login = $this->getRequest()->getPost('login');
            if(!empty($login['username']) && !empty($login['password'])){
                try {
                    // authenticate with Omni
                    $customer = Mage::getSingleton('accolade_omni/omni_customer')->authenticate($login['username'], $login['password']);

                    if($customer && $customer->getId()){
                        Mage::log('$customer : '. @var_export($customer->getId(), true), null, '_omni_test.log');
                        $session->setCustomerAsLoggedIn($customer);
                    } else {
                        throw Mage::exception(
                            'Mage_Core',
                            Mage::helper('customer')->__('Invalid login or password.'),
                            Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD
                        );
                    }
                } catch(Mage_Core_Exception $e){
                    switch($e->getCode()){
                        case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                            $message = $this->_getHelper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $this->_getHelper('customer')->getEmailConfirmationUrl($login['username']));
                            break;
                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                            $message = $e->getMessage();
                            break;
                        default:
                            $message = $e->getMessage();
                    }
                    $session->addError($message);
                    $session->setUsername($login['username']);
                    Mage::log('EXCEPTION '. @var_export($e->getMessage(), true), null, '_omni_test_error.log');
                } catch (Exception $e){
                    Mage::log('EXCEPTION 2 '. @var_export($e->getMessage(), true), null, '_omni_test_error.log');
                }
            } else {
                $session->addError($this->__('Login and password are required.'));
            }
        }

        return $this->_loginPostRedirect();
    }


	/**
	 * Create customer account action
	 */
	public function createPostAction(){
		if(!Mage::helper('accolade_omni/config')->isEnabled()){
			return parent::createPostAction();
		}

		if($this->_getSession()->isLoggedIn()){
			return $this->_redirect('*/*/');
		}

		if($this->_validateFormKey() && $this->getRequest()->isPost()){
			/** @var $session Mage_Customer_Model_Session */
			$session = $this->_getSession();

			$customer = $this->_getCustomer();
			$postData = $this->getRequest()->getPost();

			try {
				$errors = $this->_getCustomerErrors($customer);
				if(empty($errors)){
					// throws exception which will be caught further down
					Mage::getSingleton('accolade_omni/omni_customer')->create($customer, true);

					$this->_dispatchRegisterSuccess($customer);
					return $this->_successProcessRegistration($customer);
				} else {
					$this->_addSessionError($errors);
				}
			} catch(Mage_Core_Exception $e){
				$session->setCustomerFormData($this->getRequest()->getPost());
				if($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS){
					$url = $this->_getUrl('customer/account/forgotpassword');
					$message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
				} else {
					$message = $this->_escapeHtml($e->getMessage());
				}
				$session->addError($message);
				$session->setCustomerFormData($postData);
			} catch(Exception $e){
				$session->setCustomerFormData($postData);
				$session->addException($e, $this->__('Cannot save the customer.'));
			}
		}

		$errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
		return $this->_redirectError($errUrl);
	}



	/**
	 * Forgot customer account information page
	 */
	public function editAction(){
		if(Mage::helper('accolade_omni/config')->isEnabled()){
			// customer form data is added in parent, but address data not
			$addressData    = $this->_getSession()->getAddressFormData(true);
			if(empty($data)){
				/** @var Accolade_Omni_Model_Rewrite_Customer $customer */
				$customer = $this->_getSession()->getCustomer();
				// first time loading edit form - no form data has been submitted
				try {
					Mage::getSingleton('accolade_omni/omni_customer')->importByCustomer($customer);
					$customer->load($customer->getId());
					/** @var Accolade_Omni_Model_Rewrite_Customer_Address $address */
					$address = $customer->getPrimaryContactAddress();
					if($address && !empty($customerData)){
						$address->addData($addressData);
					}

				} catch(Exception $e){
					$this->_getSession()->logout()->renewSession();
					$this->_getSession()->addError($e->getMessage());
                                        $this->_getSession()->addException($exception, $this->__('Cannot log in customer.'));
					$this->_redirect('*/*/login');
					return;
				}
			}
		}

		parent::editAction();
	}

	/**
	 * Change customer password action
	 */
	public function editPostAction(){
		if(!Mage::helper('accolade_omni/config')->isEnabled()){
			return parent::editPostAction();
		}

		if(!$this->_validateFormKey()){
			return $this->_redirect('*/*/edit');
		}

		if($this->getRequest()->isPost()){
			$errors = array();

			/** CUSTOMER VALIDATION **/

			/** @var Accolade_Omni_Model_Rewrite_Customer $customer */
			$customer = $this->_getSession()->getCustomer();

			/** @var $customerForm Mage_Customer_Model_Form */
			$customerForm = $this->_getModel('customer/form');
			$customerForm->setFormCode('customer_account_edit')
				->setEntity($customer);

			$customerData   = $customerForm->extractData($this->getRequest());
			$customerErrors = $customerForm->validateData($customerData);
			if($customerErrors !== true){
				$errors = array_merge($customerErrors, $errors);
			} else {
				$customerForm->compactData($customerData);
				$errors = array();

				// Validate account and compose list of errors if any
				$customerErrors = $customer->validate();
				if(is_array($customerErrors)){
					$errors = array_merge($errors, $customerErrors);
				}
			}

			/** ADDRESS VALIDATION **/

			/** @var Accolade_Omni_Model_Rewrite_Customer_Address $address */
			$address = $customer->getPrimaryContactAddress();

			if(!$address && $this->getRequest()->getPost('create_address', false)){
				/* @var $address Mage_Customer_Model_Address */
				$address = $this->_getModel('customer/address');
				$address->setData('create_address', true);
			}

			if($address){
				/* @var Mage_Customer_Model_Form $addressForm */
				$addressForm = $this->_getModel('customer/form');
				$addressForm->setFormCode('customer_address_edit')->setEntity($address);
				$addressData    = $addressForm->extractData($this->getRequest());
				$addressErrors  = $addressForm->validateData($addressData);
				if($addressErrors !== true){
					$errors = $addressErrors;
				}

				$addressForm->compactData($addressData);
				$address->setCustomerId($customer->getId())
					->setIsDefaultContact(true)// this is always true!!!
					->setIsDefaultBilling($this->getRequest()->getParam('default_billing', false))
					->setIsDefaultShipping($this->getRequest()->getParam('default_shipping', false));

				$addressErrors = $address->validate();
				if($addressErrors !== true){
					$errors = array_merge($errors, $addressErrors);
				} else {
					if(!$address->getId()){
						$customer->addAddress($address);
					} // if new address
				}
				// setting it so since primary contact is not saved yet
				$customer->setContactAddress($address);
			}


			if(!empty($errors)){
				$this->_getSession()->setCustomerFormData($this->getRequest()->getPost());
				$this->_getSession()->setAddressFormData($this->getRequest()->getPost());
				foreach($errors as $message){
					$this->_getSession()->addError($message);
				}
				$this->_redirect('*/*/edit');
				return $this;
			}

			// address updating
			try {
                Mage::getSingleton('accolade_omni/omni_customer')->update($customer, true);

				$this->_getSession()->setCustomer($customer)
					->addSuccess($this->__('The account information has been saved.'));

				return $this->_redirect('customer/account');

			} catch(Exception $e){
				$this->_getSession()->setAddressFormData($this->getRequest()->getPost());
				$this->_getSession()->setCustomerFormData($this->getRequest()->getPost());

				if($e instanceof Accolade_Omni_Exception){
					if($e->getCode() == Accolade_Omni_Model_Omni_Exception::EXCEPTION_SESSION_EXPIRED){
						$this->_getSession()->logout()->renewSession();
						$this->_getSession()->addError($e->getMessage());
						return $this->_redirect('*/*/login');
					}

					$this->_getSession()->addError($e->getMessage());
				} else {

					if($e instanceof Mage_Core_Exception){
						$this->_getSession()->addError($e->getMessage());
					} else {
						$this->_getSession()->addException($e, $this->__('Cannot save the customer.'));
					}
				}
			}
		}

		return $this->_redirect('*/*/edit');
	}

	public function changePassAction(){
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		$this->_initLayoutMessages('catalog/session');

		$block = $this->getLayout()->getBlock('customer_edit');
		if($block){
			$block->setRefererUrl($this->_getRefererUrl());
		}
		$data = $this->_getSession()->getCustomerFormData(true);
		$customer = $this->_getSession()->getCustomer();
		if(!empty($data)){
			$customer->addData($data);
		}

		$this->getLayout()->getBlock('head')->setTitle($this->__('Change Password'));
		$this->getLayout()->getBlock('messages')->setEscapeMessageFlag(true);
		$this->renderLayout();
	}

	public function changePassPostAction(){
		if($this->_validateFormKey() && $this->getRequest()->isPost()){

			/** @var $customer Mage_Customer_Model_Customer */
			$customer = $this->_getSession()->getCustomer();

			$currPass   = $this->getRequest()->getPost('current_password');
			$newPass    = $this->getRequest()->getPost('password');
			$confPass   = $this->getRequest()->getPost('confirmation');

			$errors = array();
			if(!Zend_Validate::is($currPass, 'NotEmpty')){
				$errors[] = Mage::helper('customer')->__('Current password field cannot be empty.');
			}
			if(!Zend_Validate::is($newPass, 'NotEmpty')){
				$errors[] = Mage::helper('customer')->__('New password field cannot be empty.');
			} else if(!Zend_Validate::is($newPass, 'StringLength', array(6))){
				$errors[] = Mage::helper('customer')->__('The minimum password length is %s', 6);
			} else if($newPass != $confPass){
				$errors[] = Mage::helper('customer')->__('Please make sure your passwords match.');
			}


			if(!empty($errors)){
				foreach($errors as $message){
					$this->_getSession()->addError($message);
				}
				return $this->_redirect('*/*/changepass');
			}

			try {
				if(Mage::helper('accolade_omni/config')->isEnabled()){
					Mage::getSingleton('accolade_omni/omni_customer')->changePassword($customer, $currPass, $newPass);
				} else {
					$customer->setPassword($newPass);
					$customer->cleanPasswordsValidationData();
					$customer->save();
				}

				$this->_getSession()->addSuccess($this->_getHelper('customer')->__('Your password has been updated.'));

				return $this->_redirect('customer/account');
			} catch(Accolade_Omni_Exception $e){
				if($e->getCode() == Accolade_Omni_Model_Omni_Exception::EXCEPTION_SESSION_EXPIRED){
					$this->_getSession()->logout()->renewSession();
					$this->_getSession()->addError($e->getMessage());
					return $this->_redirect('*/*/login');
				}
				$this->_getSession()->addError($e->getMessage());
			} catch(Mage_Core_Exception $e){
				$this->_getSession()->addError($e->getMessage());
			} catch(Exception $e){
				$this->_getSession()->addException($e, $this->__('Cannot save password.'));
			}
		}

		return $this->_redirect('*/*/changepass');
	}

    /**
     * Forgot customer password action
     */
    public function forgotPasswordPostAction(){
	    if(!Mage::helper('accolade_omni/config')->isEnabled()){
		    return parent::forgotPasswordPostAction();
	    }
        $email = (string) $this->getRequest()->getPost('email');
        if($email){
            if(!Zend_Validate::is($email, 'EmailAddress')){
                $this->_getSession()->setForgottenEmail($email);
                $this->_getSession()->addError($this->__('Invalid email address.'));
	            return $this->_redirect('*/*/forgotpassword');
            }

            /** @var $customer Mage_Customer_Model_Customer */
            $customer = $this->_getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByEmail($email);

            if($customer->getId()){
                try {
                    Mage::getSingleton('accolade_omni/omni_customer')->sendPasswordResetConfirmationEmail($customer);
                } catch (Exception $exception){
                    $this->_getSession()->addError($exception->getMessage());
	                return $this->_redirect('*/*/forgotpassword');
                }
            }

            $this->_getSession()
                ->addSuccess( $this->_getHelper('customer')
                ->__('If there is an account associated with %s you will receive an email with a link to reset your password.',
                    $this->_getHelper('customer')->escapeHtml($email)));
	        return $this->_redirect('*/*/');
        } else {
            $this->_getSession()->addError($this->_getHelper('customer')->__('Please enter your email.'));
	        return $this->_redirect('*/*/forgotpassword');
        }
    }


    /**
     * Reset forgotten password
     * Used to handle data recieved from reset forgotten password form
     */
    public function resetPasswordPostAction(){
	    if(!Mage::helper('accolade_omni/config')->isEnabled()){
		    return parent::resetPasswordPostAction();
	    }

	    list($customerId, $resetCode) = $this->_getRestorePasswordParameters($this->_getSession());
	    $email      = (string)$this->getRequest()->getPost('email');    // need this for sending to Omni
	    $password   = (string)$this->getRequest()->getPost('password');
	    $passwordConfirmation = (string)$this->getRequest()->getPost('confirmation');

	    try {
		    $this->_validateResetPasswordLinkToken($customerId, $resetCode);
	    } catch (Exception $exception){
		    $this->_getSession()->addError($exception->getMessage());
		    return $this->_redirect('*/*/');
	    }

	    $errorMessages = array();
	    if(iconv_strlen($password) <= 0){
		    array_push($errorMessages, $this->_getHelper('customer')->__('New password field cannot be empty.'));
	    }

	    /** @var Accolade_Omni_Model_Rewrite_Customer $customer */
	    $customer = $this->_getModel('customer/customer');
	    $customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($email);

	    if(!$customer->getId() || !$customer->getOmniId()){
		    $errorMessages[] = $this->_getHelper('customer')->__('Wrong customer account specified.');
	    } else {
		    $customer->setPassword($password);
		    $customer->setPasswordConfirmation($passwordConfirmation);
		    $validationErrorMessages = $customer->validate();
		    if(is_array($validationErrorMessages)){
			    $errorMessages = array_merge($errorMessages, $validationErrorMessages);
		    }
	    }

	    if(!empty($errorMessages)){
		    $this->_getSession()->setCustomerFormData($this->getRequest()->getPost());
		    foreach($errorMessages as $errorMessage){
			    $this->_getSession()->addError($errorMessage);
		    }
		    return $this->_redirect('*/*/changeforgotten');
	    }

	    try {
		    Mage::getSingleton('accolade_omni/omni_customer')->resetPassword($customer, $resetCode, $customer->getPassword());

		    $this->_getSession()->unsetData(self::OMNI_RESET_CODE);
		    $this->_getSession()->unsetData(self::CUSTOMER_ID_SESSION_NAME);

		    $this->_getSession()->addSuccess($this->_getHelper('customer')->__('Your password has been updated.'));
		    return $this->_redirect('*/*/login');
	    } catch(Exception $exception){
	    	if($exception->getCode() == Mage_Customer_Model_Customer::EXCEPTION_INVALID_RESET_PASSWORD_LINK_TOKEN){
			    $this->_getSession()->addError($exception->getMessage());
			    return $this->_redirect('*/*/');
		    }
		    $this->_getSession()->addException($exception, $this->__('Cannot save a new password.'));
		    return $this->_redirect('*/*/changeforgotten');
	    }
    }

	/**
	 * Check if password reset token is valid
	 *
	 * @param int $customerId
	 * @param string $resetCodeToken
	 * @throws Mage_Core_Exception
	 */
	protected function _validateResetPasswordLinkToken($customerId, $resetCodeToken){
		if(!Mage::helper('accolade_omni/config')->isEnabled()){
			parent::_validateResetPasswordLinkToken($customerId, $resetCodeToken);
		}
		// Omni reset code is uppercase MD5
		if(!is_string($resetCodeToken) || mb_strlen($resetCodeToken) != 32){
			throw Mage::exception('Mage_Core', $this->_getHelper('customer')->__('Invalid password reset token.'));
		}
	}

	/**
	 * Get restore password params.
	 *
	 * @param Mage_Customer_Model_Session $session
	 * @return array array ($customerId, $resetPasswordToken)
	 */
	protected function _getRestorePasswordParameters(Mage_Customer_Model_Session $session){
		if(!Mage::helper('accolade_omni/config')->isEnabled()){
			return parent::_getRestorePasswordParameters($session);
		}
		return array(
			(int) $session->getData(self::CUSTOMER_ID_SESSION_NAME),
			(string) $session->getData(self::OMNI_RESET_CODE)
		);
	}

	/**
	 * Save restore password params to session.
	 *
	 * @param int $customerId
	 * @param  string $resetCodeToken
	 * @return $this
	 */
	protected function _saveRestorePasswordParameters($customerId, $resetCodeToken){
		if(!Mage::helper('accolade_omni/config')->isEnabled()){
			parent::_saveRestorePasswordParameters($customerId, $resetCodeToken);
			return $this;
		}
		$this->_getSession()
			->setData(self::CUSTOMER_ID_SESSION_NAME, $customerId)
			->setData(self::OMNI_RESET_CODE, $resetCodeToken);

		return $this;
	}

}
