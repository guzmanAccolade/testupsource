<?php

class Accolade_Omni_Adminhtml_Accolade_Omni_OrderController extends Mage_Adminhtml_Controller_Action
{

    /**
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::helper('accolade_omni')->isOrderExportAllowed();
    }

    /**
     * (force) export specified order
     */
    public function exportAction()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($orderId);

        $this->_exportSpecificOrder($order);

        $this->_redirect('*/sales_order/view', array('order_id' => $orderId));
    }


    public function indexAction()
    {
        $this->_title($this->__('Accolade Omni'))
            ->_title($this->__('Manage Order Exports'));

        $this->loadLayout()
            ->_setActiveMenu('accolade_omni/order')
            ->_addBreadcrumb($this->__('Omni'), $this->__('Omni'))
            ->_addBreadcrumb($this->__('Manage Order Exports'), $this->__('Manage Order Exports'));

        $this->renderLayout();
    }


    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }


    public function massResetAction()
    {
        if (Mage::getStoreConfigFlag('accolade_omni/orders/mass_reset_enabled')) {
            $orderIds = $this->_getOrderIds();
            $failed = array();

            if ($orderIds) {
                foreach ($orderIds as $orderId) {
                    try {
                        $order = Mage::getModel('accolade_omni/order')->load($orderId);
                        if ($order->getId()) {
                            $order->setExportStatus($order::EXPORT_UNKNOWN)->setExportDate(null)->setExportLog(null)->setNavOrderId(null)->setNavTransactionId(null);
                            $order->save();
                        } else {
                            $failed[] = $orderId;
                        }
                    } catch (Exception $e) {
                        $failed[] = $orderId;
                    }
                }

                if (count($failed)) {
                    $this->_getSession()->addError($this->__('Unable to reset export data for orders: %s.', implode(', ', $failed)));
                }

                $this->_getSession()->addSuccess($this->__('Total of %s orders export data have been reset.', count($orderIds) - count($failed)));
            }
        }

        $this->_redirect('*/*/index');
    }


    public function massFlushLogAction()
    {
        $orderIds = $this->_getOrderIds();
        $failed = array();

        if($orderIds) {
            foreach ($orderIds as $orderId) {
                try {
                    $order = Mage::getModel('accolade_omni/order')->load($orderId);
                    if ($order->getId()) {
                        $order->setExportLog(null);
                        $order->save();
                    } else {
                        $failed[] = $orderId;
                    }
                } catch (Exception $e) {
                    $failed[] = $orderId;
                }
            }

            if (count($failed)) {
                $this->_getSession()->addError($this->__('Unable to flush log for orders: %s.', implode(', ', $failed)));
            }

            $this->_getSession()->addSuccess($this->__('Total of %s orders logs have been flushed.', count($orderIds) - count($failed)));
        }

        $this->_redirect('*/*/index');
    }


    public function massSkipAction()
    {
        $orderIds = $this->_getOrderIds();
        $failed = array();

        if($orderIds) {
            foreach ($orderIds as $orderId) {
                try {
                    $order = Mage::getModel('accolade_omni/order')->load($orderId);
                    if ($order->getId() && !$order->isSuccessful() && $order->getExportStatus() != $order::EXPORT_SKIPPED) {
                        $order->setExportStatus($order::EXPORT_SKIPPED);
                        $order->addExportLog('Manually skipped.', $order::EXPORT_SKIPPED);
                        $order->save();
                    } else {
                        $failed[] = $orderId;
                    }
                } catch (Exception $e) {
                    $failed[] = $orderId;
                }
            }

            if (count($failed)) {
                $this->_getSession()->addError($this->__('Unable to set following orders as skipped: %s.', implode(', ', $failed)));
            }

            $this->_getSession()->addSuccess($this->__('Total of %s orders have been set as skipped.', count($orderIds) - count($failed)));
        }

        $this->_redirect('*/*/index');
    }


    public function massExportAction()
    {
        $orderIds = $this->_getOrderIds();

        if($orderIds) {
            foreach ($orderIds as $orderId) {
                $order = Mage::getModel('sales/order')->load($orderId);

                $this->_exportSpecificOrder($order);
            }
        }

        $this->_redirect('*/*/index');
    }


    /**
     * Export order and all responses will placed into session
     * @param Mage_Sales_Model_Order $order
     */
    protected function _exportSpecificOrder($order) {
        $helper = Mage::helper('accolade_omni');
        if ($order && $order->getId()) {
            $omniOrder = Mage::getModel('accolade_omni/order');
            if ($omniOrder->canExportSalesOrder($order)) {
                $omniOrder->load($order->getId())
                    ->setSalesOrder($order);

                if (!$omniOrder->isSuccessful()) {
                    $omniOrder->exportOrder();

                    if ($omniOrder->isSuccessful()) {
                        $this->_getSession()->addSuccess(
                            $this->__(
                                'Order %s has been exported: NAV id %s',
                                $omniOrder->getIncrementId(),
                                $omniOrder->getData('nav_order_id')
                            )
                        );
                    } else {
                        $this->_getSession()->addError(
                            $this->__(
                                'Something went wrong with %s export. Message was: %s',
                                $omniOrder->getIncrementId(),
                                $omniOrder->getLastExportLog()
                            )
                        );
                    }
                } else {
                    $this->_getSession()->addSuccess(
                        $this->__('Order %s has been already exported successfully.', $omniOrder->getIncrementId())
                    );
                }
            } else {
                $this->_getSession()->addError(
                    $this->__(
                        'Order %s cannot be exported due order status is "%s".',
                        $order->getIncrementId(),
                        $order->getState())
                );
            }
        } else {
            $this->_getSession()->addError(
                $this->__('Order cannot be exported.')
            );
        }

    }

    /**
     * to get massaction order ids and will also add session message is non was found
     * @return bool|mixed
     */
    protected function _getOrderIds() {
        $orderIds = $this->getRequest()->getParam('order_ids');

        if(!is_array($orderIds) || !count($orderIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('No orders were specified.'));
            return false;
        }

        return $orderIds;
    }

}