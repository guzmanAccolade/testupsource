<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = Mage::getModel('eav/entity_setup', 'core_setup');

$attributeCode = 'telephone';
$entityTypeId = Mage::getModel('customer/customer')->getEntityTypeId();

$installer->removeAttribute($entityTypeId, $attributeCode);

$installer->addAttribute($entityTypeId, $attributeCode, array(
    'label'         => 'Telephone',
    'type'          => 'varchar',
    'input'         => 'text',
    'required'      => false,
    'visible'       => true,
    'user_defined'  => true,
    'unique'        => false,
));
$installer->endSetup();