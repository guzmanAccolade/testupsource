<?php

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = Mage::getModel('eav/entity_setup', 'core_setup');

$installer->startSetup();

$addressAttributes = array(
    'firstname' => array(
        'min_text_length' => 1,
        'max_text_length' => 30
    ),
    'middlename' => array(
        'min_text_length' => 0,
        'max_text_length' => 30
    ),
    'lastname' => array(
        'min_text_length' => 1,
        'max_text_length' => 30
    ),
    'street' => array(
        'min_text_length' => 1,
        'max_text_length' => 50
    ),
    'city' => array(
        'min_text_length' => 1,
        'max_text_length' => 30
    ),
    'postcode' => array(
        'min_text_length' => 1,
        'max_text_length' => 20
    ),
    'telephone' => array(
        'min_text_length' => 1,
        'max_text_length' => 30
    )
);

// Serialize and update attribute validation rules
foreach ($addressAttributes as $key => $attribute) {
    $serialized = serialize($attribute);
    $installer->updateAttribute('customer_address', $key, 'validate_rules', $serialized);
}

$customerAttributes = array(
    'firstname' => array(
        'min_text_length' => 1,
        'max_text_length' => 30
    ),
    'middlename' => array(
        'min_text_length' => 0,
        'max_text_length' => 30
    ),
    'lastname' => array(
        'min_text_length' => 1,
        'max_text_length' => 30
    )
);

// Serialize and update attribute validation rules
foreach ($customerAttributes as $key => $attribute) {
    $serialized = serialize($attribute);
    $installer->updateAttribute('customer', $key, 'validate_rules', $serialized);
}
$installer->endSetup();
