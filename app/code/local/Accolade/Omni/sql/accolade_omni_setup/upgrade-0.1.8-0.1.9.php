<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = Mage::getModel('eav/entity_setup', 'core_setup');

$installer->startSetup();

// primary contact
$attributeCode = 'default_contact';
$installer->addAttribute(Mage::getModel('customer/customer')->getEntityTypeId(), $attributeCode, array(
	'label'         => 'Default Contact',
	'type'          => 'int',
	'input'         => 'text',
	'backend'       => 'accolade_omni/customer_attribute_backend_contact',
	'required'      => false,
	'visible'       => false,
	'user_defined'  => false,
	'unique'        => false,
));
$installer->endSetup();


// Putting back into forms
$attributeCode = 'telephone';
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer_address', $attributeCode);
$attribute->setData('used_in_forms', array(
	'adminhtml_customer_address',
	'customer_address_edit',
	'customer_register_address'
));
$attribute->save();
