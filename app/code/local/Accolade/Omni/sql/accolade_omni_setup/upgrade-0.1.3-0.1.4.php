<?php

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = Mage::getModel('eav/entity_setup', 'core_setup');

$installer->startSetup();

$customerAttributes = array(
    'email' => array(
        'input_validation' => 'email',
        'max_text_length' => 80
    ),
    'telephone' => array(
        'min_text_length' => 1,
        'max_text_length' => 30
    )
);

// Serialize and update attribute validation rules
foreach ($customerAttributes as $key => $attribute) {
    $serialized = serialize($attribute);
    $installer->updateAttribute('customer', $key, 'validate_rules', $serialized);
}
$installer->endSetup();
