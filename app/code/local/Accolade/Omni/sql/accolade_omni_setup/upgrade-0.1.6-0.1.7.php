<?php

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = Mage::getModel('eav/entity_setup', 'core_setup');

$installer->startSetup();

$addressAttributes = array(
    'firstname'     => 'maximum-length-30',
    'middlename'    => 'maximum-length-30',
    'lastname'      => 'maximum-length-30',
    'street'        => 'maximum-length-50',
    'city'          => 'maximum-length-30',
    'postcode'      => 'maximum-length-20'
);

foreach ($addressAttributes as $attribute => $class) {
    $installer->updateAttribute('customer_address', $attribute, 'frontend_class', 'validate-length ' . $class);
}

$installer->endSetup();
