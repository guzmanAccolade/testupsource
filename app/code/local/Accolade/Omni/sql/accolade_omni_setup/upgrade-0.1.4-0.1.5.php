<?php

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = Mage::getModel('eav/entity_setup', 'core_setup');

$installer->startSetup();

// Set telephone as not required
$serialized = serialize(array('min_text_length' => 0, 'max_text_length' => 30));
$installer->updateAttribute('customer_address', 'telephone', 'validate_rules', $serialized);
$installer->updateAttribute('customer_address', 'telephone', 'is_required', 0);

$installer->endSetup();
