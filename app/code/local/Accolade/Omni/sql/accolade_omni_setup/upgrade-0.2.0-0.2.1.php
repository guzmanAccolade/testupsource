<?php
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$tables = array();

//removing upgrade 0.1.9-0.2.0
$connection->dropColumn($installer->getTable('sales/order'), 'order_sent');


$tableNames = array();
$tableNames['order'] = $installer->getTable('accolade_omni/order');

foreach($tableNames as $tableName){
    if($connection->isTableExists($tableName)){
        $connection->dropTable($tableName);
        $connection->commit();
    }
}

$tables['order'] = $connection->newTable($tableNames['order'])
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'unsigned' => true,
        'nullable' => false,
        'unique'   => true,
        'primary' => true
    ), 'Order ID')
    ->addColumn('increment_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
    ), 'Order Increment ID')
    ->addColumn('nav_transaction_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 40, array(
        'nullable' => true,
    ), 'Transaction Id in LSNAV')
    ->addColumn('nav_order_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 20, array(
        'nullable' => true,
    ), 'Document No in LSNAV')
    ->addColumn('export_date', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => true,
        'default' => null
    ), 'Sync Date')
    ->addColumn('export_status', Varien_Db_Ddl_Table::TYPE_SMALLINT, 1, array(
        'unsigned' => true,
        'nullable' => false,
        'default' => 0
    ), 'Sync Status')
    ->addColumn('export_log', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => true,
    ), 'Sync Log')
    ->setComment('OMNI order synchronization data');

foreach($tables as $table){
    $installer->getConnection()->createTable($table);
}

$connection->addForeignKey(
    $installer->getFkName('accolade_omni/order', 'order_id', 'sales/order','entity_id'),
    $installer->getTable('accolade_omni/order'),
    'order_id',
    $installer->getTable('sales/order'),
    'entity_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE,
    Varien_Db_Ddl_Table::ACTION_CASCADE
);

$installer->endSetup();