<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();


/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = Mage::getModel('eav/entity_setup', 'core_setup');

$installer->startSetup();

$attributeCode = 'main_address';
$installer->addAttribute(Mage::getModel('customer/customer')->getEntityTypeId(), $attributeCode, array(
	'label'         => 'Main Address',
	'type'          => 'int',
	'input'         => 'text',
	'backend'       => 'accolade_omni/attribute_backend_customer_main',
	'required'      => false,
	'visible'       => false,
	'user_defined'  => false,
	'unique'        => false,
));
$installer->endSetup();