<?php

$attributeCode = 'telephone';
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', $attributeCode);

// Add attribute to customer forms
$attribute->setData('used_in_forms', array(
    'adminhtml_customer',
    'checkout_register',
    'customer_account_create',
    'customer_account_edit'
));
$attribute->setData('is_user_defined', 0);
$attribute->save();