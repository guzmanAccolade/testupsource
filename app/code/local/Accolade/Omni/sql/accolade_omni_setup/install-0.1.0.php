<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();


/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = Mage::getModel('eav/entity_setup', 'core_setup');

$installer->startSetup();

$attributeCode = 'omni_id';
$installer->removeAttribute(Mage::getModel('customer/customer')->getEntityTypeId(), $attributeCode);

$installer->addAttribute(Mage::getModel('customer/customer')->getEntityTypeId(), $attributeCode, array(
	'label'         => 'Omni ID',
	'input'         => 'text',
	'type'          => 'text',
	'required'      => 0,
	'user_defined'  => 0,
	'unique'        => 1,
));
$installer->endSetup();

$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', $attributeCode);
$attribute->setData('used_in_forms', array('adminhtml_customer'));
$attribute->setData('sort_order', 1);
$attribute->save();

$installer->endSetup();