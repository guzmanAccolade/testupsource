<?php

class Accolade_Omni_Model_Rewrite_Resource_Customer extends Mage_Customer_Model_Resource_Customer {

	/**
	 * Save/delete customer address - added contact address
	 *
	 * @param Mage_Customer_Model_Customer $customer
	 * @return Mage_Customer_Model_Resource_Customer
	 */
	protected function _saveAddresses(Mage_Customer_Model_Customer $customer){
		$defaultContactId = $customer->getData('default_contact');

		/** @var Accolade_Omni_Model_Rewrite_Customer_Address $address */
		foreach($customer->getAddresses() as $address){
			if($address->getData('_deleted')){
				if($address->getId() == $defaultContactId){
					$customer->setData('default_contact', null);
				}
				// delete is handled by parent call
			}
		}

		// this does the deleting and saving of addresses:
		parent::_saveAddresses($customer);

		foreach($customer->getAddresses() as $address){
			if(($address->getIsPrimaryContact() || $address->getIsDefaultContact()) && $address->getId() != $defaultContactId){
				$customer->setData('default_contact', $address->getId());
			}
		}

		if($customer->dataHasChangedFor('default_contact')){
			$this->saveAttribute($customer, 'default_contact');
		}

		return $this;
	}


}