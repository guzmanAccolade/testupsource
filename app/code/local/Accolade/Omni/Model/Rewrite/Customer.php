<?php

class Accolade_Omni_Model_Rewrite_Customer extends Mage_Customer_Model_Customer {

	/**
	 * Get customer default billing address
	 *
	 * @return Mage_Customer_Model_Address
	 */
	public function getPrimaryContactAddress(){
		return $this->getPrimaryAddress('default_contact');
	}

	/**
	 * Retrieve ids of default addresses
	 *
	 * @return array
	 */
	public function getPrimaryAddressIds(){
		$ids = parent::getPrimaryAddressIds();
		if($this->getPrimaryContactAddress()){
			$ids[] = $this->getPrimaryContactAddress()->getId();
		}

		return $ids;
	}

	/**
	 * Retrieve all customer default addresses
	 *
	 * @return array
	 */
	public function getPrimaryAddresses(){
		$addresses = parent::getPrimaryAddresses();

		$addressAdded       = false;
		$primaryContact     = $this->getPrimaryContactAddress();
		if($primaryContact){
			foreach($addresses as &$address){
				/** @var Mage_Customer_Model_Address $address */
				if($address->getId() == $primaryContact->getId()){
					$address->setIsPrimaryContact(true);
					$addressAdded = true;
					break;
				}
			}
			if(!$addressAdded) $addresses[] = $primaryContact;
		}

		return $addresses;
	}

	/**
	 * Check if address is primary
	 *
	 * @param Mage_Customer_Model_Address $address
	 * @return boolean
	 */
	public function isAddressPrimary(Mage_Customer_Model_Address $address){
		if($address->getId()){
			return parent::isAddressPrimary($address) || ($address->getId() == $this->getDefaultContact());
		}
		return parent::isAddressPrimary($address);
	}

	/**
	 * Authenticate customer
	 *
	 * @param  string $login
	 * @param  string $password
	 * @throws Mage_Core_Exception
	 * @return true
	 */
	public function authenticate($login, $password){
		if(Mage::helper('accolade_omni/config')->isEnabled()){
			$omniCustomer = Mage::getSingleton('accolade_omni/omni_customer');

			$customer = $omniCustomer->authenticate($login, $password);
			if($customer){
				$this->setData($customer->getData());
				Mage::getSingleton('customer/session')->setData($omniCustomer::OMNI_SECURITY_TOKEN, $this->getData($omniCustomer::OMNI_SECURITY_TOKEN));
				Mage::dispatchEvent('customer_customer_authenticated', array(
					'model'    => $this,
					'password' => $password,
				));
				return true;
			}
			throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Invalid login or password.'),
				self::EXCEPTION_INVALID_EMAIL_OR_PASSWORD
			);
		}
		return parent::authenticate($login, $password);
	}

}
