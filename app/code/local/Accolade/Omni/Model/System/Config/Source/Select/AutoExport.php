<?php

class Accolade_Omni_Model_System_Config_Source_Select_AutoExport
{
    const NO = 0;
    const ON_SUCCESS = 1;
    const BY_CRON = 2;

    public function toOptionArray()
    {
        $helper = Mage::helper('accolade_omni');
        return array(
            array(
                'label' => $helper->__('No'),
                'value' => self::NO
            ),
            array(
                'label' => $helper->__('Exported on Order Success'),
                'value' => self::ON_SUCCESS
            ),
            array(
                'label' => $helper->__('Exported by Cron'),
                'value' => self::BY_CRON
            )
        );
    }

}