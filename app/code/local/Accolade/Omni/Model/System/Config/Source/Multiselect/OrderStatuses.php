<?php

class Accolade_Omni_Model_System_Config_Source_Multiselect_OrderStatuses {

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $processedStatuses = $this->_getOrderStatuses();

        return $processedStatuses;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $processedStatuses = array();
        $orderStatuses = $this->_getOrderStatuses();

        foreach ($orderStatuses as $orderStatus) {
            $processedStatuses[$orderStatus['status']] = $orderStatus['label'];
        }

        return $processedStatuses;
    }


    protected function _getOrderStatuses() {
        return Mage::getModel('sales/order_status')->getCollection()->orderByLabel()->toOptionArray();
    }

}