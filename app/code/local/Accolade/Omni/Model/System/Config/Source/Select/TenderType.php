<?php

class Accolade_Omni_Model_System_Config_Source_Select_TenderType
{

    protected $_tenderTypes = array(
        Omni_TenderType::Cash,
        Omni_TenderType::Card,
        Omni_TenderType::Coupon,
        Omni_TenderType::Points
    );


    public function toOptionArray()
    {
        $optionArray = array();

        foreach ($this->_tenderTypes as $type) {
            $optionArray[] = array(
                'label' => Mage::helper('accolade_omni')->__($type),
                'value' => $type
            );
        }

        return $optionArray;
    }

}