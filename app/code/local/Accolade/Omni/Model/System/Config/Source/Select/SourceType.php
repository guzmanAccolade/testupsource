<?php

class Accolade_Omni_Model_System_Config_Source_Select_SourceType
{

    protected $_sourceTypes = array(
        Omni_SourceType::eCommerce,
        Omni_SourceType::LSOmni,
        Omni_SourceType::Standard
    );


    public function toOptionArray()
    {
        $optionArray = array();

        foreach ($this->_sourceTypes as $type) {
            $optionArray[] = array(
                'label' => Mage::helper('accolade_omni')->__($type),
                'value' => $type
            );
        }

        return $optionArray;
    }

}