<?php

class Accolade_Omni_Model_System_Config_Source_Select_Mode {

	public function toOptionArray(){
		return array(
			array(
				'label' => Mage::helper('accolade_omni')->__('LIVE'),
				'value' => 'live'
			),
			array(
				'label' => Mage::helper('accolade_omni')->__('development'),
				'value' => 'dev'
			)
		);
	}
}