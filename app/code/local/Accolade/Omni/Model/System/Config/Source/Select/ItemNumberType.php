<?php

class Accolade_Omni_Model_System_Config_Source_Select_ItemNumberType
{

    protected $_itemNumberTypes = array(
        Omni_ItemNumberType::ItemNo,
        Omni_ItemNumberType::ProductId
    );


    public function toOptionArray()
    {
        $optionArray = array();

        foreach ($this->_itemNumberTypes as $type) {
            $optionArray[] = array(
                'label' => Mage::helper('accolade_omni')->__($type),
                'value' => $type
            );
        }

        return $optionArray;
    }

}