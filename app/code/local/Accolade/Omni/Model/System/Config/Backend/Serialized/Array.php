<?php

class Accolade_Omni_Model_System_Config_Backend_Serialized_Array extends Mage_Adminhtml_Model_System_Config_Backend_Serialized_Array
{

    // direction for sort_order only
    protected $_direction = Varien_Data_Collection::SORT_ORDER_DESC;

    protected function _beforeSave()
    {
        $direction = ($this->_direction == Varien_Data_Collection::SORT_ORDER_DESC) ? -1 : 1;

        $value = $this->getValue();
        if (is_array($value)) {
            unset($value['__empty']);
            uasort($value, function ($a, $b) use ($direction) {
                $res = 0;
                if (is_array($a) && is_array($b)) {
                    foreach (array('sort_order', 'order') as $order) {
                        if (isset($a[$order]) && isset($b[$order])) {
                            $res = $direction * ($a[$order] * 1 - $b[$order] * 1);
                            break;
                        }
                    }
                    if ($res == 0) return strcasecmp(reset($a), reset($b));
                }
                return $res; //equal as we do not know how to compare
            });

        }
        $this->setValue($value);
        parent::_beforeSave();
    }

}