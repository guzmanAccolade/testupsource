<?php

class Accolade_Omni_Model_Omni_Order extends Accolade_Omni_Model_Omni
{

    const ORDER_CONFIG_PATH = 'accolade_omni/orders/';

    //maximum length of given element value
    protected $_maxValueLengths = array(
        'ShipmentMethodCode'    => 10,
        'CardType'              => 10,
        'EFTAuthCode'           => 9,
        'EFTCardName'           => 30,
        'EFTMessage'            => 80,
    );


    /**
     * @param Mage_Sales_Model_Order $order
     * @return bool|Omni_BasketPostSaleResponse
     * @throws Exception
     */
    public function create($order)
    {
        $results = false;
        $message = false;

        try {
            $omni = $this->getOmni();

            $basketRequest = $this->_getBasketRequest($order);

            $response = $omni->BasketPostSale(new Omni_BasketPostSale($basketRequest));
            if ($response instanceof Omni_BasketPostSaleResponse) {
                $results = $response->getBasketPostSaleResult();
            } else {
                $message = 'Received improper response.';
            }
        } catch (SoapFault $fault) {
            $code = '[' . @$fault->faultcode . ']';
            $message = $code . ' ' . @$fault->faultstring; //todo parsing will not result enough specific message atm
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if (isset($omni) && $this->getConfigFlag('log_requests')) {
            $this->_logXML('REQUEST', $order->getId(), @$omni->__getLastRequestHeaders(), @$omni->__getLastRequest());
            $this->_logXML('RESPONSE', $order->getId(), @$omni->__getLastResponseHeaders(), @$omni->__getLastResponse());
        }

        if ($message) { //last as we want to log the requests before
            Mage::getSingleton('accolade_omni/omni_exception')->throwException($message);
        }

        return $results;
    }


    /**
     * @param Mage_Sales_Model_Order $order
     * @return Omni_BasketPostSaleRequest
     */
    protected function _getBasketRequest($order)
    {
        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

        $omniId = $customer->getData('omni_id');
        if (empty($omniId)) $omniId = '0'; //todo 0 ok?

        $basketRequest = new Omni_BasketPostSaleRequest(array());

        $basketRequest->setContactId($omniId);
        $basketRequest->setFullName($order->getCustomerName());
        $basketRequest->setPhoneNumber($order->getBillingAddress()->getTelephone());
        $basketRequest->setEmail($order->getCustomerEmail());

        if ($omniId) {
            $cardId = $this->_getCardId($omniId);
            $basketRequest->setMemberCardNo($cardId);
        }

        $basketRequest->setId('');
        $basketRequest->setCurrencyCode($order->getOrderCurrencyCode());
        $basketRequest->setExternalOrderId($order->getIncrementId());
        $basketRequest->setBasketPrice($order->getGrandTotal());

        $PriceIncludesVATFlag = $this->getConfigFlag('price_includes_vat');
        $basketRequest->setPriceIncludesVAT($PriceIncludesVATFlag);

        $itemNumberType = $this->getConfig('item_number_type');
        $basketRequest->setItemNumberType($itemNumberType);

        $sourceType = $this->getConfig('source_type');
        $basketRequest->setSourceType($sourceType);

        $omniBillingAddress = $this->_getAddress($order->getBillingAddress());
        $basketRequest->setBillingAddress($omniBillingAddress);

        $omniShippingAddress = $this->_getAddress($order->getShippingAddress());
        $basketRequest->setShippingAddress($omniShippingAddress);

        $basketRequest->setBasketPostSaleDiscLineRequests(array()); //todo, can this be useful?

        $saleLines = $this->_getBasketSaleLines($order);

        $linesTotal = $this->_getSaleLinesTotal($saleLines);

        if ($linesTotal > 0) {
            if ($this->getConfigFlag('subtract_from_items')) {
                $discountAmount = $this->_getOrderLevelDiscounts($order);
                if ($discountAmount> 0) {
                    $this->_subtractAmountFromLines($saleLines, $discountAmount, $linesTotal);

                    $linesTotal = $this->_getSaleLinesTotal($saleLines);
                }
            }
        }

        $basketRequest->setBasketPostSaleLineRequests($saleLines);

        $tenderLines = $this->_getTenderLines($order);
        $basketRequest->setTenderLines($tenderLines);

        //todo shipping
        $basketRequest->setShipmentMethodCode($this->_cropValue('ShipmentMethodCode', $order->getShippingMethod())); // todo, translation table?

        return $basketRequest;
    }


    /**
     * @param Mage_Sales_Model_Order $order
     * @return Omni_BasketPostSaleLineRequest[]
     */
    protected function _getBasketSaleLines($order)
    {
        $items = $order->getAllVisibleItems();
        $saleLines = array();

        /** @var Mage_Sales_Model_Order_Item $item */
        foreach ($items as $item) {
            $saleLine = new Omni_BasketPostSaleLineRequest();

            // $saleLine->setCouponCode('test');
            $saleLine->setDiscountAmount((double) $item->getDiscountAmount()); //todo per unit or all?
            $saleLine->setDiscountPercent((double) $item->getDiscountPercent());
            $saleLine->setId('');
            $saleLine->setItemId($item->getSku());
            $saleLine->setQuantity($item->getQtyOrdered());
            $saleLine->setLineNumber(count($saleLines) + 1);
            $saleLine->setNetAmount($item->getRowTotal() - $item->getTaxAmount()); //todo with tax? examples have it without
            $saleLine->setPrice($item->getPriceInclTax() - $item->getDiscountAmount() / $item->getQtyOrdered());
            $saleLine->setTaxAmount($item->getTaxAmount()); //todo per line or unit?
            //$saleLine->setTaxProductCode('B'); //todo, is there tax product ?
            //$saleLine->setUomId('PCS'); //todo, needed?
            $saleLine->setVariantId(null); //todo needed?

            $saleLines[] = $saleLine;
        }

        $saleLine = $this->_getExtraProductLine('shipping', $order, count($saleLines) + 1);
        if ($saleLine) $saleLines[] = $saleLine;

        $saleLine = $this->_getExtraProductLine('payment', $order, count($saleLines) + 1);
        if ($saleLine) $saleLines[] = $saleLine;

        return $saleLines;
    }


    /**
     * @param string $methodType (shipping or payment)
     * @param Mage_Sales_Model_Order $order
     * @param int $lineNumber
     * @return bool|Omni_BasketPostSaleLineRequest
     * @throws Mage_Core_Exception
     */
    protected function _getExtraProductLine($methodType, $order, $lineNumber)
    {
        $price = 0;
        $tax = 0;

        if ($methodType == 'shipping') {
            if (!$this->getConfigFlag('shipping_export_on_zero') && !$order->getShippingAmount()) {
                return false;
            }

            $method = $order->getShippingMethod();

            $price = $order->getShippingAmount();
            $tax = $order->getShippingTaxAmount();
        } else {
            $methodType = 'payment';

            $method = $order->getPayment()->getMethod();

            // only in case of Klarna there can be payment fee.
            if (mb_substr($method, 0, mb_strlen('vaimo_klarna')) === 'vaimo_klarna') {
                $additionalInfo = $order->getPayment()->getAdditionalInformation();

                if (is_array($additionalInfo)) {
                    if (isset($additionalInfo['vaimo_klarna_fee']) && $additionalInfo['vaimo_klarna_fee'] > 0) {
                        $price = $additionalInfo['vaimo_klarna_fee'];
                    }
                    if (isset($additionalInfo['vaimo_klarna_fee_tax']) && $additionalInfo['vaimo_klarna_fee_tax'] > 0) {
                        $tax = $additionalInfo['vaimo_klarna_fee_tax'];
                    }
                }
            }
        }

        $navProductNo = $this->_getNavProductNo($methodType, $method);

        if ($navProductNo) {
            $saleLine = new Omni_BasketPostSaleLineRequest();

            $saleLine->setDiscountAmount(0); //todo shipping / payment discount?
            $saleLine->setDiscountPercent(0);
            $saleLine->setId('');
            $saleLine->setItemId($navProductNo);
            $saleLine->setQuantity(1);
            $saleLine->setLineNumber($lineNumber);
            $saleLine->setNetAmount($price - $tax); //todo with tax?
            $saleLine->setPrice($price);
            $saleLine->setTaxAmount($tax); //todo per line or unit?
            //$saleLine->setTaxProductCode('B'); //todo, is there tax product ?
            //$saleLine->setUomId('PCS'); //todo, needed?
            $saleLine->setVariantId(null); //todo needed?

            return $saleLine;
        } else if ($this->getConfigFlag($methodType . '_product_required')) {
            Mage::getSingleton('accolade_omni/omni_exception')->throwException(Mage::helper('accolade_omni')->__('Following %s method cannot be converted into NAV product: %s', $methodType, $method));
        }

        return false;
    }


    /**
     * retrieve Magento method -> NAV product mapping as array
     * @param string $methodType - (shipping or payment)
     * @param string $method - to be matched
     * @return bool|string
     */
    protected function _getNavProductNo($methodType = 'shipping', $method = '') {
        $mapping = @unserialize($this->getConfig(''.$methodType.'_product_mapping'));

        if (is_array($mapping)) {
            foreach ($mapping as $map) {
                $map = array_map('trim', $map);
                if (!empty($map['method']) && !empty($map['product_no'])) {
                    if (mb_substr($method, 0, mb_strlen($map['method'])) === $map['method']) {
                        return $map['product_no'];
                    }
                }
            }
        }

        return false;
    }


    /**
     * @param Mage_Sales_Model_Order_Address $address
     * @return Omni_Address
     */
    protected function _getAddress($address)
    {
        $omniAddress = new Omni_Address();

        $omniAddress->setAddress1($address->getStreet1());
        $omniAddress->setAddress2($address->getStreet2());

        $omniAddress->setCity($address->getCity());
        $omniAddress->setPostCode($address->getPostcode());
        $omniAddress->setCountry($address->getCountryId());

        return $omniAddress;
    }


    /**
     * @param Mage_Sales_Model_Order $order
     * @return array (Omni_TenderPaymentLine)
     */
    protected function _getTenderLines($order)
    {
        $tenderLines = array();

        /** @var Mage_Sales_Model_Order_Invoice $invoice */
        $invoice = $order->getInvoiceCollection()->getFirstItem();
        $payment = $order->getPayment();

        //payment info
        $tenderLine = new Omni_TenderPaymentLine();
        $tenderLine->setAmount($order->getGrandTotal());
        $tenderLine->setCurrencyCode($order->getOrderCurrencyCode());
        $tenderLine->setLineNumber(count($tenderLines) + 1);
        $tenderTypeDefault = $this->getConfig('tender_type_default');
        $tenderLine->setType($tenderTypeDefault);
        $tenderLine->setCardType($this->_cropValue('CardType', $payment->getMethod())); //todo, ok?
        $tenderLine->setEFTCardName($this->_cropValue('EFTCardName', $payment->getMethod())); //todo, ok?

        if ($invoice->getId()) {
            //todo proper placement? has any impact? (mapping from LSR_Core)
            $tenderLine->setEFTAuthCode($this->_cropValue('EFTAuthCode', $invoice->getIncrementId()));
            $tenderLine->setEFTMessage($this->_cropValue('EFTMessage', $invoice->getTransactionId()));
        }

        $tenderLines[] = $tenderLine;

        return $tenderLines;
    }


    /**
     * @param string $contactId
     * @return null|string
     */
    protected function _getCardId($contactId)
    {
        $omni = $this->getOmni();

        $searchRequest = new Omni_ContactSearch('ContactNumber', $contactId, 1);

        $response = $omni->ContactSearch($searchRequest);

        if ($response instanceof Omni_ContactSearchResponse) {
            $results = $response->getContactSearchResult();
            /** @var Omni_ContactPOS $result */
            foreach ($results as $result) {
                $card = $result->getCard();
                $cardId = $card->getId();

                if (!empty($cardId)) return $cardId;
            }

        }

        return null;
    }

    /**
     * Cropping the text if it goes over given limit.
     * @param string $elementName
     * @param string $value
     * @return string
     */
    protected function _cropValue($elementName, $value) {
        if (isset($this->_maxValueLengths[$elementName]) && !empty($value)) {
            $length = $this->_maxValueLengths[$elementName];
            if (mb_strlen($value) > $length) {
                return mb_substr($value, 0, $length);
            }
        }

        return $value;
    }

    /**
     * @return int
     */
    protected function _curlTrace() {
        if ($this->getConfigFlag('log_requests')) {
            return 1;
        }

        return parent::_curlTrace();
    }


    /**
     * todo later
     * @param Mage_Sales_Model_Order $order
     */
    public function getStatus($order)
    {
        // Operation: OrderStatusCheck
        // OrderStatus:
        //   10 : Pending
        //   20 : Processing
        //   30 : Complete
        //   40 : Canceled
        //
        // ShippingStatus:
        //   10 : ShippigNotRequired
        //   20 : NotYetShipped
        //   25 : PartiallyShipped
        //   30 : Shipped
        //   40 : Delivered
    }


    /**
     * @param string $typeReference
     * @param int $orderId
     * @param mixed $header
     * @param mixed $body
     */
    protected function _logXML($typeReference, $orderId, $header, $body) {
        Mage::log($typeReference . ' Headers (order_id=' . $orderId . '): ' .  @var_export($header, true), null, 'omni_orders.log');
        Mage::log($typeReference . ' Body raw (order_id=' . $orderId . '): ' . @var_export($body, true), null, 'omni_orders.log');

        if (is_string($body)) {
            $document = new DomDocument('1.0');
            $document->preserveWhiteSpace = false;
            $document->formatOutput = true;
            $document->loadXML($body);
            Mage::log($typeReference . ' Body parsed (order_id=' . $orderId . '): ' . $document->saveXml(), null, 'omni_orders.log');
        }
    }

    /**
     * @param string $string
     * @return mixed
     */
    public function getConfig($string){
        return Mage::getStoreConfig(self::ORDER_CONFIG_PATH  . $string);
    }

    /**
     * @param string $string
     * @return bool
     */
    public function getConfigFlag($string){
        return Mage::getStoreConfigFlag(self::ORDER_CONFIG_PATH  . $string);
    }


    /**
     * @param Mage_Sales_Model_Order $order
     * @return float - as positive that need to be subtracted
     */
    protected function _getOrderLevelDiscounts($order) {
        $amount = 0.0;

        //support for Amasty_GiftCard
        if ($order->getData('am_gift_cards_amount') > 0) {
            $amount += $order->getData('am_gift_cards_amount');
        }

        return $amount;
    }


    /**
     * @param Omni_BasketPostSaleLineRequest[] $saleLines
     * @return float
     */
    protected function _getSaleLinesTotal($saleLines) {
        $amount = 0.0;
        foreach ($saleLines as $saleLine) {
            $amount += (float) $saleLine->getPrice() * $saleLine->getQuantity();
        }

        return $amount;
    }


    /**
     * @param Omni_BasketPostSaleLineRequest[] $saleLines
     * @param double $discountAmount
     * @param double $linesTotal
     * @return Omni_BasketPostSaleLineRequest[]
     */
    protected function _subtractAmountFromLines(&$saleLines, $discountAmount, $linesTotal) {
        $coefficient = ($linesTotal - $discountAmount) / $linesTotal;

        if ($coefficient < 0) $coefficient = 0;
        else if ($coefficient > 1) $coefficient = 1;

        foreach ($saleLines as $saleLine) {
            $newPrice = (double) $saleLine->getPrice() * $coefficient;
            $qty = $saleLine->getQuantity();

            $extraDiscount = (double) $saleLine->getDiscountAmount() + ((double) $saleLine->getPrice() - $newPrice) * $qty; //as per line
            $saleLine->setDiscountAmount((double) $extraDiscount);

            $saleLine->setNetAmount($saleLine->getNetAmount() * $coefficient); //todo safest?
            $saleLine->setTaxAmount($saleLine->getTaxAmount() * $coefficient); //todo safest?

            $saleLine->setPrice($newPrice);
        }

        return $saleLines;
    }

}