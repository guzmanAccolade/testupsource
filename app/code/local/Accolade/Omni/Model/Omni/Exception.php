<?php

class Accolade_Omni_Model_Omni_Exception {

    //error log file
    const ERROR_LOG_FILE                = 'omni-error.log';

	// TODO: rename these in callouts - it now has MSG_
	const EXCEPTION_MSG_GENERAL         = 'Currently this action is unavailable';   // thrown *only* when Omni is down!
	const EXCEPTION_MSG_UNKNOWN         = 'An error has occurred';

	// TODO: name these differently
	const EXCEPTION_SESSION_EXPIRED = 1;

    // Login: user not found or password not matching.
    const FAULT_AUTHENTICATION_FAILED           = 's:AuthFailed';
    // Login & ContactCreate: password or username empty or fields missing
    const FAULT_USERNAME_PASSWORD_INVALID       = 's:UserNamePasswordInvalid';
    //TODO Login & ContactUpdate & ChangePassword
    const FAULT_DEVICE_IS_BLOCKED               = 's:DeviceIsBlocked';
    //TODO Login, or passwordInvalid
    const FAULT_INVALID_PASSWORD                = 's:InvalidPassword';
    //TODO Login
    const FAULT_LOGIN_ID_NOT_FOUND              = 's:LoginIdNotFound';

    // ContactGetById & ContactUpdate & ChangePassword: token sent but in wrong format
    const FAULT_SECURITY_TOKEN_INVALID          = 's:SecurityTokenInvalid';
    // ContactGetById & ContactUpdate & ChangePassword: token not sent
    const FAULT_ACCESS_NOT_ALLOWED              = 's:AccessNotAllowed';
    // ContactGetById & ContactUpdate & ChangePassword: token valid, but not matching up with active. Also if token and contactid do not match. Token is reset if logged in.
    const FAULT_USER_NOT_LOGGED_IN              = 's:UserNotLoggedIn';
    // ContactGetById & ContactUpdate: ContactId field empty
    //TODO ChangePassword
    const FAULT_CONTACT_ID_NOT_FOUND            = 's:ContactIdNotFound';

    // ContactCreate: password too short, length between 1-4 char (cases: 1-2 Omni, 3-4 NAV)
    //TODO needs distinction that new? ChangePassword & ResetPassword: NEW password empty or not long enough.
    const FAULT_PASSWORD_INVALID                = 's:PasswordInvalid';
    // ContactCreate: email field empty or fails validation.
    const FAULT_EMAIL_INVALID                   = 's:EmailInvalid';
    // ContactCreate: Already in use by another contact
    const FAULT_EMAIL_EXISTS                    = 's:EmailExists';
    // ContactCreate: username too short (<3 characters)
    const FAULT_USERNAME_INVALID                = 's:UserNameInvalid';
    // ContactCreate: username already in use
    const FAULT_USERNAME_EXISTS                 = 's:UserNameExists';
    // ContactCreate: first name empty
    const FAULT_MISSING_FIRST_NAME              = 's:MissingFirstName';
    // ContactCreate: last name empty
    const FAULT_MISSING_LAST_NAME               = 's:MissingLastName';
    //TODO ContactCreate
    const FAULT_ACCOUNT_NOT_FOUND               = 's:AccountNotFound';

    // ContactUpdate: username empty
    const FAULT_PARAMETER_INVALID               = 's:ParameterInvalid';

    // ChangePassword: Old password empty, not long enough, or does not match current password
    const FAULT_OLD_PASSWORD_INVALID            = 's:PasswordOldInvalid';

    // ForgotPassword & ResetPassword: username/email not found/matched or empty
    const FAULT_USERNAME_NOT_FOUND              = 's:UserNameNotFound';

    // ResetPassword: username/email not found/matched or empty
    const FAULT_PASSWORD_RESET_CODE_NOT_FOUND   = 's:ResetPasswordCodeNotFound';
    const FAULT_PASSWORD_RESET_CODE_EXPIRED     = 's:ResetPasswordCodeExpired';

    // General: illegal chars, missing required fields or fields in wrong order, fields type mismatch.
    const FAULT_DESERIALIZATION                 = 'a:DeserializationFailed';

    // Login & ContactCreate: Username >46 characters (on Demo, NAV restriction)
    // ContactGetById: ContactId field missing
    // ContactCreate: Addresses field not present or nil
    const FAULT_REFERENCE                       = 's:1';
    //TODO General
	const FAULT_ERROR                           = 's:Error';


	/**
	 * Parse SoapFault into type Mage_Core_Exception
	 *
	 * @param SoapFault $fault
	 * @throws Mage_Core_Exception
	 */
	public function parseSoapFault(SoapFault $fault){
		if(!isset($fault->faultcode)){
            $this->_logError($fault);
			$this->throwException(self::EXCEPTION_MSG_UNKNOWN);
		}

		switch($fault->faultcode){
            case self::FAULT_AUTHENTICATION_FAILED:
            case self::FAULT_USERNAME_PASSWORD_INVALID:
                throw Mage::exception(
                    'Mage_Core',
                    Mage::helper('customer')->__('Invalid login or password.'),
                    Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD
                );
            case self::FAULT_SECURITY_TOKEN_INVALID:
            case self::FAULT_USER_NOT_LOGGED_IN:
            case self::FAULT_ACCESS_NOT_ALLOWED:
                throw Mage::exception(
                    'Accolade_Omni',
                    Mage::helper('accolade_omni')->__('Your session has expired'),
                    Accolade_Omni_Model_Omni_Exception::EXCEPTION_SESSION_EXPIRED
                );
            case self::FAULT_PASSWORD_INVALID:
                $this->throwException('Invalid password.', 'customer');
                break;
            case self::FAULT_OLD_PASSWORD_INVALID:
                $this->throwException('Invalid old password.', 'customer');
                break;
            case self::FAULT_EMAIL_INVALID:
                throw Mage::exception('Mage_Core', Mage::helper('accolade_omni')->__('Invalid or empty Email.'));
            case self::FAULT_EMAIL_EXISTS:              //TODO duplicate on
            case self::FAULT_USERNAME_EXISTS:
                throw Mage::exception(
                    'Mage_Core',
                    Mage::helper('customer')->__('Invalid email.'),
                    Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS
                );
            case self::FAULT_USERNAME_INVALID:
                throw Mage::exception('Mage_Core', Mage::helper('accolade_omni')->__('Username too short.'));
            case self::FAULT_MISSING_FIRST_NAME:
                throw Mage::exception('Mage_Core', Mage::helper('accolade_omni')->__('Empty first name.'));
            case self::FAULT_MISSING_LAST_NAME:
                throw Mage::exception('Mage_Core', Mage::helper('accolade_omni')->__('Empty last name.'));
            case self::FAULT_PARAMETER_INVALID:
                throw Mage::exception('Mage_Core', Mage::helper('accolade_omni')->__('Empty username.'));
            case self::FAULT_USERNAME_NOT_FOUND:
                throw Mage::exception('Mage_Core', Mage::helper('accolade_omni')->__('User not found.'));
                //TODO appropriate feedback on forgot? The if the email does exist...
                //TODO reset password response with the same to code
            case self::FAULT_PASSWORD_RESET_CODE_NOT_FOUND:
            case self::FAULT_PASSWORD_RESET_CODE_EXPIRED:
            	// we need the code since if invalid reset code, we need different action
				throw Mage::exception(
					'Mage_Core',
					Mage::helper('customer')->__('Invalid password reset token.'),
					Mage_Customer_Model_Customer::EXCEPTION_INVALID_RESET_PASSWORD_LINK_TOKEN
				);
            case self::FAULT_REFERENCE:
                $this->_logError($fault);

                $message = $fault->faultstring;
                if(strpos($message, 'Failed to ContactGetById() contactId') !== false){ // ContactGetById
                    $message = Mage::helper('accolade_omni')->__('Request parameter missing.');
                } else if(strpos($message, 'not set to an instance of an object') !== false){ // ContactCreate
                    $message = Mage::helper('accolade_omni')->__('Some data was not properly set.');
                } else if(strpos($message, 'String or binary data would be truncated') !== false){ // Login & ContactCreate
                    $message = Mage::helper('accolade_omni')->__('Email / username was too long to handle. Please register with shorter.');
                } else {
                    $message = Mage::helper('accolade_omni')->__('There was a problem with data. Please contact customer service.');
                }

                throw Mage::exception('Mage_Core', $message);
            case self::FAULT_DESERIALIZATION:
                $this->throwException('There was a problem with data. Please check that all fields are correct or contact customer service if persisting.');
                // TODO: parse what field has the issue?
                break;
            default:
				$this->_logError($fault, true);

                $this->throwException();
		}
	}

	/**
	 * @@param string $msg      Message
	 * @@param string $module   Module
	 * @throws Mage_Core_Exception
	 */
	public function throwException($msg = self::EXCEPTION_MSG_UNKNOWN, $module = 'accolade_omni'){
		Mage::throwException(Mage::helper($module)->__($msg));
	}


    /**
     * @param SoapFault $fault
     * @param boolean $fullOutput
     */
    protected function _logError($fault, $fullOutput = false){
	    
        if (isset($fault->faultcode)) {
            Mage::log('Fault: ' . $fault->faultcode . PHP_EOL . 'Message: ' . @var_export($fault->faultstring, true), null, self::ERROR_LOG_FILE);
        }
        if (!isset($fault->faultcode) || $fullOutput) {
            Mage::log(@var_export($fault, true), null, self::ERROR_LOG_FILE);
        }
    }

}
