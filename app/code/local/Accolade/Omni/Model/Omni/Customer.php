<?php

class Accolade_Omni_Model_Omni_Customer extends Accolade_Omni_Model_Omni {

	// TODO: reloading will clear this, should we save it with customer data?
	/**
	 * @param Mage_Customer_Model_Customer $customer
	 * @param string $token
	 */
	protected function _setSecurityToken($customer, $token){
		if(empty($token)){
			if($customer) $customer->unsetData(self::OMNI_SECURITY_TOKEN);
			Mage::getSingleton('customer/session')->unsetData(self::OMNI_SECURITY_TOKEN);
		} else {
			if($customer) $customer->setData(self::OMNI_SECURITY_TOKEN, $token);  // TODO: reloading will clear this
			Mage::getSingleton('customer/session')->setData(self::OMNI_SECURITY_TOKEN, $token);
		}
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @return Mage_Customer_Model_Customer authenticated customer
	 * @throws Mage_Core_Exception
	 */
        public function authenticate($username, $password){
                $customer = null;
                try {
                    $omni = $this->getOmni();

                    $response = $omni->Login(new Omni_Login($username, $password));
                    if($response instanceof Omni_LoginResponse){
                        /** @var Omni_Contact $contact */
                        $contact = $response->getLoginResult();
                        $customerOld = Mage::getModel("customer/customer");
                        $customerOld->setWebsiteId(Mage::app()->getWebsite()->getId());
                        $customerOld->loadByEmail($username);
                        //if customer does not exist in some reason, creating it. if does, has to have matching omni id
                        if (!$customerOld->getId() || $contact->getId() == $customerOld->getOmniId()){
                            $token = $contact->getDevice()->getSecurityToken();
                            /** @var Mage_Customer_Model_Customer $customer */
                            $customer = $this->_import($contact);
                            $this->_setSecurityToken($customer, $token);
                        } else {
                            Mage::getSingleton('accolade_omni/omni_exception')->throwException('Unable to match account. Please contact customer service.');
                        }
                    } else {
                        Mage::getSingleton('accolade_omni/omni_exception')->throwException('Login failed, please try again.');
                    }
                } catch(SoapFault $fault){
                        Mage::getSingleton('accolade_omni/omni_exception')->parseSoapFault($fault);
                }
                return $customer;
        }
	/**
	 * @param Omni_Contact|Omni_ContactPOS $contact
	 * @return Mage_Customer_Model_Customer|null
	 * @throws Exception
	 */
	protected function _import($contact){
		/** @var Accolade_Omni_Model_Rewrite_Customer $customer */
		$customer = Mage::getModel('customer/customer')
			->getCollection()
			->addAttributeToSelect('omni_id')
			->addAttributeToSelect('default_contact')
			->addAttributeToFilter('omni_id', $contact->getId())
			->getFirstItem();

		try {
			$customer->setOmniId($contact->getId());
			$customer->setEmail($contact->getEmail()); 
			$customer->setFirstname($contact->getFirstName());
			$customer->setMiddlename($contact->getMiddleName());
			$customer->setLastname($contact->getLastName());
			$customer->setTelephone($contact->getPhone());

			$contactAddresses = $contact->getAddresses();

			if(!empty($contactAddresses)){  
				$address = reset($contactAddresses);

				// checking if address exists, if not, creating
				if($customer->getPrimaryContactAddress()){
					$contactAddress = $customer->getPrimaryContactAddress();
				} else {
					/** @var Mage_Customer_Model_Address $contactAddress */
					$contactAddress = Mage::getModel('customer/address')
						->setCustomerId($customer->getId());
				}

				/** @var Mage_Customer_Model_Address $contactAddress */
				if(isset($contactAddress) && $contactAddress){
					// validity check:
					$omniAddressData = array(
						$address->getAddress1(),
						$address->getAddress2(),
						$address->getCity(),
						$address->getCountry(),
						$address->getPostCode(),
						$address->getStateProvinceRegion()
					);
					$omniAddressData = array_map('trim', $omniAddressData);
					$omniAddressData = array_filter($omniAddressData);

					if(!empty($omniAddressData)){
						$contactAddress
							->setFirstname($customer->getFirstname())
							->setMiddleName($customer->getMiddlename())
							->setLastname($customer->getLastname())
							->setTelephone($contact->getPhone())
							->setStreet(array(
								$address->getAddress1(),
								$address->getAddress2()
							))
							->setCity($address->getCity())
							->setCountryId($address->getCountry())
							->setPostcode($address->getPostCode())
							->setRegion($address->getStateProvinceRegion())
							->setTelephone($contact->getPhone())    // comes from contact
							->setIsDefaultContact(true);

						// adding only when new, otherwise should have it!
						if(!$contactAddress->getId()) $customer->addAddress($contactAddress);
					}
				}
			}

			// this also triggers the saving of addresses
			$customer->save();

		} catch(Exception $e){
			Mage::logException($e);
			throw $e;
		}
		return $customer;
	}


	/**
	 * @param Mage_Customer_Model_Customer $customer
	 * @return Mage_Customer_Model_Customer $customer
	 * @throws Exception ????
	 */
	public function importByCustomer($customer){
		$omni = $this->getSecureOmni();
		$contactId = $customer->getOmniId();

		if (!empty($contactId)) {
			try {
				$response = $omni->ContactGetById(new Omni_ContactGetById($contactId));

				if ($response instanceof Omni_ContactGetByIdResponse) {
					$contact = $response->getContactGetByIdResult();

					return $this->_import($contact);
				}
				Mage::throwException(Mage::helper('accolade_omni')->__('This action is not permitted at this moment.'));
			} catch (SoapFault $fault) {
				$this->getSoapFaultParser()->parseSoapFault($fault);
			} catch (Exception $e) {
				Mage::logException($e);
				throw $e;
			}
		}
		return null;
	}


	/**
	 * @param Mage_Customer_Model_Customer $customer
	 * @param Omni_Contact                  $contact
	 * @return Omni_Contact
	 */
	protected function _updateContactFromCustomer($customer, $contact = null){
		if(!$contact) $contact = new Omni_Contact();

		$contact->setEmail($customer->getEmail());
		$contact->setUserName($customer->getEmail());
		$contact->setPassword($customer->getPassword());    

		$contact->setFirstName($customer->getFirstname());
		$contact->setMiddleName($customer->getMiddlename());
		$contact->setLastName($customer->getLastname());
		$contact->setPhone($customer->getTelephone());

		/** @var Accolade_Omni_Model_Rewrite_Customer_Address $customerAddress */
		$customerAddress = $customer->getPrimaryContactAddress();
		if(!($customerAddress instanceof Mage_Customer_Model_Address)){
			// when new address!
			/** @var Mage_Customer_Model_Address $address */
			foreach($customer->getAddresses() as $address){
				if($address->getIsDefaultContact()){
					$customerAddress = $address;
					break;
				}
			}
		}

		if($customerAddress instanceof Mage_Customer_Model_Address){
			// there should only be one address:
			if(is_array($contact->getAddresses())) $contactAddress = reset($contact->getAddresses());
			else $contactAddress = new Omni_Address();

			$contactAddress->setAddress1($customerAddress->getStreet1());
			$contactAddress->setAddress2($customerAddress->getStreet2());
			$contactAddress->setCity($customerAddress->getCity());
			$contactAddress->setPostCode($customerAddress->getPostcode());
			$contactAddress->setStateProvinceRegion($customerAddress->getRegion());    // TODO: check region format
			$contactAddress->setCountry($customerAddress->getCountry());    // TODO: check country format

			$contact->setAddresses(array($contactAddress));
		} else $contact->setAddresses(array());

		return $contact;
	}

	/**
	 * @param Mage_Customer_Model_Customer $customer
	 * @param boolean                      $save Clean password & save customer?
	 * @return Omni_Contact|SoapFault
	 * @throws Mage_Exception
	 */
	public function create($customer, $save = false){
		$omni = $this->getOmni();

		try {
			/** @var Omni_Contact $contact */
			$contact = $this->_updateContactFromCustomer($customer);

			/** @var Omni_ContactCreateResponse $result */
			$result     = $omni->ContactCreate(new Omni_ContactCreate($contact));
			$contact    = $result->getContactCreateResult();

			$this->_setSecurityToken($customer, $contact->getDevice()->getSecurityToken());

			if($save){
				//Omni Customer id is saved
				$customer->setOmniId($contact->getId());
				$customer->cleanPasswordsValidationData();
				$customer->save();
			}

			// TODO: handle the result
			return $contact; // TODO: for testing this function only

		} catch(SoapFault $fault){
			$this->getSoapFaultParser()->parseSoapFault($fault);
			// should never return this as translates SoapFault to Exception
			return 'Unable to create customer, Please try again.';
		}
	}

	/** // TODO finish this properly???
	 * @param Accolade_Omni_Model_Rewrite_Customer $customer
	 */

	/**
	 * @param Mage_Customer_Model_Customer $customer
	 * @param boolean                      $save Clean password & save customer?
	 * @return Omni_Contact|SoapFault
	 * @throws Mage_Exception
	 */
	public function update($customer, $save = false){
		$omni = $this->getSecureOmni();

		try {
			// for update we READ the contact first so as not to overwrite anything
			/** @var Omni_ContactGetByIdResponse $contact */
			$contact = $omni->ContactGetById(new Omni_ContactGetById($customer->getData('omni_id')));
			$contact = $contact->getContactGetByIdResult();

			/** @var Omni_Contact $contact */
			$contact = $this->_updateContactFromCustomer($customer, $contact);
			// as we have synced the customer before edit, this should be OK
                        //Mage::log('UPDATE FROM CUSTOMER: '.@var_export($contact, true), null, '_omni_test.log');
			/** @var Omni_ContactUpdateResponse $result */
			$result = $omni->ContactUpdate(new Omni_ContactUpdate($contact));

			/*
			 TODO: parse all these exceptions:
			StatusCode.Error
			StatusCode.SecurityTokenInvalid
			StatusCode.UserNotLoggedIn
			StatusCode.DeviceIsBlocked
			StatusCode.AccessNotAllowed
			StatusCode.ParameterInvalid
			StatusCode.EmailInvalid
			StatusCode.ContactIdNotFound
			 * */

			$contact = $result->getContactUpdateResult();
                        //Mage::log('UPDATE RESULT: '.@var_export($contact, true), null, '_omni_test.log');

			$this->_setSecurityToken($customer, $contact->getDevice()->getSecurityToken());

			if($save){
				// need to save ID with customer?
				$customer->setOmniId($contact->getId());
				$customer->cleanPasswordsValidationData();
				$customer->save();
			}

			// TODO: handle the result
			return $contact; // TODO: for testing this function only
		} catch(SoapFault $fault){
			$this->getSoapFaultParser()->parseSoapFault($fault);

			// should never return this as translates SoapFault to Exception
			return $fault;
		}
	}

	/**
	 * @param Mage_Customer_Model_Customer $customer
	 * @param string                       $currentPass
	 * @param string                       $newPass
	 * @return bool
	 * @throws Mage_Core_Exception
	 */
        public function changePassword($customer, $currentPass, $newPass){
                $omni = $this->getSecureOmni();
                try {
                        $email = $customer->getEmail();
                        if(empty($email)){
                                Mage::throwException(Mage::helper('customer')->__('Email address can\'t be empty'));
                        }
                        if(empty($currentPass)){
                                Mage::throwException(Mage::helper('customer')->__('Password can\'t be empty'));
                        }
                        if(empty($newPass)){
                                Mage::throwException(Mage::helper('customer')->__('Password can\'t be empty'));
                        }
                        /** @var Omni_ChangePasswordResponse $result */
                        $result = $omni->ChangePassword(new Omni_ChangePassword($email, $newPass, $currentPass));
                        $result = $result->getChangePasswordResult();
                        if($result) {
                            try {
                                $customer->setPassword($newPass);
                                $customer->save();
                            }
                            catch(Exception $ex) {
                                Mage::throwException(Mage::helper('customer')->__('Error : '.$ex->getMessage()));
                            }
                            return true;
                        }
                    } catch(SoapFault $fault){
                        $this->getSoapFaultParser()->parseSoapFault($fault);
                    }
                    return false;
        }

        /**
         * Send email with reset password confirmation link
         *
         * @param Mage_Customer_Model_Customer $customer
         * @return bool
         */
        public function sendPasswordResetConfirmationEmail(Mage_Customer_Model_Customer $customer){
                $omni = $this->getOmni();
                try {
                        $email = $customer->getEmail();
                        if(empty($email)){
                                Mage::throwException(Mage::helper('customer')->__('Email address can\'t be empty'));
                        }
                        /** @var Omni_ForgotPasswordResponse $result */
                        $result = $omni->ForgotPassword(new Omni_ForgotPassword($customer->getEmail()));
                        $result = $result->getForgotPasswordResult();
                        if($result) return true;
                } catch(SoapFault $fault){
                        $this->getSoapFaultParser()->parseSoapFault($fault);
                }

                return false;
        }
	/**
	 * Send email with reset password confirmation link
	 *
	 * @param Mage_Customer_Model_Customer $customer
	 * @param string                       $resetCode
	 * @param string                       $password
	 * @return bool
	 */
	public function resetPassword(Mage_Customer_Model_Customer $customer, $resetCode, $password){
		$omni = $this->getOmni();

		try {
			if(empty($resetCode)){
				Mage::throwException(Mage::helper('customer')->__('Your password reset link has expired.'));
			}

			if(!$customer->getId() || !$customer->getOmniId()){
				Mage::throwException(Mage::helper('customer')->__('Invalid customer data.'));
			}

			/** @var Omni_ResetPasswordResponse $result */
			$result = $omni->ResetPassword(new Omni_ResetPassword(
				$customer->getEmail(),
				$resetCode,
				$password
			));
			$result = $result->getResetPasswordResult();
                        #result is boolean
			if($result){
				$customer->setRpToken(null);
				$customer->setRpTokenCreatedAt(null);
				$customer->cleanPasswordsValidationData();
				$customer->save();

				return true;
			}
		} catch(SoapFault $fault){
			$this->getSoapFaultParser()->parseSoapFault($fault);
		}

		return false;
	}

	/**
	 * Debug Omni contact - removing the extra data we do not need
	 *
	 * @param Omni_Contact|Omni_ContactPOS $contact
	 * @return Omni_Contact|Omni_ContactPOS
	 */
	public function debugOmniContact($contact){
		$cleanContact = clone $contact;
		foreach($cleanContact as $k => $value){
			if(is_array($value)){
				if($value instanceof Omni_Address) continue;
				unset($cleanContact->{$k});
			}
		}
		return $cleanContact;
	}

}
