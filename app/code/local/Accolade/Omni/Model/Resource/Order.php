<?php

class Accolade_Omni_Model_Resource_Order extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('accolade_omni/order', 'order_id');
        $this->_isPkAutoIncrement = false;
    }

}