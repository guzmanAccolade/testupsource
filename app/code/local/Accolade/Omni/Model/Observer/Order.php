<?php

class Accolade_Omni_Model_Observer_Order
{

    /**
     * @param Varien_Event_Observer $observer
     */
    public function orderExportOnSuccess($observer)
    {
        $orderIds = $observer->getEvent()->getOrderIds();
        if (empty($orderIds) || !is_array($orderIds)) {
            return;
        }

        $orderId = array_pop($orderIds);

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($orderId);


        if ($order->getId() && Mage::helper('accolade_omni/config')->isEnabled()) {
            $omniOrder = Mage::getModel('accolade_omni/order');
            if ($omniOrder->canExportSalesOrder($order)) {
                $omniOrder->setSalesOrder($order);

                if (Mage::getStoreConfig('accolade_omni/orders/auto_export') == Accolade_Omni_Model_System_Config_Source_Select_AutoExport::ON_SUCCESS) {
                    $omniOrder->exportOrder();
                } else {
                    $omniOrder->save();
                }
            }
        }

    }


    public function runExport()
    {
        $helper = Mage::helper('accolade_omni');

        if (Mage::helper('accolade_omni/config')->isEnabled() &&
            Mage::getStoreConfig('accolade_omni/orders/auto_export') == Accolade_Omni_Model_System_Config_Source_Select_AutoExport::BY_CRON) {
            $total = 0;
            $successful = 0;
            $allowedStatuses = Mage::getStoreConfig('accolade_omni/orders/export_allowed_sales_order_statuses');

            $resource = Mage::getSingleton('core/resource');

            //start collecting export viable orders that in some reason have not been added to accolade_omni/order
            $collection = Mage::getModel('sales/order')->getCollection();
            $collection->addFieldToFilter('status', array('in' => explode(',', $allowedStatuses)));

            $collection->getSelect()
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns(array('main_table.entity_id', 'main_table.increment_id'))
                ->joinLeft(
                    array("aoo" => $resource->getTableName('accolade_omni/order')),
                    "main_table.entity_id = aoo.order_id",
                    array()
                )
                ->where("aoo.order_id is null");

            foreach ($collection as $salesOrder) {
                $order = Mage::getModel('accolade_omni/order')->populate($salesOrder);

                $order->save();
            }

            //exporting orders that have not been exported yet.
            $collection = Mage::getModel('accolade_omni/order')->getCollection();
            $collection->addFieldToFilter('export_status', Accolade_Omni_Model_Order::EXPORT_UNKNOWN);

            foreach ($collection as $order) {
                $salesOrder = Mage::getModel('sales/order')->load($order->getId());

                if ($salesOrder && $salesOrder->getId()) {
                    if ($order->canExportSalesOrder($salesOrder)) {
                        $total++;
                        $order->setSalesOrder($salesOrder);
                        $order->exportOrder();
                        if ($order->isSuccessful()) {
                            $successful++;
                        }
                    }
                }
            }

            return $helper->__('Exported successfully %s out of %s orders.', $successful, $total);
        }

        return $helper->__('Omni order export cron is disabled.');
    }



    public function runSkip()
    {
        $helper = Mage::helper('accolade_omni');

        if (Mage::helper('accolade_omni/config')->isEnabled() &&  Mage::getStoreConfigFlag('accolade_omni/orders/auto_skip')) {
            $skipStatuses = Mage::getStoreConfig('accolade_omni/orders/skip_sales_order_statuses');
            if (!empty($skipStatuses)) {
                $skipped = 0;

                $resource = Mage::getSingleton('core/resource');

                $collection = Mage::getModel('accolade_omni/order')->getCollection();
                $collection->addFieldToFilter('export_status',
                    array(
                        'nin'=>array(
                            Accolade_Omni_Model_Order::EXPORT_SUCCESS,
                            Accolade_Omni_Model_Order::EXPORT_SKIPPED
                        )
                    )
                );

                $collection->getSelect()
                    ->join(
                        array("so" => $resource->getTableName('sales/order')),
                        "main_table.order_id = so.entity_id", array('status')
                    );
                $collection->addFieldToFilter('status', array('in' => explode(',', $skipStatuses)));

                foreach ($collection as $order) {
                    $order->setExportDate(Mage::getSingleton('core/date')->gmtDate())
                        ->setExportStatus($order::EXPORT_SKIPPED)
                        ->addExportLog($helper->__('Automatically skipped due sales order status is %s.', $order->getData('status')), $order::EXPORT_SKIPPED);
                    $order->save();
                    $skipped++;
                }

                return $helper->__('%s orders were skipped.', $skipped);
            }
        }

        return $helper->__('Omni order skipping cron is disabled.');
    }

}