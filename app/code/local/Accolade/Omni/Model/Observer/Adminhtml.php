<?php

class Accolade_Omni_Model_Observer_Adminhtml
{

    /**
     *
	 * @param Varien_Event_Observer $observer
	 */
    public function adminhtmlWidgetContainerHtmlBefore($observer){
        $block = $observer->getBlock();

        //adding export order button
        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
            $helper = Mage::helper('accolade_omni');
            $order = $block->getOrder();
            $omniOrder = Mage::getModel('accolade_omni/order');
            if ($order && $helper->isOrderExportAllowed() && $omniOrder->canExportSalesOrder($order)) {
                $omniOrder->load($order->getId());

                if (!$omniOrder->isSuccessful() && $omniOrder != $omniOrder::EXPORT_SKIPPED) {
                    $message = $helper->__('Are you sure you want to export to OMNI?');
                    $url = $block->getUrl('*/accolade_omni_order/export');

                    $block->addButton('accolade_omni_export_order', array(
                        'label'     => $helper->__('Export Order'),
                        'onclick'   => "confirmSetLocation('{$message}', '{$url}')",
                        'class'     => 'go'
                    ));
                }
            }
        }

    }


    /**
     * @param Varien_Event_Observer $observer
     */
    public function coreBlockAbstractToHtmlAfter($observer){
        $block = $observer->getBlock();
        $helper = Mage::helper('accolade_omni');
        $request = Mage::app()->getRequest();

        if ($helper->isModuleEnabled() && $block instanceof Mage_Adminhtml_Block_Sales_Order_View_Info) {
            if ($request && $request->getControllerName() == 'sales_order' && $request->getActionName() == 'view') { //only on order view
                /** @var Accolade_Omni_Block_Adminhtml_Sales_Order_View_Export_Info $exportBlock */
                $exportBlock = $block->getChild('order_export_info');
                $exportBlock->setOrder($block->getOrder());

                $originalHtml = $observer->getTransport()->getHtml();
                $observer->getTransport()->setHtml($exportBlock->toHtml() . $originalHtml);
            }
        }
    }

}