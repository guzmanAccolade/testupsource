<?php

class Accolade_Omni_Model_Omni extends Mage_Core_Model_Abstract {

	const OMNI_SECURITY_TOKEN           = 'omni_token';
	const OMNI_SECURITY_TOKEN_HEADER    = 'LSRETAIL-TOKEN';
    const CURL_TRACE                    = 0;
	/** @var Omni_eCommerceService */
	protected $_connection;



	/** @var Omni_eCommerceService */
	protected $_secureConnection;

	/** @var string */
	protected $_token;

	protected function _construct(){
		$this->getOmni();
		parent::_construct();
	}

	/**
	 * @param string $token
	 */
	protected function _getOmni($token = null){
		$options = array('trace' => $this->_curlTrace(), 'exceptions' => 1);
		if($token){
			$options['stream_context'] = stream_context_create(array(
				'http' => array(
					'header' => self::OMNI_SECURITY_TOKEN_HEADER . ': ' . $token
				)
			));
			$this->_token = $token;
		}
		$url = Mage::helper('accolade_omni/config')->getURL();
		/** Testing if SOAP URL is valid (otherwise fatal error) */
		$curl = curl_init($url);
		curl_setopt($curl,  CURLOPT_RETURNTRANSFER, TRUE);
		$response = curl_exec($curl);
		$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		if($response && $httpCode != 404 && strpos($response, '<wsdl:definitions') !== false){
			$connection = new Omni_eCommerceService($options, $url);
			if($token) $this->_secureConnection = $connection;
			else $this->_connection = $connection;
		}
		curl_close($curl);
	}

	/**
	 * @return Omni_eCommerceService
	 * @throws Mage_Exception
	 */
	public function getOmni(){
		if(is_null($this->_connection)){
			$this->_getOmni();
		}

		$this->checkConnection();
		return $this->_connection;
	}

	/**
	 * @param string $token
	 * @return Omni_eCommerceService
	 * @throws Mage_Exception
	 */
	public function getSecureOmni($token = null){
		if(is_null($token)) $token = Mage::getSingleton('customer/session')->getData(self::OMNI_SECURITY_TOKEN);
		if(is_null($this->_secureConnection) || $this->_token != $token){
			$this->_getOmni($token);
		}
		$this->checkConnection();
		return $this->_secureConnection;
	}

	/**
	 * @return boolean
	 * @throws Mage_Exception
	 */
	public function checkConnection(){
		if($this->_connection) return true;

		Mage::getSingleton('accolade_omni/omni_exception')->throwException();
		return false;
	}

	public function getSoapFaultParser(){
		return Mage::getSingleton('accolade_omni/omni_exception');
	}

    /**
     * @return int
     */
    protected function _curlTrace() {
	    return self::CURL_TRACE;
    }

}
