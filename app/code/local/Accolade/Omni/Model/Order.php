<?php


/**
 * @method string getExportStatus()
 * @method Accolade_Omni_Model_Order setExportStatus(string $value)
 * @method string getExportDate()
 * @method Accolade_Omni_Model_Order setExportDate(string $value)
 * @method string getIncrementId()
 * @method Accolade_Omni_Model_Order setIncrementId(string $value)
 * @method string getNavOrderId()
 * @method Accolade_Omni_Model_Order setNavOrderId(string $value)
 * @method string getNavTransactionId()
 * @method Accolade_Omni_Model_Order setNavTransactionId(string $value)
 * @method int getOrderId()
 * @method Accolade_Omni_Model_Order setOrderId(int $value)
 */
class Accolade_Omni_Model_Order extends Mage_Core_Model_Abstract
{

    const EXPORT_UNKNOWN = 0;
    const EXPORT_SUCCESS = 1;
    const EXPORT_FAILED  = 2;
    const EXPORT_SKIPPED = 3;

    /** @var null|string $_lastExportedLog - last export error message */
    protected $_exportLog;

    /** @var Mage_Sales_Model_Order|bool */
    protected $_salesOrder = false;

    protected function _construct()
    {
        $this->_init('accolade_omni/order');
    }


    /**
     * export magneto order related to this
     * @return bool
     */
    public function exportOrder()
    {
        if ($this->isSuccessful()) return true;

        $order = $this->getSalesOrder();

        if ($order && $this->canExportSalesOrder()) {
            $this->populate($order);
            try {
                /** @var  $omniOrder */
                $omniOrder = Mage::getModel('accolade_omni/omni_order');
                $results = $omniOrder->create($order);

                $this->setNavTransactionId($results->Id)
                    ->setNavOrderId($results->DocumentNumber)
                    ->setExportStatus(self::EXPORT_SUCCESS)
                    ->addExportLog(Mage::helper('accolade_omni')->__('Success: NAV order "%s" (trans.id %s)', $this->getNavOrderId(), $this->getNavTransactionId()), self::EXPORT_SUCCESS); //todo can be removed later to have less clutter.
            } catch (Exception $e) {
                //todo timeout exception: status update next run to get order id as we have the transaction id to check for?
                //todo NAV table locked, to be executed again,ie no status change?
                $this->setExportStatus(self::EXPORT_FAILED)
                    ->addExportLog($e->getMessage(), self::EXPORT_FAILED);
            }

            $this->setExportDate(Mage::getSingleton('core/date')->gmtDate());

            $this->save();
            return true;
        }

        return false;
    }


    /**
     * @return bool|Mage_Sales_Model_Order
     */
    public function getSalesOrder() {
        if (!($this->_salesOrder instanceof Mage_Sales_Model_Order)) {
            $this->_salesOrder = false; // just to be safe.
            if ($this->getId()) {
                $order = Mage::getModel('sales/order')->load($this->getId());
                if ($order->getId()) {
                    $this->_salesOrder = $order;
                    $this->populate($order);
                }
            }
        }

        return $this->_salesOrder;
    }


    /**
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function setSalesOrder($order) {
        $this->_salesOrder = $order;
        $this->populate($order);
        return $this;
    }


    /**
     * @param $message
     * @param int $type
     * @return $this
     */
    public function addExportLog($message, $type = self::EXPORT_UNKNOWN)
    {
        $log = $this->getExportLog();

        $timestamp = Mage::getSingleton('core/date')->gmtTimestamp();
        $log[] = array('type' => $type, 'timestamp' => $timestamp, 'message' => $message);
        $this->_exportLog = $message;

        $this->setExportLog($log);

        return $this;
    }


    /**
     * @return array
     */
    public function getExportLog()
    {
        if (is_array($this->_exportLog)) return $this->_exportLog;

        $log = $this->getData('export_log');

        if (!empty($log)) {
            $log = @unserialize($log);

            if (!is_array($log)) {
                $log = array();
            }
        } else {
            $log = array();
        }

        $this->_exportLog = $log;

        return $log;
    }


    /**
     * @param array $log
     * @return $this
     */
    public function setExportLog($log)
    {
        if (is_array($log)) {
            $log = @serialize($log);
        } else if (is_string($log)) {
            return $this->addExportLog($log);
        } else {
            $log = null;
        }

        $this->setData('export_log', $log);

        return $this;
    }


    /**
     *
     * @return string|null
     */
    public function getLastExportLog()
    {
        $log = $this->getExportLog();

        if (is_array($log)) {
            $log = end($log);
            if (isset($log['message'])) {
                return $log['message'];
            }
        }

        return false;
    }


    /**
     * @return bool
     */
    public function isSuccessful()
    {
        return $this->getData('export_status') == self::EXPORT_SUCCESS;
    }


    /**
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function populate($order)
    {
        $this->setId($order->getId())->setIncrementId($order->getIncrementId());

        return $this;
    }


    /**
     * statuses returned as array
     * @return array
     */
    static public function getExportStatuses()
    {
        return array(
            self::EXPORT_UNKNOWN => 'Unknown',
            self::EXPORT_SUCCESS => 'Success',
            self::EXPORT_FAILED  => 'Failed',
            self::EXPORT_SKIPPED => 'Skipped',
        );
    }


    /**
     * @param Mage_Sales_Model_Order|null $order
     * @return bool
     */
    public function canExportSalesOrder($order = null)
    {
        if (!($order instanceof Mage_Sales_Model_Order)) {
            $order = $this->getSalesOrder();
        }

        if ($order instanceof Mage_Sales_Model_Order) {
            $allowedStatuses = Mage::getStoreConfig('accolade_omni/orders/export_allowed_sales_order_statuses');
            $status = $order->getStatus();

            if (!empty($allowedStatuses) && !empty($status) && in_array($status, explode(',', $allowedStatuses))) {
                return true;
            }
        }

        return false;
    }

}