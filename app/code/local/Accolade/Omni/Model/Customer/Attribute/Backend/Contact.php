<?php

class Accolade_Omni_Model_Customer_Attribute_Backend_Contact extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract {

	public function beforeSave($object){
		$defaultContact = $object->getDefaultContact();
		if(is_null($defaultContact)){
			$object->unsetDefaultContact();
		}
	}

	public function afterSave($object){
		if($defaultContact = $object->getDefaultContact()){
			$addressId = false;
			/**
			 * post_index set in customer save action for address
			 * this is $_POST array index for address
			 */
			foreach($object->getAddresses() as $address){
				if($address->getPostIndex() == $defaultContact){
					$addressId = $address->getId();
				}
			}
			if($addressId){
				$object->setDefaultContact($addressId);
				$this->getAttribute()->getEntity()
					->saveAttribute($object, $this->getAttribute()->getAttributeCode());
			}
		}
	}

}
